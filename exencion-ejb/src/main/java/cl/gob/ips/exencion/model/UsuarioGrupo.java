package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NamedQueries({
        @NamedQuery(name = UsuarioGrupo.QUERY_USUARIO_GRUPO_FIND_ALL, query = "SELECT o FROM UsuarioGrupo o"),
        @NamedQuery(name = UsuarioGrupo.QUERY_USUARIO_GRUPO_FIND_BY_GRUPO_ARCHIVO, query = "SELECT o FROM UsuarioGrupo o WHERE o.tbPrGruposArchivo1.grupArchCod = :grupoArchivo ")
})
@Table(name = "TB_USU_GRU_ARC")
@IdClass(UsuarioGrupoPK.class)
public class UsuarioGrupo implements Serializable {

    private static final long serialVersionUID = 3531830761795072067L;
    public static final String QUERY_USUARIO_GRUPO_FIND_ALL = "UsuarioGrupo.findAll";
    public static final String QUERY_USUARIO_GRUPO_FIND_BY_GRUPO_ARCHIVO = "UsuarioGrupo.findByGrupoArchivo";


    @ManyToOne
    @Id
    @JoinColumn(name = "USU_GRUP_ARCH_COD")
    private GruposArchivo tbPrGruposArchivo1;
    @ManyToOne
    @Id
    @JoinColumn(name = "USU_ARCH_USU_COD")
    private Usuarios tbUsuarios1;
    @ManyToOne
    @Id
    @JoinColumn(name = "USU_ARCH_NIVEL_COD")
    private NivelResponsabilidad tbPrNivelResponsabilidad;

    public UsuarioGrupo() {
    }

    public UsuarioGrupo(NivelResponsabilidad tbPrNivelResponsabilidad, Usuarios tbUsuarios1,
                        GruposArchivo tbPrGruposArchivo1) {
        this.tbPrNivelResponsabilidad = tbPrNivelResponsabilidad;
        this.tbUsuarios1 = tbUsuarios1;
        this.tbPrGruposArchivo1 = tbPrGruposArchivo1;
    }


    public GruposArchivo getTbPrGruposArchivo1() {
        return tbPrGruposArchivo1;
    }

    public void setTbPrGruposArchivo1(GruposArchivo tbPrGruposArchivo1) {
        this.tbPrGruposArchivo1 = tbPrGruposArchivo1;
    }

    public Usuarios getTbUsuarios1() {
        return tbUsuarios1;
    }

    public void setTbUsuarios1(Usuarios tbUsuarios1) {
        this.tbUsuarios1 = tbUsuarios1;
    }

    public NivelResponsabilidad getTbPrNivelResponsabilidad() {
        return tbPrNivelResponsabilidad;
    }

    public void setTbPrNivelResponsabilidad(NivelResponsabilidad tbPrNivelResponsabilidad) {
        this.tbPrNivelResponsabilidad = tbPrNivelResponsabilidad;
    }
}
