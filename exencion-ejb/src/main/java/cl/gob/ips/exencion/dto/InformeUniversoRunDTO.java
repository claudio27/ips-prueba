package cl.gob.ips.exencion.dto;

import java.io.Serializable;
import java.util.Date;

public class InformeUniversoRunDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2820517557961167314L;

	private String periodo;
	private Integer edad;
	private Integer pfp;
	private String tipoPfp;
	private String fechaPfp;
	private String residencia;
	private String fechaResidencia;
	private String tipoPensionado;
	private String nombreCompleto;

	public InformeUniversoRunDTO() {
		super();
	}

	public InformeUniversoRunDTO(String periodo, Integer edad, Integer pfp,
			String tipoPfp, String fechaPfp, String residencia,
			String fechaResidencia, String tipoPensionado) {
		super();
		this.periodo = periodo;
		this.edad = edad;
		this.pfp = pfp;
		this.tipoPfp = tipoPfp;
		this.fechaPfp = fechaPfp;
		this.residencia = residencia;
		this.fechaResidencia = fechaResidencia;
		this.tipoPensionado = tipoPensionado;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Integer getPfp() {
		return pfp;
	}

	public void setPfp(Integer pfp) {
		this.pfp = pfp;
	}

	public String getTipoPfp() {
		return tipoPfp;
	}

	public void setTipoPfp(String tipoPfp) {
		this.tipoPfp = tipoPfp;
	}

	public String getFechaPfp() {
		return fechaPfp;
	}

	public void setFechaPfp(String fechaPfp) {
		this.fechaPfp = fechaPfp;
	}

	public String getResidencia() {
		return residencia;
	}

	public void setResidencia(String residencia) {
		this.residencia = residencia;
	}

	public String getFechaResidencia() {
		return fechaResidencia;
	}

	public void setFechaResidencia(String fechaResidencia) {
		this.fechaResidencia = fechaResidencia;
	}

	public String getTipoPensionado() {
		return tipoPensionado;
	}

	public void setTipoPensionado(String tipoPensionado) {
		this.tipoPensionado = tipoPensionado;
	}


	public String getNombreCompleto() {return nombreCompleto;}

	public void setNombreCompleto(String nombreCompleto) {this.nombreCompleto = nombreCompleto;}
}
