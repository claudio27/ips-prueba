package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.GrupoUsuarioFunciones;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Created by practicante
 */
@Stateless
@LocalBean
public class GrupoUsuarioFuncionesDao extends BaseDAO {

    /**
     * @return
     * @throws DataAccessException
     */
    public void crearGrupoUsuarioFunciones(final GrupoUsuarioFunciones grupoUsuarioFunciones) throws DataAccessException {
        super.em.persist(grupoUsuarioFunciones);

    }

    /**
     *
     * @throws DataAccessException
     */

    public void deleteGrupoUsuarioFunciones(GrupoUsuarioFunciones grupoUsuarioFunciones) throws DataAccessException {
        super.em.remove(super.em.merge(grupoUsuarioFunciones));





    }

}