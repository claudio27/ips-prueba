package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.CalendarioDao;
import cl.gob.ips.exencion.model.Calendario;
import cl.gob.ips.exencion.exceptions.DataAccessException;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.Date;

/**
 * Created by rodrigo.reyesco@gmail.com on 07-10-2016.
 */
@Stateless
@LocalBean
public class MantenedorCalendarioService {

    @Inject
    CalendarioDao dao;

    /**
     *
     * @param dateIni
     * @param dateFin
     * @return
     * @throws Exception
     */
    public List<Calendario> buscarDiasFeriadosPorCriterio(Date dateIni, Date dateFin) throws Exception{
        return dao.buscarDiasFeriadosPorCriterio(dateIni, dateFin);
    }

    /**
     *
     * @param dateIni
     * @return
     * @throws Exception
     */
    public List<Calendario> buscarDiasFeriadosPorDia(Date dateIni) throws Exception{
        return dao.buscarDiasFeriadosPorDia(dateIni);
    }
            
    /**
     *
     * @param calendario
     * @throws Exception
     */
    public void actualizarCalendario(final Calendario calendario) throws Exception {
        dao.actualizarCalendario(calendario);
    }

    /**
     *
     * @param calendarios
     * @throws DataAccessException
     */
    public void actualizaCalendarios(final List<Calendario> calendarios) throws DataAccessException, Exception {
        dao.actualizaCalendarios(calendarios);
    }
    

    /**
     *
     * @param calendario
     * @throws DataAccessException
     */
    public void creaCalendario(final Calendario calendario) throws DataAccessException {
        dao.creaCalendario(calendario);
    }
}
