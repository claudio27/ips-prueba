package cl.gob.ips.exencion.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Created by lpino on 05-07-17.
 */
public class BaseDAO {

    public static final String JPA_PERSISTENCE_UNIT_NAME = "appTestPersistenceUnit";

    protected Query query;

    @PersistenceContext(unitName = JPA_PERSISTENCE_UNIT_NAME)
    protected EntityManager em;
}
