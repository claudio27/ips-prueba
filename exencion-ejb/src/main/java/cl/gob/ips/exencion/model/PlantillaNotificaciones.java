package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * To create ID generator sequence "TB_PLANTILLA_NOTIFICACIONES_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PLANTILLA_NOTIFICACIONES_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({
              @NamedQuery(name = "PlantillaNotificaciones.findAll",
                          query = "select o from PlantillaNotificaciones o") })
@Table(name = "TB_PLANTILLA_NOTIFICACIONES")
@SequenceGenerator(name = "PlantillaNotificaciones_Id_Seq_Gen", sequenceName = "TB_PLANTILLA_NOTIFICACIONES_ID_SEQ_GEN",
                   allocationSize = 50, initialValue = 50)
public class PlantillaNotificaciones implements Serializable {
    private static final long serialVersionUID = 7478170090888286466L;
    @Id
    @Column(name = "PLAN_NOT_COD", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PlantillaNotificaciones_Id_Seq_Gen")
    private Integer planNotCod;
    @Column(name = "PLAN_NOT_GLOSA", nullable = false, length = 100)
    private String planNotGlosa;

    public PlantillaNotificaciones() {
    }

    public PlantillaNotificaciones(Integer planNotCod, String planNotGlosa) {
        this.planNotCod = planNotCod;
        this.planNotGlosa = planNotGlosa;
    }

    public Integer getPlanNotCod() {
        return planNotCod;
    }

    public String getPlanNotGlosa() {
        return planNotGlosa;
    }

    public void setPlanNotGlosa(String planNotGlosa) {
        this.planNotGlosa = planNotGlosa;
    }
}
