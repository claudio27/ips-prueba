package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
//import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * To create ID generator sequence "TB_PERIODO_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PERIODO_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "Periodo.findAll", query = "select o from Periodo o"),
                @NamedQuery(name = Periodo.QUERY_FIND_VALID, query = "select o from Periodo o WHERE o.perPeriodo = (SELECT MIN(o.perPeriodo) FROM Periodo o where o.perFecCierre=NULL)"),
        @NamedQuery(name = "Periodo.findPeriodoId", query = "select o from Periodo o WHERE o.perPeriodo = :periodo_id")
})
@Table(name = "TB_PERIODO")
//@SequenceGenerator(name = "Periodo_Id_Seq_Gen", sequenceName = "TB_PERIODO_ID_SEQ_GEN", allocationSize = 50,
//                   initialValue = 50)
@Cacheable(false)
public class Periodo implements Serializable {
    private static final long serialVersionUID = 7508440708020368654L;
    
    public static final String QUERY_FIND_VALID = "Periodo.findValid";
    public static final String QUERY_FIND_BY_ID = "Periodo.findPeriodoId";



    @Temporal(TemporalType.DATE)
    @Column(name = "PER_FEC_CIERRE")
    private Date perFecCierre;
    @Temporal(TemporalType.DATE)
    @Column(name = "PER_FEC_CREACION")
    private Date perFecCreacion;
    @Id
    @Column(name = "PER_PERIODO", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Periodo_Id_Seq_Gen") // TODO revisar secuencia
    private Integer perPeriodo;
    @Column(name = "PER_USU_CIERRE", length = 20)
    private String perUsuCierre;
    @Column(name = "PER_VIVOS")
    private Integer vivos;
    @Column(name = "PER_SIN_SPS")
    private Integer sinSps;
    @Column(name = "PER_SIN_RSH")
    private Integer sinRsh;
    @Column(name = "PER_SIN_FOCALIZACION")
    private Integer sinFocalizacion;
    @Column(name = "PER_SIN_RESIDENCIA")
    private Integer sinResidencia;
    @Column(name = "PER_CUMPLEN_REQUISITOS")
    private Integer cumplenRequisitos;
    @Column(name = "PER_NIVEL_ACTUAL")
    private Integer nivelActual;
    @OneToMany(mappedBy = "tbPeriodo1", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<ArchivosCargaPeriodo> archivosCargaPeriodoList;
    @OneToMany(mappedBy = "tbPeriodo2", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<BenregAct> tbBenregActList1;
    @OneToMany(mappedBy = "tbPeriodo4", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<BphSinPbs> tbBphSinPbsList;
    @OneToMany(mappedBy = "tbPeriodo5", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<SisRebaja> tbSisRebajaList;
    @OneToMany(mappedBy = "periodo", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Pensionados> tbPensionadosList;
    @OneToMany(mappedBy = "tbPeriodo", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Pensiones> tbPensionesList3;
    @OneToMany(mappedBy = "tbPeriodo8", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<ConsultaPDI> tbConsultaPdiList;
    @ManyToOne
    @JoinColumn(name = "PER_ETAPA_COD")
    private EtapasPeriodo tbPrEtapasPeriodo;
    @OneToMany(mappedBy = "tbPeriodo10", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<PensionadosSinRsh> tbPensionadosSinRshList;
    @OneToMany(mappedBy = "tbPeriodo11", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Bitacora> tbBitacoraList2;
    @OneToMany(mappedBy = "tbPeriodo12", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<NominaValidacion> tbNominaValidacionList;

    //bi-directional many-to-one association to TbNominaBeneficiario
    @OneToMany(mappedBy="Periodo")
    private List<NominaBeneficiario> tbNominaBeneficiarios;

    //bi-directional many-to-one association to TbNominaBphSinPb
    @OneToMany(mappedBy="Periodo")
    private List<NominaBphSinPbs> tbNominaBphSinPbs;

    //bi-directional many-to-one association to TbNominaConsultaPdi
    @OneToMany(mappedBy="Periodo")
    private List<NominaConsultaPdi> tbNominaConsultaPdis;

    //bi-directional many-to-one association to TbNominaPenSinFicha
    @OneToMany(mappedBy="Periodo")
    private List<NominaPenSinFicha> tbNominaPenSinFichas;

    //bi-directional many-to-one association to TbNominaPotencialesAp
    @OneToMany(mappedBy="Periodo")
    private List<NominaPotencialesAps> tbNominaPotencialesAps;

    //bi-directional many-to-one association to TbNominaPotBeneficiario
    @OneToMany(mappedBy="Periodo")
    private List<NominaPotBeneficiario> tbNominaPotBeneficiarios;

    //bi-directional many-to-one association to TbNominaRebaja
    @OneToMany(mappedBy="Periodo")
    private List<NominaRebaja> tbNominaRebajas;

    public Periodo() {
    }

    public Periodo(EtapasPeriodo tbPrEtapasPeriodo, Date perFecCierre, Date perFecCreacion, Integer perPeriodo,
                   String perUsuCierre) {
        this.tbPrEtapasPeriodo = tbPrEtapasPeriodo;
        this.perFecCierre = perFecCierre;
        this.perFecCreacion = perFecCreacion;
        this.perPeriodo = perPeriodo;
        this.perUsuCierre = perUsuCierre;
    }

    public Periodo(Integer perPeriodo, EtapasPeriodo tbPrEtapasPeriodo, Date perFecCreacion) {
        this.perPeriodo = perPeriodo;        
        this.tbPrEtapasPeriodo = tbPrEtapasPeriodo;
        this.perFecCreacion = perFecCreacion;
    }
    
    public Periodo(Integer perPeriodo) {
        this.perPeriodo = perPeriodo;        
    }
    
    public Date getPerFecCierre() {
        return perFecCierre;
    }

    public void setPerFecCierre(Date perFecCierre) {
        this.perFecCierre = perFecCierre;
    }

    public Date getPerFecCreacion() {
        return perFecCreacion;
    }

    public void setPerFecCreacion(Date perFecCreacion) {
        this.perFecCreacion = perFecCreacion;
    }

    public Integer getPerPeriodo() {
        return perPeriodo;
    }

    public String getPerUsuCierre() {
        return perUsuCierre;
    }

    public void setPerUsuCierre(String perUsuCierre) {
        this.perUsuCierre = perUsuCierre;
    }

    public List<ArchivosCargaPeriodo> getArchivosCargaPeriodoList() {
        return archivosCargaPeriodoList;
    }

    public void setArchivosCargaPeriodoList(List<ArchivosCargaPeriodo> archivosCargaPeriodoList) {
        this.archivosCargaPeriodoList = archivosCargaPeriodoList;
    }

    public ArchivosCargaPeriodo addArchivosCargaPeriodo(ArchivosCargaPeriodo archivosCargaPeriodo) {
        getArchivosCargaPeriodoList().add(archivosCargaPeriodo);
        archivosCargaPeriodo.setTbPeriodo1(this);
        return archivosCargaPeriodo;
    }

    public ArchivosCargaPeriodo removeArchivosCargaPeriodo(ArchivosCargaPeriodo archivosCargaPeriodo) {
        getArchivosCargaPeriodoList().remove(archivosCargaPeriodo);
        archivosCargaPeriodo.setTbPeriodo1(null);
        return archivosCargaPeriodo;
    }

    public List<BenregAct> getTbBenregActList1() {
        return tbBenregActList1;
    }

    public void setTbBenregActList1(List<BenregAct> tbBenregActList1) {
        this.tbBenregActList1 = tbBenregActList1;
    }

    public BenregAct addBenregAct(BenregAct benregAct) {
        getTbBenregActList1().add(benregAct);
        benregAct.setTbPeriodo2(this);
        return benregAct;
    }

    public BenregAct removeBenregAct(BenregAct benregAct) {
        getTbBenregActList1().remove(benregAct);
        benregAct.setTbPeriodo2(null);
        return benregAct;
    }

    public List<BphSinPbs> getTbBphSinPbsList() {
        return tbBphSinPbsList;
    }

    public void setTbBphSinPbsList(List<BphSinPbs> tbBphSinPbsList) {
        this.tbBphSinPbsList = tbBphSinPbsList;
    }

    public BphSinPbs addBphSinPbs(BphSinPbs bphSinPbs) {
        getTbBphSinPbsList().add(bphSinPbs);
        bphSinPbs.setTbPeriodo4(this);
        return bphSinPbs;
    }

    public BphSinPbs removeBphSinPbs(BphSinPbs bphSinPbs) {
        getTbBphSinPbsList().remove(bphSinPbs);
        bphSinPbs.setTbPeriodo4(null);
        return bphSinPbs;
    }

    public List<SisRebaja> getTbSisRebajaList() {
        return tbSisRebajaList;
    }

    public void setTbSisRebajaList(List<SisRebaja> tbSisRebajaList) {
        this.tbSisRebajaList = tbSisRebajaList;
    }

    public SisRebaja addSisRebaja(SisRebaja sisRebaja) {
        getTbSisRebajaList().add(sisRebaja);
        sisRebaja.setTbPeriodo5(this);
        return sisRebaja;
    }

    public SisRebaja removeSisRebaja(SisRebaja sisRebaja) {
        getTbSisRebajaList().remove(sisRebaja);
        sisRebaja.setTbPeriodo5(null);
        return sisRebaja;
    }

    public List<Pensionados> getTbPensionadosList() {
        return tbPensionadosList;
    }

    public void setTbPensionadosList(List<Pensionados> tbPensionadosList) {
        this.tbPensionadosList = tbPensionadosList;
    }

    public Pensionados addPensionados(Pensionados pensionados) {
        getTbPensionadosList().add(pensionados);
        pensionados.setPeriodo(this);
        return pensionados;
    }

    public Pensionados removePensionados(Pensionados pensionados) {
        getTbPensionadosList().remove(pensionados);
        pensionados.setPeriodo(null);
        return pensionados;
    }

    public List<Pensiones> getTbPensionesList3() {
        return tbPensionesList3;
    }

    public void setTbPensionesList3(List<Pensiones> tbPensionesList3) {
        this.tbPensionesList3 = tbPensionesList3;
    }

    public Pensiones addPensiones(Pensiones pensiones) {
        getTbPensionesList3().add(pensiones);
        pensiones.setTbPeriodo(this);
        return pensiones;
    }

    public Pensiones removePensiones(Pensiones pensiones) {
        getTbPensionesList3().remove(pensiones);
        pensiones.setTbPeriodo(null);
        return pensiones;
    }

    public List<ConsultaPDI> getTbConsultaPdiList() {
        return tbConsultaPdiList;
    }

    public void setTbConsultaPdiList(List<ConsultaPDI> tbConsultaPdiList) {
        this.tbConsultaPdiList = tbConsultaPdiList;
    }

    public ConsultaPDI addConsultaPDI(ConsultaPDI consultaPDI) {
        getTbConsultaPdiList().add(consultaPDI);
        consultaPDI.setTbPeriodo8(this);
        return consultaPDI;
    }

    public ConsultaPDI removeConsultaPDI(ConsultaPDI consultaPDI) {
        getTbConsultaPdiList().remove(consultaPDI);
        consultaPDI.setTbPeriodo8(null);
        return consultaPDI;
    }

    public EtapasPeriodo getTbPrEtapasPeriodo() {
        return tbPrEtapasPeriodo;
    }

    public void setTbPrEtapasPeriodo(EtapasPeriodo tbPrEtapasPeriodo) {
        this.tbPrEtapasPeriodo = tbPrEtapasPeriodo;
    }

    public List<PensionadosSinRsh> getTbPensionadosSinRshList() {
        return tbPensionadosSinRshList;
    }

    public void setTbPensionadosSinRshList(List<PensionadosSinRsh> tbPensionadosSinRshList) {
        this.tbPensionadosSinRshList = tbPensionadosSinRshList;
    }

    public PensionadosSinRsh addPensionadosSinRsh(PensionadosSinRsh pensionadosSinRsh) {
        getTbPensionadosSinRshList().add(pensionadosSinRsh);
        pensionadosSinRsh.setTbPeriodo10(this);
        return pensionadosSinRsh;
    }

    public PensionadosSinRsh removePensionadosSinRsh(PensionadosSinRsh pensionadosSinRsh) {
        getTbPensionadosSinRshList().remove(pensionadosSinRsh);
        pensionadosSinRsh.setTbPeriodo10(null);
        return pensionadosSinRsh;
    }

    public List<Bitacora> getTbBitacoraList2() {
        return tbBitacoraList2;
    }

    public void setTbBitacoraList2(List<Bitacora> tbBitacoraList2) {
        this.tbBitacoraList2 = tbBitacoraList2;
    }

    public Bitacora addBitacora(Bitacora bitacora) {
        getTbBitacoraList2().add(bitacora);
        bitacora.setTbPeriodo11(this);
        return bitacora;
    }

    public Bitacora removeBitacora(Bitacora bitacora) {
        getTbBitacoraList2().remove(bitacora);
        bitacora.setTbPeriodo11(null);
        return bitacora;
    }

    public List<NominaValidacion> getTbNominaValidacionList() {
        return tbNominaValidacionList;
    }

    public void setTbNominaValidacionList(List<NominaValidacion> tbNominaValidacionList) {
        this.tbNominaValidacionList = tbNominaValidacionList;
    }

    public NominaValidacion addNominaValidacion(NominaValidacion nominaValidacion) {
        getTbNominaValidacionList().add(nominaValidacion);
        nominaValidacion.setTbPeriodo12(this);
        return nominaValidacion;
    }

    public NominaValidacion removeNominaValidacion(NominaValidacion nominaValidacion) {
        getTbNominaValidacionList().remove(nominaValidacion);
        nominaValidacion.setTbPeriodo12(null);
        return nominaValidacion;
    }

    public List<NominaBeneficiario> getTbNominaBeneficiarios() {
        return this.tbNominaBeneficiarios;
    }

    public void setTbNominaBeneficiarios(List<NominaBeneficiario> tbNominaBeneficiarios) {
        this.tbNominaBeneficiarios = tbNominaBeneficiarios;
    }

    public NominaBeneficiario addTbNominaBeneficiario(NominaBeneficiario tbNominaBeneficiario) {
        getTbNominaBeneficiarios().add(tbNominaBeneficiario);
        tbNominaBeneficiario.setPeriodo(this);

        return tbNominaBeneficiario;
    }

    public NominaBeneficiario removeTbNominaBeneficiario(NominaBeneficiario tbNominaBeneficiario) {
        getTbNominaBeneficiarios().remove(tbNominaBeneficiario);
        tbNominaBeneficiario.setPeriodo(null);

        return tbNominaBeneficiario;
    }

    public List<NominaBphSinPbs> getTbNominaBphSinPbs() {
        return this.tbNominaBphSinPbs;
    }

    public void setTbNominaBphSinPbs(List<NominaBphSinPbs> tbNominaBphSinPbs) {
        this.tbNominaBphSinPbs = tbNominaBphSinPbs;
    }

    public NominaBphSinPbs addTbNominaBphSinPb(NominaBphSinPbs tbNominaBphSinPb) {
        getTbNominaBphSinPbs().add(tbNominaBphSinPb);
        tbNominaBphSinPb.setPeriodo(this);

        return tbNominaBphSinPb;
    }

    public NominaBphSinPbs removeTbNominaBphSinPb(NominaBphSinPbs tbNominaBphSinPb) {
        getTbNominaBphSinPbs().remove(tbNominaBphSinPb);
        tbNominaBphSinPb.setPeriodo(null);

        return tbNominaBphSinPb;
    }

    public List<NominaConsultaPdi> getTbNominaConsultaPdis() {
        return this.tbNominaConsultaPdis;
    }

    public void setTbNominaConsultaPdis(List<NominaConsultaPdi> tbNominaConsultaPdis) {
        this.tbNominaConsultaPdis = tbNominaConsultaPdis;
    }

    public NominaConsultaPdi addTbNominaConsultaPdi(NominaConsultaPdi tbNominaConsultaPdi) {
        getTbNominaConsultaPdis().add(tbNominaConsultaPdi);
        tbNominaConsultaPdi.setPeriodo(this);

        return tbNominaConsultaPdi;
    }

    public NominaConsultaPdi removeTbNominaConsultaPdi(NominaConsultaPdi tbNominaConsultaPdi) {
        getTbNominaConsultaPdis().remove(tbNominaConsultaPdi);
        tbNominaConsultaPdi.setPeriodo(null);

        return tbNominaConsultaPdi;
    }

    public List<NominaPenSinFicha> getTbNominaPenSinFichas() {
        return this.tbNominaPenSinFichas;
    }

    public void setTbNominaPenSinFichas(List<NominaPenSinFicha> tbNominaPenSinFichas) {
        this.tbNominaPenSinFichas = tbNominaPenSinFichas;
    }

    public NominaPenSinFicha addTbNominaPenSinFicha(NominaPenSinFicha tbNominaPenSinFicha) {
        getTbNominaPenSinFichas().add(tbNominaPenSinFicha);
        tbNominaPenSinFicha.setPeriodo(this);

        return tbNominaPenSinFicha;
    }

    public NominaPenSinFicha removeTbNominaPenSinFicha(NominaPenSinFicha tbNominaPenSinFicha) {
        getTbNominaPenSinFichas().remove(tbNominaPenSinFicha);
        tbNominaPenSinFicha.setPeriodo(null);

        return tbNominaPenSinFicha;
    }

    public List<NominaPotencialesAps> getTbNominaPotencialesAps() {
        return this.tbNominaPotencialesAps;
    }

    public void setTbNominaPotencialesAps(List<NominaPotencialesAps> tbNominaPotencialesAps) {
        this.tbNominaPotencialesAps = tbNominaPotencialesAps;
    }

    public NominaPotencialesAps addTbNominaPotencialesAp(NominaPotencialesAps tbNominaPotencialesAp) {
        getTbNominaPotencialesAps().add(tbNominaPotencialesAp);
        tbNominaPotencialesAp.setPeriodo(this);

        return tbNominaPotencialesAp;
    }

    public NominaPotencialesAps removeTbNominaPotencialesAp(NominaPotencialesAps tbNominaPotencialesAp) {
        getTbNominaPotencialesAps().remove(tbNominaPotencialesAp);
        tbNominaPotencialesAp.setPeriodo(null);

        return tbNominaPotencialesAp;
    }

    public List<NominaPotBeneficiario> getTbNominaPotBeneficiarios() {
        return this.tbNominaPotBeneficiarios;
    }

    public void setTbNominaPotBeneficiarios(List<NominaPotBeneficiario> tbNominaPotBeneficiarios) {
        this.tbNominaPotBeneficiarios = tbNominaPotBeneficiarios;
    }

    public NominaPotBeneficiario addTbNominaPotBeneficiario(NominaPotBeneficiario tbNominaPotBeneficiario) {
        getTbNominaPotBeneficiarios().add(tbNominaPotBeneficiario);
        tbNominaPotBeneficiario.setPeriodo(this);

        return tbNominaPotBeneficiario;
    }

    public NominaPotBeneficiario removeTbNominaPotBeneficiario(NominaPotBeneficiario tbNominaPotBeneficiario) {
        getTbNominaPotBeneficiarios().remove(tbNominaPotBeneficiario);
        tbNominaPotBeneficiario.setPeriodo(null);

        return tbNominaPotBeneficiario;
    }

    public List<NominaRebaja> getTbNominaRebajas() {
        return this.tbNominaRebajas;
    }

    public void setTbNominaRebajas(List<NominaRebaja> tbNominaRebajas) {
        this.tbNominaRebajas = tbNominaRebajas;
    }

    public NominaRebaja addTbNominaRebaja(NominaRebaja tbNominaRebaja) {
        getTbNominaRebajas().add(tbNominaRebaja);
        tbNominaRebaja.setPeriodo(this);

        return tbNominaRebaja;
    }

    public NominaRebaja removeTbNominaRebaja(NominaRebaja tbNominaRebaja) {
        getTbNominaRebajas().remove(tbNominaRebaja);
        tbNominaRebaja.setPeriodo(null);

        return tbNominaRebaja;
    }

    public Integer getVivos() {
        return vivos;
    }

    public void setVivos(Integer vivos) {
        this.vivos = vivos;
    }

    public Integer getSinSps() {
        return sinSps;
    }

    public void setSinSps(Integer sinSps) {
        this.sinSps = sinSps;
    }

    public Integer getSinRsh() {
        return sinRsh;
    }

    public void setSinRsh(Integer sinRsh) {
        this.sinRsh = sinRsh;
    }

    public Integer getSinFocalizacion() {
        return sinFocalizacion;
    }

    public void setSinFocalizacion(Integer sinFocalizacion) {
        this.sinFocalizacion = sinFocalizacion;
    }

    public Integer getSinResidencia() {
        return sinResidencia;
    }

    public void setSinResidencia(Integer sinResidencia) {
        this.sinResidencia = sinResidencia;
    }

    public Integer getCumplenRequisitos() {
        return cumplenRequisitos;
    }

    public void setCumplenRequisitos(Integer cumplenRequisitos) {
        this.cumplenRequisitos = cumplenRequisitos;
    }

    public Integer getNivelActual() {
        return nivelActual;
    }

    public void setNivelActual(Integer nivelActual) {
        this.nivelActual = nivelActual;
    }
}
