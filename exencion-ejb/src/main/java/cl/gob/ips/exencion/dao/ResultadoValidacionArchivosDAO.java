package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.model.ArchivosCargaPeriodo;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Created by alaya on 29/08/17.
 */

@Stateless
@LocalBean
public class ResultadoValidacionArchivosDAO extends BaseDAO{

    public List<ArchivosCargaPeriodo> obtenerResultadoValidacionArchivosDAO(Integer periodo) throws DataAccessException, IpsBussinesException {

        return (List<ArchivosCargaPeriodo>) super.em.createQuery(
                "SELECT arch FROM " + ArchivosCargaPeriodo.class.getName() + " arch WHERE arch.tbPeriodo1.perPeriodo = :periodo",
                ArchivosCargaPeriodo.class)
                .setParameter("periodo", periodo)
                .getResultList();

        //todo poner order by id

    }

}
