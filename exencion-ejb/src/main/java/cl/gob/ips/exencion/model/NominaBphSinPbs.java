package cl.gob.ips.exencion.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the TB_NOMINA_BPH_SIN_PBS database table.
 * 
 */
@Entity
@Table(name = "TB_NOMINA_BPH_SIN_PBS")
@NamedQuery(name = "NominaBphSinPb.findAll", query = "SELECT t FROM NominaBphSinPbs t")
public class NominaBphSinPbs implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7456428498298635512L;

	@Id
	@Column(name = "NOM_ID")
	private long nomId;

	@Column(name = "NOM_CODIGO_HABER")
	private String nomCodigoHaber;

	@Column(name = "NOM_DV_BENEFICIARIO")
	private String nomDvBeneficiario;

	@Column(name = "NOM_DV_INSTITUCION")
	private String nomDvInstitucion;

	@Temporal(TemporalType.DATE)
	@Column(name = "NOM_FECHA_INIC")
	private Date nomFechaInic;

	@Temporal(TemporalType.DATE)
	@Column(name = "NOM_FECHA_VENCIMIENTO")
	private Date nomFechaVencimiento;

	@Column(name = "NOM_MONTO")
	private Integer nomMonto;

	@Column(name = "NOM_RUN_BENEFICIARIO")
	private Integer nomRunBeneficiario;

	@Column(name = "NOM_TIPO_BENEFICIO")
	private Integer nomTipoBeneficio;

	@Column(name = "NOM_TIPO_MOVIMIENTO")
	private String nomTipoMovimiento;

	@Column(name = "NOM_UNIDAD_MEDIDA")
	private String nomUnidadMedida;

	// bi-directional many-to-one association to TbPeriodo
	@ManyToOne
	@JoinColumn(name = "NOM_PERIODO")
	private Periodo periodo;

	// bi-directional many-to-one association to TbPrInstitucione
	@ManyToOne
	@JoinColumn(name = "NOM_RUT_INSTITUCION")
	private Instituciones instituciones;

	public NominaBphSinPbs() {
	}

	public long getNomId() {
		return this.nomId;
	}

	public void setNomId(long nomId) {
		this.nomId = nomId;
	}

	public String getNomCodigoHaber() {
		return this.nomCodigoHaber;
	}

	public void setNomCodigoHaber(String nomCodigoHaber) {
		this.nomCodigoHaber = nomCodigoHaber;
	}

	public String getNomDvBeneficiario() {
		return this.nomDvBeneficiario;
	}

	public void setNomDvBeneficiario(String nomDvBeneficiario) {
		this.nomDvBeneficiario = nomDvBeneficiario;
	}

	public String getNomDvInstitucion() {
		return this.nomDvInstitucion;
	}

	public void setNomDvInstitucion(String nomDvInstitucion) {
		this.nomDvInstitucion = nomDvInstitucion;
	}

	public Date getNomFechaInic() {
		return this.nomFechaInic;
	}

	public void setNomFechaInic(Date nomFechaInic) {
		this.nomFechaInic = nomFechaInic;
	}

	public Date getNomFechaVencimiento() {
		return this.nomFechaVencimiento;
	}

	public void setNomFechaVencimiento(Date nomFechaVencimiento) {
		this.nomFechaVencimiento = nomFechaVencimiento;
	}

	public Integer getNomMonto() {
		return this.nomMonto;
	}

	public void setNomMonto(Integer nomMonto) {
		this.nomMonto = nomMonto;
	}

	public Integer getNomRunBeneficiario() {
		return this.nomRunBeneficiario;
	}

	public void setNomRunBeneficiario(Integer nomRunBeneficiario) {
		this.nomRunBeneficiario = nomRunBeneficiario;
	}

	public Integer getNomTipoBeneficio() {
		return this.nomTipoBeneficio;
	}

	public void setNomTipoBeneficio(Integer nomTipoBeneficio) {
		this.nomTipoBeneficio = nomTipoBeneficio;
	}

	public String getNomTipoMovimiento() {
		return this.nomTipoMovimiento;
	}

	public void setNomTipoMovimiento(String nomTipoMovimiento) {
		this.nomTipoMovimiento = nomTipoMovimiento;
	}

	public String getNomUnidadMedida() {
		return this.nomUnidadMedida;
	}

	public void setNomUnidadMedida(String nomUnidadMedida) {
		this.nomUnidadMedida = nomUnidadMedida;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	public Instituciones getInstituciones() {
		return instituciones;
	}

	public void setInstituciones(Instituciones instituciones) {
		this.instituciones = instituciones;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder
				.append(nomTipoMovimiento ) 		// todo revisar largo de datos
				.append(nomTipoBeneficio)
				.append(nomRunBeneficiario)
				.append(nomDvBeneficiario)
				.append(nomMonto)
				.append(nomFechaInic)
				.append(nomFechaVencimiento)
				.append(nomCodigoHaber)
				.append(nomUnidadMedida);

		return 	builder.toString();


	}

}