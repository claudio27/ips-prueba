package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.ArchivosCargaPeriodo;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Created by lpino on 13-07-17.
 */
@Stateless
@LocalBean
public class CargaArchivosDAO extends BaseDAO {

    private static final String PARAM_PERIODO = "periodo";

    /**
     * @param periodo
     * @return List<ArchivosCargaPeriodo>
     * @throws DataAccessException
     */
    public List<ArchivosCargaPeriodo> obtenerArchivosPorPeriodo(final Integer periodo) throws DataAccessException {

        List<ArchivosCargaPeriodo> archivosCargaList = null;

        super.query = super.em.createQuery("SELECT arc FROM " + ArchivosCargaPeriodo.class.getName() + " arc "
                + "WHERE arc.tbPeriodo1.perPeriodo = :periodo ")
                .setParameter(PARAM_PERIODO, periodo);

        if (!super.query.getResultList().isEmpty()) {
            archivosCargaList = super.query.getResultList();
        }

        return archivosCargaList;
    }
}
