package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.ResponsableGrupoArchivoDao;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.GruposArchivo;
import cl.gob.ips.exencion.model.UsuarioGrupo;
import cl.gob.ips.exencion.model.Usuarios;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by rodrigo.reyesco@gmail.com on 27-10-2016.
 */
@Stateless
@LocalBean
public class ResponsableGrupoArchivoServices {

    @Inject
    private ResponsableGrupoArchivoDao dao;

    /**
     *
     * @param grupoArchivo
     * @return
     * @throws DataAccessException
     */
    public List<UsuarioGrupo> getUsuarioGrupoByGrupoArchivo(GruposArchivo grupoArchivo) throws DataAccessException{
        return dao.getUsuarioGrupoByGrupoArchivo(grupoArchivo);
    }

    /**
     *
     * @param email
     * @return
     * @throws DataAccessException
     */
    public Usuarios findUsuarioByEmail(final String email) throws DataAccessException{
        return dao.findUsuarioByEmail(email);
    }

    /**
     *
     * @param usuarioGrupo
     * @throws DataAccessException
     */
    public void creaUsuarioGrupoArchivo(final UsuarioGrupo usuarioGrupo) throws DataAccessException, SQLException {
        dao.creaUsuarioGrupoArchivo(usuarioGrupo);
    }

    /**
     *
     * @param usuarioGrupo
     * @throws DataAccessException
     */
    public void actualizaUsuarioGrupoArchivo(final UsuarioGrupo usuarioGrupo) throws DataAccessException{
        dao.actualizaUsuarioGrupoArchivo(usuarioGrupo);
    }
}


