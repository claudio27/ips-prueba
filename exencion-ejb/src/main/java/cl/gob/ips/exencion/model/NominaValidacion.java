package cl.gob.ips.exencion.model;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQueries({ @NamedQuery(name = "NominaValidacion.findAll", query = "select o from NominaValidacion o") })
@Table(name = "TB_NOMINA_VALIDACION")
public class NominaValidacion implements Serializable {
    private static final long serialVersionUID = 628111914653809260L;
    @Column(name = "NOM_VAL_DV_BEN", length = 1)
    private String nomValDvBen;
    @Column(name = "NOM_VAL_ELIMINADO")
    private Integer nomValEliminado;
    @Temporal(TemporalType.DATE)
    @Column(name = "NOM_VAL_FEC_ELIMINADO")
    private Date nomValFecEliminado;
    @Id
    @Column(name = "NOM_VAL_ID", nullable = false)
    private Long nomValId;
    @Column(name = "NOM_VAL_RUT_BEN", nullable = false)
    private Long nomValRutBen;
    @Column(name = "NOM_VAL_USU_ELIMINADO", length = 20)
    private String nomValUsuEliminado;
    @ManyToOne
    @JoinColumn(name = "NOM_VAL_PERIODO")
    private Periodo tbPeriodo12;

    public NominaValidacion() {
    }

    public NominaValidacion(String nomValDvBen, Integer nomValEliminado, Date nomValFecEliminado, Long nomValId,
                            Periodo tbPeriodo12, Long nomValRutBen, String nomValUsuEliminado) {
        this.nomValDvBen = nomValDvBen;
        this.nomValEliminado = nomValEliminado;
        this.nomValFecEliminado = nomValFecEliminado;
        this.nomValId = nomValId;
        this.tbPeriodo12 = tbPeriodo12;
        this.nomValRutBen = nomValRutBen;
        this.nomValUsuEliminado = nomValUsuEliminado;
    }

    public String getNomValDvBen() {
        return nomValDvBen;
    }

    public void setNomValDvBen(String nomValDvBen) {
        this.nomValDvBen = nomValDvBen;
    }

    public Integer getNomValEliminado() {
        return nomValEliminado;
    }

    public void setNomValEliminado(Integer nomValEliminado) {
        this.nomValEliminado = nomValEliminado;
    }

    public Date getNomValFecEliminado() {
        return nomValFecEliminado;
    }

    public void setNomValFecEliminado(Date nomValFecEliminado) {
        this.nomValFecEliminado = nomValFecEliminado;
    }

    public Long getNomValId() {
        return nomValId;
    }

    public void setNomValId(Long nomValId) {
        this.nomValId = nomValId;
    }


    public Long getNomValRutBen() {
        return nomValRutBen;
    }

    public void setNomValRutBen(Long nomValRutBen) {
        this.nomValRutBen = nomValRutBen;
    }

    public String getNomValUsuEliminado() {
        return nomValUsuEliminado;
    }

    public void setNomValUsuEliminado(String nomValUsuEliminado) {
        this.nomValUsuEliminado = nomValUsuEliminado;
    }

    public Periodo getTbPeriodo12() {
        return tbPeriodo12;
    }

    public void setTbPeriodo12(Periodo tbPeriodo12) {
        this.tbPeriodo12 = tbPeriodo12;
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder
                .append(StringUtils.leftPad(String.valueOf(nomValRutBen), 8, "0"))
                .append(nomValDvBen);

        return 	builder.toString();


    }
}
