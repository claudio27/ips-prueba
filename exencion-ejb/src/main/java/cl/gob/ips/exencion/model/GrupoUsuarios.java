package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_GRUPO_USUARIOS_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_GRUPO_USUARIOS_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = GrupoUsuarios.QUERY_FIND_ALL, query = "select o from GrupoUsuarios o"),
        @NamedQuery(name = GrupoUsuarios.QUERY_FIND_BY_DESCRIPCION, query = "select o from GrupoUsuarios o where o.gruUsuDescripcion = :gru_usu_descrip")})
@Table(name = "TB_GRUPO_USUARIOS")
@SequenceGenerator(name = "SEQ_GRUPO_USUARIOS", sequenceName = "SEQ_GRUPO_USUARIOS",
        allocationSize = 1, initialValue = 1)
public class GrupoUsuarios implements Serializable {
    private static final long serialVersionUID = -7770159280449004297L;

    public static final String QUERY_FIND_ALL = "GrupoUsuarios.findAll";
    public static final String QUERY_FIND_BY_DESCRIPCION = "GrupoUsuarios.findByDescripcion";

    @Id
    @Column(name = "GRU_USU_COD", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GRUPO_USUARIOS")
    private Integer gruUsuCod;

    @Column(name = "GRU_USU_DESCRIPCION", length = 40)
    private String gruUsuDescripcion;

    @OneToMany(mappedBy = "tbGrupoUsuarios", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<GrupoUsuarioFunciones> tbGrupusuFuncList;

    @OneToMany(mappedBy = "tbGrupoUsuarios1", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<GrupoUsuarioUsuarios> tbGrupusuUsuList;

    public GrupoUsuarios() {
    }

    public GrupoUsuarios(Integer gruUsuCod, String gruUsuDescripcion) {
        this.gruUsuCod = gruUsuCod;
        this.gruUsuDescripcion = gruUsuDescripcion;
    }

    public Integer getGruUsuCod() {
        return gruUsuCod;
    }

    public String getGruUsuDescripcion() {
        return gruUsuDescripcion;
    }

    public void setGruUsuDescripcion(String gruUsuDescripcion) {
        this.gruUsuDescripcion = gruUsuDescripcion;
    }

    public List<GrupoUsuarioFunciones> getTbGrupusuFuncList() {
        return tbGrupusuFuncList;
    }

    public void setTbGrupusuFuncList(List<GrupoUsuarioFunciones> tbGrupusuFuncList) {
        this.tbGrupusuFuncList = tbGrupusuFuncList;
    }

    public GrupoUsuarioFunciones addGrupoUsuarioFunciones(GrupoUsuarioFunciones grupoUsuarioFunciones) {
        getTbGrupusuFuncList().add(grupoUsuarioFunciones);
        grupoUsuarioFunciones.setTbGrupoUsuarios(this);
        return grupoUsuarioFunciones;
    }

    public GrupoUsuarioFunciones removeGrupoUsuarioFunciones(GrupoUsuarioFunciones grupoUsuarioFunciones) {
        getTbGrupusuFuncList().remove(grupoUsuarioFunciones);
        grupoUsuarioFunciones.setTbGrupoUsuarios(null);
        return grupoUsuarioFunciones;
    }

    public List<GrupoUsuarioUsuarios> getTbGrupusuUsuList() {
        return tbGrupusuUsuList;
    }

    public void setTbGrupusuUsuList(List<GrupoUsuarioUsuarios> tbGrupusuUsuList) {
        this.tbGrupusuUsuList = tbGrupusuUsuList;
    }

    public GrupoUsuarioUsuarios addGrupoUsuarioUsuarios(GrupoUsuarioUsuarios grupoUsuarioUsuarios) {
        getTbGrupusuUsuList().add(grupoUsuarioUsuarios);
        grupoUsuarioUsuarios.setTbGrupoUsuarios1(this);
        return grupoUsuarioUsuarios;
    }

    public GrupoUsuarioUsuarios removeGrupoUsuarioUsuarios(GrupoUsuarioUsuarios grupoUsuarioUsuarios) {
        getTbGrupusuUsuList().remove(grupoUsuarioUsuarios);
        grupoUsuarioUsuarios.setTbGrupoUsuarios1(null);
        return grupoUsuarioUsuarios;
    }
}
