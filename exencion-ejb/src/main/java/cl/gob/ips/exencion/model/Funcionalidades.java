package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_FUNCIONALIDADES_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_FUNCIONALIDADES_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = Funcionalidades.QUERY_FIND_ALL, query = "select o from Funcionalidades o"),
        @NamedQuery(name = Funcionalidades.QUERY_FIND_FUNCIONALIDAD_GRUPO, query = "select o from Funcionalidades o JOIN o.tbGrupusuFuncList1 g where o.funcDescripcion = :nombre_funcion AND g.tbGrupoUsuarios.gruUsuCod = :gru_usu_cod"),
        @NamedQuery(name = Funcionalidades.QUERY_FIND_FUNCIONALIDAD_NO_GRUPO, query = "select a from Funcionalidades a where a.funcCod not in (select b.tbPrFuncionalidades.funcCod from GrupoUsuarioFunciones b where b.tbGrupoUsuarios.gruUsuCod = :gru_usu_cod) AND a.funcDescripcion = :nombre_funcion"),
        @NamedQuery(name = Funcionalidades.QUERY_FIND_FUNCIONES_PERTENECE_GRUPO, query = "select o from Funcionalidades o JOIN o.tbGrupusuFuncList1 g where g.tbGrupoUsuarios.gruUsuCod = :gru_fun_cod "),
        @NamedQuery(name = Funcionalidades.QUERY_FIND_FUNCIONES_NO_PERTENECE_GRUPO, query = "select a from Funcionalidades a where a.funcCod not in (\n" +
                "select b.tbPrFuncionalidades.funcCod from GrupoUsuarioFunciones b where b.tbGrupoUsuarios.gruUsuCod = :gru_fun_cod)")
})
@Table(name = "TB_PR_FUNCIONALIDADES")
@SequenceGenerator(name = "Funcionalidades_Id_Seq_Gen", sequenceName = "TB_PR_FUNCIONALIDADES_ID_SEQ_GEN",
        allocationSize = 50, initialValue = 50)
public class Funcionalidades implements Serializable {
    private static final long serialVersionUID = 2730250818848383307L;

    public static final String QUERY_FIND_ALL = "Funcionalidades.findAll";
    public static final String QUERY_FIND_FUNCIONALIDAD_GRUPO = "GrupoUsuarioUsuarios.findFuncGroup";
    public static final String QUERY_FIND_FUNCIONALIDAD_NO_GRUPO = "GrupoUsuarioUsuarios.findFuncNoGroup";
    public static final String QUERY_FIND_FUNCIONES_PERTENECE_GRUPO = "Funcionalidades.findByGruUsuCod";
    public static final String QUERY_FIND_FUNCIONES_NO_PERTENECE_GRUPO = "Funcionalidades.findBy!GruUsuCod";

    @Id
    @Column(name = "FUNC_COD", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Funcionalidades_Id_Seq_Gen")
    private Integer funcCod;
    @Column(name = "FUNC_DESCRIPCION", nullable = false, length = 50)
    private String funcDescripcion;
    @OneToMany(mappedBy = "tbPrFuncionalidades", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<GrupoUsuarioFunciones> tbGrupusuFuncList1;

    public Funcionalidades() {
    }

    public Funcionalidades(Integer funcCod, String funcDescripcion) {
        this.funcCod = funcCod;
        this.funcDescripcion = funcDescripcion;
    }

    public Integer getFuncCod() {
        return funcCod;
    }

    public String getFuncDescripcion() {
        return funcDescripcion;
    }

    public void setFuncDescripcion(String funcDescripcion) {
        this.funcDescripcion = funcDescripcion;
    }

    public List<GrupoUsuarioFunciones> getTbGrupusuFuncList1() {
        return tbGrupusuFuncList1;
    }

    public void setTbGrupusuFuncList1(List<GrupoUsuarioFunciones> tbGrupusuFuncList1) {
        this.tbGrupusuFuncList1 = tbGrupusuFuncList1;
    }

    public GrupoUsuarioFunciones addGrupoUsuarioFunciones(GrupoUsuarioFunciones grupoUsuarioFunciones) {
        getTbGrupusuFuncList1().add(grupoUsuarioFunciones);
        grupoUsuarioFunciones.setTbPrFuncionalidades(this);
        return grupoUsuarioFunciones;
    }

    public GrupoUsuarioFunciones removeGrupoUsuarioFunciones(GrupoUsuarioFunciones grupoUsuarioFunciones) {
        getTbGrupusuFuncList1().remove(grupoUsuarioFunciones);
        grupoUsuarioFunciones.setTbPrFuncionalidades(null);
        return grupoUsuarioFunciones;
    }
}
