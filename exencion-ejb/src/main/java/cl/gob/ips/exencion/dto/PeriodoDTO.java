package cl.gob.ips.exencion.dto;

import java.io.Serializable;

/**
 * Created by practicante on 11-10-17.
 */
public class PeriodoDTO implements Serializable {

    private String perFecCierre;
    private String perFecCreacion;
    private String perPeriodo;
    private String perUsuCierre;
    private Integer nivelActual;
    private Integer etapaPeriodoCod;
    private String etapaPeriodoGlosa;

    public String getPerFecCierre() {
        return perFecCierre;
    }

    public void setPerFecCierre(String perFecCierre) {
        this.perFecCierre = perFecCierre;
    }

    public String getPerFecCreacion() {
        return perFecCreacion;
    }

    public void setPerFecCreacion(String perFecCreacion) {
        this.perFecCreacion = perFecCreacion;
    }

    public String getPerPeriodo() {
        return perPeriodo;
    }

    public void setPerPeriodo(String perPeriodo) {
        this.perPeriodo = perPeriodo;
    }

    public String getPerUsuCierre() {
        return perUsuCierre;
    }

    public void setPerUsuCierre(String perUsuCierre) {
        this.perUsuCierre = perUsuCierre;
    }

    public Integer getNivelActual() {
        return nivelActual;
    }

    public void setNivelActual(Integer nivelActual) {
        this.nivelActual = nivelActual;
    }

    public Integer getEtapaPeriodoCod() {
        return etapaPeriodoCod;
    }

    public void setEtapaPeriodoCod(Integer etapaPeriodoCod) {
        this.etapaPeriodoCod = etapaPeriodoCod;
    }

    public String getEtapaPeriodoGlosa() {
        return etapaPeriodoGlosa;
    }

    public void setEtapaPeriodoGlosa(String etapaPeriodoGlosa) {
        this.etapaPeriodoGlosa = etapaPeriodoGlosa;
    }
}
