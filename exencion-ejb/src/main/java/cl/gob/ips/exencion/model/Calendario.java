package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@NamedQueries({ 
        @NamedQuery(name = "Calendario.findAll", query = "select o from Calendario o"),
        @NamedQuery(name = Calendario.UPDATE_STATUS, query = "UPDATE Calendario o SET o.calFecEliminacion = :fecEliminacion, o.calUsuEliminacion = :usuEliminacion  WHERE o.calId = :id"),
        @NamedQuery(name = Calendario.QUERY_FIND_BY_DATES, query = "SELECT o FROM Calendario o WHERE o.calFecFeriado BETWEEN :iniFecha AND :finFecha AND o.calFecEliminacion = NULL"),        
        @NamedQuery(name = Calendario.QUERY_FIND_A_DATE, query = "SELECT o FROM Calendario o WHERE o.calFecFeriado = :iniFecha AND o.calFecEliminacion = NULL")        
})
@Table(name = "TB_CALENDARIO")
public class Calendario implements Serializable {
    private static final long serialVersionUID = 5073885284695392574L;
    
    public static final String QUERY_FIND_ALL = "Calendario.findAll";
    public static final String QUERY_FIND_BY_DATES = "Calendario.findByDates";
    public static final String QUERY_FIND_A_DATE = "Calendario.findADate";
    public static final String UPDATE_STATUS = "Calendario.updateStatus";

    
    @Column(name = "CAL_DESCRIPCION", length = 40)
    private String calDescripcion;
    @Temporal(TemporalType.DATE)
    @Column(name = "CAL_FEC_CREACION")
    private Date calFecCreacion;
    @Temporal(TemporalType.DATE)
    @Column(name = "CAL_FEC_ELIMINACION")
    private Date calFecEliminacion;
    @Temporal(TemporalType.DATE)
    @Column(name = "CAL_FEC_FERIADO")
    private Date calFecFeriado;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_calendario")
    @SequenceGenerator(name="seq_calendario", sequenceName="SEQ_CALENDARIO", allocationSize=1)
    @Column(name = "CAL_ID", nullable = false)
    private Long calId;
    @Column(name = "CAL_USU_CREACION", length = 20)
    private String calUsuCreacion;
    @Column(name = "CAL_USU_ELIMINACION", length = 20)
    private String calUsuEliminacion;

    public Calendario() {
    }

    public Calendario(String calDescripcion, Date calFecCreacion, Date calFecEliminacion, Date calFecFeriado,
                      Long calId, String calUsuCreacion, String calUsuEliminacion) {
        this.calDescripcion = calDescripcion;
        this.calFecCreacion = calFecCreacion;
        this.calFecEliminacion = calFecEliminacion;
        this.calFecFeriado = calFecFeriado;
        this.calId = calId;
        this.calUsuCreacion = calUsuCreacion;
        this.calUsuEliminacion = calUsuEliminacion;
    }

    public String getCalDescripcion() {
        return calDescripcion;
    }

    public void setCalDescripcion(String calDescripcion) {
        this.calDescripcion = calDescripcion;
    }

    public Date getCalFecCreacion() {
        return calFecCreacion;
    }

    public void setCalFecCreacion(Date calFecCreacion) {
        this.calFecCreacion = calFecCreacion;
    }

    public Date getCalFecEliminacion() {
        return calFecEliminacion;
    }

    public void setCalFecEliminacion(Date calFecEliminacion) {
        this.calFecEliminacion = calFecEliminacion;
    }

    public Date getCalFecFeriado() {
        return calFecFeriado;
    }

    public void setCalFecFeriado(Date calFecFeriado) {
        this.calFecFeriado = calFecFeriado;
    }

    public Long getCalId() {
        return calId;
    }

    public void setCalId(Long calId) {
        this.calId = calId;
    }

    public String getCalUsuCreacion() {
        return calUsuCreacion;
    }

    public void setCalUsuCreacion(String calUsuCreacion) {
        this.calUsuCreacion = calUsuCreacion;
    }

    public String getCalUsuEliminacion() {
        return calUsuEliminacion;
    }

    public void setCalUsuEliminacion(String calUsuEliminacion) {
        this.calUsuEliminacion = calUsuEliminacion;
    }
}
