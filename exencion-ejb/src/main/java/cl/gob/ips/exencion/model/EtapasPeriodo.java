package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_ETAPAS_PERIODO_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_ETAPAS_PERIODO_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "EtapasPeriodo.findAll", query = "select o from EtapasPeriodo o") })
@Table(name = "TB_PR_ETAPAS_PERIODO")
@SequenceGenerator(name = "EtapasPeriodo_Id_Seq_Gen", sequenceName = "TB_PR_ETAPAS_PERIODO_ID_SEQ_GEN",
                   allocationSize = 50, initialValue = 50)
public class EtapasPeriodo implements Serializable {
    private static final long serialVersionUID = 897989821832583889L;
    @Id
    @Column(name = "ETAPA_PER_COD", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EtapasPeriodo_Id_Seq_Gen")
    private Integer etapaPerCod;
    @Column(name = "ETAPA_PER_GLOSA", nullable = false, length = 20)
    private String etapaPerGlosa;
    @OneToMany(mappedBy = "tbPrEtapasPeriodo", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Periodo> tbPeriodoList;

    public EtapasPeriodo() {
    }

    public EtapasPeriodo(Integer etapaPerCod, String etapaPerGlosa) {
        this.etapaPerCod = etapaPerCod;
        this.etapaPerGlosa = etapaPerGlosa;
    }

    public Integer getEtapaPerCod() {
        return etapaPerCod;
    }

    public String getEtapaPerGlosa() {
        return etapaPerGlosa;
    }

    public void setEtapaPerGlosa(String etapaPerGlosa) {
        this.etapaPerGlosa = etapaPerGlosa;
    }

    public List<Periodo> getTbPeriodoList() {
        return tbPeriodoList;
    }

    public void setTbPeriodoList(List<Periodo> tbPeriodoList) {
        this.tbPeriodoList = tbPeriodoList;
    }

    public Periodo addPeriodo(Periodo periodo) {
        getTbPeriodoList().add(periodo);
        periodo.setTbPrEtapasPeriodo(this);
        return periodo;
    }

    public Periodo removePeriodo(Periodo periodo) {
        getTbPeriodoList().remove(periodo);
        periodo.setTbPrEtapasPeriodo(null);
        return periodo;
    }
}
