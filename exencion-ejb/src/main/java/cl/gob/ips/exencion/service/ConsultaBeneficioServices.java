package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.ConsultaBeneficioDAO;
import cl.gob.ips.exencion.dao.TipoRespuestaDao;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.ConsultaBeneficio;
import cl.gob.ips.exencion.model.TipoRespuesta;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lpino on 05-07-17.
 */
@Stateless
@LocalBean
public class ConsultaBeneficioServices {

    @Inject
    private ConsultaBeneficioDAO consultaBeneficioDAO;

    @Inject
    private TipoRespuestaDao tipoRespuestaDao;

    public List<ConsultaBeneficio> getConsultaBeneficio(Integer run) throws DataAccessException {
        return consultaBeneficioDAO.obtenerConsultaBeneficio(run);
    }

    public void guardarConsultaBeneficio(final Integer run, final String dv,
                                         final Integer periodo, final String observacion,
                                         final String usuario, final String instituciones,
                                         final Integer codigoRespuesta) throws DataAccessException {

        TipoRespuesta tipoRespuesta= tipoRespuestaDao.obtenerTipoRespuestaPorCodigo(codigoRespuesta);
        List<TipoRespuesta> listaTipoRespuestas = new ArrayList<TipoRespuesta>();
        if(tipoRespuesta !=null)
         listaTipoRespuestas.add(tipoRespuesta);

        ConsultaBeneficio consulta = consultaBeneficioDAO.guardarConsultaBeneficio(run, dv,
                periodo, observacion, usuario, instituciones,codigoRespuesta, listaTipoRespuestas);

    }

    public List<String> obtenerNombreInstituciones(final Integer rut, final Integer periodo) throws DataAccessException {
        return consultaBeneficioDAO.obtenerNombreInstituciones(rut, periodo);
    }

    public List<String> obtenerNombreInstituciones(final Integer rut) throws DataAccessException {
        return consultaBeneficioDAO.obtenerNombreInstituciones(rut);
    }

}
