package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Calendario;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

/**
 * Created by senshi.cristian@gmail.com on 17-10-2016.
 */
@Stateless
@LocalBean
public class CalendarioDao extends BaseDAO {

    private static final String PARAM_FECHA_INICIO = "iniFecha";

    private static final String PARAM_FECHA_FIN = "finFecha";

    private static final String PARAM_ID = "id";

    private static final String PARAM_USU_ELIM = "usuEliminacion";

    private static final String PATAM_FEC_ELIM = "fecEliminacion";

    /**
     * @param dateIni
     * @param dateFin
     * @return List<Calendario>
     * @throws DataAccessException
     */
    public List<Calendario> buscarDiasFeriadosPorCriterio(Date dateIni, Date dateFin) throws DataAccessException {

        List<Calendario> calendarios = null;

        super.query =  super.em.createNamedQuery(Calendario.QUERY_FIND_BY_DATES)
                .setParameter(PARAM_FECHA_INICIO, dateIni, TemporalType.DATE)
                .setParameter(PARAM_FECHA_FIN, dateFin, TemporalType.DATE);

        if (!super.query.getResultList().isEmpty()) {
            calendarios = super.query.getResultList();
        }

        return calendarios;
    }

    /**
     * @param dateIni
     * @return List<Calendario>
     * @throws DataAccessException
     */
    public List<Calendario> buscarDiasFeriadosPorDia(Date dateIni) throws DataAccessException {
        List<Calendario> calendarios = null;

        super.query = super.em.createNamedQuery(Calendario.QUERY_FIND_A_DATE)
                .setParameter(PARAM_FECHA_INICIO, dateIni, TemporalType.DATE);

        if (!super.query.getResultList().isEmpty()) {
            calendarios = super.query.getResultList();
        }

        return calendarios;
    }
    
    /**
     * @param calendario
     * @throws Exception
     */
    public void actualizarCalendario(final Calendario calendario) throws DataAccessException {
        super.em.merge(calendario);
    }

    /**
     * @param calendarios
     * @throws Exception
     */
    public void actualizaCalendarios(final List<Calendario> calendarios) throws DataAccessException {
        for (Calendario calendario : calendarios) {
            super.em.createNamedQuery(Calendario.UPDATE_STATUS)
                    .setParameter(PATAM_FEC_ELIM, calendario.getCalFecEliminacion(), TemporalType.DATE)
                    .setParameter(PARAM_USU_ELIM, calendario.getCalUsuEliminacion())
                    .setParameter(PARAM_ID, calendario.getCalId())
                    .executeUpdate();
        }
    }

    /**
     * @param calendario
     * @throws DataAccessException
     */
    public void creaCalendario(final Calendario calendario) throws DataAccessException{
        super.em.persist(calendario);
    }        
}
