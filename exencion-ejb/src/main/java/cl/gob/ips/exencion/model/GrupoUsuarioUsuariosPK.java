package cl.gob.ips.exencion.model;

import java.io.Serializable;

public class GrupoUsuarioUsuariosPK implements Serializable {
    private Integer tbGrupoUsuarios1;
    private Integer tbUsuarios;

    public GrupoUsuarioUsuariosPK() {
    }

    public GrupoUsuarioUsuariosPK(Integer tbGrupoUsuarios1, Integer tbUsuarios) {
        this.tbGrupoUsuarios1 = tbGrupoUsuarios1;
        this.tbUsuarios = tbUsuarios;
    }

    public boolean equals(Object other) {
        if (other instanceof GrupoUsuarioUsuariosPK) {
            final GrupoUsuarioUsuariosPK otherGrupoUsuarioUsuariosPK = (GrupoUsuarioUsuariosPK) other;
            final boolean areEqual = true;
            return areEqual;
        }
        return false;
    }

    public int hashCode() {
        return super.hashCode();
    }

    public Integer getTbGrupoUsuarios1() {
        return tbGrupoUsuarios1;
    }

    public void setTbGrupoUsuarios1(Integer tbGrupoUsuarios1) {
        this.tbGrupoUsuarios1 = tbGrupoUsuarios1;
    }

    public Integer getTbUsuarios() {
        return tbUsuarios;
    }

    public void setTbUsuarios(Integer tbUsuarios) {
        this.tbUsuarios = tbUsuarios;
    }
}
