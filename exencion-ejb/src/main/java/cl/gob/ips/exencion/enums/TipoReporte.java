package cl.gob.ips.exencion.enums;

public enum TipoReporte {

    MOVIMIENTOS_RUN_TEMPLATE("templateArchivos/TEMPLATE_MOVIMIENTOS_RUN.xlsx"),
    MOVIMIENTOS_RUN("Movimientos_por_run.xlsx"),
    INFORME_GESTION_TEMPLATE("templateArchivos/TEMPLATE_INFORME_GESTION.xlsx"),
    INFORME_GESTION("Informe_gestion.xlsx"),
    INFORME_GESTION_NUEVOS_TEMPLATE("templateArchivos/TEMPLATE_INFORME_GESTION_NUEVOS.xlsx"),
    INFORME_GESTION_NUEVOS("Informe_gestion_nuevos.xlsx"),
    INFORME_GESTION_EXTINCION_TEMPLATE("templateArchivos/TEMPLATE_INFORME_GESTION_EXTINCION.xlsx"),
    INFORME_GESTION_EXTINCION("Informe_gestion_extincion.xlsx"),
    INFORME_GESTION_MANTIENEN_TEMPLATE("templateArchivos/TEMPLATE_INFORME_GESTION_MANTIENEN.xlsx"),
    INFORME_GESTION_MANTIENEN("Informe_gestion_mantienen.xlsx"),
    INFORME_GESTION_VER_TEMPLATE("templateArchivos/TEMPLATE_INFORME_GESTION_VER.xlsx"),
    INFORME_GESTION_VER("Informe_gestion_ver.xlsx"),
    INFORME_UNIVERSO_TEMPLATE("templateArchivos/TEMPLATE_INFORME_UNIVERSO.xlsx"),
    INFORME_UNIVERSO("Informe_universo.xlsx"),
    INFORME_UNIVERSO_VER_TEMPLATE("templateArchivos/TEMPLATE_INFORME_UNIVERSO_VER.xlsx"),
    INFORME_UNIVERSO_VER("Informe_universo_ver.xlsx"),
    INFORME_UNIVERSO_RUN_TEMPLATE("templateArchivos/TEMPLATE_INFORME_UNIVERSO_RUN.xlsx"),
    INFORME_UNIVERSO_RUN("Informe_universo_run.xlsx"),
    RESUMEN_REG_ARCHIVOS_ENTRADA_TEMPLATE("templateArchivos/TEMPLATE_RESUMEN_REG_ARCHIVOS_ENTRADA.xlsx"),
    RESUMEN_REG_ARCHIVOS_ENTRADA("Resumen_reg_archivos_entrada.xlsx"),
    RESULTADO_VALIDACION_ARCHIVOS_TEMPLATE("templateArchivos/TEMPLATE_REVISAR_RESULT_VALIDACION_ARCH.xlsx"),
    RESULTADO_VALIDACION_ARCHIVOS("Resultado_validacion_archivos_.xlsx"),
    EVENTOS_PROCESOS_SISTEMA_TEMPLATE("templateArchivos/TEMPLATE_EVENTOS_PROCESOS_SISTEMA.xlsx"),
    EVENTOS_PROCESOS_SISTEMA("Eventos_procesos_sistema.xlsx"),
    ATENCION_PENSIONADO_TEMPLATE("templateArchivos/TEMPLATE_ATENCION_PENSIONADO.xlsx"),
    ATENCION_PENSIONADO("Atencion_pensionado.xlsx"),
    NOMINA_BENEFICIARIO_TEMPLATE("templateArchivos/TEMPLATE_NOMINA_BENEFICIARIO.xlsx"),
    NOMINA_BENEFICIARIO("Nomina_beneficiario.xlsx"),
    NOMINA_CONSULTAPDI_TEMPLATE("templateArchivos/TEMPLATE_NOMINA_CONSULTA_PDI.xlsx"),
    NOMINA_CONSULTAPDI("Nomina_consulta_pdi.xlsx"),
    NOMINA_CONSULTA_POTBENEF_TEMPLATE("templateArchivos/TEMPLATE_NOMINA_CONSULTA_POT.xlsx"),
    NOMINA_CONSULTA_POTBENEF("Nomina_consulta_pot_benef.xlsx"),
    NOMINA_CONSULTA_SINPBS_TEMPLATE("templateArchivos/TEMPLATE_NOMINA_SIN_PBS.xlsx"),
    NOMINA_CONSULTA_SINPBS("Nomina_consulta_sin_pbs.xlsx"),
    NOMINA_CONSULTA_BENEFAPS_TEMPLATE("templateArchivos/TEMPLATE_NOMINA_BENEFAPS.xlsx"),
    NOMINA_CONSULTA_BENEFAPS("Nomina_consulta_benef_aps.xlsx"),
    NOMINA_CONSULTA_SINFICHA_TEMPLATE("templateArchivos/TEMPLATE_NOMINA_SINFICHA.xlsx"),
    NOMINA_CONSULTA_SINFICHA("Nomina_consulta_sin_ficha.xlsx"),
    NOMINA_CONSULTA_REBAJA_TEMPLATE("templateArchivos/TEMPLATE_NOMINA_REBAJA.xlsx"),
    NOMINA_CONSULTA_REBAJA("Nomina_consulta_rebaja.xlsx");

    private String codigo;

    private TipoReporte(String codigo) {
        this.codigo = codigo;
    }

    public static TipoReporte getInstance(String codigo) {
        for (TipoReporte itemEnum : values()) {
            if (itemEnum.getCodigo().equals(codigo)){
                return itemEnum;
            }
        }
        return null;
    }

    public String getCodigo() {
        return this.codigo;
    }

}
