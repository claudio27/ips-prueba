package cl.gob.ips.exencion.model;

import org.eclipse.persistence.internal.descriptors.PersistenceObject;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by HancacuraB on 21-07-2017
 */
@Entity
@Table(name = "TB_NOMINA_POT_BENEFICIARIO")
@SequenceGenerator(name = "nomPotBen_Id_Seq_Gen", sequenceName = "TB_PENSIONES_ID_SEQ_GEN", allocationSize = 50,
        initialValue = 50)
public class NominaPotencialesBenef implements Serializable {

    private static final long serialVersionUID = 6786873051748068689L;
    @Id
    @Column(name="NOM_ID" ,nullable=false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "nomPotBen_Id_Seq_Gen")
    private Integer nomId;
    @ManyToOne
    @JoinColumn(name= "NOM_PERIODO")
    private Periodo nomPeriodo;
    @ManyToOne
    @JoinColumn(name= "NOM_RUT_INSTITUCION")
    private Instituciones nomRutInstitucion;
    @Column(name="NOM_DV_INSTITUCION", length = 1)
    private String nomDvInstitucion;
    @Column(name="NOM_RUN_BENEFICIARIO")
    private Integer nomRunBeneficiario;
    @Column(name="NOM_DV_BENEFICIARIO", length = 1)
    private String nomDvBeneficiario;
    @Column(name="NOM_APELLIDO_PAT", length = 100)
    private String nomApellidoPat;
    @Column(name="NOM_APELLIDO_MAT", length = 100)
    private String nomApellidoMat;
    @Column(name="NOM_NOMBRES", length = 100)
    private String nomNombres;
    @Column(name="NOM_TIPO_PENSION")
    private Integer nomTipoPension;
    @Column(name="NOM_REQ_NO_ACREDITA", length = 2)
    private String nomReqNoAcredita;

    public NominaPotencialesBenef(){
    }

    public NominaPotencialesBenef(Integer nomId, Periodo nomPeriodo, Instituciones nomRutInstitucion, String nomDvInstitucion, Integer nomRunBeneficiario, String nomDvBeneficiario, String nomApellidoPat, String nomApellidoMat, String nomNombres, Integer nomTipoPension, String nomReqNoAcredita) {
        this.nomId = nomId;
        this.nomPeriodo = nomPeriodo;
        this.nomRutInstitucion = nomRutInstitucion;
        this.nomDvInstitucion = nomDvInstitucion;
        this.nomRunBeneficiario = nomRunBeneficiario;
        this.nomDvBeneficiario = nomDvBeneficiario;
        this.nomApellidoPat = nomApellidoPat;
        this.nomApellidoMat = nomApellidoMat;
        this.nomNombres = nomNombres;
        this.nomTipoPension = nomTipoPension;
        this.nomReqNoAcredita = nomReqNoAcredita;
    }

    public Integer getNomId() {
        return nomId;
    }

    public void setNomId(Integer nomId) {
        this.nomId = nomId;
    }

    public Periodo getNomPeriodo() {
        return nomPeriodo;
    }

    public void setNomPeriodo(Periodo nomPeriodo) {
        this.nomPeriodo = nomPeriodo;
    }

    public Instituciones getNomRutInstitucion() {
        return nomRutInstitucion;
    }

    public void setNomRutInstitucion(Instituciones nomRutInstitucion) {
        this.nomRutInstitucion = nomRutInstitucion;
    }

    public String getNomDvInstitucion() {
        return nomDvInstitucion;
    }

    public void setNomDvInstitucion(String nomDvInstitucion) {
        this.nomDvInstitucion = nomDvInstitucion;
    }

    public Integer getNomRunBeneficiario() {
        return nomRunBeneficiario;
    }

    public void setNomRunBeneficiario(Integer nomRunBeneficiario) {
        this.nomRunBeneficiario = nomRunBeneficiario;
    }

    public String getNomDvBeneficiario() {
        return nomDvBeneficiario;
    }

    public void setNomDvBeneficiario(String nomDvBeneficiario) {
        this.nomDvBeneficiario = nomDvBeneficiario;
    }

    public String getNomApellidoPat() {
        return nomApellidoPat;
    }

    public void setNomApellidoPat(String nomApellidoPat) {
        this.nomApellidoPat = nomApellidoPat;
    }

    public String getNomApellidoMat() {
        return nomApellidoMat;
    }

    public void setNomApellidoMat(String nomApellidoMat) {
        this.nomApellidoMat = nomApellidoMat;
    }

    public String getNomNombres() {
        return nomNombres;
    }

    public void setNomNombres(String nomNombres) {
        this.nomNombres = nomNombres;
    }

    public Integer getNomTipoPension() {
        return nomTipoPension;
    }

    public void setNomTipoPension(Integer nomTipoPension) {
        this.nomTipoPension = nomTipoPension;
    }

    public String getNomReqNoAcredita() {
        return nomReqNoAcredita;
    }

    public void setNomReqNoAcredita(String nomReqNoAcredita) {
        this.nomReqNoAcredita = nomReqNoAcredita;
    }

}
