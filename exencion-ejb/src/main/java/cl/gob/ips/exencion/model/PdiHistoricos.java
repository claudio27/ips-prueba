package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQueries({ @NamedQuery(name = "PdiHistoricos.findAll", query = "select o from PdiHistoricos o") })
@Table(name = "TB_PDI_HIS")
public class PdiHistoricos implements Serializable {
    private static final long serialVersionUID = -8564951083668721824L;
    @Column(name = "PDI_DESDE_1", length = 8)
    private String pdiDesde1;
    @Column(name = "PDI_DESDE_2", length = 8)
    private String pdiDesde2;
    @Column(name = "PDI_ELIMINADO")
    private Integer pdiEliminado;
    @Temporal(TemporalType.DATE)
    @Column(name = "PDI_FEC_ELIMINADO")
    private Date pdiFecEliminado;
    @Column(name = "PDI_FECHA_NAC", nullable = false, length = 8)
    private String pdiFechaNac;
    @Column(name = "PDI_HASTA_1", length = 8)
    private String pdiHasta1;
    @Column(name = "PDI_HASTA_2", length = 8)
    private String pdiHasta2;
    @Id
    @Column(name = "PDI_ID", nullable = false)
    private Long pdiId;
    @Column(name = "PDI_MATERNO", length = 20)
    private String pdiMaterno;
    @Column(name = "PDI_NOMBRES", length = 30)
    private String pdiNombres;
    @Column(name = "PDI_PATERNO", length = 20)
    private String pdiPaterno;
    @Column(name = "PDI_RESPUESTA_1")
    private Integer pdiRespuesta1;
    @Column(name = "PDI_RESPUESTA_2")
    private Integer pdiRespuesta2;
    @Column(name = "PDI_RUN", nullable = false)
    private Integer pdiRun;
    @Column(name = "PDI_SEXO", length = 1)
    private String pdiSexo;
    @Column(name = "PDI_USU_ELIMINADO", length = 20)
    private String pdiUsuEliminado;
    @ManyToOne
    @JoinColumn(name = "PDI_ARC_ID")
    private ArchivosCargaPeriodo archivosCargaPeriodo2;

    public PdiHistoricos() {
    }

    public PdiHistoricos(ArchivosCargaPeriodo archivosCargaPeriodo2, String pdiDesde1, String pdiDesde2,
                         Integer pdiEliminado, Date pdiFecEliminado, String pdiFechaNac, String pdiHasta1,
                         String pdiHasta2, Long pdiId, String pdiMaterno, String pdiNombres, String pdiPaterno,
                         Integer pdiRespuesta1, Integer pdiRespuesta2, Integer pdiRun, String pdiSexo,
                         String pdiUsuEliminado) {
        this.archivosCargaPeriodo2 = archivosCargaPeriodo2;
        this.pdiDesde1 = pdiDesde1;
        this.pdiDesde2 = pdiDesde2;
        this.pdiEliminado = pdiEliminado;
        this.pdiFecEliminado = pdiFecEliminado;
        this.pdiFechaNac = pdiFechaNac;
        this.pdiHasta1 = pdiHasta1;
        this.pdiHasta2 = pdiHasta2;
        this.pdiId = pdiId;
        this.pdiMaterno = pdiMaterno;
        this.pdiNombres = pdiNombres;
        this.pdiPaterno = pdiPaterno;
        this.pdiRespuesta1 = pdiRespuesta1;
        this.pdiRespuesta2 = pdiRespuesta2;
        this.pdiRun = pdiRun;
        this.pdiSexo = pdiSexo;
        this.pdiUsuEliminado = pdiUsuEliminado;
    }


    public String getPdiDesde1() {
        return pdiDesde1;
    }

    public void setPdiDesde1(String pdiDesde1) {
        this.pdiDesde1 = pdiDesde1;
    }

    public String getPdiDesde2() {
        return pdiDesde2;
    }

    public void setPdiDesde2(String pdiDesde2) {
        this.pdiDesde2 = pdiDesde2;
    }

    public Integer getPdiEliminado() {
        return pdiEliminado;
    }

    public void setPdiEliminado(Integer pdiEliminado) {
        this.pdiEliminado = pdiEliminado;
    }

    public Date getPdiFecEliminado() {
        return pdiFecEliminado;
    }

    public void setPdiFecEliminado(Date pdiFecEliminado) {
        this.pdiFecEliminado = pdiFecEliminado;
    }

    public String getPdiFechaNac() {
        return pdiFechaNac;
    }

    public void setPdiFechaNac(String pdiFechaNac) {
        this.pdiFechaNac = pdiFechaNac;
    }

    public String getPdiHasta1() {
        return pdiHasta1;
    }

    public void setPdiHasta1(String pdiHasta1) {
        this.pdiHasta1 = pdiHasta1;
    }

    public String getPdiHasta2() {
        return pdiHasta2;
    }

    public void setPdiHasta2(String pdiHasta2) {
        this.pdiHasta2 = pdiHasta2;
    }

    public Long getPdiId() {
        return pdiId;
    }

    public void setPdiId(Long pdiId) {
        this.pdiId = pdiId;
    }

    public String getPdiMaterno() {
        return pdiMaterno;
    }

    public void setPdiMaterno(String pdiMaterno) {
        this.pdiMaterno = pdiMaterno;
    }

    public String getPdiNombres() {
        return pdiNombres;
    }

    public void setPdiNombres(String pdiNombres) {
        this.pdiNombres = pdiNombres;
    }

    public String getPdiPaterno() {
        return pdiPaterno;
    }

    public void setPdiPaterno(String pdiPaterno) {
        this.pdiPaterno = pdiPaterno;
    }

    public Integer getPdiRespuesta1() {
        return pdiRespuesta1;
    }

    public void setPdiRespuesta1(Integer pdiRespuesta1) {
        this.pdiRespuesta1 = pdiRespuesta1;
    }

    public Integer getPdiRespuesta2() {
        return pdiRespuesta2;
    }

    public void setPdiRespuesta2(Integer pdiRespuesta2) {
        this.pdiRespuesta2 = pdiRespuesta2;
    }

    public Integer getPdiRun() {
        return pdiRun;
    }

    public void setPdiRun(Integer pdiRun) {
        this.pdiRun = pdiRun;
    }

    public String getPdiSexo() {
        return pdiSexo;
    }

    public void setPdiSexo(String pdiSexo) {
        this.pdiSexo = pdiSexo;
    }

    public String getPdiUsuEliminado() {
        return pdiUsuEliminado;
    }

    public void setPdiUsuEliminado(String pdiUsuEliminado) {
        this.pdiUsuEliminado = pdiUsuEliminado;
    }

    public ArchivosCargaPeriodo getArchivosCargaPeriodo2() {
        return archivosCargaPeriodo2;
    }

    public void setArchivosCargaPeriodo2(ArchivosCargaPeriodo archivosCargaPeriodo2) {
        this.archivosCargaPeriodo2 = archivosCargaPeriodo2;
    }
}
