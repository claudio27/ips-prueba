package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_PROCEDENCIA_INST_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_PROCEDENCIA_INST_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "ProcedenciaInst.findAll", query = "select o from ProcedenciaInst o") })
@Table(name = "TB_PR_PROCEDENCIA_INST")
@SequenceGenerator(name = "ProcedenciaInst_Id_Seq_Gen", sequenceName = "TB_PR_PROCEDENCIA_INST_ID_SEQ_GEN",
                   allocationSize = 50, initialValue = 50)
public class ProcedenciaInst implements Serializable {
    private static final long serialVersionUID = -3854063235856370196L;
    @Id
    @Column(name = "PROC_INST_COD", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ProcedenciaInst_Id_Seq_Gen")
    private Integer procInstCod;
    @Column(name = "PROC_INST_DESCRIPCION", nullable = false, length = 20)
    private String procInstDescripcion;
    @OneToMany(mappedBy = "tbPrProcedenciaInst", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Pensiones> tbPensionesList2;

    public ProcedenciaInst() {
    }

    public ProcedenciaInst(Integer procInstCod, String procInstDescripcion) {
        this.procInstCod = procInstCod;
        this.procInstDescripcion = procInstDescripcion;
    }

    public Integer getProcInstCod() {
        return procInstCod;
    }

    public String getProcInstDescripcion() {
        return procInstDescripcion;
    }

    public void setProcInstDescripcion(String procInstDescripcion) {
        this.procInstDescripcion = procInstDescripcion;
    }

    public List<Pensiones> getTbPensionesList2() {
        return tbPensionesList2;
    }

    public void setTbPensionesList2(List<Pensiones> tbPensionesList2) {
        this.tbPensionesList2 = tbPensionesList2;
    }

    public Pensiones addPensiones(Pensiones pensiones) {
        getTbPensionesList2().add(pensiones);
        pensiones.setTbPrProcedenciaInst(this);
        return pensiones;
    }

    public Pensiones removePensiones(Pensiones pensiones) {
        getTbPensionesList2().remove(pensiones);
        pensiones.setTbPrProcedenciaInst(null);
        return pensiones;
    }
}
