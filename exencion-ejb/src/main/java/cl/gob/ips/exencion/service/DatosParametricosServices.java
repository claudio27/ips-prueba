package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.DatosParametricosDao;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.GruposArchivo;
import cl.gob.ips.exencion.model.NivelResponsabilidad;
import cl.gob.ips.exencion.model.TipoInstitucion;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by rodrigo.reyesco@gmail.com on 06-10-2016.
 */
@Stateless
@LocalBean
public class DatosParametricosServices {

    @Inject
    DatosParametricosDao dao;

    /**
     * servicio de negocio que retorna una lista de TB_PR_TIPO_INSTITUCION desde la base de datos,
     * se implementa este metodo para incluir alguna logica de negicio independiente de la base de datos.
     * @return List<TipoInstitucion>
     */
    public List<TipoInstitucion> getTiposInstitucion() throws DataAccessException {
        return dao.getTiposInstitucion();
    }

    /**
     * servicio de negocio que retorna una lista de TB_PR_GRUPOS_ARCHIVO desde la base de datos,
     * se implementa este metodo para incluir alguna logica de negicio independiente de la base de datos.
     * @return List<GruposArchivo>
     */
    public List<GruposArchivo> getGruposArchivo() throws DataAccessException {
        return dao.getGruposArchivo();
    }

    /**
     * servicio de negocio que retorna una lista de TB_PR_NIVEL_RESPONSABILIDAD desde la base de datos,
     * se implementa este metodo para incluir alguna logica de negicio independiente de la base de datos.
     * @return List<NivelResponsabilidad>
     * @throws DataAccessException
     */
    public List<NivelResponsabilidad> getNivelesResponsabilidad() throws DataAccessException {
        return dao.getNivelesResponsabilidad();
    }
}
