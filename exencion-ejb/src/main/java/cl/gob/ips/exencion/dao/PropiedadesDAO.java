package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.dto.PropiedadesDTO;
import cl.gob.ips.exencion.model.Propiedades;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Created by practicante on 16-10-17.
 */
@Stateless
@LocalBean
public class PropiedadesDAO extends BaseDAO{


    public Propiedades obtenerPropiedadesFTP(String clave){
        Propiedades propiedades = null;

        super.query = super.em.createNativeQuery("SELECT *\n" +
                "FROM TB_PR_PROPIEDADES\n" +
                "WHERE PROP_CLAVE = ?clave", Propiedades.class)
                .setParameter("clave", clave);

        if (!super.query.getResultList().isEmpty()){
            propiedades = (Propiedades) super.query.getSingleResult();
        }

        return propiedades;

    }

}
