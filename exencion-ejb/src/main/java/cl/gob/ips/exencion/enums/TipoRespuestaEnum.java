package cl.gob.ips.exencion.enums;

public enum TipoRespuestaEnum {

    FALLECIDO(1),//PENVIVO
    BENIFICIARIO_EXENCION(2),//PEN_BENEFICIARIO
    BENEFICIARIO_PILAR_SOLIDARIO(3),//PEN_SPS
    NO_ESTA_PENSIONADO(4),//NO TENGO IDEA
    NO_CUMPLE_RESIDENCIA(5),//PENRESIDENCIA
    NO_FOCALIZA(6);//PEN_FOCALIZACION

    private Integer codigo;

    private TipoRespuestaEnum(Integer codigo) {
        this.codigo = codigo;
    }

    public static TipoRespuestaEnum getInstance(Integer codigo) {
        for (TipoRespuestaEnum itemEnum : values()) {
            if (itemEnum.getCodigo().equals(codigo)){
                return itemEnum;
            }
        }
        return null;
    }

    public Integer getCodigo() {
        return this.codigo;
    }

}
