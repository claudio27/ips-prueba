package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = Instituciones.QUERY_FIND_ALL, query = "select o from Instituciones o"),
        @NamedQuery(name = Instituciones.QUERY_FIND_BY_RUT, query = "select o from Instituciones o WHERE o.instRut = :rutInstitucion"),
        @NamedQuery(name = Instituciones.QUERY_FIND_BY_ESTADO, query = "select o from Instituciones o WHERE o.tbPrEstInstitucion.estInstCod = :estadoInstitucion"),
        @NamedQuery(name = Instituciones.UPDATE_STATUS, query = "UPDATE Instituciones o SET o.tbPrEstInstitucion = :estadoInstitucion WHERE o.instRut = :rutInstitucion"),
        @NamedQuery(name = Instituciones.QUERY_FIND_BY_SEARCH_CRITERIA,
                query = "SELECT o FROM Instituciones o " +
                        "WHERE o.tbPrEstInstitucion = :estadoInstitucion " +
                        "AND (" +
                            "((:rutInstitucion is null) OR (o.instRut = :rutInstitucion)) AND" +
                            "((:nombreInstitucion is null) OR (o.instNombre LIKE :nombreInstitucion)) AND" +
                            "((:tipoInstitucion = 0) OR (o.tbPrTipoInstitucion.tipInstCod = :tipoInstitucion))" +
                        ")")
})
@Table(name = "TB_PR_INSTITUCIONES")
@Cacheable(false)
public class Instituciones implements Serializable {
    private static final long serialVersionUID = -7802954763037345242L;

    public static final String QUERY_FIND_ALL = "Instituciones.findAll";
    public static final String QUERY_FIND_BY_RUT = "Instituciones.findByRut";
    public static final String QUERY_FIND_BY_SEARCH_CRITERIA = "Instituciones.findBySearchCriteria";
    public static final String UPDATE_STATUS = "Instituciones.updateStatus";
    public static final String QUERY_FIND_BY_ESTADO = "Instituciones.findByEstado";
    public static final String QUERY_FIND_NOMBRE_INST = "Instituciones.findNombreByRut";

    @Column(name = "INST_COD_INSTITUCION")
    private Integer instCodInstitucion;
    @Column(name = "INST_DV", nullable = false, length = 1)
    private String instDv;
    @Column(name = "INST_NOMBRE", nullable = false, length = 100)
    private String instNombre;
    @Column(name = "INST_NOMBRE_CORTO", nullable = false, length = 25)
    private String instNombreCorto;
    @Id
    @Column(name = "INST_RUT", nullable = false)
    private Long instRut;
    @Column(name = "INST_TIP_INST_EXTENCION", nullable = false, length = 9)
    private String instTipInstExtencion;
    @ManyToOne
    @JoinColumn(name = "INST_EST_INST_COD")
    private EstadoInstitucion tbPrEstInstitucion;
    @ManyToOne
    @JoinColumn(name = "INST_TIP_INST_COD")
    private TipoInstitucion tbPrTipoInstitucion;
    @OneToMany(mappedBy = "tbPrInstituciones", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Pensiones> tbPensionesList;

    //bi-directional many-to-one association to TbNominaBeneficiario
    @OneToMany(mappedBy="Instituciones")
    private List<NominaBeneficiario> nominaBeneficiarios;

    //bi-directional many-to-one association to TbNominaBphSinPb
    @OneToMany(mappedBy="Instituciones")
    private List<NominaBphSinPbs> tbNominaBphSinPbs;

    //bi-directional many-to-one association to TbNominaConsultaPdi
    @OneToMany(mappedBy="Instituciones")
    private List<NominaConsultaPdi> tbNominaConsultaPdis;

    //bi-directional many-to-one association to TbNominaPenSinFicha
    @OneToMany(mappedBy="Instituciones")
    private List<NominaPenSinFicha> tbNominaPenSinFichas;

    //bi-directional many-to-one association to TbNominaPotencialesAp
    @OneToMany(mappedBy="Instituciones")
    private List<NominaPotencialesAps> tbNominaPotencialesAps;

    //bi-directional many-to-one association to TbNominaPotBeneficiario
    @OneToMany(mappedBy="Instituciones")
    private List<NominaPotBeneficiario> tbNominaPotBeneficiarios;

    //bi-directional many-to-one association to TbNominaRebaja
    @OneToMany(mappedBy="Instituciones")
    private List<NominaRebaja> tbNominaRebajas;

    public Instituciones() {
    }

    public Instituciones(Long instRut, String instDv, String instNombre, TipoInstitucion tbPrTipoInstitucion) {
        this.instRut = instRut;
        this.instDv = instDv;
        this.instNombre = instNombre;
        this.tbPrTipoInstitucion = tbPrTipoInstitucion;
    }

    public Instituciones(Integer instCodInstitucion, String instDv, EstadoInstitucion tbPrEstInstitucion,
                         String instNombre, String instNombreCorto, Long instRut, TipoInstitucion tbPrTipoInstitucion,
                         String instTipInstExtencion) {
        this.instCodInstitucion = instCodInstitucion;
        this.instDv = instDv;
        this.tbPrEstInstitucion = tbPrEstInstitucion;
        this.instNombre = instNombre;
        this.instNombreCorto = instNombreCorto;
        this.instRut = instRut;
        this.tbPrTipoInstitucion = tbPrTipoInstitucion;
        this.instTipInstExtencion = instTipInstExtencion;
    }

    public Instituciones(TipoInstitucion tbPrTipoInstitucion) {
        this.tbPrTipoInstitucion = tbPrTipoInstitucion;
    }

    public Integer getInstCodInstitucion() {
        return instCodInstitucion;
    }

    public void setInstCodInstitucion(Integer instCodInstitucion) {
        this.instCodInstitucion = instCodInstitucion;
    }

    public String getInstDv() {
        return instDv;
    }

    public void setInstDv(String instDv) {
        this.instDv = instDv;
    }


    public String getInstNombre() {
        return instNombre;
    }

    public void setInstNombre(String instNombre) {
        this.instNombre = instNombre;
    }

    public String getInstNombreCorto() {
        return instNombreCorto;
    }

    public void setInstNombreCorto(String instNombreCorto) {
        this.instNombreCorto = instNombreCorto;
    }

    public Long getInstRut() {
        return instRut;
    }

    public void setInstRut(Long instRut) {
        this.instRut = instRut;
    }


    public String getInstTipInstExtencion() {
        return instTipInstExtencion;
    }

    public void setInstTipInstExtencion(String instTipInstExtencion) {
        this.instTipInstExtencion = instTipInstExtencion;
    }

    public EstadoInstitucion getTbPrEstInstitucion() {
        return tbPrEstInstitucion;
    }

    public void setTbPrEstInstitucion(EstadoInstitucion tbPrEstInstitucion) {
        this.tbPrEstInstitucion = tbPrEstInstitucion;
    }

    public TipoInstitucion getTbPrTipoInstitucion() {
        return tbPrTipoInstitucion;
    }

    public void setTbPrTipoInstitucion(TipoInstitucion tbPrTipoInstitucion) {
        this.tbPrTipoInstitucion = tbPrTipoInstitucion;
    }

    public List<Pensiones> getTbPensionesList() {
        return tbPensionesList;
    }

    public void setTbPensionesList(List<Pensiones> pensionesList) {
        this.tbPensionesList = pensionesList;
    }

    public Pensiones addPensiones(Pensiones pensiones) {
        getTbPensionesList().add(pensiones);
        pensiones.setTbPrInstituciones(this);
        return pensiones;
    }

    public Pensiones removePensiones(Pensiones pensiones) {
        getTbPensionesList().remove(pensiones);
        pensiones.setTbPrInstituciones(null);
        return pensiones;
    }

    public List<NominaBeneficiario> getNominaBeneficiarios() {
        return this.nominaBeneficiarios;
    }

    public void setTbNominaBeneficiarios(List<NominaBeneficiario> tbNominaBeneficiarios) {
        this.nominaBeneficiarios = tbNominaBeneficiarios;
    }

    public NominaBeneficiario addNominaBeneficiario(NominaBeneficiario nominaBeneficiario) {
        getNominaBeneficiarios().add(nominaBeneficiario);
        nominaBeneficiario.setInstituciones(this);

        return nominaBeneficiario;
    }

    public NominaBeneficiario removeNominaBeneficiario(NominaBeneficiario nominaBeneficiario) {
        getNominaBeneficiarios().remove(nominaBeneficiario);
        nominaBeneficiario.setInstituciones(null);

        return nominaBeneficiario;
    }

    public List<NominaBphSinPbs> getTbNominaBphSinPbs() {
        return this.tbNominaBphSinPbs;
    }

    public void setTbNominaBphSinPbs(List<NominaBphSinPbs> tbNominaBphSinPbs) {
        this.tbNominaBphSinPbs = tbNominaBphSinPbs;
    }

    public NominaBphSinPbs addTbNominaBphSinPb(NominaBphSinPbs tbNominaBphSinPb) {
        getTbNominaBphSinPbs().add(tbNominaBphSinPb);
        tbNominaBphSinPb.setInstituciones(this);

        return tbNominaBphSinPb;
    }

    public NominaBphSinPbs removeTbNominaBphSinPb(NominaBphSinPbs tbNominaBphSinPb) {
        getTbNominaBphSinPbs().remove(tbNominaBphSinPb);
        tbNominaBphSinPb.setInstituciones(null);

        return tbNominaBphSinPb;
    }

    public List<NominaConsultaPdi> getTbNominaConsultaPdis() {
        return this.tbNominaConsultaPdis;
    }

    public void setTbNominaConsultaPdis(List<NominaConsultaPdi> tbNominaConsultaPdis) {
        this.tbNominaConsultaPdis = tbNominaConsultaPdis;
    }

    public NominaConsultaPdi addTbNominaConsultaPdi(NominaConsultaPdi tbNominaConsultaPdi) {
        getTbNominaConsultaPdis().add(tbNominaConsultaPdi);
        tbNominaConsultaPdi.setInstituciones(this);

        return tbNominaConsultaPdi;
    }

    public NominaConsultaPdi removeTbNominaConsultaPdi(NominaConsultaPdi tbNominaConsultaPdi) {
        getTbNominaConsultaPdis().remove(tbNominaConsultaPdi);
        tbNominaConsultaPdi.setInstituciones(null);

        return tbNominaConsultaPdi;
    }

    public List<NominaPenSinFicha> getTbNominaPenSinFichas() {
        return this.tbNominaPenSinFichas;
    }

    public void setTbNominaPenSinFichas(List<NominaPenSinFicha> tbNominaPenSinFichas) {
        this.tbNominaPenSinFichas = tbNominaPenSinFichas;
    }

    public NominaPenSinFicha addTbNominaPenSinFicha(NominaPenSinFicha tbNominaPenSinFicha) {
        getTbNominaPenSinFichas().add(tbNominaPenSinFicha);
        tbNominaPenSinFicha.setInstituciones(this);

        return tbNominaPenSinFicha;
    }

    public NominaPenSinFicha removeTbNominaPenSinFicha(NominaPenSinFicha tbNominaPenSinFicha) {
        getTbNominaPenSinFichas().remove(tbNominaPenSinFicha);
        tbNominaPenSinFicha.setInstituciones(null);

        return tbNominaPenSinFicha;
    }

    public List<NominaPotencialesAps> getTbNominaPotencialesAps() {
        return this.tbNominaPotencialesAps;
    }

    public void setTbNominaPotencialesAps(List<NominaPotencialesAps> tbNominaPotencialesAps) {
        this.tbNominaPotencialesAps = tbNominaPotencialesAps;
    }

    public NominaPotencialesAps addTbNominaPotencialesAp(NominaPotencialesAps tbNominaPotencialesAp) {
        getTbNominaPotencialesAps().add(tbNominaPotencialesAp);
        tbNominaPotencialesAp.setInstituciones(this);

        return tbNominaPotencialesAp;
    }

    public NominaPotencialesAps removeTbNominaPotencialesAp(NominaPotencialesAps tbNominaPotencialesAp) {
        getTbNominaPotencialesAps().remove(tbNominaPotencialesAp);
        tbNominaPotencialesAp.setInstituciones(null);

        return tbNominaPotencialesAp;
    }

    public List<NominaPotBeneficiario> getTbNominaPotBeneficiarios() {
        return this.tbNominaPotBeneficiarios;
    }

    public void setTbNominaPotBeneficiarios(List<NominaPotBeneficiario> tbNominaPotBeneficiarios) {
        this.tbNominaPotBeneficiarios = tbNominaPotBeneficiarios;
    }

    public NominaPotBeneficiario addTbNominaPotBeneficiario(NominaPotBeneficiario tbNominaPotBeneficiario) {
        getTbNominaPotBeneficiarios().add(tbNominaPotBeneficiario);
        tbNominaPotBeneficiario.setInstituciones(this);

        return tbNominaPotBeneficiario;
    }

    public NominaPotBeneficiario removeTbNominaPotBeneficiario(NominaPotBeneficiario tbNominaPotBeneficiario) {
        getTbNominaPotBeneficiarios().remove(tbNominaPotBeneficiario);
        tbNominaPotBeneficiario.setInstituciones(null);

        return tbNominaPotBeneficiario;
    }

    public List<NominaRebaja> getTbNominaRebajas() {
        return this.tbNominaRebajas;
    }

    public void setTbNominaRebajas(List<NominaRebaja> tbNominaRebajas) {
        this.tbNominaRebajas = tbNominaRebajas;
    }

    public NominaRebaja addTbNominaRebaja(NominaRebaja tbNominaRebaja) {
        getTbNominaRebajas().add(tbNominaRebaja);
        tbNominaRebaja.setInstituciones(this);

        return tbNominaRebaja;
    }

    public NominaRebaja removeTbNominaRebaja(NominaRebaja tbNominaRebaja) {
        getTbNominaRebajas().remove(tbNominaRebaja);
        tbNominaRebaja.setInstituciones(null);

        return tbNominaRebaja;
    }
}
