package cl.gob.ips.exencion.dao;


import cl.gob.ips.exencion.dto.InformeGestionDTO;
import cl.gob.ips.exencion.model.InformeGestionExtinto;
import cl.gob.ips.exencion.model.InformeGestionMantienen;
import cl.gob.ips.exencion.model.InformeGestionNuevos;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.model.*;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HancacuraB on 26-07-2017.
 */

@Stateless
@LocalBean
public class InformeGestionDAO extends BaseDAO {

    public List<InformeGestionDTO> obtenerInformeGestion(Integer periodoDesde, Integer periodoHasta, Integer codTipoPeriodo, Integer codPresentadoPor) throws DataAccessException, IpsBussinesException{

        String query;
        //query="SELECT  INF_PERIODO,INF_IPS,INF_ISL,INF_BPH,INF_IPS + INF_ISL + INF_BPH INF_TOTAL_INTERNOS,INF_AFP,INF_CSV,INF_MUTUAL, INF_AFP + INF_CSV + INF_MUTUAL INF_TOTAL_EXTERNOS, INF_IPS + INF_ISL + INF_BPH + INF_AFP + INF_CSV + INF_MUTUAL INT_TOTAL, INF_NUEVOS,INF_EXTINCIONES,INF_MANTIENEN\n" +
        //      "FROM (\n" +
        query="     SELECT  PDO.PER_PERIODO                                                                                             INF_PERIODO\n" +
                "       ,COUNT(#DISTINCT#(CASE WHEN PEN1.PEN_BENEFICIARIO  = 1 AND INS.INST_TIP_INST_COD = 1 THEN PEN1.PEN_RUN END)) INF_IPS\n" +
                "       ,COUNT(#DISTINCT#(CASE WHEN PEN1.PEN_BENEFICIARIO  = 1 AND INS.INST_TIP_INST_COD = 5 THEN PEN1.PEN_RUN END)) INF_ISL\n" +
                "       ,COUNT(#DISTINCT#(CASE WHEN PEN1.PEN_BENEFICIARIO  = 1 AND INS.INST_TIP_INST_COD = 6 THEN PEN1.PEN_RUN END)) INF_BPH\n" +
                "       ,COUNT(#DISTINCT#(CASE WHEN PEN1.PEN_BENEFICIARIO  = 1 AND INS.INST_TIP_INST_COD = 2 THEN PEN1.PEN_RUN END)) INF_AFP\n" +
                "       ,COUNT(#DISTINCT#(CASE WHEN PEN1.PEN_BENEFICIARIO  = 1 AND INS.INST_TIP_INST_COD = 3 THEN PEN1.PEN_RUN END)) INF_CSV\n" +
                "       ,COUNT(#DISTINCT#(CASE WHEN PEN1.PEN_BENEFICIARIO  = 1 AND INS.INST_TIP_INST_COD = 4 THEN PEN1.PEN_RUN END)) INF_MUTUAL\n" +
                "       ,COUNT(#DISTINCT#(CASE WHEN PEN1.PEN_BENEFICIARIO  = 1 AND PEN2.PEN_BENEFICIARIO = 0 THEN PEN1.PEN_RUN END)) INF_NUEVOS\n" +
                "       ,COUNT(#DISTINCT#(CASE WHEN PEN1.PEN_BENEFICIARIO  = 0 AND PEN2.PEN_BENEFICIARIO = 1 THEN PEN1.PEN_RUN END)) INF_EXTINCIONES\n" +
                "       ,COUNT(#DISTINCT#(CASE WHEN PEN1.PEN_BENEFICIARIO  = 1 AND PEN2.PEN_BENEFICIARIO = 1 THEN PEN1.PEN_RUN END)) INF_MANTIENEN\n" +
                "     FROM      TB_PERIODO  PDO\n" +
                "     LEFT JOIN TB_PENSIONADOS      PEN1  ON (   PEN1.#PERIODO#    = PDO.PER_PERIODO)\n" +
                "     LEFT JOIN TB_PENSIONES        PES   ON (   PES.PNS_PERIODO   = PDO.PER_PERIODO" +
                "                                            AND PES.PNS_RUN       = PEN1.PEN_RUN )\n" +
                "     LEFT JOIN TB_PR_INSTITUCIONES INS   ON (   INS.INST_RUT      = PES.PNS_RUT_INSTITUCION )\n" +
                "     LEFT JOIN TB_PENSIONADOS      PEN2  ON (   PEN2.PEN_PERIODO  = TO_NUMBER(TO_CHAR(ADD_MONTHS(TO_DATE(PEN1.PEN_PERIODO ,'YYYYMM'), -1),'YYYYMM'))\n" +
                "                                            AND PEN2.PEN_RUN      = PEN1.PEN_RUN ) \n" +
                "     WHERE  PDO.PER_PERIODO BETWEEN ? AND ? \n" +
                "     GROUP BY PDO.PER_PERIODO\n" +
                "     ORDER BY PDO.PER_PERIODO DESC";
        //") INFORME";

        if(codTipoPeriodo==0){
            query = query.replace("#PERIODO#","PEN_PERIODO");
        }else{
            query = query.replace("#PERIODO#","PEN_PERIODO_BONIFICACION");
        }
        if(codPresentadoPor==0){
            query = query.replace("#DISTINCT#","DISTINCT");
        }else{
            query = query.replace("#DISTINCT#","");
        }
        List<InformeGestionDTO> listaInformrGestion = null;
        List<InformeGestion> listaInformrGestion2;
        listaInformrGestion2 = em.createNativeQuery(query,InformeGestion.class)
                .setParameter(1, periodoDesde)
                .setParameter(2, periodoHasta)
                .getResultList();

        if (!listaInformrGestion2.isEmpty()){
            listaInformrGestion = new ArrayList<>();
            for(InformeGestion infGestion: listaInformrGestion2){
                InformeGestionDTO infGestionDTO = new InformeGestionDTO();
                infGestionDTO.setPeriodo(infGestion.getPeriodo().toString());
                infGestionDTO.setIps(infGestion.getIps());
                infGestionDTO.setIsl(infGestion.getIsl());
                infGestionDTO.setBph(infGestion.getBph());
                infGestionDTO.setTotalInternos(infGestion.getIps() + infGestion.getIsl() + infGestion.getBph());
                infGestionDTO.setAfp(infGestion.getAfp());
                infGestionDTO.setCsv(infGestion.getCsv());
                infGestionDTO.setMutual(infGestion.getMutual());
                infGestionDTO.setTotalExternos(infGestion.getAfp() + infGestion.getCsv() + infGestion.getMutual());
                infGestionDTO.setTotal(infGestionDTO.getTotalExternos() + infGestionDTO.getTotalInternos());
                infGestionDTO.setNuevos(infGestion.getNuevos());
                infGestionDTO.setExtinciones(infGestion.getExtinciones());
                infGestionDTO.setMantienen(infGestion.getMantienen());
                listaInformrGestion.add(infGestionDTO);
            }

        }
        return listaInformrGestion;



/*
        StoredProcedureQuery query = super.em.createStoredProcedureQuery("PRC_GENERAR_INFORME_GESTION", InformeGestion.class);
        query.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(4, Integer.class, ParameterMode.IN);
        query.setParameter(1, periodoDesde);
        query.setParameter(2, periodoHasta);
        query.setParameter(3, codTipoPeriodo);
        query.setParameter(4, codPresentadoPor);

        boolean status = query.execute();

        List<InformeGestionDTO> informeGestionDTOList = super.em.createQuery("SELECT inf FROM " + InformeGestion.class.getName() + " inf ").getResultList();

        return informeGestionDTOList;
  */
    }

    public List<InformeGestionExtinto> obtenerInformeExtintos(Integer periodo, Integer codTipoPeriodo) throws DataAccessException, IpsBussinesException{

        String query = "";
        List<InformeGestionExtinto> informeExtintos = new ArrayList<>();

        //construct the query
        if(codTipoPeriodo != null && codTipoPeriodo == 0){
            //Concesion
            query = "SELECT\n" +
                    "DISTINCT (dos.PEN_RUN) || '-' ||  dos.PEN_DV run,\n" +
                    "SUBSTR(dos.PEN_NOMBRES,0,30) nombre,\n" +
                    "dos.PEN_APE_PATERNO apellidoPaterno,\n" +
                    "dos.PEN_APE_MATERNO apellidoMaterno, \n" +
                    "replace( replace (replace (dos.PEN_TIPO, 1,'Externo'), 2,'Interno'), 3, 'Mixto') tipoPensionado, \n" +
                    "ins.INST_NOMBRE institucion\n" +
                    "FROM TB_PENSIONADOS dos, TB_PENSIONES nes, TB_PR_INSTITUCIONES ins\n" +
                    "WHERE dos.PEN_RUN IN\n" +
                    "(SELECT dos1.PEN_RUN \n" +
                    "FROM TB_PENSIONADOS dos1\n" +
                    "WHERE dos1.PEN_PERIODO = TO_NUMBER(TO_CHAR(ADD_MONTHS(TO_DATE( ? ,'YYYYMM'), -1),'YYYYMM'))\n" +
                    "AND dos1.PEN_BENEFICIARIO = 1)\n" +
                    "AND nes.PNS_RUT_INSTITUCION = ins.INST_RUT\n" +
                    "AND dos.PEN_BENEFICIARIO = 0\n" +
                    "AND dos.PEN_PERIODO = ? ";
        }else if(codTipoPeriodo != null && codTipoPeriodo == 1){
            //Bonificacion
            query = "SELECT\n" +
                    "DISTINCT (dos.PEN_RUN) || '-' ||  dos.PEN_DV run,\n" +
                    "SUBSTR(dos.PEN_NOMBRES,0,30) nombre,\n" +
                    "dos.PEN_APE_PATERNO apellidoPaterno,\n" +
                    "dos.PEN_APE_MATERNO apellidoMaterno, \n" +
                    "replace( replace (replace (dos.PEN_TIPO, 1,'Externo'), 2,'Interno'), 3, 'Mixto') tipoPensionado, \n" +
                    "ins.INST_NOMBRE institucion\n" +
                    "FROM TB_PENSIONADOS dos, TB_PENSIONES nes, TB_PR_INSTITUCIONES ins\n" +
                    "WHERE dos.PEN_RUN IN\n" +
                    "(SELECT dos1.PEN_RUN \n" +
                    "FROM TB_PENSIONADOS dos1\n" +
                    "WHERE dos1.PEN_PERIODO_BONIFICACION = TO_NUMBER(TO_CHAR(ADD_MONTHS(TO_DATE( ? ,'YYYYMM'), -1),'YYYYMM'))\n" +
                    "AND dos1.PEN_BENEFICIARIO = 1)\n" +
                    "AND nes.PNS_RUT_INSTITUCION = ins.INST_RUT\n" +
                    "AND dos.PEN_BENEFICIARIO = 0\n" +
                    "AND dos.PEN_PERIODO_BONIFICACION = ? ";
        }


//
        informeExtintos = (List<InformeGestionExtinto>)super.em.createNativeQuery(query,InformeGestionExtinto.class)
                .setParameter(1, periodo)
                .setParameter(2, periodo)
                .getResultList();

        if (informeExtintos==null){
            informeExtintos= new ArrayList<InformeGestionExtinto>();
        }

        return informeExtintos;

    }
    public List<InformeGestionMantienen> getInformeMantienen(Integer periodo, Integer codTipoPeriodo) throws DataAccessException, IpsBussinesException{

        String query = "";
        List<InformeGestionMantienen> informeMantienen = new ArrayList<>();


        //construct the query
        if(codTipoPeriodo != null && codTipoPeriodo == 0){
            //Concesion
            query = "SELECT\n" +
                    "DISTINCT (dos.PEN_RUN) || '-' ||  dos.PEN_DV run,\n" +
                    "SUBSTR(dos.PEN_NOMBRES,0,30) nombre,\n" +
                    "dos.PEN_APE_PATERNO apellidoPaterno,\n" +
                    "dos.PEN_APE_MATERNO apellidoMaterno, \n" +
                    "replace( replace (replace (dos.PEN_TIPO, 1,'Externo'), 2,'Interno'), 3, 'Mixto') tipoPensionado, \n" +
                    "ins.INST_NOMBRE institucion\n" +
                    "FROM TB_PENSIONADOS dos, TB_PENSIONES nes, TB_PR_INSTITUCIONES ins\n" +
                    "WHERE dos.PEN_RUN IN\n" +
                    "(SELECT dos1.PEN_RUN \n" +
                    "FROM TB_PENSIONADOS dos1\n" +
                    "WHERE dos1.PEN_PERIODO = TO_NUMBER(TO_CHAR(ADD_MONTHS(TO_DATE( ? ,'YYYYMM'), -1),'YYYYMM'))\n" +
                    "AND dos1.PEN_BENEFICIARIO = 1)\n" +
                    "AND nes.PNS_RUT_INSTITUCION = ins.INST_RUT\n" +
                    "AND dos.PEN_BENEFICIARIO = 1\n" +
                    "AND dos.PEN_PERIODO = ? ";
        }else if(codTipoPeriodo != null && codTipoPeriodo == 1){
            //Bonificacion
            query = "SELECT\n" +
                    "DISTINCT (dos.PEN_RUN) || '-' ||  dos.PEN_DV run,\n" +
                    "SUBSTR(dos.PEN_NOMBRES,0,30) nombre,\n" +
                    "dos.PEN_APE_PATERNO apellidoPaterno,\n" +
                    "dos.PEN_APE_MATERNO apellidoMaterno, \n" +
                    "replace( replace (replace (dos.PEN_TIPO, 1,'Externo'), 2,'Interno'), 3, 'Mixto') tipoPensionado, \n" +
                    "ins.INST_NOMBRE institucion\n" +
                    "FROM TB_PENSIONADOS dos, TB_PENSIONES nes, TB_PR_INSTITUCIONES ins\n" +
                    "WHERE dos.PEN_RUN IN\n" +
                    "(SELECT dos1.PEN_RUN \n" +
                    "FROM TB_PENSIONADOS dos1\n" +
                    "WHERE dos1.PEN_PERIODO_BONIFICACION = TO_NUMBER(TO_CHAR(ADD_MONTHS(TO_DATE( ? ,'YYYYMM'), -1),'YYYYMM'))\n" +
                    "AND dos1.PEN_BENEFICIARIO = 1)\n" +
                    "AND nes.PNS_RUT_INSTITUCION = ins.INST_RUT\n" +
                    "AND dos.PEN_BENEFICIARIO = 1\n" +
                    "AND dos.PEN_PERIODO_BONIFICACION = ? ";
        }

        informeMantienen = (List<InformeGestionMantienen>)super.em.createNativeQuery(query, InformeGestionMantienen.class)
                .setParameter(1, periodo)
                .setParameter(2, periodo)
                .getResultList();

        if (informeMantienen==null){
            informeMantienen= new ArrayList<InformeGestionMantienen>();
        }

        return informeMantienen;
    }

    public List<InformeGestionNuevos> getInformeNuevos(Integer periodo, Integer codTipoPeriodo) throws DataAccessException, IpsBussinesException{

        String query = "";
        List<InformeGestionNuevos> informeNuevos = new ArrayList<>();

        if(codTipoPeriodo != null && codTipoPeriodo == 0){
            query = "SELECT\n" +
                    "DISTINCT (dos.PEN_RUN) || '-' ||  dos.PEN_DV run,\n" +
                    "SUBSTR(dos.PEN_NOMBRES,0,30) nombre,\n" +
                    "dos.PEN_APE_PATERNO apellidoPaterno,\n" +
                    "dos.PEN_APE_MATERNO apellidoMaterno, \n" +
                    "replace( replace (replace (dos.PEN_TIPO, 1,'Externo'), 2,'Interno'), 3, 'Mixto') tipoInstitucion, \n" +
                    "ins.INST_NOMBRE institucion\n" +
                    "FROM TB_PENSIONADOS dos, TB_PENSIONES nes, TB_PR_INSTITUCIONES ins\n" +
                    "WHERE dos.PEN_RUN IN\n" +
                    "(SELECT dos1.PEN_RUN \n" +
                    "FROM TB_PENSIONADOS dos1\n" +
                    "WHERE dos1.PEN_PERIODO = TO_NUMBER(TO_CHAR(ADD_MONTHS(TO_DATE( ? ,'YYYYMM'), -1),'YYYYMM'))\n" +
                    "AND dos1.PEN_BENEFICIARIO = 0)\n" +
                    "AND nes.PNS_RUT_INSTITUCION = ins.INST_RUT\n" +
                    "AND dos.PEN_BENEFICIARIO = 1\n" +
                    "AND dos.PEN_PERIODO = ? ";
        }else if(codTipoPeriodo != null && codTipoPeriodo == 1){
            query = "SELECT\n" +
                    "DISTINCT (dos.PEN_RUN) || '-' ||  dos.PEN_DV run,\n" +
                    "SUBSTR(dos.PEN_NOMBRES,0,30) nombre,\n" +
                    "dos.PEN_APE_PATERNO apellidoPaterno,\n" +
                    "dos.PEN_APE_MATERNO apellidoMaterno, \n" +
                    "replace( replace (replace (dos.PEN_TIPO, 1,'Externo'), 2,'Interno'), 3, 'Mixto') tipoInstitucion, \n" +
                    "ins.INST_NOMBRE institucion \n" +
                    "FROM TB_PENSIONADOS dos, TB_PENSIONES nes, TB_PR_INSTITUCIONES ins\n" +
                    "WHERE dos.PEN_RUN IN\n" +
                    "(SELECT dos1.PEN_RUN \n" +
                    "FROM TB_PENSIONADOS dos1\n" +
                    "WHERE dos1.PEN_PERIODO_BONIFICACION = TO_NUMBER(TO_CHAR(ADD_MONTHS(TO_DATE( ? ,'YYYYMM'), -1),'YYYYMM'))\n" +
                    "AND dos1.PEN_BENEFICIARIO = 0)\n" +
                    "AND nes.PNS_RUT_INSTITUCION = ins.INST_RUT\n" +
                    "AND dos.PEN_BENEFICIARIO = 1\n" +
                    "AND dos.PEN_PERIODO_BONIFICACION = ? ";
        }

        informeNuevos = (List<InformeGestionNuevos>) super.em.createNativeQuery(query, InformeGestionNuevos.class)
                .setParameter(1, periodo)
                .setParameter(2, periodo)
                .getResultList();

        if (informeNuevos==null){
            informeNuevos= new ArrayList<InformeGestionNuevos>();
        }

        return informeNuevos;
    }

    public List<InformeGestionVer> obtenerDatosVerPorTipoPeriodo(Integer periodo, Integer tipoPeriodo) throws DataAccessException, IpsBussinesException {
//        List<InformeGestionVer> list = (List<InformeGestionVer>) super.em.createNamedQuery(InformeGestionVer.QUERY_FIND_BY_TIPO_PERIODO, InformeGestionVer.class)
//                .setParameter("periodo", periodo)
//                .setParameter("tipoPeriodo", tipoPeriodo)
//                .getResultList();
//
//        super.em.flush();
        return null;
    }

    public List<InformeGestionNuevos> obtenerDatosNuevos(Integer periodo) throws DataAccessException, IpsBussinesException {
        String query = "SELECT \n" +
                "pen_run || pen_dv AS RUN,\n" +
                "pen_nombres AS nombre,\n" +
                "pen_ape_paterno AS apellidoPaterno,\n" +
                "pen_ape_materno AS apellidoMaterno,\n" +
                "pen_tipo AS tipoInstitucion,\n" +
                "pns.tip_inst_descripcion AS institucion\n" +
                "FROM tb_pensionados pen, \n" +
                "(\n" +
                "    SELECT  pns.pns_run, pns.pns_dv, pns.pns_rut_institucion, tip.tip_inst_descripcion FROM tb_pensiones pns\n" +
                "    INNER JOIN tb_pr_instituciones insti\n" +
                "    ON pns.pns_rut_institucion = insti.inst_rut\n" +
                "    INNER JOIN tb_pr_tipo_institucion tip\n" +
                "    ON tip.tip_inst_cod = insti.inst_tip_inst_cod\n" +
                "    WHERE pns_periodo = ?\n" +
                "    GROUP BY pns.pns_run, pns.pns_dv, pns.pns_rut_institucion, tip.tip_inst_descripcion\n" +
                "    HAVING COUNT(pns.pns_run) > 0\n" +
                "    ORDER BY COUNT(pns.pns_run) DESC\n" +
                ") pns\n" +
                "WHERE \n" +
                "pns.pns_run = pen.pen_run AND \n" +
                "pen.pen_periodo = ? AND\n" +
                "pen.pen_beneficiario = 1 AND \n" +
                "pen.pen_run IN (\n" +
                "SELECT pen.pen_run FROM tb_pensionados\n" +
                "WHERE pen_periodo = to_number(to_char(add_months(to_date(?, 'yyyyMM'), -1), 'yyyyMM')) AND pen_beneficiario = 0\n" +
                ")\n";

        return super.em.createNativeQuery(query, InformeGestionNuevos.class)
                .setParameter(1, periodo)
                .setParameter(2, periodo)
                .setParameter(3, periodo)
                .getResultList();

    }


    public List<InformeGestionNuevos> obtenerDatosVerNuevos(Integer periodo, String institucion) throws DataAccessException, IpsBussinesException {
        String query = "SELECT\n" +
                " pen_run AS RUN, \n" +
                "    pen_nombres AS nombre, \n" +
                "    pen_ape_paterno AS apellidoPaterno, \n" +
                "    pen_ape_materno AS apellidoMaterno, \n" +
                "    pen_tipo AS tipoInstitucion, \n" +
                "    pns.tip_inst_descripcion AS institucion \n" +
                "    FROM tb_pensionados pen,  \n" +
                "    ( \n" +
                "        INNER JOIN tb_pr_instituciones insti \n" +
                "        SELECT  pns.pns_run, pns.pns_dv, pns.pns_rut_institucion, tip.tip_inst_descripcion FROM tb_pensiones pns \n" +
                "        ON pns.pns_rut_institucion = insti.inst_rut \n" +
                "        INNER JOIN tb_pr_tipo_institucion tip \n" +
                "        ON tip.tip_inst_cod = insti.inst_tip_inst_cod \n" +
                "        WHERE pns_periodo = ? \n" +
                "        GROUP BY pns.pns_run, pns.pns_dv, pns.pns_rut_institucion, tip.tip_inst_descripcion \n" +
                "        HAVING COUNT(pns.pns_run) > 0 \n" +
                "        ORDER BY COUNT(pns.pns_run) DESC \n" +
                "    ) pns \n" +
                "    WHERE  \n" +
                "    pns.pns_run = pen.pen_run AND  \n" +
                "    pen.pen_periodo = ? AND \n" +
                "    pen.pen_beneficiario = 1 AND\n" +
                "    pns.tip_inst_descripcion = ? AND\n" +
                "    pen.pen_run IN ( \n" +
                "    SELECT pen.pen_run FROM tb_pensionados \n" +
                "    WHERE pen_periodo = to_number(to_char(add_months(to_date(?, 'yyyyMM'), -1), 'yyyyMM')) AND pen_beneficiario = 0 \n" +
                "    )";

        return super.em.createNativeQuery(query, InformeGestionNuevos.class)
                .setParameter(1, periodo)
                .setParameter(2, periodo)
                .setParameter(3, institucion)
                .setParameter(4, periodo)
                .getResultList();
    }

}
