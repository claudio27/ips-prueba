package cl.gob.ips.exencion.service;


import cl.gob.ips.exencion.dao.GrupoUsuariosDao;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Funcionalidades;
import cl.gob.ips.exencion.model.GrupoUsuarios;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Created by practicante
 *
 */
@Stateless
@LocalBean
public class GrupoUsuariosService {

    @EJB
    GrupoUsuariosDao grupoUsuarioDAOService;

    /**
     * @return List<GrupoUsuarios>
     */

    public List<GrupoUsuarios> getAllGrupos() throws DataAccessException {
        return grupoUsuarioDAOService.getGruposUsuario();
    }


    /**
     *
     * @return
     * @throws DataAccessException
     */
    public GrupoUsuarios buscarGrupoUsuario(final String descripcion) throws DataAccessException {
        return grupoUsuarioDAOService.findGrupoUsuario(descripcion);
    }

    /**
     *
     * @param grupoUsuarios
     * @throws Exception
     */

    public void crearGrupoUsuarios(final GrupoUsuarios grupoUsuarios) throws DataAccessException {
        grupoUsuarioDAOService.createGrupoUsuarios(grupoUsuarios);
    }


    /**
     *
     * @param grupoUsuarios
     * @throws Exception
     */

    public void eliminarGrupoUsuarios(final GrupoUsuarios grupoUsuarios) throws DataAccessException {
        grupoUsuarioDAOService.deleteGrupoUsuarios(grupoUsuarios);
    }


}