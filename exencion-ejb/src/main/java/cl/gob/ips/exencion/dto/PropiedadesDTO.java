package cl.gob.ips.exencion.dto;

import java.io.Serializable;

/**
 * Created by practicante on 16-10-17.
 */
public class PropiedadesDTO implements Serializable{

    private String ftpCarpeta;
    private String ftpClaveUsuario;
    private String ftpUsuarioArchivos;
    private String ftpUrlArchivos;

    public PropiedadesDTO(String ftpCarpeta, String ftpClaveUsuario, String ftpUsuarioArchivos, String ftpUrlArchivos) {
        this.ftpCarpeta = ftpCarpeta;
        this.ftpClaveUsuario = ftpClaveUsuario;
        this.ftpUsuarioArchivos = ftpUsuarioArchivos;
        this.ftpUrlArchivos = ftpUrlArchivos;
    }

    public String getFtpCarpeta() {
        return ftpCarpeta;
    }

    public void setFtpCarpeta(String ftpCarpeta) {
        this.ftpCarpeta = ftpCarpeta;
    }

    public String getFtpClaveUsuario() {
        return ftpClaveUsuario;
    }

    public void setFtpClaveUsuario(String ftpClaveUsuario) {
        this.ftpClaveUsuario = ftpClaveUsuario;
    }

    public String getFtpUsuarioArchivos() {
        return ftpUsuarioArchivos;
    }

    public void setFtpUsuarioArchivos(String ftpUsuarioArchivos) {
        this.ftpUsuarioArchivos = ftpUsuarioArchivos;
    }

    public String getFtpUrlArchivos() {
        return ftpUrlArchivos;
    }

    public void setFtpUrlArchivos(String ftpUrlArchivos) {
        this.ftpUrlArchivos = ftpUrlArchivos;
    }
}
