package cl.gob.ips.exencion.dto;

import java.io.Serializable;

/**
 * Created by practicante on 12-09-17.
 */
public class FuncionalidadesDTO implements Serializable {



    private Integer funcCod;
    private String funcDescripcion;

    public Integer getFuncCod() {
        return funcCod;
    }

    public void setFuncCod(Integer funcCod) {
        this.funcCod = funcCod;
    }

    public String getFuncDescripcion() {
        return funcDescripcion;
    }

    public void setFuncDescripcion(String funcDescripcion) {
        this.funcDescripcion = funcDescripcion;
    }
}
