package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.ConsultaBeneficio;
import cl.gob.ips.exencion.model.Instituciones;
import cl.gob.ips.exencion.model.Pensiones;
import cl.gob.ips.exencion.model.TipoRespuesta;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.List;

/**
 * Created by lpino on 03-07-17.
 */
@Stateless
@LocalBean
public class ConsultaBeneficioDAO extends BaseDAO {

    private static final String PARAM_CON_BEN_RUT_PEN = "conBenRutPen";

    private static final String PARAM_PERIODO = "periodo";

    private static final String PARAM_RUT = "rut";

    public ConsultaBeneficio guardarConsultaBeneficio(final Integer run, final String dv,
                                         final Integer periodo, final String observacion,
                                         final String usuario, final String instituciones, final Integer tipoRespuesta, final List<TipoRespuesta> listaTipoRespuesta) throws DataAccessException {
        ConsultaBeneficio consulta = new ConsultaBeneficio();
        consulta.setCanBenInstituciones(instituciones);
        consulta.setCanBenUsuario(usuario);
        consulta.setConBenDvPen(dv);
        consulta.setConBenFecConsulta(new Date());
        consulta.setConBenObservacionUsu(observacion);
        consulta.setConBenRutPen(run);
        consulta.setConBenPeriodo(periodo);

        if(listaTipoRespuesta.isEmpty())
            consulta.setTbPrTipoRespuestas(null);
        else
            consulta.setTbPrTipoRespuestas(listaTipoRespuesta);

        super.em.persist(consulta);
        super.em.flush();

        this.guardarConsultaTipoRespuesta(consulta.getConBenId(),tipoRespuesta);

        return consulta;
    }

    public List<ConsultaBeneficio> obtenerConsultaBeneficio(Integer run) throws DataAccessException {
        List<ConsultaBeneficio> consultaBeneficios = null;

        super.query = super.em.createNamedQuery(ConsultaBeneficio.QUERY_FIND_BY_RUN)
                .setParameter(PARAM_CON_BEN_RUT_PEN, run);

        if (!super.query.getResultList().isEmpty()) {
            consultaBeneficios = super.query.getResultList();
        }

        return consultaBeneficios;
    }

    public List<String> obtenerNombreInstituciones(Integer rut, Integer periodo) throws DataAccessException {

        List<String> listaInstituciones = null;

        super.query = super.em.createQuery("SELECT ins.instNombreCorto FROM " + Instituciones.class.getName() + " ins "
                    + "WHERE ins.instRut in (SELECT pns.tbPrInstituciones.instRut FROM " + Pensiones.class.getName() + " pns " + "WHERE pns.pnsRun= :rut AND" +
                    " pns.tbPeriodo.perPeriodo= :periodo) ")
                    .setParameter(PARAM_RUT, rut).setParameter(PARAM_PERIODO, periodo);

        if (!super.query.getResultList().isEmpty()) {
            listaInstituciones = super.query.getResultList();

        }


        return listaInstituciones;

    }

    public List<String> obtenerNombreInstituciones(Integer rut) throws DataAccessException {
        List<String> listaInstituciones = null;

        super.query = super.em.createQuery("SELECT ins.instNombreCorto FROM " + Instituciones.class.getName() + " ins "
                + "WHERE ins.instRut in (SELECT pns.tbPrInstituciones.instRut FROM " + Pensiones.class.getName() + " pns " + "WHERE pns.pnsRun= :rut " +
                ") ")
                .setParameter(PARAM_RUT, rut);

        if (!super.query.getResultList().isEmpty()) {
            listaInstituciones = super.query.getResultList();
        }

        return listaInstituciones;

    }

    public void guardarConsultaTipoRespuesta(Integer idConsulta, Integer idTipoRespuesta) {

        String query = "INSERT INTO TB_CONSULTA_BENEF_TIPO_RESP\n" +
                "(CON_BEN_ID, TIP_RESP_COD)\n" +
                "VALUES(?, ?)";
        super.em.createNativeQuery(query)
                .setParameter(1, idConsulta)
                .setParameter(2, idTipoRespuesta)
                .executeUpdate();
        super.em.flush();
    }
}
