package cl.gob.ips.exencion.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the TB_NOMINA_POTENCIALES_APS database table.
 * 
 */
@Entity
@Table(name = "TB_NOMINA_POTENCIALES_APS")
@NamedQuery(name = "NominaPotencialesAp.findAll", query = "SELECT t FROM NominaPotencialesAps t")
public class NominaPotencialesAps implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2332231017150762823L;

	@Id
	@Column(name = "NOM_ID")
	private long nomId;

	@Column(name = "NOM_APELLIDO_MAT")
	private String nomApellidoMat;

	@Column(name = "NOM_APELLIDO_PAT")
	private String nomApellidoPat;

	@Column(name = "NOM_DV_BENEFICIARIO")
	private String nomDvBeneficiario;

	@Column(name = "NOM_DV_INSTITUCION")
	private String nomDvInstitucion;

	@Column(name = "NOM_INDICADOR_FPS")
	private BigDecimal nomIndicadorFps;

	@Column(name = "NOM_NOMBRES")
	private String nomNombres;

	@Column(name = "NOM_RUN_BENEFICIARIO")
	private Integer nomRunBeneficiario;

	// bi-directional many-to-one association to TbPeriodo
	@ManyToOne
	@JoinColumn(name = "NOM_PERIODO")
	private Periodo periodo;

	// bi-directional many-to-one association to TbPrInstitucione
	@ManyToOne
	@JoinColumn(name = "NOM_RUT_INSTITUCION")
	private Instituciones instituciones;

	public NominaPotencialesAps() {
		super();
	}

	public long getNomId() {
		return this.nomId;
	}

	public void setNomId(long nomId) {
		this.nomId = nomId;
	}

	public String getNomApellidoMat() {
		return this.nomApellidoMat;
	}

	public void setNomApellidoMat(String nomApellidoMat) {
		this.nomApellidoMat = nomApellidoMat;
	}

	public String getNomApellidoPat() {
		return this.nomApellidoPat;
	}

	public void setNomApellidoPat(String nomApellidoPat) {
		this.nomApellidoPat = nomApellidoPat;
	}

	public String getNomDvBeneficiario() {
		return this.nomDvBeneficiario;
	}

	public void setNomDvBeneficiario(String nomDvBeneficiario) {
		this.nomDvBeneficiario = nomDvBeneficiario;
	}

	public String getNomDvInstitucion() {
		return this.nomDvInstitucion;
	}

	public void setNomDvInstitucion(String nomDvInstitucion) {
		this.nomDvInstitucion = nomDvInstitucion;
	}

	public BigDecimal getNomIndicadorFps() {
		return this.nomIndicadorFps;
	}

	public void setNomIndicadorFps(BigDecimal nomIndicadorFps) {
		this.nomIndicadorFps = nomIndicadorFps;
	}

	public String getNomNombres() {
		return this.nomNombres;
	}

	public void setNomNombres(String nomNombres) {
		this.nomNombres = nomNombres;
	}

	public Integer getNomRunBeneficiario() {
		return this.nomRunBeneficiario;
	}

	public void setNomRunBeneficiario(Integer nomRunBeneficiario) {
		this.nomRunBeneficiario = nomRunBeneficiario;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	public Instituciones getInstituciones() {
		return instituciones;
	}

	public void setInstituciones(Instituciones instituciones) {
		this.instituciones = instituciones;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder
				.append(periodo.getPerPeriodo() ) 		// todo revisar largo de datos
				.append(instituciones.getInstRut())
				.append(instituciones.getInstDv())
				.append(nomRunBeneficiario)
				.append(nomDvBeneficiario)
				.append(nomApellidoPat)
				.append(nomApellidoMat)
				.append(nomNombres)
				.append(nomIndicadorFps.intValue() == 1 ? "Si" : "No");

		return 	builder.toString();
	}

}