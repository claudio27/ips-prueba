package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.GrupoUsuarioUsuarios;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Created by practicante
 */
@Stateless
@LocalBean
public class GrupoUsuarioUsuariosDao extends BaseDAO {

    /**
     * @return
     * @throws DataAccessException
     */
    public void crearGrupoUsuarioUsuarios(final GrupoUsuarioUsuarios grupoUsuarioUsuarios) throws DataAccessException {
        super.em.persist(grupoUsuarioUsuarios);

    }

    /**
     *
     * @throws DataAccessException
     */

    public void deleteGrupoUsuarioUsuarios(GrupoUsuarioUsuarios grupoUsuarioUsuarios) throws DataAccessException {
        super.em.remove(super.em.merge(grupoUsuarioUsuarios));





    }

}