package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.GruposArchivo;
import cl.gob.ips.exencion.model.UsuarioGrupo;
import cl.gob.ips.exencion.model.Usuarios;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by rodrigo.reyesco@gmail.com on 27-10-2016.
 */
@Stateless
@LocalBean
public class ResponsableGrupoArchivoDao extends BaseDAO {

    private static final String PARAM_GRUPO_ARCHIVO = "grupoArchivo";

    private static final String PARAM_CORREO_USUARIO = "usuarioCorreo";

    /**
     *
     * @param grupoArchivo
     * @return List<UsuarioGrupo>
     * @throws DataAccessException
     */
    public List<UsuarioGrupo> getUsuarioGrupoByGrupoArchivo(GruposArchivo grupoArchivo) throws DataAccessException{
        List<UsuarioGrupo> usuarioGrupoLista = null;

        super.query = super.em.createNamedQuery(UsuarioGrupo.QUERY_USUARIO_GRUPO_FIND_BY_GRUPO_ARCHIVO)
                .setParameter(PARAM_GRUPO_ARCHIVO, grupoArchivo.getGrupArchCod());

        if (!super.query.getResultList().isEmpty()) {
            usuarioGrupoLista = super.query.getResultList();
        }

        return usuarioGrupoLista;
    }

    /**
     *
     * @param email
     * @return Usuarios
     * @throws DataAccessException
     */
    public Usuarios findUsuarioByEmail(final String email) throws DataAccessException{
        Usuarios usuarios = null;

        super.query = super.em.createNamedQuery(Usuarios.QUERY_FIND_BY_EMAIL).setParameter(PARAM_CORREO_USUARIO, email);

        if (!super.query.getResultList().isEmpty()) {
            usuarios = (Usuarios) super.query.getSingleResult();
        }

        return usuarios;
    }

    /**
     *
     * @param usuarioGrupo
     * @throws DataAccessException
     */
    public void creaUsuarioGrupoArchivo(final UsuarioGrupo usuarioGrupo) throws DataAccessException, SQLException {
        super.em.persist(usuarioGrupo);
        super.em.flush();
    }

    /**
     *
     * @param usuarioGrupo
     * @throws DataAccessException
     */
    public void actualizaUsuarioGrupoArchivo(final UsuarioGrupo usuarioGrupo) throws DataAccessException{
        super.em.merge(usuarioGrupo);
        super.em.flush();
    }
}
