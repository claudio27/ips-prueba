package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * To create ID generator sequence "TB_CONSULTA_BENEFICIO_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_CONSULTA_BENEFICIO_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "ConsultaBeneficio.findAll", query = "select o from ConsultaBeneficio o"),
        @NamedQuery(name = "ConsultaBeneficio.UPDATE_OBSERVACION", query ="UPDATE ConsultaBeneficio o SET o.conBenObservacionUsu = :observacion " +
                "WHERE o.conBenRutPen = :rutPensionado " +
                "AND o.conBenPeriodo = :periodo"),
        @NamedQuery(name = "ConsultaBeneficio.findByRun", query = "select o from ConsultaBeneficio o where o.conBenRutPen = :conBenRutPen")})
@Table(name = "TB_CONSULTA_BENEFICIO")
@SequenceGenerator(name = "ConBeneficio_Id_Seq_Gen", sequenceName = "SEQ_CONSULTA_BENEFICIO",
        allocationSize = 1)
public class ConsultaBeneficio implements Serializable {
    private static final long serialVersionUID = 2626281015306856286L;

    public static final String UPDATE_OBSERVACION = "ConsultaBeneficio.updateObservacion";

    public static final String QUERY_FIND_BY_RUN = "ConsultaBeneficio.findByRun";

    @Column(name = "CAN_BEN_INSTITUCIONES", length = 45)
    private String canBenInstituciones;
    @Column(name = "CAN_BEN_USUARIO", length = 20)
    private String canBenUsuario;
    @Column(name = "CON_BEN_DV_PEN", length = 1)
    private String conBenDvPen;
    @Temporal(TemporalType.DATE)
    @Column(name = "CON_BEN_FEC_CONSULTA")
    private Date conBenFecConsulta;
    @Id
    @Column(name = "CON_BEN_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ConBeneficio_Id_Seq_Gen")
    private Integer conBenId;
    @Column(name = "CON_BEN_OBSERVACION_USU", length = 250)
    private String conBenObservacionUsu;
    @Column(name = "CON_BEN_PERIODO")
    private Integer conBenPeriodo;
    @Column(name = "CON_BEN_RUT_PEN", nullable = false)
    private Integer conBenRutPen;

    //bi-directional many-to-many association to TbPrTipoRespuesta
    @ManyToMany(mappedBy="consultaBeneficios", cascade = CascadeType.PERSIST)
    private List<TipoRespuesta> tipoRespuestas;

    public ConsultaBeneficio() {
    }

    public ConsultaBeneficio(String canBenInstituciones, String canBenUsuario,
                             String conBenDvPen, Date conBenFecConsulta, Integer conBenId, String conBenObservacionUsu,
                             Integer conBenPeriodo, Integer conBenRutPen) {
        this.canBenInstituciones = canBenInstituciones;
        this.canBenUsuario = canBenUsuario;
        this.conBenDvPen = conBenDvPen;
        this.conBenFecConsulta = conBenFecConsulta;
        this.conBenId = conBenId;
        this.conBenObservacionUsu = conBenObservacionUsu;
        this.conBenPeriodo = conBenPeriodo;
        this.conBenRutPen = conBenRutPen;
    }

    public String getCanBenInstituciones() {
        return canBenInstituciones;
    }

    public void setCanBenInstituciones(String canBenInstituciones) {
        this.canBenInstituciones = canBenInstituciones;
    }

    public String getCanBenUsuario() {
        return canBenUsuario;
    }

    public void setCanBenUsuario(String canBenUsuario) {
        this.canBenUsuario = canBenUsuario;
    }

    public String getConBenDvPen() {
        return conBenDvPen;
    }

    public void setConBenDvPen(String conBenDvPen) {
        this.conBenDvPen = conBenDvPen;
    }

    public Date getConBenFecConsulta() {
        return conBenFecConsulta;
    }

    public void setConBenFecConsulta(Date conBenFecConsulta) {
        this.conBenFecConsulta = conBenFecConsulta;
    }

    public Integer getConBenId() {
        return conBenId;
    }

    public String getConBenObservacionUsu() {
        return conBenObservacionUsu;
    }

    public void setConBenObservacionUsu(String conBenObservacionUsu) {
        this.conBenObservacionUsu = conBenObservacionUsu;
    }

    public Integer getConBenPeriodo() {
        return conBenPeriodo;
    }

    public void setConBenPeriodo(Integer conBenPeriodo) {
        this.conBenPeriodo = conBenPeriodo;
    }

    public Integer getConBenRutPen() {
        return conBenRutPen;
    }

    public void setConBenRutPen(Integer conBenRutPen) {
        this.conBenRutPen = conBenRutPen;
    }

    public List<TipoRespuesta> getTipoRespuestas() {
        return this.tipoRespuestas;
    }

    public void setTbPrTipoRespuestas(List<TipoRespuesta> tipoRespuestas) {
        this.tipoRespuestas = tipoRespuestas;
    }
}
