package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.PropiedadesDAO;
import cl.gob.ips.exencion.dto.PropiedadesDTO;
import cl.gob.ips.exencion.model.Propiedades;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by practicante on 16-10-17.
 */
@Stateless
@LocalBean
public class PropiedadesService {

    @Inject
    PropiedadesDAO dao;

    public Propiedades propiedadesFTP(String clave){
        return dao.obtenerPropiedadesFTP(clave);
    }
}
