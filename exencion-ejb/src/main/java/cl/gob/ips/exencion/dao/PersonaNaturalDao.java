package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.PersonaNatural;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author rereyes@emergya.com
 * @since 18-08-2016
 */
@Stateless
@LocalBean
public class PersonaNaturalDao extends BaseDAO {

    public List<PersonaNatural> getPersonas() throws DataAccessException {
        return (List<PersonaNatural>) super.em.createNamedQuery(PersonaNatural.QUERY_FIND_ALL).getResultList();
    }

    public List<PersonaNatural> getPersonasByApellidoPaterno(final String apellidoPaterno) throws DataAccessException {
        return (List<PersonaNatural>) super.em.createNamedQuery(PersonaNatural.QUERY_FIND_BY_APELLIDO_PATERNO)
                .setParameter("apellidoPaterno", apellidoPaterno + "%")
                .getResultList();
    }
}
