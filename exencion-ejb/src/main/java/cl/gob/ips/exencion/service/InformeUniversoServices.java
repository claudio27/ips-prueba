package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.InformeUniversoDAO;
import cl.gob.ips.exencion.dto.InformeUniversoDTO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.model.Pensionados;
import cl.gob.ips.exencion.model.Periodo;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alaya on 13/07/17.
 */
public class InformeUniversoServices {

    @Inject
    private InformeUniversoDAO informeUniversoDAO;


    public List<InformeUniversoDTO> obtenerInformeUniverso(Integer periodoDesde, Integer periodoHasta) throws DataAccessException, IpsBussinesException {

        List<InformeUniversoDTO> listaInfUniverso = new ArrayList<>();

        List<Periodo> listaPeriodos = informeUniversoDAO.obtenerInformeUniverso(periodoDesde, periodoHasta);

        for (Periodo per : listaPeriodos) {

            InformeUniversoDTO uni = new InformeUniversoDTO();

            uni.setPeriodo(String.valueOf(per.getPerPeriodo()));
            uni.setVivos(per.getVivos());
            uni.setSinSps(per.getSinSps());
            uni.setSinRsh(per.getSinRsh());
            uni.setSinFocalizacion(per.getSinFocalizacion());
            uni.setSinResidencia(per.getSinResidencia());
            uni.setCumpleRequisitos(per.getCumplenRequisitos());

            listaInfUniverso.add(uni);
        }

        return listaInfUniverso;
    }


    public List<Pensionados> obtenerInformeUniversoPorRun(Integer periodo) throws DataAccessException, IpsBussinesException {

        return  informeUniversoDAO.obtenerInformeUniversoDeRunsPorPeriodo(periodo);

    }




}
