package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * To create ID generator sequence "TB_PR_TPR_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_TPR_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "Tpr.findAll", query = "select o from Tpr o") })
@Table(name = "TB_PR_TPR")
@SequenceGenerator(name = "Tpr_Id_Seq_Gen", sequenceName = "TB_PR_TPR_ID_SEQ_GEN", allocationSize = 50,
                   initialValue = 50)
public class Tpr implements Serializable {
    private static final long serialVersionUID = 7349888795457219111L;
    @Column(name = "TPR_COD", nullable = false)
    private Integer tprCod;
    @Column(name = "TPR_COD_ORI_PENS_INP")
    private Integer tprCodOriPensInp;
    @Id
    @Column(name = "TPR_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Tpr_Id_Seq_Gen")
    private Integer tprId;
    @Column(name = "TPR_TIP_PNS_COD", nullable = false)
    private Integer tprTipPnsCod;
    @ManyToOne
    @JoinColumn(name = "TPR_ORI_COD")
    private Origen tbPrTblOrigen1;

    public Tpr() {
    }

    public Tpr(Integer tprCod, Integer tprCodOriPensInp, Integer tprId, Origen tbPrTblOrigen1, Integer tprTipPnsCod) {
        this.tprCod = tprCod;
        this.tprCodOriPensInp = tprCodOriPensInp;
        this.tprId = tprId;
        this.tbPrTblOrigen1 = tbPrTblOrigen1;
        this.tprTipPnsCod = tprTipPnsCod;
    }

    public Integer getTprCod() {
        return tprCod;
    }

    public void setTprCod(Integer tprCod) {
        this.tprCod = tprCod;
    }

    public Integer getTprCodOriPensInp() {
        return tprCodOriPensInp;
    }

    public void setTprCodOriPensInp(Integer tprCodOriPensInp) {
        this.tprCodOriPensInp = tprCodOriPensInp;
    }

    public Integer getTprId() {
        return tprId;
    }


    public Integer getTprTipPnsCod() {
        return tprTipPnsCod;
    }

    public void setTprTipPnsCod(Integer tprTipPnsCod) {
        this.tprTipPnsCod = tprTipPnsCod;
    }

    public Origen getTbPrTblOrigen1() {
        return tbPrTblOrigen1;
    }

    public void setTbPrTblOrigen1(Origen tbPrTblOrigen1) {
        this.tbPrTblOrigen1 = tbPrTblOrigen1;
    }
}
