package cl.gob.ips.exencion.service;


import cl.gob.ips.exencion.dao.GrupoUsuarioUsuariosDao;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.GrupoUsuarioUsuarios;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Created by practicante
 *
 */
@Stateless
@LocalBean
public class GrupoUsuarioUsuariosService {

    @EJB
    GrupoUsuarioUsuariosDao dao;

    /**
     */

    public void createGrupoUsuarioUsuarios(final GrupoUsuarioUsuarios grupoUsuarioUsuarios) throws DataAccessException {
        dao.crearGrupoUsuarioUsuarios(grupoUsuarioUsuarios);
    }

    /**
     *
     * @param grupoUsuarioUsuarios
     * @throws Exception
     */

    public void eliminarGrupoUsuarioUsuarios(GrupoUsuarioUsuarios grupoUsuarioUsuarios) throws DataAccessException {
        dao.deleteGrupoUsuarioUsuarios(grupoUsuarioUsuarios);
    }


}