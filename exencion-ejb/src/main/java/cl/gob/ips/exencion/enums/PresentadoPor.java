package cl.gob.ips.exencion.enums;

public enum PresentadoPor {

	RUN(0, "Run (&uacute;nicos)"),
	PENSIONES(1, "Pensiones");

	private Integer codigo;
	private String descripcion;

	private PresentadoPor(Integer codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public static PresentadoPor getInstance(Integer codigo) {
		for (PresentadoPor itemEnum : values()) {
			if (itemEnum.getCodigo() == codigo) {
				return itemEnum;
			}
		}
		return null;
	}

	public Integer getCodigo() {
		return this.codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
}
