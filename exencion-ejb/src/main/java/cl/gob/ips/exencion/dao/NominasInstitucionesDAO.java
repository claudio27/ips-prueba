package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.model.*;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by lpino on 07-07-17.
 */
@Stateless
@LocalBean
public class NominasInstitucionesDAO extends BaseDAO {

    public void generarNominas(Integer periodo) throws DataAccessException {

        super.em.createNativeQuery("{call PRC_GENERAR_NOMINAS(?)}") // FIXME No existe este sp
                .setParameter(1, periodo);
        super.em.flush();
    }

    public Integer obtenerEstadoValidacionNominas() throws DataAccessException {
        Integer estado = Integer.valueOf(0);
        TypedQuery<Integer> tp = null;

        tp = super.em.createQuery("SELECT mant.mantEjecValIni1 FROM " + MantencionEjecucion.class.getName() + " mant "
                + " WHERE mant.mantEjecId = :id", Integer.class).setParameter("id", 7);

        if (!tp.getResultList().isEmpty()) {
            estado = tp.getSingleResult();
        }

        return estado;
    }

    public boolean verificaEstadoArchivoRebajaAPS(final Integer periodo) throws DataAccessException {

        Long estadoCarga = Long.valueOf(0);

        estadoCarga = (Long) super.em.createQuery("SELECT count(arch.arcId) from " + ArchivosCargaPeriodo.class.getName() + " arch  " +
                "WHERE arch.tbPrArchivosIniciales.arcIniId = :archivo AND arch.tbPrEstCarga.estCarId = :estado " +
                " AND arch.tbPeriodo1.perPeriodo = :periodo")
                .setParameter("archivo", 15)
                .setParameter("estado", 2)
                .setParameter("periodo", periodo)
                .getSingleResult();


        return estadoCarga == 0 ? false : true;
    }

    public List<NominaBeneficiario> obtenerNominaBeneficiario(
            Integer periodo, Integer rutInstitucion) throws IpsBussinesException, DataAccessException {

        //SELECT * FROM TB_NOMINA_BENEFICIARIO WHERE NOM_PERIODO = ? AND NOM_RUT_INSTITUCION = ?;
        List<NominaBeneficiario> nominas = super.em.createQuery("SELECT nom FROM " + NominaBeneficiario.class.getName() + " nom "
                + "WHERE nom.periodo.perPeriodo= :periodo AND" + " nom.instituciones.instRut= :rutInstitucion ", NominaBeneficiario.class)
                .setParameter("periodo", periodo).setParameter("rutInstitucion", rutInstitucion).getResultList();

        return nominas;
    }


    public List<NominaBeneficiario> obtenerNominaBeneficiario(
            Integer periodo) throws IpsBussinesException, DataAccessException {

        //SELECT * FROM TB_NOMINA_BENEFICIARIO WHERE NOM_PERIODO = ? AND NOM_RUT_INSTITUCION = ?;
        List<NominaBeneficiario> nominas = super.em.createQuery("SELECT nom FROM " + NominaBeneficiario.class.getName() + " nom "
                + "WHERE nom.periodo.perPeriodo= :periodo", NominaBeneficiario.class)
                .setParameter("periodo", periodo)
                .getResultList();

        return nominas;
    }

    public List<NominaBphSinPbs> obtenerNominaBphSinPbs(
            Integer periodo, Integer rutInstitucion) throws DataAccessException {

        List<NominaBphSinPbs> nominas = super.em.createQuery("SELECT nom FROM " + NominaBphSinPbs.class.getName() + " nom "
                + "WHERE nom.periodo.perPeriodo= :periodo AND" + " nom.instituciones.instRut= :rutInstitucion ", NominaBphSinPbs.class)
                .setParameter("periodo", periodo).setParameter("rutInstitucion", rutInstitucion).getResultList();

        return nominas;
    }

    public List<NominaBphSinPbs> obtenerNominaBphSinPbs(
            Integer periodo) throws DataAccessException {

        List<NominaBphSinPbs> nominas = super.em.createQuery("SELECT nom FROM " + NominaBphSinPbs.class.getName() + " nom "
                + "WHERE nom.periodo.perPeriodo= :periodo", NominaBphSinPbs.class)
                .setParameter("periodo", periodo)
                .getResultList();

        return nominas;
    }

    public List<NominaConsultaPdi> obtenerNominaConsultaPdi(
            Integer periodo, Integer rutInstitucion) throws DataAccessException {

        List<NominaConsultaPdi> nominas = super.em.createQuery("SELECT nom FROM " + NominaConsultaPdi.class.getName() + " nom "
                + "WHERE nom.periodo.perPeriodo= :periodo AND" + " nom.instituciones.instRut= :rutInstitucion ", NominaConsultaPdi.class)
                .setParameter("periodo", periodo).setParameter("rutInstitucion", rutInstitucion).getResultList();

        return nominas;
    }

    public List<NominaConsultaPdi> obtenerNominaConsultaPdi(
            Integer periodo) throws DataAccessException {

        List<NominaConsultaPdi> nominas = super.em.createQuery("SELECT nom FROM " + NominaConsultaPdi.class.getName() + " nom "
                + "WHERE nom.periodo.perPeriodo= :periodo", NominaConsultaPdi.class)
                .setParameter("periodo", periodo)
                .getResultList();

        return nominas;
    }

    public List<NominaPenSinFicha> obtenerNominaPenSinFicha(Integer periodo) throws DataAccessException {

                List<NominaPenSinFicha> nominas = super.em.createNativeQuery("select  max(NOM_ID) NOM_ID, \n" +
                        "\n" +
                        "        max(NOM_PERIODO) NOM_PERIODO, \n" +
                        "        max(NOM_RUT_INSTITUCION) NOM_RUT_INSTITUCION,\n" +
                        "        max(NOM_DV_INSTITUCION) NOM_DV_INSTITUCION, \n" +
                        "        NOM_RUN_BENEFICIARIO, \n" +
                        "        NOM_DV_BENEFICIARIO\n" +
                        "\n" +
                        "from TB_NOMINA_PEN_SIN_FICHA\n" +
                        "where NOM_PERIODO = ?periodo\n" +
                        "group by \n" +
                        "        NOM_RUN_BENEFICIARIO, \n" +
                        "        NOM_DV_BENEFICIARIO", NominaPenSinFicha.class).setParameter("periodo", periodo).getResultList();

//        List<NominaPenSinFicha> nominas = super.em.createQuery("SELECT nom FROM " + NominaPenSinFicha.class.getName() + " nom "
//                + "WHERE nom.periodo.perPeriodo= :periodo ", NominaPenSinFicha.class)
//                .setParameter("periodo", periodo).getResultList();

        return nominas;
    }

    public List<NominaPotencialesAps> obtenerNominaPotencionalesAps(
            Integer periodo, Integer rutInstitucion) throws DataAccessException {

        List<NominaPotencialesAps> nominas = super.em.createQuery("SELECT nom FROM " + NominaPotencialesAps.class.getName() + " nom "
                + "WHERE nom.periodo.perPeriodo= :periodo AND" + " nom.instituciones.instRut= :rutInstitucion ", NominaPotencialesAps.class)
                .setParameter("periodo", periodo).setParameter("rutInstitucion", rutInstitucion).getResultList();

        return nominas;
    }

    public List<NominaPotencialesAps> obtenerNominaPotencionalesAps(
            Integer periodo) throws DataAccessException {

        List<NominaPotencialesAps> nominas = super.em.createQuery("SELECT nom FROM " + NominaPotencialesAps.class.getName() + " nom "
                + "WHERE nom.periodo.perPeriodo= :periodo AND" + " nom.instituciones.instRut= :rutInstitucion ", NominaPotencialesAps.class)
                .setParameter("periodo", periodo)
                .getResultList();

        return nominas;
    }

    public List<NominaPotBeneficiario> obtenerNominaPotBeneficiario(
            Integer periodo, Integer rutInstitucion) throws DataAccessException {

        List<NominaPotBeneficiario> nominas = super.em.createQuery("SELECT nom FROM " + NominaPotBeneficiario.class.getName() + " nom "
                + "WHERE nom.periodo.perPeriodo= :periodo AND" + " nom.instituciones.instRut= :rutInstitucion ", NominaPotBeneficiario.class)
                .setParameter("periodo", periodo).setParameter("rutInstitucion", rutInstitucion).getResultList();

        return nominas;
    }

    public List<NominaPotBeneficiario> obtenerNominaPotBeneficiario(
            Integer periodo) throws DataAccessException {

        List<NominaPotBeneficiario> nominas = super.em.createQuery("SELECT nom FROM " + NominaPotBeneficiario.class.getName() + " nom "
                + "WHERE nom.periodo.perPeriodo= :periodo", NominaPotBeneficiario.class)
                .setParameter("periodo", periodo)
                .getResultList();

        return nominas;
    }

    public List<NominaRebaja> obtenerNominaRebaja(
            Integer periodo) throws DataAccessException {

        List<NominaRebaja> nominas = super.em.createQuery("SELECT nom FROM " + NominaRebaja.class.getName() + " nom "
                + "WHERE nom.periodo.perPeriodo= :periodo ", NominaRebaja.class)
                .setParameter("periodo", periodo).getResultList();

        return nominas;
    }

    public List<NominaValidacion> obtenerNominaValidacion(Integer periodo) throws DataAccessException {

        List<NominaValidacion> nominas = super.em.createQuery("SELECT nom FROM " + NominaValidacion.class.getName() + " nom "
                + "WHERE nom.periodo.perPeriodo= :periodo ", NominaValidacion.class)
                .setParameter("periodo", periodo).getResultList();

        return nominas;
    }
}
