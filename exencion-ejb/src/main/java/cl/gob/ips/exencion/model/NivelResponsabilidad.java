package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_NIVEL_RESPONSABILIDAD_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_NIVEL_RESPONSABILIDAD_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = NivelResponsabilidad.QUERY_FIND_ALL, query = "select o from NivelResponsabilidad o ORDER BY o.nivelCod ASC") })
@Table(name = "TB_PR_NIVEL_RESPONSABILIDAD")
@SequenceGenerator(name = "NivelResponsabilidad_Id_Seq_Gen", sequenceName = "TB_PR_NIVEL_RESPONSABILIDAD_ID_SEQ_GEN",
                   allocationSize = 50, initialValue = 50)
public class NivelResponsabilidad implements Serializable {
    private static final long serialVersionUID = -764071101886810258L;
    public static final String QUERY_FIND_ALL = "NivelResponsabilidad.findAll";

    @Id
    @Column(name = "NIVEL_COD", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NivelResponsabilidad_Id_Seq_Gen")
    private Integer nivelCod;
    @OneToMany(mappedBy = "tbPrNivelResponsabilidad", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<UsuarioGrupo> tbUsuGrupoList2;

    public NivelResponsabilidad() {
    }

    public NivelResponsabilidad(Integer nivelCod) {
        this.nivelCod = nivelCod;
    }

    public Integer getNivelCod() {
        return nivelCod;
    }

    public List<UsuarioGrupo> getTbUsuGrupoList2() {
        return tbUsuGrupoList2;
    }

    public void setTbUsuGrupoList2(List<UsuarioGrupo> tbUsuGrupoList2) {
        this.tbUsuGrupoList2 = tbUsuGrupoList2;
    }

    public UsuarioGrupo addUsuarioGrupo(UsuarioGrupo usuarioGrupo) {
        getTbUsuGrupoList2().add(usuarioGrupo);
        usuarioGrupo.setTbPrNivelResponsabilidad(this);
        return usuarioGrupo;
    }

    public UsuarioGrupo removeUsuarioGrupo(UsuarioGrupo usuarioGrupo) {
        getTbUsuGrupoList2().remove(usuarioGrupo);
        usuarioGrupo.setTbPrNivelResponsabilidad(null);
        return usuarioGrupo;
    }
}
