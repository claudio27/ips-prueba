package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQueries({ @NamedQuery(name = "BphHistorico.findAll", query = "select o from BphHistorico o") })
@Table(name = "TB_BPH_HIS")
public class BphHistorico implements Serializable {
    private static final long serialVersionUID = 3165483498712478225L;
    @Column(name = "BPH_ELIMINADO")
    private Integer bphEliminado;
    @Temporal(TemporalType.DATE)
    @Column(name = "BPH_FEC_ELIMINADO")
    private Date bphFecEliminado;
    @Id
    @Column(name = "BPH_ID", nullable = false)
    private Long bphId;
    @Column(name = "BPH_RUN_BENEFICIARIO", nullable = false)
    private Integer bphRunBeneficiario;
    @Column(name = "BPH_USU_ELIMINADO", length = 20)
    private String bphUsuEliminado;
    @ManyToOne
    @JoinColumn(name = "BPH_ARC_ID")
    private ArchivosCargaPeriodo archivosCargaPeriodo3;

    public BphHistorico() {
    }

    public BphHistorico(ArchivosCargaPeriodo archivosCargaPeriodo3, Integer bphEliminado, Date bphFecEliminado,
                        Long bphId, Integer bphRunBeneficiario, String bphUsuEliminado) {
        this.archivosCargaPeriodo3 = archivosCargaPeriodo3;
        this.bphEliminado = bphEliminado;
        this.bphFecEliminado = bphFecEliminado;
        this.bphId = bphId;
        this.bphRunBeneficiario = bphRunBeneficiario;
        this.bphUsuEliminado = bphUsuEliminado;
    }


    public Integer getBphEliminado() {
        return bphEliminado;
    }

    public void setBphEliminado(Integer bphEliminado) {
        this.bphEliminado = bphEliminado;
    }

    public Date getBphFecEliminado() {
        return bphFecEliminado;
    }

    public void setBphFecEliminado(Date bphFecEliminado) {
        this.bphFecEliminado = bphFecEliminado;
    }

    public Long getBphId() {
        return bphId;
    }

    public void setBphId(Long bphId) {
        this.bphId = bphId;
    }

    public Integer getBphRunBeneficiario() {
        return bphRunBeneficiario;
    }

    public void setBphRunBeneficiario(Integer bphRunBeneficiario) {
        this.bphRunBeneficiario = bphRunBeneficiario;
    }

    public String getBphUsuEliminado() {
        return bphUsuEliminado;
    }

    public void setBphUsuEliminado(String bphUsuEliminado) {
        this.bphUsuEliminado = bphUsuEliminado;
    }

    public ArchivosCargaPeriodo getArchivosCargaPeriodo3() {
        return archivosCargaPeriodo3;
    }

    public void setArchivosCargaPeriodo3(ArchivosCargaPeriodo archivosCargaPeriodo3) {
        this.archivosCargaPeriodo3 = archivosCargaPeriodo3;
    }
}
