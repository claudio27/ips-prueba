package cl.gob.ips.exencion.dto;

import cl.alaya.component.utils.DateUtil;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EventosProcesosSistemaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2301337585125391662L;

	private String fecha;
	private String descripcion;
	private String responsables;
	private String tipoEvento;
	private String destinatarioNotificacion;
	private String estadoNotificacion;

	public EventosProcesosSistemaDTO() {
		super();
	}

	public EventosProcesosSistemaDTO(String fecha, String descripcion,
			String responsables, String tipoEvento,
			String destinatarioNotificacion, String estadoNotificacion) {
		super();
		this.fecha = fecha;
		this.descripcion = descripcion;
		this.responsables = responsables;
		this.tipoEvento = tipoEvento;
		this.destinatarioNotificacion = destinatarioNotificacion;
		this.estadoNotificacion = estadoNotificacion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getResponsables() {
		return responsables;
	}

	public void setResponsables(String responsables) {
		this.responsables = responsables;
	}

	public String getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public String getDestinatarioNotificacion() {
		return destinatarioNotificacion;
	}

	public void setDestinatarioNotificacion(String destinatarioNotificacion) {
		this.destinatarioNotificacion = destinatarioNotificacion;
	}

	public String getEstadoNotificacion() {
		return estadoNotificacion;
	}

	public void setEstadoNotificacion(String estadoNotificacion) {
		this.estadoNotificacion = estadoNotificacion;
	}

}
