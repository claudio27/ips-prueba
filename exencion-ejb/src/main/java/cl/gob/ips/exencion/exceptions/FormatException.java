package cl.gob.ips.exencion.exceptions;

/**
 * Created by rodrigo.reyesco@gmail.com on 19-10-2016.
 */
public class FormatException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5399211394556848972L;

	public FormatException() {
    }

    public FormatException(String message) {
        super(message);
    }

    public FormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public FormatException(Throwable cause) {
        super(cause);
    }

    public FormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
