package cl.gob.ips.exencion.enums;

/**
 * Created by alaya on 17/07/17.
 */
public enum TipoResidenciaAcreditado {

    SIN_INFO(0, "Sin información"),
    ACREDITADO(1, "Acreditado proceso anterior"),
    SOLICITUD(2, "Solicitud"),
    DECLARACION_JURADA(3, "Declaración Jurada"),
    COTIZACIONES(4, "Cotizaciones");

    private Integer codigo;
    private String descripcion;

    TipoResidenciaAcreditado(Integer codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static TipoResidenciaAcreditado getInstance(Integer codigo) {
        for (TipoResidenciaAcreditado itemEnum : values()) {
            if (itemEnum.getCodigo() == codigo) {
                return itemEnum;
            }
        }
        return null;
    }

    public Integer getCodigo() {
        return this.codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
