package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Pensionados;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class PensionadosDAO extends BaseDAO {

    public List<Pensionados> obtenerPensionadoPorRun(final Long penRun) throws DataAccessException {
        List<Pensionados> pensionado = null;
        TypedQuery<Pensionados> query = super.em.createNamedQuery(Pensionados.QUERY_FIND_BY_RUN, Pensionados.class);
        query.setParameter("penRun", penRun);

        if (!query.getResultList().isEmpty()){
            pensionado = query.getResultList();
        }
        //super.em.flush();
        return pensionado;
    }

    public List<Pensionados> obtenerPensionadoPorRunPeriodo(final Long penRun, final Integer periodoDesde, final Integer periodoHasta ) throws DataAccessException {
        List<Pensionados> pensionado = (List<Pensionados>) super.em.createNamedQuery(Pensionados.QUERY_FIND_BY_RUN_PERIODO)
                .setParameter("run", penRun)
                .setParameter("periodoDesde",periodoDesde)
                .setParameter("periodoHasta",periodoHasta).getResultList();
        super.em.flush();
        return pensionado;
    }

    //TODO implementar logica
    public Pensionados obtenerPensionado(){
        return null;
    }

    //TODO implementar logica
    public List<Pensionados> obtenerPensionados() {
        return null;
    }

    //TODO implementar logica
    public List<Pensionados> obtenerPensionadosPeriodo() {
        return null;
    }
}
