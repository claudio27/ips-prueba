package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_GRUPOS_ARCHIVO_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_GRUPOS_ARCHIVO_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@Table(name = "TB_PR_GRUPOS_ARCHIVO")
@SequenceGenerator(name = "GruposArchivo_Id_Seq_Gen", sequenceName = "TB_PR_GRUPOS_ARCHIVO_ID_SEQ_GEN",
        allocationSize = 50, initialValue = 50)
@NamedQueries({ @NamedQuery(name = GruposArchivo.QUERY_FIND_ALL, query = "SELECT o from cl.gob.ips.exencion.model.GruposArchivo o ORDER BY o.grupArchGlosa ASC") })
public class GruposArchivo implements Serializable {
    private static final long serialVersionUID = -4976548241160996210L;

    public static final String QUERY_FIND_ALL = "GruposArchivo.findAll";

    @Id
    @Column(name = "GRUP_ARCH_COD", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GruposArchivo_Id_Seq_Gen")
    private Integer grupArchCod;
    @Column(name = "GRUP_ARCH_GLOSA", nullable = false, length = 20)
    private String grupArchGlosa;
    @OneToMany(mappedBy = "tbPrGruposArchivo", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<ArchivosIniciales> tbPrArchivosInicialesList;
    @OneToMany(mappedBy = "tbPrGruposArchivo1", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<UsuarioGrupo> tbUsuGrupoList;

    public GruposArchivo() {
    }

    public GruposArchivo(Integer grupArchCod, String grupArchGlosa) {
        this.grupArchCod = grupArchCod;
        this.grupArchGlosa = grupArchGlosa;
    }

    public Integer getGrupArchCod() {
        return grupArchCod;
    }

    public String getGrupArchGlosa() {
        return grupArchGlosa;
    }

    public void setGrupArchGlosa(String grupArchGlosa) {
        this.grupArchGlosa = grupArchGlosa;
    }

    public List<ArchivosIniciales> getTbPrArchivosInicialesList() {
        return tbPrArchivosInicialesList;
    }

    public void setTbPrArchivosInicialesList(List<ArchivosIniciales> tbPrArchivosInicialesList) {
        this.tbPrArchivosInicialesList = tbPrArchivosInicialesList;
    }

    public ArchivosIniciales addArchivosIniciales(ArchivosIniciales archivosIniciales) {
        getTbPrArchivosInicialesList().add(archivosIniciales);
        archivosIniciales.setTbPrGruposArchivo(this);
        return archivosIniciales;
    }

    public ArchivosIniciales removeArchivosIniciales(ArchivosIniciales archivosIniciales) {
        getTbPrArchivosInicialesList().remove(archivosIniciales);
        archivosIniciales.setTbPrGruposArchivo(null);
        return archivosIniciales;
    }

    public List<UsuarioGrupo> getTbUsuGrupoList() {
        return tbUsuGrupoList;
    }

    public void setTbUsuGrupoList(List<UsuarioGrupo> tbUsuGrupoList) {
        this.tbUsuGrupoList = tbUsuGrupoList;
    }

    public UsuarioGrupo addUsuarioGrupo(UsuarioGrupo usuarioGrupo) {
        getTbUsuGrupoList().add(usuarioGrupo);
        usuarioGrupo.setTbPrGruposArchivo1(this);
        return usuarioGrupo;
    }

    public UsuarioGrupo removeUsuarioGrupo(UsuarioGrupo usuarioGrupo) {
        getTbUsuGrupoList().remove(usuarioGrupo);
        usuarioGrupo.setTbPrGruposArchivo1(null);
        return usuarioGrupo;
    }
}
