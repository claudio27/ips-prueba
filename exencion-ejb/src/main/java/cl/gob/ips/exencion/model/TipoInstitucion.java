package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_TIPO_INSTITUCION_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_TIPO_INSTITUCION_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = TipoInstitucion.QUERY_FIND_ALL, query = "select o from TipoInstitucion o") })
@Table(name = "TB_PR_TIPO_INSTITUCION")
@SequenceGenerator(name = "TipoInstitucion_Id_Seq_Gen", sequenceName = "TB_PR_TIPO_INSTITUCION_ID_SEQ_GEN",
                   allocationSize = 50, initialValue = 50)
public class TipoInstitucion implements Serializable {
    private static final long serialVersionUID = 7751721099952593277L;

    public static final String QUERY_FIND_ALL = "TipoInstitucion.findAll";

    @Id
    @Column(name = "TIP_INST_COD", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipoInstitucion_Id_Seq_Gen")
    private Integer tipInstCod;
    @Column(name = "TIP_INST_DESCRIPCION", nullable = false, length = 50)
    private String tipInstDescripcion;
    @OneToMany(mappedBy = "tbPrTipoInstitucion", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Instituciones> tbPrInstitucionesList1;
    @OneToMany(mappedBy = "tbPrTipoInstitucion1", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<BenregHis> tbBenregHisList2;
    @OneToMany(mappedBy = "tbPrTipoInstitucion2", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<BenregAct> tbBenregActList2;

    public TipoInstitucion() {
    }

    public TipoInstitucion(Integer tipInstCod, String tipInstDescripcion) {
        this.tipInstCod = tipInstCod;
        this.tipInstDescripcion = tipInstDescripcion;
    }

    public Integer getTipInstCod() {
        return tipInstCod;
    }

    public String getTipInstDescripcion() {
        return tipInstDescripcion;
    }

    public void setTipInstCod(Integer tipInstCod) {
        this.tipInstCod = tipInstCod;
    }

    public void setTipInstDescripcion(String tipInstDescripcion) {
        this.tipInstDescripcion = tipInstDescripcion;
    }

    public List<Instituciones> getTbPrInstitucionesList1() {
        return tbPrInstitucionesList1;
    }

    public void setTbPrInstitucionesList1(List<Instituciones> tbPrInstitucionesList1) {
        this.tbPrInstitucionesList1 = tbPrInstitucionesList1;
    }

    public Instituciones addInstituciones(Instituciones instituciones) {
        getTbPrInstitucionesList1().add(instituciones);
        instituciones.setTbPrTipoInstitucion(this);
        return instituciones;
    }

    public Instituciones removeInstituciones(Instituciones instituciones) {
        getTbPrInstitucionesList1().remove(instituciones);
        instituciones.setTbPrTipoInstitucion(null);
        return instituciones;
    }

    public List<BenregHis> getTbBenregHisList2() {
        return tbBenregHisList2;
    }

    public void setTbBenregHisList2(List<BenregHis> tbBenregHisList2) {
        this.tbBenregHisList2 = tbBenregHisList2;
    }

    public BenregHis addBenregHis(BenregHis benregHis) {
        getTbBenregHisList2().add(benregHis);
        benregHis.setTbPrTipoInstitucion1(this);
        return benregHis;
    }

    public BenregHis removeBenregHis(BenregHis benregHis) {
        getTbBenregHisList2().remove(benregHis);
        benregHis.setTbPrTipoInstitucion1(null);
        return benregHis;
    }

    public List<BenregAct> getTbBenregActList2() {
        return tbBenregActList2;
    }

    public void setTbBenregActList2(List<BenregAct> tbBenregActList2) {
        this.tbBenregActList2 = tbBenregActList2;
    }

    public BenregAct addBenregAct(BenregAct benregAct) {
        getTbBenregActList2().add(benregAct);
        benregAct.setTbPrTipoInstitucion2(this);
        return benregAct;
    }

    public BenregAct removeBenregAct(BenregAct benregAct) {
        getTbBenregActList2().remove(benregAct);
        benregAct.setTbPrTipoInstitucion2(null);
        return benregAct;
    }
}
