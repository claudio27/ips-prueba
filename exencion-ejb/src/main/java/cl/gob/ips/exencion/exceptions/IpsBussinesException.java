package cl.gob.ips.exencion.exceptions;

import javax.ejb.EJBException;
import javax.ejb.EJBTransactionRolledbackException;

/**
 * Created by lpino on 24-07-17.
 */
public class IpsBussinesException extends EJBException {

    /**
     *
     */
    private static final long serialVersionUID = 193382527286472928L;

    public IpsBussinesException() {
    }

    public IpsBussinesException(String message) {
        super(message);
    }

}
