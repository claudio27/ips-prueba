package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_ARCHIVOS_INICIALES_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_ARCHIVOS_INICIALES_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "ArchivosIniciales.findAll", query = "select o from ArchivosIniciales o") })
@Table(name = "TB_PR_ARCHIVOS_INICIALES")
@SequenceGenerator(name = "ArchivosIniciales_Id_Seq_Gen", sequenceName = "TB_PR_ARCHIVOS_INICIALES_ID_SEQ_GEN",
                   allocationSize = 50, initialValue = 50)
public class ArchivosIniciales implements Serializable {
    private static final long serialVersionUID = 1408190095436191052L;
    @Column(name = "ARC_INI_EXTENCION", length = 10)
    private String arcIniExtencion;
    @Id
    @Column(name = "ARC_INI_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ArchivosIniciales_Id_Seq_Gen")
    private Integer arcIniId;
    @Column(name = "ARC_INI_NOM_ARCHIVO", nullable = false, length = 50)
    private String arcIniNomArchivo;
    @Column(name = "ARC_INI_NOMBRE", nullable = false, length = 50)
    private String arcIniNombre;
    @Column(name = "ARC_INI_OBLIGATOIRIO", nullable = false)
    private Integer arcIniObligatoirio;
    @Column(name = "ARC_INI_TIPO", nullable = false, length = 1)
    private String arcIniTipo;
    @ManyToOne
    @JoinColumn(name = "ARC_INI_GRUP_ARCH_COD")
    private GruposArchivo tbPrGruposArchivo;
    @OneToMany(mappedBy = "tbPrArchivosIniciales", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<ArchivosCargaPeriodo> archivosCargaPeriodoList2;

    public ArchivosIniciales() {
    }

    public ArchivosIniciales(String arcIniExtencion, GruposArchivo tbPrGruposArchivo, Integer arcIniId,
                             String arcIniNomArchivo, String arcIniNombre, Integer arcIniObligatoirio,
                             String arcIniTipo) {
        this.arcIniExtencion = arcIniExtencion;
        this.tbPrGruposArchivo = tbPrGruposArchivo;
        this.arcIniId = arcIniId;
        this.arcIniNomArchivo = arcIniNomArchivo;
        this.arcIniNombre = arcIniNombre;
        this.arcIniObligatoirio = arcIniObligatoirio;
        this.arcIniTipo = arcIniTipo;
    }

    public String getArcIniExtencion() {
        return arcIniExtencion;
    }

    public void setArcIniExtencion(String arcIniExtencion) {
        this.arcIniExtencion = arcIniExtencion;
    }


    public Integer getArcIniId() {
        return arcIniId;
    }

    public String getArcIniNomArchivo() {
        return arcIniNomArchivo;
    }

    public void setArcIniNomArchivo(String arcIniNomArchivo) {
        this.arcIniNomArchivo = arcIniNomArchivo;
    }

    public String getArcIniNombre() {
        return arcIniNombre;
    }

    public void setArcIniNombre(String arcIniNombre) {
        this.arcIniNombre = arcIniNombre;
    }

    public Integer getArcIniObligatoirio() {
        return arcIniObligatoirio;
    }

    public void setArcIniObligatoirio(Integer arcIniObligatoirio) {
        this.arcIniObligatoirio = arcIniObligatoirio;
    }

    public String getArcIniTipo() {
        return arcIniTipo;
    }

    public void setArcIniTipo(String arcIniTipo) {
        this.arcIniTipo = arcIniTipo;
    }

    public GruposArchivo getTbPrGruposArchivo() {
        return tbPrGruposArchivo;
    }

    public void setTbPrGruposArchivo(GruposArchivo tbPrGruposArchivo) {
        this.tbPrGruposArchivo = tbPrGruposArchivo;
    }

    public List<ArchivosCargaPeriodo> getArchivosCargaPeriodoList2() {
        return archivosCargaPeriodoList2;
    }

    public void setArchivosCargaPeriodoList2(List<ArchivosCargaPeriodo> archivosCargaPeriodoList2) {
        this.archivosCargaPeriodoList2 = archivosCargaPeriodoList2;
    }

    public ArchivosCargaPeriodo addArchivosCargaPeriodo(ArchivosCargaPeriodo archivosCargaPeriodo) {
        getArchivosCargaPeriodoList2().add(archivosCargaPeriodo);
        archivosCargaPeriodo.setTbPrArchivosIniciales(this);
        return archivosCargaPeriodo;
    }

    public ArchivosCargaPeriodo removeArchivosCargaPeriodo(ArchivosCargaPeriodo archivosCargaPeriodo) {
        getArchivosCargaPeriodoList2().remove(archivosCargaPeriodo);
        archivosCargaPeriodo.setTbPrArchivosIniciales(null);
        return archivosCargaPeriodo;
    }
}
