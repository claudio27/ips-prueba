package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.NominasInstitucionesDAO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.model.*;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by lpino on 07-07-17.
 */
public class NominasInstitucionesServices {

    @Inject
    private NominasInstitucionesDAO nominasInstitucionesDAO;

    public void generarNominas(Integer periodo) throws DataAccessException {
        nominasInstitucionesDAO.generarNominas(periodo);
    }

    public Integer obtenerEstadoValidacionNominas() throws DataAccessException {
        return nominasInstitucionesDAO.obtenerEstadoValidacionNominas();
    }

    public boolean verificaCargaArchivo(Integer periodo) throws DataAccessException {
        return nominasInstitucionesDAO.verificaEstadoArchivoRebajaAPS(periodo);
    }

    public List<NominaBeneficiario> obtenerNominaBeneficiario(
            Integer periodo, Integer rutInstitucion) throws IpsBussinesException, DataAccessException {
        return nominasInstitucionesDAO.obtenerNominaBeneficiario(periodo, rutInstitucion);
    }

    public List<NominaBeneficiario> obtenerNominaBeneficiario(
            Integer periodo) throws IpsBussinesException, DataAccessException {
        return nominasInstitucionesDAO.obtenerNominaBeneficiario(periodo);
    }

    public List<NominaBphSinPbs> obtenerNominaBphSinPbs(
            Integer periodo, Integer rutInstitucion) throws DataAccessException {
        return nominasInstitucionesDAO.obtenerNominaBphSinPbs(periodo, rutInstitucion);
    }


    public List<NominaBphSinPbs> obtenerNominaBphSinPbs(
            Integer periodo) throws DataAccessException {
        return nominasInstitucionesDAO.obtenerNominaBphSinPbs(periodo);
    }

    public List<NominaConsultaPdi> obtenerNominaConsultaPdi(
            Integer periodo, Integer rutInstitucion) throws DataAccessException {
        return nominasInstitucionesDAO.obtenerNominaConsultaPdi(periodo, rutInstitucion);
    }

    public List<NominaConsultaPdi> obtenerNominaConsultaPdi(
            Integer periodo) throws DataAccessException {
        return nominasInstitucionesDAO.obtenerNominaConsultaPdi(periodo);
    }

    public List<NominaPenSinFicha> obtenerNominaPenSinFicha(
            Integer periodo) throws DataAccessException {
        return nominasInstitucionesDAO.obtenerNominaPenSinFicha(periodo);
    }

    public List<NominaPotencialesAps> obtenerNominaPotencialesAps(
            Integer periodo, Integer rutInstitucion) throws DataAccessException {
        return nominasInstitucionesDAO.obtenerNominaPotencionalesAps(periodo, rutInstitucion);
    }

    public List<NominaPotencialesAps> obtenerNominaPotencialesAps(
            Integer periodo) throws DataAccessException {
        return nominasInstitucionesDAO.obtenerNominaPotencionalesAps(periodo);
    }

    public List<NominaPotBeneficiario> obtenerNominaPotBeneficiario(
            Integer periodo, Integer rutInstitucion) throws DataAccessException {
        return nominasInstitucionesDAO.obtenerNominaPotBeneficiario(periodo, rutInstitucion);
    }

    public List<NominaPotBeneficiario> obtenerNominaPotBeneficiario(
            Integer periodo) throws DataAccessException {
        return nominasInstitucionesDAO.obtenerNominaPotBeneficiario(periodo);
    }

    public List<NominaRebaja> obtenerNominaRebaja(
            Integer periodo) throws DataAccessException {
        return nominasInstitucionesDAO.obtenerNominaRebaja(periodo);
    }

    public List obtenerNominaValidacion(Integer periodo) throws DataAccessException {
        return nominasInstitucionesDAO.obtenerNominaValidacion(periodo);
    }
}
