package cl.gob.ips.exencion.enums;

public enum TipoNomina {

	BENEFICIARIOS(0, "Beneficiarios"),
	POTENCIALES_BENEFICIARIOS(1, "Potenciales Beneficiarios"),
	SIN_PBS(2, "Sin PBS"),
	POTENCIALES_APS(3, "Potenciales APS"),
	SIN_FICHA(4, "Sin Ficha"),
	CONSULTAPDI(5, "Consulta PDI"),
	REBAJA(6, "Rebaja");
			
	private Integer codigo;
	
	private String descripcion;

	private TipoNomina(Integer codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public static TipoNomina getInstance(Integer codigo) {
		for (TipoNomina itemEnum : values()) {
			if (itemEnum.getCodigo() == codigo) {
				return itemEnum;
			}
		}
		return null;
	}

	public Integer getCodigo() {
		return this.codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
}
