package cl.gob.ips.exencion.service;


import cl.gob.ips.exencion.dao.GrupoUsuarioFuncionesDao;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.GrupoUsuarioFunciones;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Created by practicante
 *
 */
@Stateless
@LocalBean
public class GrupoUsuarioFuncionesService {

    @EJB
    GrupoUsuarioFuncionesDao dao;

    /**
     */

    public void createGrupoUsuarioFunciones(final GrupoUsuarioFunciones grupoUsuarioFunciones) throws DataAccessException {
        dao.crearGrupoUsuarioFunciones(grupoUsuarioFunciones);
    }

    /**
     *
     * @param grupoUsuarioFunciones
     * @throws Exception
     */

    public void eliminarGrupoUsuarioFunciones(GrupoUsuarioFunciones grupoUsuarioFunciones) throws DataAccessException {
        dao.deleteGrupoUsuarioFunciones(grupoUsuarioFunciones);
    }


}