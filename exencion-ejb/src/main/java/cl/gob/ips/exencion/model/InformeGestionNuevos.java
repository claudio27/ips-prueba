package cl.gob.ips.exencion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class InformeGestionNuevos implements Serializable {

	/**
	 * Created by practicante on 20-10-17.
	 */
	private static final long serialVersionUID = -4866961320730453142L;
	@Id
	@Column(name = "run")
	private String run;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "apellidoPaterno")
	private String apellidoPaterno;
	@Column(name = "apellidoMaterno")
	private String apellidoMaterno;
	@Column(name = "tipoPensionado")
	private String tipoPensionado;
	@Column(name = "institucion")
	private String institucion;

	public InformeGestionNuevos() {
		super();
	}

	public InformeGestionNuevos(String run, String nombre, String apellidoPaterno,
                                String apellidoMaterno, String tipoPensionado, String institucion) {
		super();

		this.run = run;
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.tipoPensionado = tipoPensionado;
		this.institucion = institucion;

	}

	public String getRun() {
		return run;
	}

	public void setRun(String run) {
		this.run = run;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getTipoPensionado() {
		return tipoPensionado;
	}

	public void setTipoPensionado(String tipoPensionado) {
		this.tipoPensionado = tipoPensionado;
	}

	public String getInstitucion() {
		return institucion;
	}

	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}

}
