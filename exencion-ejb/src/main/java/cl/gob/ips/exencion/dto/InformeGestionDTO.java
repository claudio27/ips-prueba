package cl.gob.ips.exencion.dto;

import java.io.Serializable;

public class InformeGestionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4866961320730453142L;

	private String periodo;
	private Integer ips;
	private Integer isl;
	private Integer bph;
	private Integer totalInternos;
	private Integer afp;
	private Integer csv;
	private Integer mutual;
	private Integer totalExternos;
	private Integer total;
	private Integer nuevos;
	private Integer extinciones;

	private Integer mantienen;
	private String acciones;
	private String run;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String tipoPensionado;
	private String institucion;
	private String motivoExtincion;
	private String tipoEntidad;

	public InformeGestionDTO() {
		super();
	}

	public InformeGestionDTO(String periodo, Integer ips, Integer isl,
			Integer bph, Integer totalInternos, Integer afp, Integer csv,
			Integer mutual, Integer totalExternos, Integer total,
			Integer nuevos, Integer extinciones, Integer mantienen,
			String acciones, String run, String nombre, String apellidoPaterno,
			String apellidoMaterno, String tipoPensionado, String institucion,
			String motivoExtincion, String tipoEntidad) {
		super();
		this.periodo = periodo;
		this.ips = ips;
		this.isl = isl;
		this.bph = bph;
		this.totalInternos = totalInternos;
		this.afp = afp;
		this.csv = csv;
		this.mutual = mutual;
		this.totalExternos = totalExternos;
		this.total = total;
		this.nuevos = nuevos;
		this.extinciones = extinciones;
		this.mantienen = mantienen;
		this.acciones = acciones;
		this.run = run;
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.tipoPensionado = tipoPensionado;
		this.institucion = institucion;
		this.motivoExtincion = motivoExtincion;
		this.tipoEntidad = tipoEntidad;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Integer getIps() {
		return ips;
	}

	public void setIps(Integer ips) {
		this.ips = ips;
	}

	public Integer getIsl() {
		return isl;
	}

	public void setIsl(Integer isl) {
		this.isl = isl;
	}

	public Integer getBph() {
		return bph;
	}

	public void setBph(Integer bph) {
		this.bph = bph;
	}

	public Integer getTotalInternos() {
		return totalInternos;
	}

	public void setTotalInternos(Integer totalInternos) {
		this.totalInternos = totalInternos;
	}

	public Integer getAfp() {
		return afp;
	}

	public void setAfp(Integer afp) {
		this.afp = afp;
	}

	public Integer getCsv() {
		return csv;
	}

	public void setCsv(Integer csv) {
		this.csv = csv;
	}

	public Integer getMutual() {
		return mutual;
	}

	public void setMutual(Integer mutual) {
		this.mutual = mutual;
	}

	public Integer getTotalExternos() {
		return totalExternos;
	}

	public void setTotalExternos(Integer totalExternos) {
		this.totalExternos = totalExternos;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getNuevos() {
		return nuevos;
	}

	public void setNuevos(Integer nuevos) {
		this.nuevos = nuevos;
	}

	public Integer getExtinciones() {
		return extinciones;
	}

	public void setExtinciones(Integer extinciones) {
		this.extinciones = extinciones;
	}

	public Integer getMantienen() {
		return mantienen;
	}

	public void setMantienen(Integer mantienen) {
		this.mantienen = mantienen;
	}

	public String getAcciones() {
		return acciones;
	}

	public void setAcciones(String acciones) {
		this.acciones = acciones;
	}

	public String getRun() {
		return run;
	}

	public void setRun(String run) {
		this.run = run;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getTipoPensionado() {
		return tipoPensionado;
	}

	public void setTipoPensionado(String tipoPensionado) {
		this.tipoPensionado = tipoPensionado;
	}

	public String getInstitucion() {
		return institucion;
	}

	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}

	public String getMotivoExtincion() {
		return motivoExtincion;
	}

	public void setMotivoExtincion(String motivoExtincion) {
		this.motivoExtincion = motivoExtincion;
	}

	public String getTipoEntidad() {
		return tipoEntidad;
	}

	public void setTipoEntidad(String tipoEntidad) {
		this.tipoEntidad = tipoEntidad;
	}

}
