package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQueries({ @NamedQuery(name = "Bitacora.findAll", query = "select o from Bitacora o") })
@Table(name = "TB_BITACORA")
public class Bitacora implements Serializable {
    @Column(name = "BIT_DESCRIPCION_EVENTO", length = 500)
    private String bitDescripcionEvento;
    @Column(name = "BIT_DESTINATARIOS", length = 200)
    private String bitDestinatarios;
    @Temporal(TemporalType.DATE)
    @Column(name = "BIT_FECHA_EVENTO")
    private Date bitFechaEvento;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_bitacora")
    @SequenceGenerator(name="seq_bitacora", sequenceName="SEQ_BITACORA", allocationSize=1)    
    @Column(name = "BIT_ID", nullable = false)
    private Long bitId;
    @Column(name = "BIT_RESPONSABLE", length = 50)
    private String bitResponsable;
    @ManyToOne
    @JoinColumn(name = "BIT_TIPO_EVEN_COD")
    private TipoEventos tbPrTipoEventos;
    @ManyToOne
    @JoinColumn(name = "BIT_EST_NOT_COD")
    private EstadoNotificacion tbPrEstadoNotificacion;
    @ManyToOne
    @JoinColumn(name = "BIT_PERIODO")
    private Periodo tbPeriodo11;

    public Bitacora() {
    }

    public Bitacora(String bitDescripcionEvento, String bitDestinatarios, EstadoNotificacion tbPrEstadoNotificacion,
                    Date bitFechaEvento, Long bitId, Periodo tbPeriodo11, String bitResponsable,
                    TipoEventos tbPrTipoEventos) {
        this.bitDescripcionEvento = bitDescripcionEvento;
        this.bitDestinatarios = bitDestinatarios;
        this.tbPrEstadoNotificacion = tbPrEstadoNotificacion;
        this.bitFechaEvento = bitFechaEvento;
        this.bitId = bitId;
        this.tbPeriodo11 = tbPeriodo11;
        this.bitResponsable = bitResponsable;
        this.tbPrTipoEventos = tbPrTipoEventos;
    }

    public String getBitDescripcionEvento() {
        return bitDescripcionEvento;
    }

    public void setBitDescripcionEvento(String bitDescripcionEvento) {
        this.bitDescripcionEvento = bitDescripcionEvento;
    }

    public String getBitDestinatarios() {
        return bitDestinatarios;
    }

    public void setBitDestinatarios(String bitDestinatarios) {
        this.bitDestinatarios = bitDestinatarios;
    }


    public Date getBitFechaEvento() {
        return bitFechaEvento;
    }

    public void setBitFechaEvento(Date bitFechaEvento) {
        this.bitFechaEvento = bitFechaEvento;
    }

    public Long getBitId() {
        return bitId;
    }

    public void setBitId(Long bitId) {
        this.bitId = bitId;
    }


    public String getBitResponsable() {
        return bitResponsable;
    }

    public void setBitResponsable(String bitResponsable) {
        this.bitResponsable = bitResponsable;
    }


    public TipoEventos getTbPrTipoEventos() {
        return tbPrTipoEventos;
    }

    public void setTbPrTipoEventos(TipoEventos tbPrTipoEventos) {
        this.tbPrTipoEventos = tbPrTipoEventos;
    }

    public EstadoNotificacion getTbPrEstadoNotificacion() {
        return tbPrEstadoNotificacion;
    }

    public void setTbPrEstadoNotificacion(EstadoNotificacion tbPrEstadoNotificacion) {
        this.tbPrEstadoNotificacion = tbPrEstadoNotificacion;
    }

    public Periodo getTbPeriodo11() {
        return tbPeriodo11;
    }

    public void setTbPeriodo11(Periodo tbPeriodo11) {
        this.tbPeriodo11 = tbPeriodo11;
    }
}
