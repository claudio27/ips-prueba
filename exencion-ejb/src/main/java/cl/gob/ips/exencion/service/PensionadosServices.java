package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.PensionadosDAO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Pensionados;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

/**
 * Created by lpino on 03-07-17.
 */
@Stateless
@LocalBean
public class PensionadosServices {

    @Inject
    private PensionadosDAO pensionadosDao;

    public List<Pensionados> getPensionadoPorRun(Long run) throws DataAccessException {
        return pensionadosDao.obtenerPensionadoPorRun(run);
    }

        public List<Pensionados> getPensionadoPorRunPeriodo(Long run, Integer periodoDesde , Integer periodoHasta ) throws DataAccessException {
        return pensionadosDao.obtenerPensionadoPorRunPeriodo(run , periodoDesde, periodoHasta);
    }

}
