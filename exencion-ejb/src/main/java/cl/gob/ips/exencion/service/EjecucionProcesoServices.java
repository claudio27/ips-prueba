package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.MantencionEjecucionDao;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.MantencionEjecucion;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by senshi.cristian@gmail.com on 23-11-2016.
 */
@Stateless
@LocalBean
public class EjecucionProcesoServices {

    @Inject
    MantencionEjecucionDao dao;

    /**
     *
     * @param mantencionEjecucion
     * @throws DataAccessException
     */
    public void actualizarParametroEjecucion(final MantencionEjecucion mantencionEjecucion) throws DataAccessException {
        dao.actualizarParametroEjecucion(mantencionEjecucion);
    }

    /**
     *
     * @param parametros
     * @throws DataAccessException
     */
    public void actualizarParametrosEjecucion(final List<MantencionEjecucion> parametros) throws DataAccessException {
        dao.actualizarParametrosEjecucion(parametros); 
    }    
    
    /**
     *
     * @return
     * @throws DataAccessException
     */
    public List<MantencionEjecucion> findParametrosEjecucion() throws DataAccessException {
        return dao.buscarParametrosEjecucion();
    }    
}
