package cl.gob.ips.exencion.dto;

import java.io.Serializable;

public class NominaPeriodoPdiDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1005929525423763616L;

    private Integer run;

    private String apellidoPaterno;

    private String apellidoMaterno;

    private String nombres;

    private String sexo;

    private String fechaNacimiento;

    private String desdeUno;

    private String hastaUno;

    private String desdeDos;

    private String hastaDos;

    public Integer getRun() {
        return run;
    }

    public void setRun(Integer run) {
        this.run = run;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDesdeUno() {
        return desdeUno;
    }

    public void setDesdeUno(String desdeUno) {
        this.desdeUno = desdeUno;
    }

    public String getHastaUno() {
        return hastaUno;
    }

    public void setHastaUno(String hastaUno) {
        this.hastaUno = hastaUno;
    }

    public String getDesdeDos() {
        return desdeDos;
    }

    public void setDesdeDos(String desdeDos) {
        this.desdeDos = desdeDos;
    }

    public String getHastaDos() {
        return hastaDos;
    }

    public void setHastaDos(String hastaDos) {
        this.hastaDos = hastaDos;
    }

}
