package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_TIPO_EVENTOS_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_TIPO_EVENTOS_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TipoEventos.findAll", query = "select o from TipoEventos o") })
@Table(name = "TB_PR_TIPO_EVENTOS")
//@SequenceGenerator(name = "TipoEventos_Id_Seq_Gen", sequenceName = "TB_PR_TIPO_EVENTOS_ID_SEQ_GEN", allocationSize = 50,
//                   initialValue = 50)
public class TipoEventos implements Serializable {
    private static final long serialVersionUID = 179609563118329533L;
    @Id
    @Column(name = "TIP_EVEN_COD", nullable = false)
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipoEventos_Id_Seq_Gen")
    private Integer tipEvenCod;
    @Column(name = "TIP_EVEN_DESCRIPCION", nullable = false, length = 100)
    private String tipEvenDescripcion;
    @OneToMany(mappedBy = "tbPrTipoEventos", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Bitacora> tbBitacoraList;

    public TipoEventos() {
    }

    public TipoEventos(Integer tipEvenCod, String tipEvenDescripcion) {
        this.tipEvenCod = tipEvenCod;
        this.tipEvenDescripcion = tipEvenDescripcion;
    }

    public TipoEventos(Integer tipEvenCod) {
        this.tipEvenCod = tipEvenCod;
    }
    
    public Integer getTipEvenCod() {
        return tipEvenCod;
    }

    public String getTipEvenDescripcion() {
        return tipEvenDescripcion;
    }

    public void setTipEvenDescripcion(String tipEvenDescripcion) {
        this.tipEvenDescripcion = tipEvenDescripcion;
    }

    public List<Bitacora> getTbBitacoraList() {
        return tbBitacoraList;
    }

    public void setTbBitacoraList(List<Bitacora> tbBitacoraList) {
        this.tbBitacoraList = tbBitacoraList;
    }

    public Bitacora addBitacora(Bitacora bitacora) {
        getTbBitacoraList().add(bitacora);
        bitacora.setTbPrTipoEventos(this);
        return bitacora;
    }

    public Bitacora removeBitacora(Bitacora bitacora) {
        getTbBitacoraList().remove(bitacora);
        bitacora.setTbPrTipoEventos(null);
        return bitacora;
    }
}
