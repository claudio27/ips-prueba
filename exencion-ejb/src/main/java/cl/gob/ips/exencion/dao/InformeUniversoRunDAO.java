package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.model.Pensionados;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Created by alaya on 17/07/17.
 */
@Stateless
@LocalBean
public class InformeUniversoRunDAO extends BaseDAO {

    public List<Pensionados> obtenerInformeUniversoPorRun(Integer periodoDesde, Integer periodoHasta, Long run ) throws IpsBussinesException, DataAccessException {

        return (List<Pensionados>) super.em.createQuery("SELECT pen FROM " + Pensionados.class.getName() + " pen " +
                "WHERE pen.periodo.perPeriodo BETWEEN :periodoDesde AND :periodoHasta AND pen.penRun = :run", Pensionados.class)
                .setParameter("periodoDesde", periodoDesde)
                .setParameter("periodoHasta", periodoHasta)
                .setParameter("run", run)
                .getResultList();
    }

}
