package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_TIPO_PENSION_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_TIPO_PENSION_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TipoPension.findAll", query = "select o from TipoBeneficio o") })
@Table(name = "TB_PR_TIPO_BENEFICIO")
public class TipoBeneficio implements Serializable {
    private static final long serialVersionUID = -1340801148631696443L;
    @Id
    @Column(name = "TIP_BEN_COD", nullable = false)
    private Integer tipBenCod;

    @Column(name = "TIP_BEN_DESCRIPCION", nullable = false, length = 50)
    private String tipBenDescripcion;

    @OneToMany(mappedBy = "tipoBeneficio", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<BenregAct> tbBenregActList;

    @OneToMany(mappedBy = "tipoBeneficio", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Pensiones> tbPensionesList;

    @OneToMany(mappedBy = "tipoBeneficio", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<BenregHis> tbBenregHisList;

    @OneToMany(mappedBy = "tipoBeneficio", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<ArchEstandarHis> tbArchEstandarHisList;

    public TipoBeneficio() {
    }

    public TipoBeneficio(Integer tipBenCod, String tipBenDescripcion) {
        this.tipBenCod = tipBenCod;
        this.tipBenDescripcion = tipBenDescripcion;
    }

    public Integer getTipBenCod() {
        return tipBenCod;
    }

    public String getTipBenDescripcion() {
        return tipBenDescripcion;
    }

    public void setTipBenDescripcion(String tipPnsDescripcion) {
        this.tipBenDescripcion = tipPnsDescripcion;
    }

    public List<BenregAct> getTbBenregActList() {
        return tbBenregActList;
    }

    public void setTbBenregActList(List<BenregAct> tbBenregActList) {
        this.tbBenregActList = tbBenregActList;
    }

    public BenregAct addBenregAct(BenregAct benregAct) {
        getTbBenregActList().add(benregAct);
        benregAct.setTipoBeneficio(this);
        return benregAct;
    }

    public BenregAct removeBenregAct(BenregAct benregAct) {
        getTbBenregActList().remove(benregAct);
        benregAct.setTipoBeneficio(null);
        return benregAct;
    }

    public List<Pensiones> getTbPensionesList() {
        return tbPensionesList;
    }

    public void setTbPensionesList(List<Pensiones> tbPensionesList1) {
        this.tbPensionesList = tbPensionesList1;
    }

    public Pensiones addPensiones(Pensiones pensiones) {
        getTbPensionesList().add(pensiones);
        pensiones.setTipoBeneficio(this);
        return pensiones;
    }

    public Pensiones removePensiones(Pensiones pensiones) {
        getTbPensionesList().remove(pensiones);
        pensiones.setTipoBeneficio(null);
        return pensiones;
    }

    public List<BenregHis> getTbBenregHisList() {
        return tbBenregHisList;
    }

    public void setTbBenregHisList(List<BenregHis> tbBenregHisList1) {
        this.tbBenregHisList = tbBenregHisList1;
    }

    public BenregHis addBenregHis(BenregHis benregHis) {
        getTbBenregHisList().add(benregHis);
        benregHis.setTipoBeneficio(this);
        return benregHis;
    }

    public BenregHis removeBenregHis(BenregHis benregHis) {
        getTbBenregHisList().remove(benregHis);
        benregHis.setTipoBeneficio(null);
        return benregHis;
    }

    public List<ArchEstandarHis> getTbArchEstandarHisList() {
        return tbArchEstandarHisList;
    }

    public void setTbArchEstandarHisList(List<ArchEstandarHis> tbArchEstandarHisList) {
        this.tbArchEstandarHisList = tbArchEstandarHisList;
    }

    public ArchEstandarHis addArchEstandarHis(ArchEstandarHis archEstandarHis) {
        getTbArchEstandarHisList().add(archEstandarHis);
        archEstandarHis.setTipoBeneficio(this);
        return archEstandarHis;
    }

    public ArchEstandarHis removeArchEstandarHis(ArchEstandarHis archEstandarHis) {
        getTbArchEstandarHisList().remove(archEstandarHis);
        archEstandarHis.setTipoBeneficio(null);
        return archEstandarHis;
    }
}
