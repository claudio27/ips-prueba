package cl.gob.ips.exencion.model;

import java.io.Serializable;

/**
 * Created by rreyes on 17-10-2016.
 */
public class Rut implements Serializable {

    private Long rut;
    private String dv;

    public Long getRut() {
        return rut;
    }

    public void setRut(Long rut) {
        this.rut = rut;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }
}
