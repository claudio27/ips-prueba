package cl.gob.ips.exencion.dto;

import java.io.Serializable;

public class InformeGestionMantienenDTO implements Serializable {

	/**
	 * Created by practicante on 23-10-17.
	 */
	private static final long serialVersionUID = -4866961320730453142L;
	private String run;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String tipoPensionado;
	private String institucion;

	public InformeGestionMantienenDTO() {
		super();
	}

	public InformeGestionMantienenDTO(String run, String nombre, String apellidoPaterno,
                                      String apellidoMaterno, String tipoPensionado, String institucion) {
		super();

		this.run = run;
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.tipoPensionado = tipoPensionado;
		this.institucion = institucion;

	}

	public String getRun() {
		return run;
	}

	public void setRun(String run) {
		this.run = run;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getTipoPensionado() {
		return tipoPensionado;
	}

	public void setTipoPensionado(String tipoPensionado) {
		this.tipoPensionado = tipoPensionado;
	}

	public String getInstitucion() {
		return institucion;
	}

	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}

}
