package cl.gob.ips.exencion.enums;

/**
 * Created by alaya on 27/07/17.
 */
public enum  TipoGrupo {


    PRODUCCION(1, "Producción"),
    GESTION_BENEFICIOS(2, "Gestión de Beneficios"),
    ADMINISTRACION(3, "Administración"),
    RESPONSABLE_NEGOCIO(4, "Responsable");

    private Integer codigo;
    private String descripcion;

    TipoGrupo(Integer codigo, String descripcion){
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static TipoGrupo getInstance(Integer codigo){
        for(TipoGrupo itemEnum : values()){
            if(itemEnum.getCodigo() == codigo.intValue()){
                return itemEnum;
            }
        }
        return null;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }
}




