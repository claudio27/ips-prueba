package cl.gob.ips.exencion.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the TB_NOMINA_POT_BENEFICIARIO database table.
 * 
 */
@Entity
@Table(name = "TB_NOMINA_POT_BENEFICIARIO")
@NamedQuery(name = "NominaPotBeneficiario.findAll", query = "SELECT t FROM NominaPotBeneficiario t")
public class NominaPotBeneficiario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 913474269150043134L;

	@Id
	@Column(name = "NOM_ID")
	private long nomId;

	@Column(name = "NOM_APELLIDO_MAT")
	private String nomApellidoMat;

	@Column(name = "NOM_APELLIDO_PAT")
	private String nomApellidoPat;

	@Column(name = "NOM_DV_BENEFICIARIO")
	private String nomDvBeneficiario;

	@Column(name = "NOM_DV_INSTITUCION")
	private String nomDvInstitucion;

	@Column(name = "NOM_NOMBRES")
	private String nomNombres;

	@Column(name = "NOM_REQ_NO_ACREDITA")
	private String nomReqNoAcredita;

	@Column(name = "NOM_RUN_BENEFICIARIO")
	private Integer nomRunBeneficiario;

	@Column(name = "NOM_TIPO_PENSION")
	private BigDecimal nomTipoPension;

	// bi-directional many-to-one association to TbPeriodo
	@ManyToOne
	@JoinColumn(name = "NOM_PERIODO")
	private Periodo periodo;

	// bi-directional many-to-one association to TbPrInstitucione
	@ManyToOne
	@JoinColumn(name = "NOM_RUT_INSTITUCION")
	private Instituciones instituciones;

	public NominaPotBeneficiario() {
	}

	public long getNomId() {
		return this.nomId;
	}

	public void setNomId(long nomId) {
		this.nomId = nomId;
	}

	public String getNomApellidoMat() {
		return this.nomApellidoMat;
	}

	public void setNomApellidoMat(String nomApellidoMat) {
		this.nomApellidoMat = nomApellidoMat;
	}

	public String getNomApellidoPat() {
		return this.nomApellidoPat;
	}

	public void setNomApellidoPat(String nomApellidoPat) {
		this.nomApellidoPat = nomApellidoPat;
	}

	public String getNomDvBeneficiario() {
		return this.nomDvBeneficiario;
	}

	public void setNomDvBeneficiario(String nomDvBeneficiario) {
		this.nomDvBeneficiario = nomDvBeneficiario;
	}

	public String getNomDvInstitucion() {
		return this.nomDvInstitucion;
	}

	public void setNomDvInstitucion(String nomDvInstitucion) {
		this.nomDvInstitucion = nomDvInstitucion;
	}

	public String getNomNombres() {
		return this.nomNombres;
	}

	public void setNomNombres(String nomNombres) {
		this.nomNombres = nomNombres;
	}

	public String getNomReqNoAcredita() {
		return this.nomReqNoAcredita;
	}

	public void setNomReqNoAcredita(String nomReqNoAcredita) {
		this.nomReqNoAcredita = nomReqNoAcredita;
	}

	public Integer getNomRunBeneficiario() {
		return this.nomRunBeneficiario;
	}

	public void setNomRunBeneficiario(Integer nomRunBeneficiario) {
		this.nomRunBeneficiario = nomRunBeneficiario;
	}

	public BigDecimal getNomTipoPension() {
		return this.nomTipoPension;
	}

	public void setNomTipoPension(BigDecimal nomTipoPension) {
		this.nomTipoPension = nomTipoPension;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	public Instituciones getInstituciones() {
		return instituciones;
	}

	public void setInstituciones(Instituciones instituciones) {
		this.instituciones = instituciones;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder
				.append(periodo.getPerPeriodo() ) 		// todo revisar AAAAMM periodo
				.append(instituciones.getInstRut())
				.append(instituciones.getInstDv())
				.append(nomRunBeneficiario.toString())
				.append(nomDvBeneficiario)
				.append(nomApellidoPat)
				.append(nomApellidoMat)
				.append(nomNombres)
				.append(nomTipoPension)
				.append(nomReqNoAcredita);

		return 	builder.toString();


	}
}