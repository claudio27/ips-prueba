package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
//import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TB_PR_PROPIEDADES")
public class Propiedades implements Serializable {
    private static final long serialVersionUID = 7508440708020368654L;

    @Id
    @Column(name = "PROP_CLAVE", nullable = false)
    private String propClave;
    @Column(name = "PROP_VALOR")
    private String propValor;
    @Temporal(TemporalType.DATE)
    @Column(name = "PROP_FECHA_CREACION")
    private Date propFecCreacion;
    @Temporal(TemporalType.DATE)
    @Column(name = "PROP_FECHA_MODIFICACION")
    private Date propFecModificacion;
    @Column(name = "PROP_USUARIO", length = 20)
    private String propUsuario;

    public Propiedades() {
    }

    public Propiedades(String propClave, String propValor, Date propFecCreacion, Date propFecModificacion,  String propUsuario) {
        this.propClave = propClave;
        this.propValor = propValor;
        this.propFecCreacion = propFecCreacion;
        this.propFecModificacion = propFecModificacion;
        this.propUsuario = propUsuario;
    }

    public String getPropClave() {
        return propClave;
    }

    public void setPropClave(String propClave) {
        this.propClave = propClave;
    }

    public String getPropValor() {
        return propValor;
    }

    public void setPropValor(String propValor) {
        this.propValor = propValor;
    }

    public Date getPropFecCreacion() {
        return propFecCreacion;
    }

    public void setPropFecCreacion(Date propFecCreacion) {
        this.propFecCreacion = propFecCreacion;
    }

    public Date getPropFecModificacion() {
        return propFecModificacion;
    }

    public void setPropFecModificacion(Date propFecModificacion) {
        this.propFecModificacion = propFecModificacion;
    }

    public String getPropUsuario() {
        return propUsuario;
    }

    public void setPropUsuario(String propUsuario) {
        this.propUsuario = propUsuario;
    }
}
