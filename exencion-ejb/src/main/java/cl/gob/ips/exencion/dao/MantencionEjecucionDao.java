package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.MantencionEjecucion;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


/**
 * Created by senshi.cristian@gmail.com on 23-11-2016.
 */
@Stateless
@LocalBean
public class MantencionEjecucionDao extends BaseDAO {

    /**
     * @return
     * @throws DataAccessException
     */
    public List<MantencionEjecucion> buscarParametrosEjecucion() throws DataAccessException {
        return (List<MantencionEjecucion>) super.em.createNamedQuery(MantencionEjecucion.QUERY_FIND_ALL)
                .getResultList();
    }
    
    /**
     * @param mantencionEjecucion
     * @throws DataAccessException
     */
    public void actualizarParametroEjecucion(final MantencionEjecucion mantencionEjecucion) throws DataAccessException {
        super.em.merge(mantencionEjecucion);
    }
    
    /**
     * @param parametrosEjecucion
     * @throws DataAccessException
     */
    public void actualizarParametrosEjecucion(final List<MantencionEjecucion> parametrosEjecucion) throws DataAccessException {
        for (MantencionEjecucion parametro : parametrosEjecucion) {
            super.em.merge(parametro);
        }
    }    
}
