/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Bitacora;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

/**
 *
 * @author Cristian Angulo Created by senshi.cristian@gmail.com on 12-12-2016.
 */
@Stateless
@LocalBean
public class BitacoraDao extends BaseDAO {

    private static final String PARAM_PERIODO = "periodo";

    /**
     *
     * @param bitacora
     * @throws DataAccessException
     */
    public void creaBitacora(final Bitacora bitacora) throws DataAccessException{
        super.em.persist(bitacora);
    }

    /**
     * @param periodo
     * @return List<Bitacora>
     * @throws DataAccessException
     */
    public List<Bitacora> obtenerBitacoraPorPeriodo(Integer periodo)throws DataAccessException {
        List<Bitacora> bitacoras = null;

        super.query = this.em.createQuery("SELECT bit FROM " + Bitacora.class.getName() + " bit "
                + "where bit.tbPeriodo11.perPeriodo = :periodo", Bitacora.class).
                setParameter(PARAM_PERIODO, periodo);

        if (!super.query.getResultList().isEmpty()) {
            bitacoras = super.query.getResultList();
        }

        return bitacoras;
    }
}
