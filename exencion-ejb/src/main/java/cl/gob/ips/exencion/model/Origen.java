package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_TBL_ORIGEN_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_TBL_ORIGEN_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({@NamedQuery(name = "Origen.findAll", query = "select o from Origen o")})
@Table(name = "TB_PR_TBL_ORIGEN")
@SequenceGenerator(name = "Origen_Id_Seq_Gen", sequenceName = "TB_PR_TBL_ORIGEN_ID_SEQ_GEN", allocationSize = 50,
        initialValue = 50)
public class Origen implements Serializable {
    private static final long serialVersionUID = 6374830017156436178L;
    @Id
    @Column(name = "TBL_ORI_COD", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Origen_Id_Seq_Gen")
    private Integer tblOriCod;
    @Column(name = "TBL_ORI_DES", nullable = false, length = 20)
    private String tblOriDes;
    @OneToMany(mappedBy = "tbPrTblOrigen", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Pensiones> tbPensionesList;
    @OneToMany(mappedBy = "tbPrTblOrigen1", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Tpr> tbPrTprList;

    public Origen() {
    }

    public Origen(Integer tblOriCod, String tblOriDes) {
        this.tblOriCod = tblOriCod;
        this.tblOriDes = tblOriDes;
    }

    public Integer getTblOriCod() {
        return tblOriCod;
    }

    public String getTblOriDes() {
        return tblOriDes;
    }

    public void setTblOriDes(String tblOriDes) {
        this.tblOriDes = tblOriDes;
    }

    public List<Pensiones> getTbPensionesList() {
        return tbPensionesList;
    }

    public void setTbPensionesList(List<Pensiones> tbPensionesList) {
        this.tbPensionesList = tbPensionesList;
    }

    public Pensiones addPensiones(Pensiones pensiones) {
        getTbPensionesList().add(pensiones);
        pensiones.setTbPrTblOrigen(this);
        return pensiones;
    }

    public Pensiones removePensiones(Pensiones pensiones) {
        getTbPensionesList().remove(pensiones);
        pensiones.setTbPrTblOrigen(null);
        return pensiones;
    }

    public List<Tpr> getTbPrTprList() {
        return tbPrTprList;
    }

    public void setTbPrTprList(List<Tpr> tbPrTprList) {
        this.tbPrTprList = tbPrTprList;
    }

    public Tpr addTpr(Tpr tpr) {
        getTbPrTprList().add(tpr);
        tpr.setTbPrTblOrigen1(this);
        return tpr;
    }

    public Tpr removeTpr(Tpr tpr) {
        getTbPrTprList().remove(tpr);
        tpr.setTbPrTblOrigen1(null);
        return tpr;
    }
}
