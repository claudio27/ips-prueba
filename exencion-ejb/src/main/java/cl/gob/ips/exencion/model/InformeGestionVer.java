package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class InformeGestionVer implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="idInstitucion")
    private Integer idInstitucion;
    @Column(name = "institucion")
    private String institucion;
    @Column(name = "tipoEntidad")
    private String tipoEntidad;
    @Column(name = "total")
    private Integer total;
    @Column(name = "nuevos")
    private Integer nuevos;
    @Column(name="extintos")
    private Integer extintos;
    @Column(name="mantienen")
    private Integer mantienen;

    public InformeGestionVer() {
        super();
    }

    public InformeGestionVer(Integer idInstitucion, String institucion, String tipoEntidad, Integer total, Integer nuevos, Integer extintos, Integer mantienen) {
        this.idInstitucion = idInstitucion;
        this.institucion = institucion;
        this.tipoEntidad = tipoEntidad;
        this.total = total;
        this.nuevos = nuevos;
        this.extintos = extintos;
        this.mantienen = mantienen;
    }

    public Integer getIdInstitucion() {
        return idInstitucion;
    }

    public void setIdInstitucion(Integer idInstitucion) {
        this.idInstitucion = idInstitucion;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getTipoEntidad() {
        return tipoEntidad;
    }

    public void setTipoEntidad(String tipoEntidad) {
        this.tipoEntidad = tipoEntidad;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getNuevos() {
        return nuevos;
    }

    public void setNuevos(Integer nuevos) {
        this.nuevos = nuevos;
    }

    public Integer getExtintos() {
        return extintos;
    }

    public void setExtintos(Integer extintos) {
        this.extintos = extintos;
    }

    public Integer getMantienen() {
        return mantienen;
    }

    public void setMantienen(Integer mantienen) {
        this.mantienen = mantienen;
    }
}