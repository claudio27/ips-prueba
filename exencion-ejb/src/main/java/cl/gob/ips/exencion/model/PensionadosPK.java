package cl.gob.ips.exencion.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by lpino on 30-06-17.
 */
@Embeddable
public class PensionadosPK implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6575726135384005828L;

    @Column(name = "PEN_PERIODO", insertable = false, updatable = false)
    private long penPeriodo;

    @Column(name = "PEN_RUN")
    private long penRun;

    public PensionadosPK() {
    }

    public long getPenPeriodo() {
        return this.penPeriodo;
    }

    public void setPenPeriodo(long penPeriodo) {
        this.penPeriodo = penPeriodo;
    }

    public long getPenRun() {
        return this.penRun;
    }

    public void setPenRun(long penRun) {
        this.penRun = penRun;
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof PensionadosPK)) {
            return false;
        }
        PensionadosPK castOther = (PensionadosPK) other;
        return (this.penPeriodo == castOther.penPeriodo)
                && (this.penRun == castOther.penRun);
    }

    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime
                + ((int) (this.penPeriodo ^ (this.penPeriodo >>> 32)));
        hash = hash * prime + ((int) (this.penRun ^ (this.penRun >>> 32)));

        return hash;
    }
}