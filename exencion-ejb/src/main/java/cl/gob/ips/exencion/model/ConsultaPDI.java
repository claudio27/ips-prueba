package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * To create ID generator sequence "TB_CONSULTA_PDI_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_CONSULTA_PDI_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "ConsultaPDI.findAll", query = "select o from ConsultaPDI o") })
@Table(name = "TB_CONSULTA_PDI")
@SequenceGenerator(name = "ConsultaPDI_Id_Seq_Gen", sequenceName = "TB_CONSULTA_PDI_ID_SEQ_GEN", allocationSize = 50,
                   initialValue = 50)
public class ConsultaPDI implements Serializable {
    private static final long serialVersionUID = -5696025978176151529L;
    @Column(name = "CON_PDI_APE_MAT", length = 20)
    private String conPdiApeMat;
    @Column(name = "CON_PDI_APE_PAT", length = 20)
    private String conPdiApePat;
    @Column(name = "CON_PDI_DESDE1", length = 8)
    private String conPdiDesde1;
    @Column(name = "CON_PDI_DESDE2", length = 8)
    private String conPdiDesde2;
    @Column(name = "CON_PDI_ELIMINADO")
    private Integer conPdiEliminado;
    @Temporal(TemporalType.DATE)
    @Column(name = "CON_PDI_FEC_ELIMINADO")
    private Date conPdiFecEliminado;
    @Column(name = "CON_PDI_FEC_NAC")
    private Integer conPdiFecNac;
    @Column(name = "CON_PDI_HASTA1", length = 8)
    private String conPdiHasta1;
    @Column(name = "CON_PDI_HASTA2", length = 8)
    private String conPdiHasta2;
    @Id
    @Column(name = "CON_PDI_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ConsultaPDI_Id_Seq_Gen")
    private BigDecimal conPdiId;
    @Column(name = "CON_PDI_NOMBRES", length = 30)
    private String conPdiNombres;
    @Column(name = "CON_PDI_RUN", nullable = false)
    private Integer conPdiRun;
    @Column(name = "CON_PDI_SEXO", length = 1)
    private String conPdiSexo;
    @Column(name = "CON_PDI_USU_ELIMINADO", length = 20)
    private String conPdiUsuEliminado;
    @ManyToOne
    @JoinColumn(name = "CON_PDI_PERIODO")
    private Periodo tbPeriodo8;

    public ConsultaPDI() {
    }

    public ConsultaPDI(String conPdiApeMat, String conPdiApePat, String conPdiDesde1, String conPdiDesde2,
                       Integer conPdiEliminado, Date conPdiFecEliminado, Integer conPdiFecNac, String conPdiHasta1,
                       String conPdiHasta2, BigDecimal conPdiId, String conPdiNombres, Periodo tbPeriodo8,
                       Integer conPdiRun, String conPdiSexo, String conPdiUsuEliminado) {
        this.conPdiApeMat = conPdiApeMat;
        this.conPdiApePat = conPdiApePat;
        this.conPdiDesde1 = conPdiDesde1;
        this.conPdiDesde2 = conPdiDesde2;
        this.conPdiEliminado = conPdiEliminado;
        this.conPdiFecEliminado = conPdiFecEliminado;
        this.conPdiFecNac = conPdiFecNac;
        this.conPdiHasta1 = conPdiHasta1;
        this.conPdiHasta2 = conPdiHasta2;
        this.conPdiId = conPdiId;
        this.conPdiNombres = conPdiNombres;
        this.tbPeriodo8 = tbPeriodo8;
        this.conPdiRun = conPdiRun;
        this.conPdiSexo = conPdiSexo;
        this.conPdiUsuEliminado = conPdiUsuEliminado;
    }

    public String getConPdiApeMat() {
        return conPdiApeMat;
    }

    public void setConPdiApeMat(String conPdiApeMat) {
        this.conPdiApeMat = conPdiApeMat;
    }

    public String getConPdiApePat() {
        return conPdiApePat;
    }

    public void setConPdiApePat(String conPdiApePat) {
        this.conPdiApePat = conPdiApePat;
    }

    public String getConPdiDesde1() {
        return conPdiDesde1;
    }

    public void setConPdiDesde1(String conPdiDesde1) {
        this.conPdiDesde1 = conPdiDesde1;
    }

    public String getConPdiDesde2() {
        return conPdiDesde2;
    }

    public void setConPdiDesde2(String conPdiDesde2) {
        this.conPdiDesde2 = conPdiDesde2;
    }

    public Integer getConPdiEliminado() {
        return conPdiEliminado;
    }

    public void setConPdiEliminado(Integer conPdiEliminado) {
        this.conPdiEliminado = conPdiEliminado;
    }

    public Date getConPdiFecEliminado() {
        return conPdiFecEliminado;
    }

    public void setConPdiFecEliminado(Date conPdiFecEliminado) {
        this.conPdiFecEliminado = conPdiFecEliminado;
    }

    public Integer getConPdiFecNac() {
        return conPdiFecNac;
    }

    public void setConPdiFecNac(Integer conPdiFecNac) {
        this.conPdiFecNac = conPdiFecNac;
    }

    public String getConPdiHasta1() {
        return conPdiHasta1;
    }

    public void setConPdiHasta1(String conPdiHasta1) {
        this.conPdiHasta1 = conPdiHasta1;
    }

    public String getConPdiHasta2() {
        return conPdiHasta2;
    }

    public void setConPdiHasta2(String conPdiHasta2) {
        this.conPdiHasta2 = conPdiHasta2;
    }

    public BigDecimal getConPdiId() {
        return conPdiId;
    }

    public String getConPdiNombres() {
        return conPdiNombres;
    }

    public void setConPdiNombres(String conPdiNombres) {
        this.conPdiNombres = conPdiNombres;
    }


    public Integer getConPdiRun() {
        return conPdiRun;
    }

    public void setConPdiRun(Integer conPdiRun) {
        this.conPdiRun = conPdiRun;
    }

    public String getConPdiSexo() {
        return conPdiSexo;
    }

    public void setConPdiSexo(String conPdiSexo) {
        this.conPdiSexo = conPdiSexo;
    }

    public String getConPdiUsuEliminado() {
        return conPdiUsuEliminado;
    }

    public void setConPdiUsuEliminado(String conPdiUsuEliminado) {
        this.conPdiUsuEliminado = conPdiUsuEliminado;
    }

    public Periodo getTbPeriodo8() {
        return tbPeriodo8;
    }

    public void setTbPeriodo8(Periodo tbPeriodo8) {
        this.tbPeriodo8 = tbPeriodo8;
    }
}
