package cl.gob.ips.exencion.enums;

/**
 * Created by alaya on 17/07/17.
 */
public enum TipoPFP {

    SIN_INFO(0, "Sin Información"),
    ADM(1, "ADM"),
    REG(2, "REG");

    private Integer codigo;
    private String descripcion;

    TipoPFP(Integer codigo, String descripcion){
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static TipoPFP getInstance(Integer codigo){
        for(TipoPFP itemEnum : values()){
            if(itemEnum.getCodigo() == codigo){
                return itemEnum;
            }
        }
        return null;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
