package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.TipoRespuesta;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Created by lpino on 06-07-17.
 */
@Stateless
@LocalBean
public class TipoRespuestaDao extends BaseDAO {

    public TipoRespuesta obtenerTipoRespuestaPorCodigo(Integer codigo) throws DataAccessException {

        return em.createQuery("SELECT resp FROM " + TipoRespuesta.class.getName() + " resp "
                + "WHERE resp.tipRespCod= :codigo", TipoRespuesta.class ).setParameter("codigo", codigo).getSingleResult();
    }

    public List<TipoRespuesta> buscarTiposRespuestas() throws DataAccessException {
        return (List<TipoRespuesta>) em.createNamedQuery(TipoRespuesta.QUERY_FIND_ALL)
                .getResultList();
    }

    public void actualizarTipoRespuesta(final TipoRespuesta tipoRespuesta) throws DataAccessException {
        em.merge(tipoRespuesta);
    }
}