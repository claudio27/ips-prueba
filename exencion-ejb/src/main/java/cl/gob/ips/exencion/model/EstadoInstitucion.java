package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_EST_INSTITUCION_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_EST_INSTITUCION_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "EstadoInstitucion.findAll", query = "select o from EstadoInstitucion o") })
@Table(name = "TB_PR_EST_INSTITUCION")
@SequenceGenerator(name = "EstadoInstitucion_Id_Seq_Gen", sequenceName = "TB_PR_EST_INSTITUCION_ID_SEQ_GEN",
                   allocationSize = 50, initialValue = 50)
public class EstadoInstitucion implements Serializable {
    private static final long serialVersionUID = 1701983118684209882L;
    @Id
    @Column(name = "EST_INST_COD", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EstadoInstitucion_Id_Seq_Gen")
    private Integer estInstCod;
    @Column(name = "EST_INST_DESCRIPCION", nullable = false, length = 50)
    private String estInstDescripcion;
    @OneToMany(mappedBy = "tbPrEstInstitucion", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Instituciones> tbPrInstitucionesList;

    public EstadoInstitucion() {
    }

    public EstadoInstitucion(Integer estInstCod) {
        this.estInstCod = estInstCod;
    }


    public EstadoInstitucion(Integer estInstCod, String estInstDescripcion) {
        this.estInstCod = estInstCod;
        this.estInstDescripcion = estInstDescripcion;
    }

    public Integer getEstInstCod() {
        return estInstCod;
    }

    public void setEstInstCod(Integer estInstCod) {
        this.estInstCod = estInstCod;
    }

    public String getEstInstDescripcion() {
        return estInstDescripcion;
    }

    public void setEstInstDescripcion(String estInstDescripcion) {
        this.estInstDescripcion = estInstDescripcion;
    }

    public List<Instituciones> getTbPrInstitucionesList() {
        return tbPrInstitucionesList;
    }

    public void setTbPrInstitucionesList(List<Instituciones> tbPrInstitucionesList) {
        this.tbPrInstitucionesList = tbPrInstitucionesList;
    }

    public Instituciones addInstituciones(Instituciones instituciones) {
        getTbPrInstitucionesList().add(instituciones);
        instituciones.setTbPrEstInstitucion(this);
        return instituciones;
    }

    public Instituciones removeInstituciones(Instituciones instituciones) {
        getTbPrInstitucionesList().remove(instituciones);
        instituciones.setTbPrEstInstitucion(null);
        return instituciones;
    }
}
