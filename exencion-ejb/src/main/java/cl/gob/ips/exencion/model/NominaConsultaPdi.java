package cl.gob.ips.exencion.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the TB_NOMINA_CONSULTA_PDI database table.
 * 
 */
@Entity
@Table(name = "TB_NOMINA_CONSULTA_PDI")
@NamedQuery(name = "NominaConsultaPdi.findAll", query = "SELECT t FROM NominaConsultaPdi t")
public class NominaConsultaPdi implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3686958565447478802L;

	@Id
	@Column(name = "NOM_ID")
	private long nomId;

	@Column(name = "NOM_APELLIDO_MAT")
	private String nomApellidoMat;

	@Column(name = "NOM_APELLIDO_PAT")
	private String nomApellidoPat;

	@Column(name = "NOM_DESDE1")
	private BigDecimal nomDesde1;

	@Column(name = "NOM_DESDE2")
	private BigDecimal nomDesde2;

	@Column(name = "NOM_DV_INSTITUCION")
	private String nomDvInstitucion;

	@Column(name = "NOM_FECHA_NAC")
	private String nomFechaNac;

	@Column(name = "NOM_HASTA1")
	private BigDecimal nomHasta1;

	@Column(name = "NOM_HASTA2")
	private BigDecimal nomHasta2;

	@Column(name = "NOM_NOMBRES")
	private String nomNombres;

	@Column(name = "NOM_RUN_BENEFICIARIO")
	private Integer nomRunBeneficiario;

	@Column(name = "NOM_SEXO")
	private String nomSexo;

	// bi-directional many-to-one association to TbPeriodo
	@ManyToOne
	@JoinColumn(name = "NOM_PERIODO")
	private Periodo periodo;

	// bi-directional many-to-one association to TbPrInstitucione
	@ManyToOne
	@JoinColumn(name = "NOM_RUT_INSTITUCION")
	private Instituciones instituciones;

	public NominaConsultaPdi() {
	}

	public long getNomId() {
		return this.nomId;
	}

	public void setNomId(long nomId) {
		this.nomId = nomId;
	}

	public String getNomApellidoMat() {
		return this.nomApellidoMat;
	}

	public void setNomApellidoMat(String nomApellidoMat) {
		this.nomApellidoMat = nomApellidoMat;
	}

	public String getNomApellidoPat() {
		return this.nomApellidoPat;
	}

	public void setNomApellidoPat(String nomApellidoPat) {
		this.nomApellidoPat = nomApellidoPat;
	}

	public BigDecimal getNomDesde1() {
		return this.nomDesde1;
	}

	public void setNomDesde1(BigDecimal nomDesde1) {
		this.nomDesde1 = nomDesde1;
	}

	public BigDecimal getNomDesde2() {
		return this.nomDesde2;
	}

	public void setNomDesde2(BigDecimal nomDesde2) {
		this.nomDesde2 = nomDesde2;
	}

	public String getNomDvInstitucion() {
		return this.nomDvInstitucion;
	}

	public void setNomDvInstitucion(String nomDvInstitucion) {
		this.nomDvInstitucion = nomDvInstitucion;
	}

	public String getNomFechaNac() {
		return this.nomFechaNac;
	}

	public void setNomFechaNac(String nomFechaNac) {
		this.nomFechaNac = nomFechaNac;
	}

	public BigDecimal getNomHasta1() {
		return this.nomHasta1;
	}

	public void setNomHasta1(BigDecimal nomHasta1) {
		this.nomHasta1 = nomHasta1;
	}

	public BigDecimal getNomHasta2() {
		return this.nomHasta2;
	}

	public void setNomHasta2(BigDecimal nomHasta2) {
		this.nomHasta2 = nomHasta2;
	}

	public String getNomNombres() {
		return this.nomNombres;
	}

	public void setNomNombres(String nomNombres) {
		this.nomNombres = nomNombres;
	}

	public Integer getNomRunBeneficiario() {
		return this.nomRunBeneficiario;
	}

	public void setNomRunBeneficiario(Integer nomRunBeneficiario) {
		this.nomRunBeneficiario = nomRunBeneficiario;
	}

	public String getNomSexo() {
		return this.nomSexo;
	}

	public void setNomSexo(String nomSexo) {
		this.nomSexo = nomSexo;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	public Instituciones getInstituciones() {
		return instituciones;
	}

	public void setInstituciones(Instituciones instituciones) {
		this.instituciones = instituciones;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder
				.append(StringUtils.leftPad(String.valueOf(nomRunBeneficiario), 9, "0")).append("|")
				.append(nomApellidoPat).append("|")
				.append(nomApellidoMat).append("|")
				.append(nomNombres).append("|")
				.append(nomSexo).append("|")
				.append(nomFechaNac).append("|")
				.append(nomDesde1).append("|")
				.append(nomHasta1).append("|")
				.append(nomDesde2).append("|")
				.append(nomHasta2);

		return 	builder.toString();
	}
}