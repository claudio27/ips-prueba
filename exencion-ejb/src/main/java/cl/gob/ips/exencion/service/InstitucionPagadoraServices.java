package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.InstitucionPagadoraDao;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Instituciones;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by rodrigo.reyesco@gmail.com on 07-10-2016.
 */
@Stateless
@LocalBean
public class InstitucionPagadoraServices {

    @Inject
    InstitucionPagadoraDao dao;

    /**
     *
     * @param institucion
     * @return
     * @throws Exception
     */
    public List<Instituciones> buscarInstitucionesPorCriterio(final Instituciones institucion) throws DataAccessException {
        return dao.buscarInstitucionesPorCriterio(institucion);
    }

    /**
     *
     * @param institucion
     * @return
     * @throws DataAccessException
     */
    public Instituciones findInstitucionByRut(final Instituciones institucion) throws DataAccessException {
        return dao.findInstitucionByRut(institucion);
    }

    /**
     *
     * @param institucion
     * @throws Exception
     */
    public void actualizarIntitucion(final Instituciones institucion) throws DataAccessException {
        dao.actualizarIntitucion(institucion);
    }

    /**
     *
     * @param instituciones
     * @throws Exception
     */
    public void actualizaInstituciones(final List<Instituciones> instituciones) throws DataAccessException {
        dao.actualizaInstituciones(instituciones);
    }

    /**
     *
     * @param institucion
     * @throws Exception
     */
    public void creaInstitucion(final Instituciones institucion) throws DataAccessException {
        dao.creaInstitucion(institucion);
    }
    
    /**
    *
    * @return
    * @throws Exception
    */
   public List<Instituciones> buscarInstitucionesHabilitadas() throws EJBTransactionRolledbackException {
       return dao.buscarInstitucionesHabilitadas();
   }


}
