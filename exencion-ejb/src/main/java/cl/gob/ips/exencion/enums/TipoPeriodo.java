package cl.gob.ips.exencion.enums;

public enum TipoPeriodo {

	RUN(0, "Concesi&oacute;n"),
	PENSIONES(1, "Bonificaci&oacute;n");
			
	private Integer codigo;
	private String descripcion;

	private TipoPeriodo(Integer codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public static TipoPeriodo getInstance(Integer codigo) {
		for (TipoPeriodo itemEnum : values()) {
			if (itemEnum.getCodigo() == codigo) {
				return itemEnum;
			}
		}
		return null;
	}

	public Integer getCodigo() {
		return this.codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
}
