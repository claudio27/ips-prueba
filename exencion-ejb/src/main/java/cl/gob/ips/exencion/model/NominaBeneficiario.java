package cl.gob.ips.exencion.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the TB_NOMINA_BENEFICIARIO database table.
 * 
 */
@Entity
@Table(name = "TB_NOMINA_BENEFICIARIO")
@NamedQuery(name = "NominaBeneficiario.findAll", query = "SELECT t FROM NominaBeneficiario t")
public class NominaBeneficiario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5601396816666869247L;

	@Id
	@Column(name = "NOM_ID")
	private long nomId;

	@Column(name = "NOM_APE_MATERNO")
	private String nomApeMaterno;

	@Column(name = "NOM_APE_PATERNO")
	private String nomApePaterno;

	@Column(name = "NOM_DV_BENEFICIARIO")
	private String nomDvBeneficiario;

	@Column(name = "NOM_DV_INSTITUCION")
	private String nomDvInstitucion;

	@Temporal(TemporalType.DATE)
	@Column(name = "NOM_FECHA_DEV_BENEF")
	private Date nomFechaDevBenef;

	@Column(name = "NOM_NOMBRES")
	private String nomNombres;

	@Column(name = "NOM_RUN_BENEFICIARIO")
	private Integer nomRunBeneficiario;

	@Column(name = "NOM_TIPO_PENSION")
	private String nomTipoPension;

	// bi-directional many-to-one association to Periodo
	@ManyToOne
	@JoinColumn(name = "NOM_PERIODO")
	private Periodo periodo;

	// bi-directional many-to-one association to Instituciones
	@ManyToOne
	@JoinColumn(name = "NOM_RUT_INSTITUCION")
	private Instituciones instituciones;

	public NominaBeneficiario() {
	}

	public long getNomId() {
		return this.nomId;
	}

	public void setNomId(long nomId) {
		this.nomId = nomId;
	}

	public String getNomApeMaterno() {
		return this.nomApeMaterno;
	}

	public void setNomApeMaterno(String nomApeMaterno) {
		this.nomApeMaterno = nomApeMaterno;
	}

	public String getNomApePaterno() {
		return this.nomApePaterno;
	}

	public void setNomApePaterno(String nomApePaterno) {
		this.nomApePaterno = nomApePaterno;
	}

	public String getNomDvBeneficiario() {
		return this.nomDvBeneficiario;
	}

	public void setNomDvBeneficiario(String nomDvBeneficiario) {
		this.nomDvBeneficiario = nomDvBeneficiario;
	}

	public String getNomDvInstitucion() {
		return this.nomDvInstitucion;
	}

	public void setNomDvInstitucion(String nomDvInstitucion) {
		this.nomDvInstitucion = nomDvInstitucion;
	}

	public Date getNomFechaDevBenef() {
		return this.nomFechaDevBenef;
	}

	public void setNomFechaDevBenef(Date nomFechaDevBenef) {
		this.nomFechaDevBenef = nomFechaDevBenef;
	}

	public String getNomNombres() {
		return this.nomNombres;
	}

	public void setNomNombres(String nomNombres) {
		this.nomNombres = nomNombres;
	}

	public Integer getNomRunBeneficiario() {
		return this.nomRunBeneficiario;
	}

	public void setNomRunBeneficiario(Integer nomRunBeneficiario) {
		this.nomRunBeneficiario = nomRunBeneficiario;
	}

	public String getNomTipoPension() {
		return this.nomTipoPension;
	}

	public void setNomTipoPension(String nomTipoPension) {
		this.nomTipoPension = nomTipoPension;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	public Instituciones getInstituciones() {
		return instituciones;
	}

	public void setInstituciones(Instituciones instituciones) {
		this.instituciones = instituciones;
	}


	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder
				.append(periodo.getPerPeriodo() ) 		// todo revisar AAAAMM periodo
				.append(nomFechaDevBenef.toString())
				.append(instituciones.getInstRut())
				.append(instituciones.getInstDv())
				.append(nomRunBeneficiario.toString())
				.append(nomDvBeneficiario)
				.append(nomApePaterno)
				.append(nomApeMaterno)
				.append(nomNombres)
				.append(nomTipoPension);

		return 	builder.toString();
	}
}