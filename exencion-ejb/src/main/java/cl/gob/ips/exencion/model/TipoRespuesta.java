package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_TIPO_RESPUESTA_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_TIPO_RESPUESTA_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = TipoRespuesta.QUERY_FIND_ALL, query = "select o from TipoRespuesta o") })
@Table(name = "TB_PR_TIPO_RESPUESTA")
@SequenceGenerator(name = "TipoRespuesta_Id_Seq_Gen", sequenceName = "TB_PR_TIPO_RESPUESTA_ID_SEQ_GEN",
                   allocationSize = 50, initialValue = 50)
public class TipoRespuesta implements Serializable {
    private static final long serialVersionUID = -1374477507333210633L;
    
    public static final String QUERY_FIND_ALL = "TipoRespuesta.findAll";
    
    @Id
    @Column(name = "TIP_RESP_COD", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TipoRespuesta_Id_Seq_Gen")
    private Integer tipRespCod;
    @Column(name = "TIP_RESP_DESC_PENSIONADO", length = 250)
    private String tipRespDescPensionado;
    @Column(name = "TIP_RESP_DESC_TECNICA", nullable = false, length = 25)
    private String tipRespDescTecnica;
    @Temporal(TemporalType.DATE)
    @Column(name = "TIP_RESP_FEC_MODIFI")
    private Date tipRespFecModifi;
    @Column(name = "TIP_RESP_USUARIO_MODIFI", length = 20)
    private String tipRespUsuarioModifi;

    //bi-directional many-to-many association to ConsultaBeneficio
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name="TB_CONSULTA_BENEF_TIPO_RESP"
            , joinColumns={
            @JoinColumn(name="TIP_RESP_COD")
    }
            , inverseJoinColumns={
            @JoinColumn(name="CON_BEN_ID")
    })
    private List<ConsultaBeneficio> consultaBeneficios;

    public TipoRespuesta() {
    }
    
    public TipoRespuesta(Integer tipRespCod, String tipRespDescTecnica) {
        this.tipRespCod = tipRespCod;
        this.tipRespDescTecnica = tipRespDescTecnica;
    }    

    public TipoRespuesta(Integer tipRespCod, String tipRespDescPensionado, String tipRespDescTecnica,
                         Date tipRespFecModifi, String tipRespUsuarioModifi) {
        this.tipRespCod = tipRespCod;
        this.tipRespDescPensionado = tipRespDescPensionado;
        this.tipRespDescTecnica = tipRespDescTecnica;
        this.tipRespFecModifi = tipRespFecModifi;
        this.tipRespUsuarioModifi = tipRespUsuarioModifi;
    }

    public Integer getTipRespCod() {
        return tipRespCod;
    }

    public String getTipRespDescPensionado() {
        return tipRespDescPensionado;
    }

    public void setTipRespDescPensionado(String tipRespDescPensionado) {
        this.tipRespDescPensionado = tipRespDescPensionado;
    }

    public String getTipRespDescTecnica() {
        return tipRespDescTecnica;
    }

    public void setTipRespDescTecnica(String tipRespDescTecnica) {
        this.tipRespDescTecnica = tipRespDescTecnica;
    }

    public Date getTipRespFecModifi() {
        return tipRespFecModifi;
    }

    public void setTipRespFecModifi(Date tipRespFecModifi) {
        this.tipRespFecModifi = tipRespFecModifi;
    }

    public String getTipRespUsuarioModifi() {
        return tipRespUsuarioModifi;
    }

    public void setTipRespUsuarioModifi(String tipRespUsuarioModifi) {
        this.tipRespUsuarioModifi = tipRespUsuarioModifi;
    }

    public List<ConsultaBeneficio> getConsultaBeneficios() {
        return this.consultaBeneficios;
    }

    public void setTbConsultaBeneficios(List<ConsultaBeneficio> consultaBeneficios) {
        this.consultaBeneficios = consultaBeneficios;
    }
}
