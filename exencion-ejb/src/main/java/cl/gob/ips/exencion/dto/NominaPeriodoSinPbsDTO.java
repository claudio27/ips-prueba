package cl.gob.ips.exencion.dto;

import java.io.Serializable;
import java.util.Date;

public class NominaPeriodoSinPbsDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String tipoMovimiento;
    private Integer tipoBeneficio;
    private Integer runBeneficiario;
    private String dvBeneficiario;
    private Integer monto;
    private String fechaInicioPago;
    private String codigoHaber;
    private String unidadMedida;
    private String fechaVencimiento;


    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoBeneficio(Integer tipoBeneficio) {
        this.tipoBeneficio = tipoBeneficio;
    }

    public Integer getTipoBeneficio() {
        return tipoBeneficio;
    }

    public void setRunBeneficiario(Integer runBeneficiario) {
        this.runBeneficiario = runBeneficiario;
    }

    public Integer getRunBeneficiario() {
        return runBeneficiario;
    }

    public void setDvBeneficiario(String dvBeneficiario) {
        this.dvBeneficiario = dvBeneficiario;
    }

    public String getDvBeneficiario() {
        return dvBeneficiario;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setFechaInicioPago(String fechaInicioPago) {
        this.fechaInicioPago = fechaInicioPago;
    }

    public String getFechaInicioPago() {
        return fechaInicioPago;
    }

    public void setCodigoHaber(String codigoHaber) {
        this.codigoHaber = codigoHaber;
    }

    public String getCodigoHaber() {
        return codigoHaber;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }
}
