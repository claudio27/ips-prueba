package cl.gob.ips.exencion.dto;

import java.util.Date;

/**
 * Created by lpino on 03-07-17.
 */
public class AtencionPensionadoDTO {

    private String fecha;
    private String periodo;
    private String respuesta;
    private String observacion;
    private String institucionPagadora;
    private String usuario;

    public AtencionPensionadoDTO() {
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getInstitucionPagadora() {
        return institucionPagadora;
    }

    public void setInstitucionPagadora(String institucionPagadora) {
        this.institucionPagadora = institucionPagadora;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
