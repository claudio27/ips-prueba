package cl.gob.ips.exencion.enums;

/**
 * Created by alaya on 5/09/17.
 */
public enum TipoArchivosIniciales {

    NOMINA_CONSULTA_PDI(0,"Consulta PDI"),
    NOMINA_REBAJA(1,"Rebaja con APS suspendida no cobro (Suspensiones)"),
    NOMINA_BENEFICIARIO(2,"Beneficiarios Exención"),
    NOMINA_POTENCIALES_BENEFICIARIOS_EXENCION(3,"Potenciales Beneficiarios Exención"),
    NOMINA_SIN_FICHA(4,"Nómina sin ficha"),
    NOMINA_VALIDACION(5,"Nómina de Validación de la Concesión"),
    NOMINA_BPH_SIN_PBS(6,"BPH sin PBS"),
    NOMINA_POTENCIALES_APS(7,"Potenciales Beneficiarios APS");

    private Integer codigo;
    private String descripcion;

    TipoArchivosIniciales(Integer codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static TipoArchivosIniciales getInstance(Integer codigo) {
        for (TipoArchivosIniciales itemEnum : values()) {
            if (itemEnum.getCodigo() == codigo) {
                return itemEnum;
            }
        }
        return null;
    }

    public Integer getCodigo() {
        return this.codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
