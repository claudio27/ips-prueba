package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQueries({ @NamedQuery(name = "ArchEstandarHis.findAll", query = "select o from ArchEstandarHis o") })
@Table(name = "TB_ARCH_ESTANDAR_HIS")
public class ArchEstandarHis implements Serializable {
    @Column(name = "EST_DV_BENEFICIARIO", nullable = false, length = 1)
    private String estDvBeneficiario;
    @Column(name = "EST_ELIMINADO")
    private Integer estEliminado;
    @Temporal(TemporalType.DATE)
    @Column(name = "EST_FEC_ELIMINADO")
    private Date estFecEliminado;
    @Id
    @Column(name = "EST_ID", nullable = false)
    private Long estId;
    @Column(name = "EST_PERIODO_PAGO", nullable = false)
    private Integer estPeriodoPago;
    @Column(name = "EST_RUN_BENEFICIAIO", nullable = false)
    private Integer estRunBeneficiaio;
    @Column(name = "EST_USU_ELIMINADO", length = 20)
    private String estUsuEliminado;
    @ManyToOne
    @JoinColumn(name = "EST_TIPO_BENEFICIO")
    private TipoBeneficio tipoBeneficio;
    @ManyToOne
    @JoinColumn(name = "EST_ARC_ID")
    private ArchivosCargaPeriodo archivosCargaPeriodo4;

    public ArchEstandarHis() {
    }

    public ArchEstandarHis(ArchivosCargaPeriodo archivosCargaPeriodo4, String estDvBeneficiario, Integer estEliminado,
                           Date estFecEliminado, Long estId, Integer estPeriodoPago, Integer estRunBeneficiaio,
                           TipoBeneficio tipoBeneficio, String estUsuEliminado) {
        this.archivosCargaPeriodo4 = archivosCargaPeriodo4;
        this.estDvBeneficiario = estDvBeneficiario;
        this.estEliminado = estEliminado;
        this.estFecEliminado = estFecEliminado;
        this.estId = estId;
        this.estPeriodoPago = estPeriodoPago;
        this.estRunBeneficiaio = estRunBeneficiaio;
        this.tipoBeneficio = tipoBeneficio;
        this.estUsuEliminado = estUsuEliminado;
    }


    public String getEstDvBeneficiario() {
        return estDvBeneficiario;
    }

    public void setEstDvBeneficiario(String estDvBeneficiario) {
        this.estDvBeneficiario = estDvBeneficiario;
    }

    public Integer getEstEliminado() {
        return estEliminado;
    }

    public void setEstEliminado(Integer estEliminado) {
        this.estEliminado = estEliminado;
    }

    public Date getEstFecEliminado() {
        return estFecEliminado;
    }

    public void setEstFecEliminado(Date estFecEliminado) {
        this.estFecEliminado = estFecEliminado;
    }

    public Long getEstId() {
        return estId;
    }

    public void setEstId(Long estId) {
        this.estId = estId;
    }

    public Integer getEstPeriodoPago() {
        return estPeriodoPago;
    }

    public void setEstPeriodoPago(Integer estPeriodoPago) {
        this.estPeriodoPago = estPeriodoPago;
    }

    public Integer getEstRunBeneficiaio() {
        return estRunBeneficiaio;
    }

    public void setEstRunBeneficiaio(Integer estRunBeneficiaio) {
        this.estRunBeneficiaio = estRunBeneficiaio;
    }


    public String getEstUsuEliminado() {
        return estUsuEliminado;
    }

    public void setEstUsuEliminado(String estUsuEliminado) {
        this.estUsuEliminado = estUsuEliminado;
    }

    public TipoBeneficio getTipoBeneficio() {
        return tipoBeneficio;
    }

    public void setTipoBeneficio(TipoBeneficio tipoBeneficio) {
        this.tipoBeneficio = tipoBeneficio;
    }

    public ArchivosCargaPeriodo getArchivosCargaPeriodo4() {
        return archivosCargaPeriodo4;
    }

    public void setArchivosCargaPeriodo4(ArchivosCargaPeriodo archivosCargaPeriodo4) {
        this.archivosCargaPeriodo4 = archivosCargaPeriodo4;
    }
}
