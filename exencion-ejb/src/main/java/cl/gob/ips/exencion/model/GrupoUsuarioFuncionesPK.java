package cl.gob.ips.exencion.model;

import java.io.Serializable;

public class GrupoUsuarioFuncionesPK implements Serializable {
    private Integer tbGrupoUsuarios;
    private Integer tbPrFuncionalidades;

    public GrupoUsuarioFuncionesPK() {
    }

    public GrupoUsuarioFuncionesPK(Integer tbGrupoUsuarios, Integer tbPrFuncionalidades) {
        this.tbGrupoUsuarios = tbGrupoUsuarios;
        this.tbPrFuncionalidades = tbPrFuncionalidades;
    }

    public boolean equals(Object other) {
        if (other instanceof GrupoUsuarioFuncionesPK) {
            final GrupoUsuarioFuncionesPK otherGrupoUsuarioFuncionesPK = (GrupoUsuarioFuncionesPK) other;
            final boolean areEqual = true;
            return areEqual;
        }
        return false;
    }

    public int hashCode() {
        return super.hashCode();
    }

    public Integer getTbGrupoUsuarios() {
        return tbGrupoUsuarios;
    }

    public void setTbGrupoUsuarios(Integer tbGrupoUsuarios) {
        this.tbGrupoUsuarios = tbGrupoUsuarios;
    }

    public Integer getTbPrFuncionalidades() {
        return tbPrFuncionalidades;
    }

    public void setTbPrFuncionalidades(Integer tbPrFuncionalidades) {
        this.tbPrFuncionalidades = tbPrFuncionalidades;
    }
}
