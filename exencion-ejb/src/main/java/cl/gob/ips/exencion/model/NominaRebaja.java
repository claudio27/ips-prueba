package cl.gob.ips.exencion.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the TB_NOMINA_REBAJA database table.
 * 
 */
@Entity
@Table(name = "TB_NOMINA_REBAJA")
@NamedQuery(name = "NominaRebaja.findAll", query = "SELECT t FROM NominaRebaja t")
public class NominaRebaja implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2347114274583365706L;

	@Id
	@Column(name = "NOM_ID")
	private long nomId;

	@Column(name = "NOM_DV_INSTITUCION")
	private String nomDvInstitucion;

	@Column(name = "NOM_ESTADO")
	private Integer nomEstado;

	@Column(name = "NOM_RUN_BENEFICIARIO")
	private Integer nomRunBeneficiario;

	// bi-directional many-to-one association to TbPrInstitucione
	@ManyToOne
	@JoinColumn(name = "NOM_RUT_INSTITUCION")
	private Instituciones instituciones;

	// bi-directional many-to-one association to TbPeriodo
	@ManyToOne
	@JoinColumn(name = "NOM_PERIODO")
	private Periodo periodo;

	public NominaRebaja() {
	}

	public long getNomId() {
		return this.nomId;
	}

	public void setNomId(long nomId) {
		this.nomId = nomId;
	}

	public String getNomDvInstitucion() {
		return this.nomDvInstitucion;
	}

	public void setNomDvInstitucion(String nomDvInstitucion) {
		this.nomDvInstitucion = nomDvInstitucion;
	}

	public Integer getNomEstado() {
		return this.nomEstado;
	}

	public void setNomEstado(Integer nomEstado) {
		this.nomEstado = nomEstado;
	}

	public Integer getNomRunBeneficiario() {
		return this.nomRunBeneficiario;
	}

	public void setNomRunBeneficiario(Integer nomRunBeneficiario) {
		this.nomRunBeneficiario = nomRunBeneficiario;
	}

	public Instituciones getInstituciones() {
		return instituciones;
	}

	public void setInstituciones(Instituciones instituciones) {
		this.instituciones = instituciones;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder
				.append(StringUtils.leftPad(String.valueOf(nomRunBeneficiario),8, "0"))
				.append("|")
				.append(StringUtils.leftPad(String.valueOf(nomEstado),2, "0"));

		return builder.toString();
	}
}