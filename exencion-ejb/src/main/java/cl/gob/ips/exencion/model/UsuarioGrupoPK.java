package cl.gob.ips.exencion.model;

import java.io.Serializable;

public class UsuarioGrupoPK implements Serializable {
    private Integer tbPrGruposArchivo1;
    private Integer tbUsuarios1;
    private Integer tbPrNivelResponsabilidad;

    public UsuarioGrupoPK() {
    }

    public UsuarioGrupoPK(Integer tbPrGruposArchivo1, Integer tbUsuarios1, Integer tbPrNivelResponsabilidad) {
        this.tbPrGruposArchivo1 = tbPrGruposArchivo1;
        this.tbUsuarios1 = tbUsuarios1;
        this.tbPrNivelResponsabilidad = tbPrNivelResponsabilidad;
    }

    public boolean equals(Object other) {
        if (other instanceof UsuarioGrupoPK) {
            final UsuarioGrupoPK otherUsuarioGrupoPK = (UsuarioGrupoPK) other;
            final boolean areEqual = true;
            return areEqual;
        }
        return false;
    }

    public int hashCode() {
        return super.hashCode();
    }

    public Integer getTbPrGruposArchivo1() {
        return tbPrGruposArchivo1;
    }

    public void setTbPrGruposArchivo1(Integer tbPrGruposArchivo1) {
        this.tbPrGruposArchivo1 = tbPrGruposArchivo1;
    }

    public Integer getTbUsuarios1() {
        return tbUsuarios1;
    }

    public void setTbUsuarios1(Integer tbUsuarios1) {
        this.tbUsuarios1 = tbUsuarios1;
    }

    public Integer getTbPrNivelResponsabilidad() {
        return tbPrNivelResponsabilidad;
    }

    public void setTbPrNivelResponsabilidad(Integer tbPrNivelResponsabilidad) {
        this.tbPrNivelResponsabilidad = tbPrNivelResponsabilidad;
    }
}
