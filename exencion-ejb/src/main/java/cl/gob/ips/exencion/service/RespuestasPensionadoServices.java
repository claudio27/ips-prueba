package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.TipoRespuestaDao;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.TipoRespuesta;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by Cristian Angulo F. on 08-11-2016.
 */
@Stateless
@LocalBean
public class RespuestasPensionadoServices {

    @Inject
    TipoRespuestaDao dao;

    /**
     * @return
     * @throws DataAccessException
     */
    public List<TipoRespuesta> buscarTiposRespuesta() throws DataAccessException {
        return dao.buscarTiposRespuestas();
    }

    /**
     * @param tipoRespuesta
     * @throws DataAccessException
     */
    public void actualizarTipoRespuesta(final TipoRespuesta tipoRespuesta) throws DataAccessException {
        dao.actualizarTipoRespuesta(tipoRespuesta);
    }

}
