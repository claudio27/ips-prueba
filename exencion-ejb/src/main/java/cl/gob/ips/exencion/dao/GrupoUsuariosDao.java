package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.GrupoUsuarios;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;


/**
 * Created by practicante
 */
@Stateless
@LocalBean
public class GrupoUsuariosDao extends BaseDAO {

    private static final String PARAM_GRUPO_USUARIO_DESC = "gru_usu_descrip";

    /**
     * @return listaGrupoUsuarios
     * @throws DataAccessException
     */
    public List<GrupoUsuarios> getGruposUsuario() throws DataAccessException {
        List<GrupoUsuarios> listaGrupoUsuarios = null;

        super.query =  super.em.createNamedQuery(GrupoUsuarios.QUERY_FIND_ALL);

        if (!super.query.getResultList().isEmpty()) {
            listaGrupoUsuarios = super.query.getResultList();
        }
        return listaGrupoUsuarios;
    }

    /**
     * @param grupoUsuarios
     * @throws DataAccessException
     */
    public void deleteGrupoUsuarios(GrupoUsuarios grupoUsuarios) throws DataAccessException {
        if (!super.em.contains(grupoUsuarios)) {
            super.em.remove(em.merge(grupoUsuarios));
        }
    }

    /**
     * @param descripcion
     * @return GrupoUsuarios
     * @throws DataAccessException
     */
    public GrupoUsuarios findGrupoUsuario(final String descripcion) throws DataAccessException {
        GrupoUsuarios grupoUsuarios = null;

        super.query = super.em.createNamedQuery(GrupoUsuarios.QUERY_FIND_BY_DESCRIPCION)
                        .setParameter(PARAM_GRUPO_USUARIO_DESC, descripcion);

        if (!super.query.getResultList().isEmpty()) {
            grupoUsuarios = (GrupoUsuarios) super.query.getSingleResult();
        }

        return grupoUsuarios;
    }

    /**
     * @param grupoUsuarios
     * @throws DataAccessException
     */
    public void createGrupoUsuarios(final GrupoUsuarios grupoUsuarios) throws DataAccessException {
        super.em.persist(grupoUsuarios);
    }
}