package cl.gob.ips.exencion.dto;

import java.io.Serializable;

public class DetalleInformeUniversoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8043601150425256430L;

	private String run;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String vivo;
	private String rsh;
	private String focalizacion;
	private String residencia;
	private String cumpleRequisitos;

	public DetalleInformeUniversoDTO() {
		super();
	}

	public DetalleInformeUniversoDTO(String run, String nombre,
			String apellidoPaterno, String apellidoMaterno, String vivo,
			String rsh, String focalizacion, String residencia,
			String cumpleRequisitos) {
		super();
		this.run = run;
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.vivo = vivo;
		this.rsh = rsh;
		this.focalizacion = focalizacion;
		this.residencia = residencia;
		this.cumpleRequisitos = cumpleRequisitos;
	}

	public String getRun() {
		return run;
	}

	public void setRun(String run) {
		this.run = run;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getVivo() {
		return vivo;
	}

	public void setVivo(String vivo) {
		this.vivo = vivo;
	}

	public String getRsh() {
		return rsh;
	}

	public void setRsh(String rsh) {
		this.rsh = rsh;
	}

	public String getFocalizacion() {
		return focalizacion;
	}

	public void setFocalizacion(String focalizacion) {
		this.focalizacion = focalizacion;
	}

	public String getResidencia() {
		return residencia;
	}

	public void setResidencia(String residencia) {
		this.residencia = residencia;
	}

	public String getCumpleRequisitos() {
		return cumpleRequisitos;
	}

	public void setCumpleRequisitos(String cumpleRequisitos) {
		this.cumpleRequisitos = cumpleRequisitos;
	}

}
