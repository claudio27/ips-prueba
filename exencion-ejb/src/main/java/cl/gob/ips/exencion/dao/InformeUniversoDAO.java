package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.model.Pensionados;
import cl.gob.ips.exencion.model.Periodo;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Created by alaya on 13/07/17.
 */

@Stateless
@LocalBean
public class InformeUniversoDAO extends BaseDAO{

    public List<Periodo> obtenerInformeUniverso(Integer periodoDesde, Integer periodoHasta) throws DataAccessException, IpsBussinesException {

            return (List<Periodo>) super.em.createQuery("SELECT per FROM " + Periodo.class.getName() + " per "
                    + "WHERE per.perPeriodo BETWEEN :desde AND  :hasta", Periodo.class)
                    .setParameter("desde", periodoDesde)
                    .setParameter("hasta", periodoHasta)
                    .getResultList();

    }


    public List<Pensionados> obtenerInformeUniversoDeRunsPorPeriodo(Integer periodo) throws DataAccessException, IpsBussinesException {

        return (List<Pensionados>) super.em.createQuery("SELECT pen FROM " + Pensionados.class.getName() + " pen " +
            "WHERE pen.periodo.perPeriodo = :periodo", Pensionados.class)
                .setParameter("periodo", periodo)
                .getResultList();

    }





}
