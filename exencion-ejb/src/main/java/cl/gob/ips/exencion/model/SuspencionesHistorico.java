package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQueries({ @NamedQuery(name = "SuspencionesHistorico.findAll", query = "select o from SuspencionesHistorico o") })
@Table(name = "TB_SUSPENCIONES_HIS")
public class SuspencionesHistorico implements Serializable {
    private static final long serialVersionUID = -1346869388040394527L;
    @Column(name = "SUSPENCION_DV_BENEFICIARIO", nullable = false, length = 1)
    private String suspencionDvBeneficiario;
    @Column(name = "SUSPENCION_ELIMINADO")
    private Integer suspencionEliminado;
    @Id
    @Column(name = "SUSPENCION_ID", nullable = false)
    private Long suspencionId;
    @Column(name = "SUSPENCION_RUT_BENEFICIARIO", nullable = false)
    private Integer suspencionRutBeneficiario;
    @Temporal(TemporalType.DATE)
    @Column(name = "SUSPENSION_FEC_ELIMINADO")
    private Date suspensionFecEliminado;
    @Column(name = "SUSPENSION_USU_ELIMINADO", length = 20)
    private String suspensionUsuEliminado;
    @ManyToOne
    @JoinColumn(name = "SUSPENCION_ARC_ID")
    private ArchivosCargaPeriodo archivosCargaPeriodo1;

    public SuspencionesHistorico() {
    }

    public SuspencionesHistorico(ArchivosCargaPeriodo archivosCargaPeriodo1, String suspencionDvBeneficiario,
                                 Integer suspencionEliminado, Long suspencionId, Integer suspencionRutBeneficiario,
                                 Date suspensionFecEliminado, String suspensionUsuEliminado) {
        this.archivosCargaPeriodo1 = archivosCargaPeriodo1;
        this.suspencionDvBeneficiario = suspencionDvBeneficiario;
        this.suspencionEliminado = suspencionEliminado;
        this.suspencionId = suspencionId;
        this.suspencionRutBeneficiario = suspencionRutBeneficiario;
        this.suspensionFecEliminado = suspensionFecEliminado;
        this.suspensionUsuEliminado = suspensionUsuEliminado;
    }


    public String getSuspencionDvBeneficiario() {
        return suspencionDvBeneficiario;
    }

    public void setSuspencionDvBeneficiario(String suspencionDvBeneficiario) {
        this.suspencionDvBeneficiario = suspencionDvBeneficiario;
    }

    public Integer getSuspencionEliminado() {
        return suspencionEliminado;
    }

    public void setSuspencionEliminado(Integer suspencionEliminado) {
        this.suspencionEliminado = suspencionEliminado;
    }

    public Long getSuspencionId() {
        return suspencionId;
    }

    public void setSuspencionId(Long suspencionId) {
        this.suspencionId = suspencionId;
    }

    public Integer getSuspencionRutBeneficiario() {
        return suspencionRutBeneficiario;
    }

    public void setSuspencionRutBeneficiario(Integer suspencionRutBeneficiario) {
        this.suspencionRutBeneficiario = suspencionRutBeneficiario;
    }

    public Date getSuspensionFecEliminado() {
        return suspensionFecEliminado;
    }

    public void setSuspensionFecEliminado(Date suspensionFecEliminado) {
        this.suspensionFecEliminado = suspensionFecEliminado;
    }

    public String getSuspensionUsuEliminado() {
        return suspensionUsuEliminado;
    }

    public void setSuspensionUsuEliminado(String suspensionUsuEliminado) {
        this.suspensionUsuEliminado = suspensionUsuEliminado;
    }

    public ArchivosCargaPeriodo getArchivosCargaPeriodo1() {
        return archivosCargaPeriodo1;
    }

    public void setArchivosCargaPeriodo1(ArchivosCargaPeriodo archivosCargaPeriodo1) {
        this.archivosCargaPeriodo1 = archivosCargaPeriodo1;
    }
}
