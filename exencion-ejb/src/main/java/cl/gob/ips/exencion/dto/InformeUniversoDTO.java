package cl.gob.ips.exencion.dto;

import java.io.Serializable;

public class InformeUniversoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2182143234695152784L;

	private String periodo;
	private Integer vivos;
	private Integer sinSps;
	private Integer sinRsh;
	private Integer sinFocalizacion;
	private Integer sinResidencia;
	private Integer cumplenRequisitos;

	public InformeUniversoDTO() {
		super();
	}

	public InformeUniversoDTO(String periodo, Integer vivos, Integer sinSps,
			Integer sinRsh, Integer sinFocalizacion, Integer sinResidencia,
			Integer cumplenRequisitos) {
		super();
		this.periodo = periodo;
		this.vivos = vivos;
		this.sinSps = sinSps;
		this.sinRsh = sinRsh;
		this.sinFocalizacion = sinFocalizacion;
		this.sinResidencia = sinResidencia;
		this.cumplenRequisitos = cumplenRequisitos;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Integer getVivos() {
		return vivos;
	}

	public void setVivos(Integer vivos) {
		this.vivos = vivos;
	}

	public Integer getSinSps() {
		return sinSps;
	}

	public void setSinSps(Integer sinSps) {
		this.sinSps = sinSps;
	}

	public Integer getSinRsh() {
		return sinRsh;
	}

	public void setSinRsh(Integer sinRsh) {
		this.sinRsh = sinRsh;
	}

	public Integer getSinFocalizacion() {
		return sinFocalizacion;
	}

	public void setSinFocalizacion(Integer sinFocalizacion) {
		this.sinFocalizacion = sinFocalizacion;
	}

	public Integer getSinResidencia() {
		return sinResidencia;
	}

	public void setSinResidencia(Integer sinResidencia) {
		this.sinResidencia = sinResidencia;
	}

	public Integer getCumplenRequisitos() {
		return cumplenRequisitos;
	}

	public void setCumpleRequisitos(Integer cumplenRequisitos) {
		this.cumplenRequisitos = cumplenRequisitos;
	}

}
