package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * To create ID generator sequence "TB_GRUPO_USUARIOS_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_GRUPO_USUARIOS_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = PeriodoSistema.QUERY_FIND_ALL, query = "select o from PeriodoSistema o")})
@Table(name = "TB_PERIODO_SISTEMA")
public class PeriodoSistema implements Serializable {
    private static final long serialVersionUID = -7770159280449004297L;

    public static final String QUERY_FIND_ALL = "PeriodoSistema.findAll";

    @Id
    @Column(name = "PDO_PERIODO", nullable = false)
    private Integer pdoPeriodo;

    public PeriodoSistema() {
    }

    public Integer getPdoPeriodo() {
        return pdoPeriodo;
    }

    public void setPdoPeriodo(Integer pdoPeriodo) {
        this.pdoPeriodo = pdoPeriodo;
    }

}
