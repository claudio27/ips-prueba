package cl.gob.ips.exencion.dto;

import java.io.Serializable;
import java.util.Date;

public class NominasPeriodoDTO implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -3708067444571332379L;

	private String periodo;
	private Date fechaDevengamientoBeneficio;
	private Integer rutEntidad;
	private String dvEntidad;
	private Integer runBeneficiario;
	private String dvBeneficiario;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombres;
	private String tipoPension;
	private String requisitosNoAcreaditados;
	private Integer indicadorRSH;
	private Integer estado;
	private String indicadorFps;
	private Integer tipoBeneficio;
	private String tipoMovimiento;
	private Integer monto;
	private Date fechaInicioPago;
	private String codigoHaber;
	private String unidadMedida;

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Date getFechaDevengamientoBeneficio() {
		return fechaDevengamientoBeneficio;
	}

	public void setFechaDevengamientoBeneficio(Date fechaDevengamientoBeneficio) {
		this.fechaDevengamientoBeneficio = fechaDevengamientoBeneficio;
	}

	public Integer getRutEntidad() {
		return rutEntidad;
	}

	public void setRutEntidad(Integer rutEntidad) {
		this.rutEntidad = rutEntidad;
	}

	public String getDvEntidad() {
		return dvEntidad;
	}

	public void setDvEntidad(String dvEntidad) {
		this.dvEntidad = dvEntidad;
	}

	public Integer getRunBeneficiario() {
		return runBeneficiario;
	}

	public void setRunBeneficiario(Integer runBeneficiario) {
		this.runBeneficiario = runBeneficiario;
	}

	public String getDvBeneficiario() {
		return dvBeneficiario;
	}

	public void setDvBeneficiario(String dvBeneficiario) {
		this.dvBeneficiario = dvBeneficiario;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getTipoPension() {
		return tipoPension;
	}

	public void setTipoPension(String tipoPension) {
		this.tipoPension = tipoPension;
	}

	public String getRequisitosNoAcreaditados() {
		return requisitosNoAcreaditados;
	}

	public void setRequisitosNoAcreaditados(String requisitosNoAcreaditados) {
		this.requisitosNoAcreaditados = requisitosNoAcreaditados;
	}

	public Integer getIndicadorRSH() {
		return indicadorRSH;
	}

	public void setIndicadorRSH(Integer indicadorRSH) {
		this.indicadorRSH = indicadorRSH;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public String getIndicadorFps() {
		return indicadorFps;
	}

	public void setIndicadorFps(String indicadorFps) {
		this.indicadorFps = indicadorFps;
	}

	public Integer getTipoBeneficio() {
		return tipoBeneficio;
	}

	public void setTipoBeneficio(Integer tipoBeneficio) {
		this.tipoBeneficio = tipoBeneficio;
	}

	public String getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public Integer getMonto() {
		return monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

	public Date getFechaInicioPago() {
		return fechaInicioPago;
	}

	public void setFechaInicioPago(Date fechaInicioPago) {
		this.fechaInicioPago = fechaInicioPago;
	}

	public String getCodigoHaber() {
		return codigoHaber;
	}

	public void setCodigoHaber(String codigoHaber) {
		this.codigoHaber = codigoHaber;
	}

	public String getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
}