package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * To create ID generator sequence "TB_PR_MANT_EJECUCION_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_MANT_EJECUCION_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = MantencionEjecucion.QUERY_FIND_ALL, query = "select o from MantencionEjecucion o")                
})

@Table(name = "TB_PR_MANT_EJECUCION")
@SequenceGenerator(name = "MantencionEjecucion_Id_Seq_Gen", sequenceName = "TB_PR_MANT_EJECUCION_ID_SEQ_GEN",
                   allocationSize = 50, initialValue = 50)
public class MantencionEjecucion implements Serializable {
    private static final long serialVersionUID = 3526634852660916908L;
    
    public static final String QUERY_FIND_ALL = "MantencionEjecucion.findAll";
    
    @Column(name = "MANT_EJEC_CONCEPTO", nullable = false, length = 20)
    private String mantEjecConcepto;
    @Column(name = "MANT_EJEC_DESCRIPCION", nullable = false, length = 30)
    private String mantEjecDescripcion;
    @Temporal(TemporalType.DATE)
    @Column(name = "MANT_EJEC_FEC_MODIFICACION")
    private Date mantEjecFecModificacion;
    @Column(name = "MANT_EJEC_FRECUENCIA")
    private Integer mantEjecFrecuencia;
    @Id
    @Column(name = "MANT_EJEC_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MantencionEjecucion_Id_Seq_Gen")
    private Integer mantEjecId;
    @Column(name = "MANT_EJEC_USU_MODIFICA", length = 20)
    private String mantEjecUsuModifica;
    @Column(name = "MANT_EJEC_VAL_INI_1")
    private Integer mantEjecValIni1;
    @Column(name = "MANT_EJEC_VAL_INI_2")
    private Integer mantEjecValIni2;
    @Column(name = "MANT_EJEC_VAL_INI_3")
    private Integer mantEjecValIni3;

    public MantencionEjecucion() {
    }

    public MantencionEjecucion(String mantEjecConcepto, String mantEjecDescripcion, Date mantEjecFecModificacion,
                               Integer mantEjecFrecuencia, Integer mantEjecId, String mantEjecUsuModifica,
                               Integer mantEjecValIni1, Integer mantEjecValIni2, Integer mantEjecValIni3) {
        this.mantEjecConcepto = mantEjecConcepto;
        this.mantEjecDescripcion = mantEjecDescripcion;
        this.mantEjecFecModificacion = mantEjecFecModificacion;
        this.mantEjecFrecuencia = mantEjecFrecuencia;
        this.mantEjecId = mantEjecId;
        this.mantEjecUsuModifica = mantEjecUsuModifica;
        this.mantEjecValIni1 = mantEjecValIni1;
        this.mantEjecValIni2 = mantEjecValIni2;
        this.mantEjecValIni3 = mantEjecValIni3;
    }

    public String getMantEjecConcepto() {
        return mantEjecConcepto;
    }

    public void setMantEjecConcepto(String mantEjecConcepto) {
        this.mantEjecConcepto = mantEjecConcepto;
    }

    public String getMantEjecDescripcion() {
        return mantEjecDescripcion;
    }

    public void setMantEjecDescripcion(String mantEjecDescripcion) {
        this.mantEjecDescripcion = mantEjecDescripcion;
    }

    public Date getMantEjecFecModificacion() {
        return mantEjecFecModificacion;
    }

    public void setMantEjecFecModificacion(Date mantEjecFecModificacion) {
        this.mantEjecFecModificacion = mantEjecFecModificacion;
    }

    public Integer getMantEjecFrecuencia() {
        return mantEjecFrecuencia;
    }

    public void setMantEjecFrecuencia(Integer mantEjecFrecuencia) {
        this.mantEjecFrecuencia = mantEjecFrecuencia;
    }

    public Integer getMantEjecId() {
        return mantEjecId;
    }

    public String getMantEjecUsuModifica() {
        return mantEjecUsuModifica;
    }

    public void setMantEjecUsuModifica(String mantEjecUsuModifica) {
        this.mantEjecUsuModifica = mantEjecUsuModifica;
    }

    public Integer getMantEjecValIni1() {
        return mantEjecValIni1;
    }

    public void setMantEjecValIni1(Integer mantEjecValIni1) {
        this.mantEjecValIni1 = mantEjecValIni1;
    }

    public Integer getMantEjecValIni2() {
        return mantEjecValIni2;
    }

    public void setMantEjecValIni2(Integer mantEjecValIni2) {
        this.mantEjecValIni2 = mantEjecValIni2;
    }

    public Integer getMantEjecValIni3() {
        return mantEjecValIni3;
    }

    public void setMantEjecValIni3(Integer mantEjecValIni3) {
        this.mantEjecValIni3 = mantEjecValIni3;
    }
}
