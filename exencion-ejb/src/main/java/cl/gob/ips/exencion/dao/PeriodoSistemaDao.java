package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.GrupoUsuarios;
import cl.gob.ips.exencion.model.PeriodoSistema;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;


/**
 * Created by practicante
 */
@Stateless
@LocalBean
public class PeriodoSistemaDao extends BaseDAO {

    /**
     * @return listaPeriodoSistema
     * @throws DataAccessException
     */
    public List<PeriodoSistema> getGruposUsuario() throws DataAccessException {
        List<PeriodoSistema> listaPeriodoSistema = null;

        super.query =  super.em.createNamedQuery(PeriodoSistema.QUERY_FIND_ALL);

        if (!super.query.getResultList().isEmpty()) {
            listaPeriodoSistema = super.query.getResultList();
        }
        return listaPeriodoSistema;
    }


}