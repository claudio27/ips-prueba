package cl.gob.ips.exencion.dto;

import java.io.Serializable;
import java.util.Date;

public class ResultadoValidacionArchivosDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -908412136934289741L;

	private Long id;
	private String nombre;
	private String nombreArchivoCargado;
	private String fechaCarga;
	private Integer registrosInformados;
	private Integer registrosCargados;
	private Integer filasSinDatos;
	private Integer registrosIlegibles;
	private String formatoNombre;
	private String estructura;
	private String resultadoCarga;
	private Boolean isFallido;
	private String nombreArchInicial;
	private String mensajeError;


	public ResultadoValidacionArchivosDTO() {
		super();
	}

	public ResultadoValidacionArchivosDTO(String nombre, String nombreArchivoCargado, String fechaCarga,
			Integer registrosInformados, Integer registrosCargados,
			Integer filasSinDatos, Integer registrosIlegibles,
			String formatoNombre, String estructura, String resultadoCarga, Boolean isFallido, String mensajeError) {
		super();
		this.nombre = nombre;
		this.nombreArchivoCargado = nombreArchivoCargado;
		this.fechaCarga = fechaCarga;
		this.registrosInformados = registrosInformados;
		this.registrosCargados = registrosCargados;
		this.filasSinDatos = filasSinDatos;
		this.registrosIlegibles = registrosIlegibles;
		this.formatoNombre = formatoNombre;
		this.estructura = estructura;
		this.resultadoCarga = resultadoCarga;
		this.isFallido = isFallido;
		this.mensajeError = mensajeError;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombreArchivoCargado() {
		return nombreArchivoCargado;
	}

	public void setNombreArchivoCargado(String nombreArchivoCargado) {
		this.nombreArchivoCargado = nombreArchivoCargado;
	}

	public String getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(String fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	public Integer getRegistrosInformados() {
		return registrosInformados;
	}

	public void setRegistrosInformados(Integer registrosInformados) {
		this.registrosInformados = registrosInformados;
	}

	public Integer getRegistrosCargados() {
		return registrosCargados;
	}

	public void setRegistrosCargados(Integer registrosCargados) {
		this.registrosCargados = registrosCargados;
	}

	public Integer getFilasSinDatos() {
		return filasSinDatos;
	}

	public void setFilasSinDatos(Integer filasSinDatos) {
		this.filasSinDatos = filasSinDatos;
	}

	public Integer getRegistrosIlegibles() {
		return registrosIlegibles;
	}

	public void setRegistrosIlegibles(Integer registrosIlegibles) {
		this.registrosIlegibles = registrosIlegibles;
	}

	public String getFormatoNombre() {
		return formatoNombre;
	}

	public void setFormatoNombre(String formatoNombre) {
		this.formatoNombre = formatoNombre;
	}

	public String getEstructura() {
		return estructura;
	}

	public void setEstructura(String estructura) {
		this.estructura = estructura;
	}

	public String getResultadoCarga() {
		return resultadoCarga;
	}

	public void setResultadoCarga(String resultadoCarga) {
		this.resultadoCarga = resultadoCarga;
	}

	public Boolean getFallido() {
		return isFallido;
	}

	public void setFallido(Boolean fallido) {
		isFallido = fallido;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreArchInicial() {
		return nombreArchInicial;
	}

	public void setNombreArchInicial(String nombreArchInicial) {
		this.nombreArchInicial = nombreArchInicial;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
}
