package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.EstadoInstitucion;
import cl.gob.ips.exencion.model.Instituciones;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by rodrigo.reyesco@gmail.com on 07-10-2016.
 */
@Stateless
@LocalBean
public class InstitucionPagadoraDao extends BaseDAO {

    private static final String PARAM_RUT = "rutInstitucion";

    private static final String PARAM_NOMBRE = "nombreInstitucion";

    private static final String PARAM_TIPO = "tipoInstitucion";

    private static final String PARAM_ESTADO = "estadoInstitucion";

    /**
     * @param institucion
     * @return List<Instituciones>
     * @throws DataAccessException
     */
    public List<Instituciones> buscarInstitucionesPorCriterio(final Instituciones institucion) throws DataAccessException {
        List<Instituciones> instituciones = null;

        super.query = super.em.createNamedQuery(Instituciones.QUERY_FIND_BY_SEARCH_CRITERIA, Instituciones.class)
                .setParameter(PARAM_RUT, institucion.getInstRut())
                .setParameter(PARAM_NOMBRE, "%"+institucion.getInstNombre()+"%")
                .setParameter(PARAM_TIPO, institucion.getTbPrTipoInstitucion().getTipInstCod())
                .setParameter(PARAM_ESTADO, new EstadoInstitucion(new Integer(1)));

        if (!super.query.getResultList().isEmpty()) {
            instituciones = super.query.getResultList();
        }

        return instituciones;
    }

    /**
     *
     * @param institucion
     * @return Instituciones
     * @throws DataAccessException
     */
    public Instituciones findInstitucionByRut(final Instituciones institucion) throws DataAccessException{
        Instituciones instituciones = null;

        super.query = super.em.createNamedQuery(Instituciones.QUERY_FIND_BY_RUT)
                .setParameter(PARAM_RUT, institucion.getInstRut());

        if (!super.query.getResultList().isEmpty()) {
            instituciones = (Instituciones) super.query.getSingleResult();
        }

        return instituciones;
    }

    /**
     * @param institucion
     * @throws DataAccessException
     */
    public void actualizarIntitucion(final Instituciones institucion) throws DataAccessException {
        super.em.merge(institucion);
}

    /**
     * @param instituciones
     * @throws DataAccessException
     */
    public void actualizaInstituciones(final List<Instituciones> instituciones) throws DataAccessException {
        for (Instituciones institucion : instituciones) {
            super.em.createNamedQuery(Instituciones.UPDATE_STATUS).
                    setParameter(PARAM_ESTADO, new EstadoInstitucion(institucion.getTbPrEstInstitucion().getEstInstCod(), institucion.getTbPrEstInstitucion().getEstInstDescripcion())).
                    setParameter(PARAM_RUT, institucion.getInstRut()).
                    executeUpdate();

        }
    }

    /**
     *
     * @param institucion
     * @throws DataAccessException
     */
    public void creaInstitucion(final Instituciones institucion) throws DataAccessException{
        super.em.persist(institucion);
    }
    
    public List<Instituciones> buscarInstitucionesHabilitadas() throws EJBTransactionRolledbackException {
        List<Instituciones> instituciones = null;

        TypedQuery<Instituciones> tq = super.em.createNamedQuery(Instituciones.QUERY_FIND_BY_ESTADO,
                Instituciones.class).setParameter(PARAM_ESTADO, new Integer(1));

        if (!tq.getResultList().isEmpty()) {
            instituciones = tq.getResultList();
        }

        return instituciones;
    }
}
