package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.ResultadoValidacionArchivosDAO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.ArchivosCargaPeriodo;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by alaya on 29/08/17.
 */
public class ResultadoValidacionArchivosServices {

    @Inject
    private ResultadoValidacionArchivosDAO validacionArchivosDAO;

    public List<ArchivosCargaPeriodo> obtenerResultadoValidacionArchivosDAO(Integer periodo) throws DataAccessException {
        return validacionArchivosDAO.obtenerResultadoValidacionArchivosDAO(periodo);
    }
}


