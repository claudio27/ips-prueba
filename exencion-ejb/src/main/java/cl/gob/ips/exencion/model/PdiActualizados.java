package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQueries({ @NamedQuery(name = "PdiActualizados.findAll", query = "select o from PdiActualizados o") })
@Table(name = "TB_PDI_ACT")
public class PdiActualizados implements Serializable {
    private static final long serialVersionUID = 9016658035814293382L;
    @Column(name = "PDI_ACT_DESDE_1", length = 8)
    private String pdiActDesde1;
    @Column(name = "PDI_ACT_DESDE_2", length = 8)
    private String pdiActDesde2;
    @Column(name = "PDI_ACT_ELIMINADO")
    private Integer pdiActEliminado;
    @Temporal(TemporalType.DATE)
    @Column(name = "PDI_ACT_FEC_ELIMINADO")
    private Date pdiActFecEliminado;
    @Column(name = "PDI_ACT_FEC_NAC", length = 8)
    private String pdiActFecNac;
    @Temporal(TemporalType.DATE)
    @Column(name = "PDI_ACT_FEC_RESPUESTA", nullable = false)
    private Date pdiActFecRespuesta;
    @Column(name = "PDI_ACT_HASTA_1", length = 8)
    private String pdiActHasta1;
    @Column(name = "PDI_ACT_HASTA_2", length = 8)
    private String pdiActHasta2;
    @Id
    @Column(name = "PDI_ACT_ID", nullable = false)
    private Long pdiActId;
    @Column(name = "PDI_ACT_MATERNO", length = 20)
    private String pdiActMaterno;
    @Column(name = "PDI_ACT_NOMBRES", length = 30)
    private String pdiActNombres;
    @Column(name = "PDI_ACT_PATERNO", length = 20)
    private String pdiActPaterno;
    @Column(name = "PDI_ACT_PDI_ID", nullable = false)
    private Long pdiActPdiId;
    @Column(name = "PDI_ACT_RESPUESTA_1")
    private Integer pdiActRespuesta1;
    @Column(name = "PDI_ACT_RESPUESTA_2")
    private Integer pdiActRespuesta2;
    @Column(name = "PDI_ACT_RUN", nullable = false)
    private Integer pdiActRun;
    @Column(name = "PDI_ACT_SEXO", length = 1)
    private String pdiActSexo;
    @Column(name = "PDI_ACT_USU_ELIMINADO", length = 20)
    private String pdiActUsuEliminado;

    public PdiActualizados() {
    }

    public PdiActualizados(String pdiActDesde1, String pdiActDesde2, Integer pdiActEliminado, Date pdiActFecEliminado,
                           String pdiActFecNac, Date pdiActFecRespuesta, String pdiActHasta1, String pdiActHasta2,
                           Long pdiActId, String pdiActMaterno, String pdiActNombres, String pdiActPaterno,
                           Long pdiActPdiId, Integer pdiActRespuesta1, Integer pdiActRespuesta2, Integer pdiActRun,
                           String pdiActSexo, String pdiActUsuEliminado) {
        this.pdiActDesde1 = pdiActDesde1;
        this.pdiActDesde2 = pdiActDesde2;
        this.pdiActEliminado = pdiActEliminado;
        this.pdiActFecEliminado = pdiActFecEliminado;
        this.pdiActFecNac = pdiActFecNac;
        this.pdiActFecRespuesta = pdiActFecRespuesta;
        this.pdiActHasta1 = pdiActHasta1;
        this.pdiActHasta2 = pdiActHasta2;
        this.pdiActId = pdiActId;
        this.pdiActMaterno = pdiActMaterno;
        this.pdiActNombres = pdiActNombres;
        this.pdiActPaterno = pdiActPaterno;
        this.pdiActPdiId = pdiActPdiId;
        this.pdiActRespuesta1 = pdiActRespuesta1;
        this.pdiActRespuesta2 = pdiActRespuesta2;
        this.pdiActRun = pdiActRun;
        this.pdiActSexo = pdiActSexo;
        this.pdiActUsuEliminado = pdiActUsuEliminado;
    }

    public String getPdiActDesde1() {
        return pdiActDesde1;
    }

    public void setPdiActDesde1(String pdiActDesde1) {
        this.pdiActDesde1 = pdiActDesde1;
    }

    public String getPdiActDesde2() {
        return pdiActDesde2;
    }

    public void setPdiActDesde2(String pdiActDesde2) {
        this.pdiActDesde2 = pdiActDesde2;
    }

    public Integer getPdiActEliminado() {
        return pdiActEliminado;
    }

    public void setPdiActEliminado(Integer pdiActEliminado) {
        this.pdiActEliminado = pdiActEliminado;
    }

    public Date getPdiActFecEliminado() {
        return pdiActFecEliminado;
    }

    public void setPdiActFecEliminado(Date pdiActFecEliminado) {
        this.pdiActFecEliminado = pdiActFecEliminado;
    }

    public String getPdiActFecNac() {
        return pdiActFecNac;
    }

    public void setPdiActFecNac(String pdiActFecNac) {
        this.pdiActFecNac = pdiActFecNac;
    }

    public Date getPdiActFecRespuesta() {
        return pdiActFecRespuesta;
    }

    public void setPdiActFecRespuesta(Date pdiActFecRespuesta) {
        this.pdiActFecRespuesta = pdiActFecRespuesta;
    }

    public String getPdiActHasta1() {
        return pdiActHasta1;
    }

    public void setPdiActHasta1(String pdiActHasta1) {
        this.pdiActHasta1 = pdiActHasta1;
    }

    public String getPdiActHasta2() {
        return pdiActHasta2;
    }

    public void setPdiActHasta2(String pdiActHasta2) {
        this.pdiActHasta2 = pdiActHasta2;
    }

    public Long getPdiActId() {
        return pdiActId;
    }

    public void setPdiActId(Long pdiActId) {
        this.pdiActId = pdiActId;
    }

    public String getPdiActMaterno() {
        return pdiActMaterno;
    }

    public void setPdiActMaterno(String pdiActMaterno) {
        this.pdiActMaterno = pdiActMaterno;
    }

    public String getPdiActNombres() {
        return pdiActNombres;
    }

    public void setPdiActNombres(String pdiActNombres) {
        this.pdiActNombres = pdiActNombres;
    }

    public String getPdiActPaterno() {
        return pdiActPaterno;
    }

    public void setPdiActPaterno(String pdiActPaterno) {
        this.pdiActPaterno = pdiActPaterno;
    }

    public Long getPdiActPdiId() {
        return pdiActPdiId;
    }

    public void setPdiActPdiId(Long pdiActPdiId) {
        this.pdiActPdiId = pdiActPdiId;
    }

    public Integer getPdiActRespuesta1() {
        return pdiActRespuesta1;
    }

    public void setPdiActRespuesta1(Integer pdiActRespuesta1) {
        this.pdiActRespuesta1 = pdiActRespuesta1;
    }

    public Integer getPdiActRespuesta2() {
        return pdiActRespuesta2;
    }

    public void setPdiActRespuesta2(Integer pdiActRespuesta2) {
        this.pdiActRespuesta2 = pdiActRespuesta2;
    }

    public Integer getPdiActRun() {
        return pdiActRun;
    }

    public void setPdiActRun(Integer pdiActRun) {
        this.pdiActRun = pdiActRun;
    }

    public String getPdiActSexo() {
        return pdiActSexo;
    }

    public void setPdiActSexo(String pdiActSexo) {
        this.pdiActSexo = pdiActSexo;
    }

    public String getPdiActUsuEliminado() {
        return pdiActUsuEliminado;
    }

    public void setPdiActUsuEliminado(String pdiActUsuEliminado) {
        this.pdiActUsuEliminado = pdiActUsuEliminado;
    }
}
