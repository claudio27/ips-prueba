package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Funcionalidades;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by practicante.
 */
@Stateless
@LocalBean
public class FuncionalidadesDAO extends BaseDAO {


    private static final String PARAM_CODIGO_GRUPO_USUARIO = "gru_fun_cod";

    private static final String PARAM_NOMBRE_FUNCIONALIDAD = "nombre_funcion";


    /**
     * @return
     * @throws DataAccessException
     */
    public List<Funcionalidades> getFuncionalidadesPerteneceGrupo(final Integer cod) throws DataAccessException {
        List<Funcionalidades> funcionalidadesPertenece = new ArrayList<>();

        TypedQuery<Funcionalidades> query = super.em.createNamedQuery(Funcionalidades.QUERY_FIND_FUNCIONES_PERTENECE_GRUPO, Funcionalidades.class)
                .setParameter(PARAM_CODIGO_GRUPO_USUARIO, cod);
        if (!query.getResultList().isEmpty()){
            funcionalidadesPertenece = query.getResultList();
        }
        return funcionalidadesPertenece;
    }

    /**
     * @return
     * @throws DataAccessException
     */
    public List<Funcionalidades> getFuncionalidadesNoPerteneceGrupo(final Integer cod) throws DataAccessException {
        List<Funcionalidades> funcionalidadesNoPertenece = new ArrayList<>();

        TypedQuery<Funcionalidades> query = super.em.createNamedQuery(Funcionalidades.QUERY_FIND_FUNCIONES_NO_PERTENECE_GRUPO, Funcionalidades.class)
                .setParameter(PARAM_CODIGO_GRUPO_USUARIO, cod);
        if (!query.getResultList().isEmpty()){
            funcionalidadesNoPertenece = query.getResultList();
        }
        return funcionalidadesNoPertenece;
    }

    /**
     * @return
     * @throws DataAccessException
     */
    public Funcionalidades validarFuncionalidadPerteneceGru(String nomFuncion, final Integer cod) throws DataAccessException {

        Funcionalidades funcionalidades = null;

        super.query = super.em.createNamedQuery(Funcionalidades.QUERY_FIND_FUNCIONALIDAD_GRUPO)
                .setParameter(PARAM_NOMBRE_FUNCIONALIDAD, nomFuncion)
                .setParameter("gru_usu_cod", cod);

        if (!super.query.getResultList().isEmpty()) {
            funcionalidades = (Funcionalidades) super.query.getSingleResult();
        }

        return funcionalidades;
    }

    /**
     * @return
     * @throws DataAccessException
     */
    public Funcionalidades validarFuncionalidadNoPerteneceGru(String nomFuncion, final Integer cod) throws DataAccessException {

        Funcionalidades funcionalidades = null;

        super.query = super.em.createNamedQuery(Funcionalidades.QUERY_FIND_FUNCIONALIDAD_NO_GRUPO)
                .setParameter(PARAM_NOMBRE_FUNCIONALIDAD, nomFuncion)
                .setParameter("gru_usu_cod", cod);

        if (!super.query.getResultList().isEmpty()) {
            funcionalidades = (Funcionalidades) super.query.getSingleResult();
        }

        return funcionalidades;
    }

}
