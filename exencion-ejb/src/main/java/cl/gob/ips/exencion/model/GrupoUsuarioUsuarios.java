package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NamedQueries({
        @NamedQuery(name = "GrupoUsuarioUsuarios.findAll", query = "select o from GrupoUsuarioUsuarios o") })

@Table(name = "TB_GRUPUSU_USU")
@IdClass(GrupoUsuarioUsuariosPK.class)
public class GrupoUsuarioUsuarios implements Serializable {
    private static final long serialVersionUID = -1070481481185762882L;
    @ManyToOne
    @Id
    @JoinColumn(name = "GRU_USU_COD")
    private GrupoUsuarios tbGrupoUsuarios1;
    @ManyToOne
    @Id
    @JoinColumn(name = "USU_COD")
    private Usuarios tbUsuarios;

    public GrupoUsuarioUsuarios() {
    }

    public GrupoUsuarioUsuarios(GrupoUsuarios tbGrupoUsuarios1, Usuarios tbUsuarios) {
        this.tbGrupoUsuarios1 = tbGrupoUsuarios1;
        this.tbUsuarios = tbUsuarios;
    }


    public GrupoUsuarios getTbGrupoUsuarios1() {
        return tbGrupoUsuarios1;
    }

    public void setTbGrupoUsuarios1(GrupoUsuarios tbGrupoUsuarios1) {
        this.tbGrupoUsuarios1 = tbGrupoUsuarios1;
    }

    public Usuarios getTbUsuarios() {
        return tbUsuarios;
    }

    public void setTbUsuarios(Usuarios tbUsuarios) {
        this.tbUsuarios = tbUsuarios;
    }
}