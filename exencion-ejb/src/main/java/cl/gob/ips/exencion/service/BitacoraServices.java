/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.BitacoraDao;
import cl.gob.ips.exencion.dto.EventosProcesosSistemaDTO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.model.Bitacora;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Cristian Angulo
 */
@Stateless
@LocalBean
public class BitacoraServices {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
        @Inject
    BitacoraDao dao;
        
    /**
     *
     * @param bitacora
     * @throws DataAccessException
     */
    public void creaBitacora(final Bitacora bitacora) throws DataAccessException {
        dao.creaBitacora(bitacora);
    }        
    

    public List<EventosProcesosSistemaDTO> obtenerBitacoraPorPeriodo(Integer periodo) throws DataAccessException, IpsBussinesException {

        List<Bitacora> listaBitacora = null;
        List<EventosProcesosSistemaDTO> eventos = null;

        if(dao.obtenerBitacoraPorPeriodo(periodo)!=null){
            listaBitacora = dao.obtenerBitacoraPorPeriodo(periodo);
            eventos = new ArrayList<EventosProcesosSistemaDTO>();
            for (Bitacora bit : listaBitacora) {
                EventosProcesosSistemaDTO elem = new EventosProcesosSistemaDTO();
                this.mapeaObjetoBDaDTO(elem, bit);

                eventos.add(elem);
            }
        }

        return eventos;
    }

    public void mapeaObjetoBDaDTO(EventosProcesosSistemaDTO evento, Bitacora bitacora){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        evento.setDescripcion(bitacora.getBitDescripcionEvento()!=null ? bitacora.getBitDescripcionEvento():"");
        evento.setDestinatarioNotificacion(bitacora.getBitDestinatarios()!=null ? bitacora.getBitDestinatarios():"");
        evento.setFecha(bitacora.getBitFechaEvento()!=null ? dateFormat.format(bitacora.getBitFechaEvento()) :"");
        evento.setResponsables(bitacora.getBitResponsable()!=null ? bitacora.getBitResponsable():"");
        evento.setEstadoNotificacion(bitacora.getTbPrEstadoNotificacion().getEstNotDescripcion()!=null ? bitacora.getTbPrEstadoNotificacion().getEstNotDescripcion():"");
        evento.setTipoEvento(bitacora.getTbPrTipoEventos().getTipEvenDescripcion()!=null ? bitacora.getTbPrTipoEventos().getTipEvenDescripcion():"");

    }
}
