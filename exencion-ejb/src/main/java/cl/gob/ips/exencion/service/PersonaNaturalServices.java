package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.PersonaNaturalDao;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.PersonaNatural;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * @author rereyes@emergya.com
 * @since 18-08-2016
 */
@Stateless
@LocalBean
public class PersonaNaturalServices {

    @Inject
    PersonaNaturalDao dao;

    /**
     * servicio de negocio que retorna una lista de PERSONA_NATURAL desde la base de datos,
     * se implementa este metodo para incluir alguna logica de negicio independiente de la base de datos.
     * @return List<PersonaNatural>
     */
    public List<PersonaNatural> getAllPersonas() throws DataAccessException{
        return dao.getPersonas();
    }

    public List<PersonaNatural> getPersonasByApellidoPaterno(final String apellidoPaterno) throws DataAccessException{
        return dao.getPersonasByApellidoPaterno(apellidoPaterno);
    }
}
