package cl.gob.ips.exencion.service.util;

import cl.gob.ips.exencion.enums.TipoArchivosIniciales;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.service.NominasInstitucionesServices;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alaya on 5/09/17.
 */
public class SeleccionadorDeNominas {

    @Inject
    private NominasInstitucionesServices nominasInstitucionesServices;

    public List obtieneDatos(Integer periodo, String nombreNomina) throws DataAccessException {

        List resultado = new ArrayList();

        if(TipoArchivosIniciales.NOMINA_CONSULTA_PDI.getDescripcion().equals(nombreNomina)){
            resultado = nominasInstitucionesServices.obtenerNominaConsultaPdi(periodo);
        }else if(TipoArchivosIniciales.NOMINA_REBAJA.getDescripcion().equals(nombreNomina)){
            resultado = nominasInstitucionesServices.obtenerNominaRebaja(periodo);
        }else if(TipoArchivosIniciales.NOMINA_BENEFICIARIO.getDescripcion().equals(nombreNomina)){
            resultado = nominasInstitucionesServices.obtenerNominaBeneficiario(periodo);
        }else if(TipoArchivosIniciales.NOMINA_POTENCIALES_BENEFICIARIOS_EXENCION.getDescripcion().equals(nombreNomina)){
            resultado = nominasInstitucionesServices.obtenerNominaPotBeneficiario(periodo);
        }else if(TipoArchivosIniciales.NOMINA_SIN_FICHA.getDescripcion().equals(nombreNomina)){
            resultado = nominasInstitucionesServices.obtenerNominaPenSinFicha(periodo);
        }else if(TipoArchivosIniciales.NOMINA_BPH_SIN_PBS.getDescripcion().equals(nombreNomina)){
            resultado = nominasInstitucionesServices.obtenerNominaBphSinPbs(periodo);
        }else if(TipoArchivosIniciales.NOMINA_POTENCIALES_APS.getDescripcion().equals(nombreNomina)){
            resultado = nominasInstitucionesServices.obtenerNominaPotencialesAps(periodo);
        }else{
            throw new DataAccessException("No se encontro archivo de nomina");
        }


        return resultado;
    }
}
