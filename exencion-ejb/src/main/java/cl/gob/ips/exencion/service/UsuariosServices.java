package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.UsuariosDAO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Usuarios;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by lpino on 26-07-17.
 */
@Stateless
@LocalBean
public class UsuariosServices {

    @Inject
    private UsuariosDAO usuariosDAOService;

    public List<Integer> obtenerFuncionalidades(String nombreUsuario) throws DataAccessException {
        return this.usuariosDAOService.obtenerFuncionalidades(nombreUsuario);
    }
    
    public List<BigDecimal> obtenerGrupos(String nombreUsuario) throws DataAccessException {
        return this.usuariosDAOService.obtenerGrupos(nombreUsuario);
    }

    public Usuarios encontrarUsuario(String nomUser) throws DataAccessException{
        return this.usuariosDAOService.existeUsuario(nomUser);
    }

    /**
     *
     * @return
     * @throws DataAccessException
     */
    public List<Usuarios> getAllUsuariosPerteneceGrupo(final Integer cod) throws DataAccessException {
        return this.usuariosDAOService.getUsuariosGrupo(cod);
    }


    /**
     *
     * @return
     * @throws DataAccessException
     */
    public List<Usuarios> getAllNoUsuariosNoPerteneceGrupo(final Integer cod) throws DataAccessException {
        return this.usuariosDAOService.getNoUsuariosGrupo(cod);
    }

    /**
     *
     * @return
     * @throws DataAccessException
     */
    public Usuarios validarUsuarioPerteneceGru(String nomUser, final Integer cod) throws DataAccessException {
        return this.usuariosDAOService.validarUsuarioPerteneceGru(nomUser, cod);
    }


    /**
     *
     * @return
     * @throws DataAccessException
     */
    public Usuarios validarUsuarioNoPerteneceGru(String nomUser, final Integer cod) throws DataAccessException {
        return this.usuariosDAOService.validarUsuarioNoPerteneceGru(nomUser, cod);
    }
}
