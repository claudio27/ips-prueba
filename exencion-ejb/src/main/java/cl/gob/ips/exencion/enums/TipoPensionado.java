package cl.gob.ips.exencion.enums;

/**
 * Created by alaya on 17/07/17.
 */
public enum TipoPensionado {

    //1 externo, 2 interno , 3 mixto y 4 sólo BPH

    EXTERNO(1, "Externo"),
    INTERNO(2, "Interno"),
    MIXTO(3, "Mixto"),
    SOLO_BPH(4, "Sólo BPH");

    private Integer codigo;
    private String descripcion;

    TipoPensionado(Integer codigo, String descripcion){
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static TipoPensionado getInstance(Integer codigo){
        for(TipoPensionado itemEnum : values()){
            if(itemEnum.getCodigo() == codigo){
                return itemEnum;
            }
        }
        return null;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
