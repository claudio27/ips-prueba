package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * To create ID generator sequence "TB_SIS_REBAJA_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_SIS_REBAJA_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "SisRebaja.findAll", query = "select o from SisRebaja o") })
@Table(name = "TB_SIS_REBAJA")
@SequenceGenerator(name = "SisRebaja_Id_Seq_Gen", sequenceName = "TB_SIS_REBAJA_ID_SEQ_GEN", allocationSize = 50,
                   initialValue = 50)
public class SisRebaja implements Serializable {
    private static final long serialVersionUID = -1477109555523873424L;
    @Column(name = "SIS_REB_ELIMINADO")
    private Integer sisRebEliminado;
    @Column(name = "SIS_REB_ESTADO_SIS", nullable = false)
    private Integer sisRebEstadoSis;
    @Temporal(TemporalType.DATE)
    @Column(name = "SIS_REB_FEC_ELIMINADO")
    private Date sisRebFecEliminado;
    @Id
    @Column(name = "SIS_REB_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SisRebaja_Id_Seq_Gen")
    private BigDecimal sisRebId;
    @Column(name = "SIS_REB_RUN", nullable = false)
    private Integer sisRebRun;
    @Column(name = "SIS_REB_USU_ELIMINADO", length = 20)
    private String sisRebUsuEliminado;
    @ManyToOne
    @JoinColumn(name = "SIS_REB_PERIODO")
    private Periodo tbPeriodo5;

    public SisRebaja() {
    }

    public SisRebaja(Integer sisRebEliminado, Integer sisRebEstadoSis, Date sisRebFecEliminado, BigDecimal sisRebId,
                     Periodo tbPeriodo5, Integer sisRebRun, String sisRebUsuEliminado) {
        this.sisRebEliminado = sisRebEliminado;
        this.sisRebEstadoSis = sisRebEstadoSis;
        this.sisRebFecEliminado = sisRebFecEliminado;
        this.sisRebId = sisRebId;
        this.tbPeriodo5 = tbPeriodo5;
        this.sisRebRun = sisRebRun;
        this.sisRebUsuEliminado = sisRebUsuEliminado;
    }

    public Integer getSisRebEliminado() {
        return sisRebEliminado;
    }

    public void setSisRebEliminado(Integer sisRebEliminado) {
        this.sisRebEliminado = sisRebEliminado;
    }

    public Integer getSisRebEstadoSis() {
        return sisRebEstadoSis;
    }

    public void setSisRebEstadoSis(Integer sisRebEstadoSis) {
        this.sisRebEstadoSis = sisRebEstadoSis;
    }

    public Date getSisRebFecEliminado() {
        return sisRebFecEliminado;
    }

    public void setSisRebFecEliminado(Date sisRebFecEliminado) {
        this.sisRebFecEliminado = sisRebFecEliminado;
    }

    public BigDecimal getSisRebId() {
        return sisRebId;
    }


    public Integer getSisRebRun() {
        return sisRebRun;
    }

    public void setSisRebRun(Integer sisRebRun) {
        this.sisRebRun = sisRebRun;
    }

    public String getSisRebUsuEliminado() {
        return sisRebUsuEliminado;
    }

    public void setSisRebUsuEliminado(String sisRebUsuEliminado) {
        this.sisRebUsuEliminado = sisRebUsuEliminado;
    }

    public Periodo getTbPeriodo5() {
        return tbPeriodo5;
    }

    public void setTbPeriodo5(Periodo tbPeriodo5) {
        this.tbPeriodo5 = tbPeriodo5;
    }
}
