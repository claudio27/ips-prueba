package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_EST_CARGA_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_EST_CARGA_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "EstadoCarga.findAll", query = "select o from EstadoCarga o") })
@Table(name = "TB_PR_EST_CARGA")
@SequenceGenerator(name = "EstadoCarga_Id_Seq_Gen", sequenceName = "TB_PR_EST_CARGA_ID_SEQ_GEN", allocationSize = 50,
                   initialValue = 50)
public class EstadoCarga implements Serializable {
    private static final long serialVersionUID = -8965330922971869210L;
    @Column(name = "EST_CAR_DESCRIPCION", nullable = false, length = 40)
    private String estCarDescripcion;
    @Id
    @Column(name = "EST_CAR_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EstadoCarga_Id_Seq_Gen")
    private Integer estCarId;
    @OneToMany(mappedBy = "tbPrEstCarga", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<ArchivosCargaPeriodo> archivosCargaPeriodoList1;

    public EstadoCarga() {
    }

    public EstadoCarga(String estCarDescripcion, Integer estCarId) {
        this.estCarDescripcion = estCarDescripcion;
        this.estCarId = estCarId;
    }

    public String getEstCarDescripcion() {
        return estCarDescripcion;
    }

    public void setEstCarDescripcion(String estCarDescripcion) {
        this.estCarDescripcion = estCarDescripcion;
    }

    public Integer getEstCarId() {
        return estCarId;
    }

    public List<ArchivosCargaPeriodo> getArchivosCargaPeriodoList1() {
        return archivosCargaPeriodoList1;
    }

    public void setArchivosCargaPeriodoList1(List<ArchivosCargaPeriodo> archivosCargaPeriodoList1) {
        this.archivosCargaPeriodoList1 = archivosCargaPeriodoList1;
    }

    public ArchivosCargaPeriodo addArchivosCargaPeriodo(ArchivosCargaPeriodo archivosCargaPeriodo) {
        getArchivosCargaPeriodoList1().add(archivosCargaPeriodo);
        archivosCargaPeriodo.setTbPrEstCarga(this);
        return archivosCargaPeriodo;
    }

    public ArchivosCargaPeriodo removeArchivosCargaPeriodo(ArchivosCargaPeriodo archivosCargaPeriodo) {
        getArchivosCargaPeriodoList1().remove(archivosCargaPeriodo);
        archivosCargaPeriodo.setTbPrEstCarga(null);
        return archivosCargaPeriodo;
    }
}
