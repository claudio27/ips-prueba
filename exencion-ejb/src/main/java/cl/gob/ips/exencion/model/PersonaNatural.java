package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author rereyes@emergya.com
 * @since 18-08-2016
 */
@Entity
@NamedQueries({
        @NamedQuery(name = PersonaNatural.QUERY_FIND_ALL, query = "select o from PersonaNatural o"),
        @NamedQuery(name = PersonaNatural.QUERY_FIND_BY_APELLIDO_PATERNO, query = "select o from PersonaNatural o WHERE o.apellidoPaterno LIKE :apellidoPaterno ORDER BY o.apellidoPaterno ASC ")
})
@Table(name = "PERS_PERSONA_NATURAL")
public class PersonaNatural implements Serializable {

    private static final long serialVersionUID = -4878085291418220822L;
    
    public static final String QUERY_FIND_ALL = "PersonaNatural.findAll";
    public static final String QUERY_FIND_BY_APELLIDO_PATERNO = "PersonaNatural.findByApellidoPaterno";

    @Id
    @Column(nullable = false)
    private BigDecimal id;
    @Column(name = "APELLIDO_MATERNO")
    private String apellidoMaterno;
    @Column(name = "APELLIDO_PATERNO")
    private String apellidoPaterno;
    @Column(name = "NOMBRE_COMPLETO")
    private String nombreCompleto;
    @Column(length = 512)
    private String nombres;
    @Column(length = 2)
    private String sexo;
    @Column(name="PASAPORTE")
    private String pasaporte;

    public PersonaNatural() {
    }

    public PersonaNatural(BigDecimal id, String apellidoMaterno, String apellidoPaterno, String nombreCompleto, String nombres, String sexo, String pasaporte) {
        this.id = id;
        this.apellidoMaterno = apellidoMaterno;
        this.apellidoPaterno = apellidoPaterno;
        this.nombreCompleto = nombreCompleto;
        this.nombres = nombres;
        this.sexo = sexo;
        this.pasaporte = pasaporte;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getPasaporte() {
        return pasaporte;
    }

    public void setPasaporte(String pasaporte) {
        this.pasaporte = pasaporte;
    }
}
