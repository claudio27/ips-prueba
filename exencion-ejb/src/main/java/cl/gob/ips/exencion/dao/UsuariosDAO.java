package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Usuarios;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lpino on 26-07-17.
 */
@Stateless
@LocalBean
public class UsuariosDAO extends BaseDAO {

    private static final String PARAM_CODIGO_GRUPO_USUARIO = "gru_usu_cod";

    private static final String PARAM_NOMBRE_USUARIO = "nombre_usuario";

    private static final String PARAM_USUARIO_NOMBRE_USUARIO = "usuarioNomUsuario";

    public List<Integer> obtenerFuncionalidades(String nombreUsuario) throws DataAccessException {
        String query = "SELECT DISTINCT fun.FUNC_COD FROM \n" +
                "TB_USUARIOS u,\n" +
                "TB_GRUPUSU_USU g,\n" +
                "TB_GRUPO_USUARIOS gus,\n" +
                "TB_GRUPUSU_FUNC gfun,\n" +
                "TB_PR_FUNCIONALIDADES fun \n" +
                "WHERE \n" +
                "u.USU_COD = g.USU_COD AND\n" +
                "g.GRU_USU_COD = gus.GRU_USU_COD AND\n" +
                "gus.GRU_USU_COD = gfun.GRU_USU_COD AND\n" +
                "gfun.FUNC_COD = fun.FUNC_COD AND\n" +
                "u.USU_USUARIO = ? \n" +
                "ORDER BY fun.FUNC_COD ASC";

        return super.em.createNativeQuery(query)
                .setParameter(1, nombreUsuario)
                .getResultList();
    }

    public List<BigDecimal> obtenerGrupos(String nombreUsuario) throws DataAccessException {
        String query = "SELECT gus.GRU_USU_COD FROM \n" +
                "TB_USUARIOS u,\n" +
                "TB_GRUPUSU_USU g,\n" +
                "TB_GRUPO_USUARIOS gus\n" +
                "WHERE \n" +
                "u.USU_COD = g.USU_COD AND\n" +
                "g.GRU_USU_COD = gus.GRU_USU_COD AND\n" +
                "u.USU_USUARIO = ? \n" +
                "ORDER BY gus.GRU_USU_COD asc\n";

        return super.em.createNativeQuery(query)
                .setParameter(1, nombreUsuario)
                .getResultList();
    }


    public Usuarios existeUsuario(String nombreUsuario) throws DataAccessException {
        Usuarios usuarios = null;

        super.query = super.em.createNamedQuery(Usuarios.QUERY_FIND_BY_NOMUSER)
                        .setParameter("usuarioNomUsuario", nombreUsuario);

        if (!super.query.getResultList().isEmpty()) {
            usuarios = (Usuarios) super.query.getSingleResult();
        }

        return usuarios;
    }

    /**
     * @return
     * @throws DataAccessException
     */
    public Usuarios validarUsuarioPerteneceGru(String nomUser, final Integer cod) throws DataAccessException {

        Usuarios usuarios = null;

        super.query = super.em.createNamedQuery(Usuarios.QUERY_FIND_USUARIO_GRUPO)
                .setParameter(PARAM_NOMBRE_USUARIO, nomUser)
                .setParameter("gru_usu_cod", cod);

        if (!super.query.getResultList().isEmpty()) {
            usuarios = (Usuarios) super.query.getSingleResult();
        }

        return usuarios;
    }

    /**
     * @return
     * @throws DataAccessException
     */
    public Usuarios validarUsuarioNoPerteneceGru(String nomUser, final Integer cod) throws DataAccessException {

        Usuarios usuarios = null;

        super.query = super.em.createNamedQuery(Usuarios.QUERY_FIND_USUARIO_NO_GRUPO)
                .setParameter(PARAM_NOMBRE_USUARIO, nomUser)
                .setParameter("gru_usu_cod", cod);

        if (!super.query.getResultList().isEmpty()) {
            usuarios = (Usuarios) super.query.getSingleResult();
        }

        return usuarios;
    }

    /**
     * @return List<Usuarios>
     * @throws DataAccessException
     */
    public List<Usuarios> getUsuariosGrupo(final Integer cod) throws DataAccessException {
        List<Usuarios> usuariosPertenece = new ArrayList<>();

        TypedQuery<Usuarios> query =  super.em.createNamedQuery(Usuarios.QUERY_FIND_USUARIO_PERTENCE_GRUPO, Usuarios.class)
                .setParameter(PARAM_CODIGO_GRUPO_USUARIO, cod);

        if(!query.getResultList().isEmpty()) {
            usuariosPertenece = query.getResultList();
        }

        return usuariosPertenece;
    }

    /**
     * @return List<Usuarios>
     * @throws DataAccessException
     */
    public List<Usuarios> getNoUsuariosGrupo(final Integer cod) throws DataAccessException {
        List<Usuarios> usuariosNoPertenece = new ArrayList<>();

        TypedQuery<Usuarios> query =  super.em.createNamedQuery(Usuarios.QUERY_FIND_USUARIO_NO_PERTENECE_GRUPO, Usuarios.class)
                .setParameter(PARAM_CODIGO_GRUPO_USUARIO, cod);

        if(!query.getResultList().isEmpty()) {
            usuariosNoPertenece = query.getResultList();
        }

        return usuariosNoPertenece;
    }

}
