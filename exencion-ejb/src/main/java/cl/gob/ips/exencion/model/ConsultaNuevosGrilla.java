package cl.gob.ips.exencion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by HancacuraB on 17-08-2017.
 */
@Entity
public class ConsultaNuevosGrilla {

    @Id
    @Column(name = "RUN")
    private String run;
    @Column(name = "NOMBRES")
    private String nombre;
    @Column(name = "PATERNO")
    private String apellidoPaterno;
    @Column(name = "MATERNO")
    private String apellidoMaterno;
    @Column(name = "TIPO")
    private String tipoPensionado;
    @Column(name = "INSTITUCION")
    private String institucion;

    public ConsultaNuevosGrilla() {

    }

    public String getRun() {
        return run;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getTipoPensionado() {
        return tipoPensionado;
    }

    public void setTipoPensionado(String tipoPensionado) {
        this.tipoPensionado = tipoPensionado;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }
}
