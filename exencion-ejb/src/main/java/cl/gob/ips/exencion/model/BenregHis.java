package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQueries({ @NamedQuery(name = "BenregHis.findAll", query = "select o from BenregHis o") })
@Table(name = "TB_BENREG_HIS")
public class BenregHis implements Serializable {
    @Column(name = "BEN_DV", nullable = false, length = 1)
    private String benDv;
    @Column(name = "BEN_DV_RUT_ENTIDAD", length = 1)
    private String benDvRutEntidad;
    @Column(name = "BEN_ELIMINADO")
    private Integer benEliminado;
    @Temporal(TemporalType.DATE)
    @Column(name = "BEN_FEC_ELIMINADO")
    private Date benFecEliminado;
    @Id
    @Column(name = "BEN_ID", nullable = false)
    private Long benId;
    @Column(name = "BEN_MONTO")
    private Integer benMonto;
    @Column(name = "BEN_PERIODO")
    private Integer benPeriodo;
    @Column(name = "BEN_REGION")
    private Integer benRegion;
    @Column(name = "BEN_RUN", nullable = false)
    private Integer benRun;
    @Column(name = "BEN_RUT_ENTIDAD")
    private Integer benRutEntidad;
    @Column(name = "BEN_USU_ELIMINADO", length = 20)
    private String benUsuEliminado;
    @ManyToOne
    @JoinColumn(name = "BEN_ARC_ID")
    private ArchivosCargaPeriodo archivosCargaPeriodo;
    @ManyToOne
    @JoinColumn(name = "BEN_TIPO_BENEFICIO")
    private TipoBeneficio tipoBeneficio;
    @ManyToOne
    @JoinColumn(name = "BEN_TIPO_ENTIDAD")
    private TipoInstitucion tbPrTipoInstitucion1;

    public BenregHis() {
    }

    public BenregHis(ArchivosCargaPeriodo archivosCargaPeriodo, String benDv, String benDvRutEntidad,
                     Integer benEliminado, Date benFecEliminado, Long benId, Integer benMonto, Integer benPeriodo,
                     Integer benRegion, Integer benRun, Integer benRutEntidad, TipoBeneficio tipoBeneficio,
                     TipoInstitucion tbPrTipoInstitucion1, String benUsuEliminado) {
        this.archivosCargaPeriodo = archivosCargaPeriodo;
        this.benDv = benDv;
        this.benDvRutEntidad = benDvRutEntidad;
        this.benEliminado = benEliminado;
        this.benFecEliminado = benFecEliminado;
        this.benId = benId;
        this.benMonto = benMonto;
        this.benPeriodo = benPeriodo;
        this.benRegion = benRegion;
        this.benRun = benRun;
        this.benRutEntidad = benRutEntidad;
        this.tipoBeneficio = tipoBeneficio;
        this.tbPrTipoInstitucion1 = tbPrTipoInstitucion1;
        this.benUsuEliminado = benUsuEliminado;
    }


    public String getBenDv() {
        return benDv;
    }

    public void setBenDv(String benDv) {
        this.benDv = benDv;
    }

    public String getBenDvRutEntidad() {
        return benDvRutEntidad;
    }

    public void setBenDvRutEntidad(String benDvRutEntidad) {
        this.benDvRutEntidad = benDvRutEntidad;
    }

    public Integer getBenEliminado() {
        return benEliminado;
    }

    public void setBenEliminado(Integer benEliminado) {
        this.benEliminado = benEliminado;
    }

    public Date getBenFecEliminado() {
        return benFecEliminado;
    }

    public void setBenFecEliminado(Date benFecEliminado) {
        this.benFecEliminado = benFecEliminado;
    }

    public Long getBenId() {
        return benId;
    }

    public void setBenId(Long benId) {
        this.benId = benId;
    }

    public Integer getBenMonto() {
        return benMonto;
    }

    public void setBenMonto(Integer benMonto) {
        this.benMonto = benMonto;
    }

    public Integer getBenPeriodo() {
        return benPeriodo;
    }

    public void setBenPeriodo(Integer benPeriodo) {
        this.benPeriodo = benPeriodo;
    }

    public Integer getBenRegion() {
        return benRegion;
    }

    public void setBenRegion(Integer benRegion) {
        this.benRegion = benRegion;
    }

    public Integer getBenRun() {
        return benRun;
    }

    public void setBenRun(Integer benRun) {
        this.benRun = benRun;
    }

    public Integer getBenRutEntidad() {
        return benRutEntidad;
    }

    public void setBenRutEntidad(Integer benRutEntidad) {
        this.benRutEntidad = benRutEntidad;
    }


    public String getBenUsuEliminado() {
        return benUsuEliminado;
    }

    public void setBenUsuEliminado(String benUsuEliminado) {
        this.benUsuEliminado = benUsuEliminado;
    }

    public ArchivosCargaPeriodo getArchivosCargaPeriodo() {
        return archivosCargaPeriodo;
    }

    public void setArchivosCargaPeriodo(ArchivosCargaPeriodo archivosCargaPeriodo) {
        this.archivosCargaPeriodo = archivosCargaPeriodo;
    }

    public TipoBeneficio getTipoBeneficio() {
        return tipoBeneficio;
    }

    public void setTipoBeneficio(TipoBeneficio tipoBeneficio) {
        this.tipoBeneficio = tipoBeneficio;
    }

    public TipoInstitucion getTbPrTipoInstitucion1() {
        return tbPrTipoInstitucion1;
    }

    public void setTbPrTipoInstitucion1(TipoInstitucion tbPrTipoInstitucion1) {
        this.tbPrTipoInstitucion1 = tbPrTipoInstitucion1;
    }
}
