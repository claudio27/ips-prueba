package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_PR_ESTADO_NOTIFICACION_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PR_ESTADO_NOTIFICACION_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "EstadoNotificacion.findAll", query = "select o from EstadoNotificacion o") })
@Table(name = "TB_PR_ESTADO_NOTIFICACION")
@SequenceGenerator(name = "EstadoNotificacion_Id_Seq_Gen", sequenceName = "TB_PR_ESTADO_NOTIFICACION_ID_SEQ_GEN",
                   allocationSize = 50, initialValue = 50)
public class EstadoNotificacion implements Serializable {
    private static final long serialVersionUID = -1336959839867246127L;
    @Id
    @Column(name = "EST_NOT_COD", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EstadoNotificacion_Id_Seq_Gen")
    private Integer estNotCod;
    @Column(name = "EST_NOT_DESCRIPCION", nullable = false, length = 20)
    private String estNotDescripcion;
    @OneToMany(mappedBy = "tbPrEstadoNotificacion", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<Bitacora> tbBitacoraList1;

    public EstadoNotificacion() {
    }

    public EstadoNotificacion(Integer estNotCod, String estNotDescripcion) {
        this.estNotCod = estNotCod;
        this.estNotDescripcion = estNotDescripcion;
    }

    public Integer getEstNotCod() {
        return estNotCod;
    }

    public String getEstNotDescripcion() {
        return estNotDescripcion;
    }

    public void setEstNotDescripcion(String estNotDescripcion) {
        this.estNotDescripcion = estNotDescripcion;
    }

    public List<Bitacora> getTbBitacoraList1() {
        return tbBitacoraList1;
    }

    public void setTbBitacoraList1(List<Bitacora> tbBitacoraList1) {
        this.tbBitacoraList1 = tbBitacoraList1;
    }

    public Bitacora addBitacora(Bitacora bitacora) {
        getTbBitacoraList1().add(bitacora);
        bitacora.setTbPrEstadoNotificacion(this);
        return bitacora;
    }

    public Bitacora removeBitacora(Bitacora bitacora) {
        getTbBitacoraList1().remove(bitacora);
        bitacora.setTbPrEstadoNotificacion(null);
        return bitacora;
    }
}
