package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NamedQueries({
        @NamedQuery(name = "GrupoUsuarioFunciones.findAll", query = "select o from GrupoUsuarioFunciones o")})
@Table(name = "TB_GRUPUSU_FUNC")
@IdClass(GrupoUsuarioFuncionesPK.class)
public class GrupoUsuarioFunciones implements Serializable {
    private static final long serialVersionUID = -6244040361788974509L;
    @ManyToOne
    @Id
    @JoinColumn(name = "GRU_USU_COD")
    private GrupoUsuarios tbGrupoUsuarios;
    @ManyToOne
    @Id
    @JoinColumn(name = "FUNC_COD")
    private Funcionalidades tbPrFuncionalidades;

    public GrupoUsuarioFunciones() {
    }

    public GrupoUsuarioFunciones(Funcionalidades tbPrFuncionalidades, GrupoUsuarios tbGrupoUsuarios) {
        this.tbPrFuncionalidades = tbPrFuncionalidades;
        this.tbGrupoUsuarios = tbGrupoUsuarios;
    }


    public GrupoUsuarios getTbGrupoUsuarios() {
        return tbGrupoUsuarios;
    }

    public void setTbGrupoUsuarios(GrupoUsuarios tbGrupoUsuarios) {
        this.tbGrupoUsuarios = tbGrupoUsuarios;
    }

    public Funcionalidades getTbPrFuncionalidades() {
        return tbPrFuncionalidades;
    }

    public void setTbPrFuncionalidades(Funcionalidades tbPrFuncionalidades) {
        this.tbPrFuncionalidades = tbPrFuncionalidades;
    }
}