package cl.gob.ips.exencion.model;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the TB_NOMINA_PEN_SIN_FICHA database table.
 * 
 */
@Entity
@Table(name = "TB_NOMINA_PEN_SIN_FICHA")
@NamedQuery(name = "NominaPenSinFicha.findAll", query = "SELECT t FROM NominaPenSinFicha t")
public class NominaPenSinFicha implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4237291707927447808L;

	@Id
	@Column(name = "NOM_ID")
	private long nomId;

	@Column(name = "NOM_DV_BENEFICIARIO")
	private String nomDvBeneficiario;

	@Column(name = "NOM_DV_INSTITUCION")
	private String nomDvInstitucion;

	@Column(name = "NOM_RUN_BENEFICIARIO")
	private Integer nomRunBeneficiario;

	// bi-directional many-to-one association to TbPeriodo
	@ManyToOne
	@JoinColumn(name = "NOM_PERIODO")
	private Periodo periodo;

	// bi-directional many-to-one association to TbPrInstitucione
	@ManyToOne
	@JoinColumn(name = "NOM_RUT_INSTITUCION")
	private Instituciones instituciones;

	public NominaPenSinFicha() {
	}

	public long getNomId() {
		return this.nomId;
	}

	public void setNomId(long nomId) {
		this.nomId = nomId;
	}

	public String getNomDvBeneficiario() {
		return this.nomDvBeneficiario;
	}

	public void setNomDvBeneficiario(String nomDvBeneficiario) {
		this.nomDvBeneficiario = nomDvBeneficiario;
	}

	public String getNomDvInstitucion() {
		return this.nomDvInstitucion;
	}

	public void setNomDvInstitucion(String nomDvInstitucion) {
		this.nomDvInstitucion = nomDvInstitucion;
	}

	public Integer getNomRunBeneficiario() {
		return this.nomRunBeneficiario;
	}

	public void setNomRunBeneficiario(Integer nomRunBeneficiario) {
		this.nomRunBeneficiario = nomRunBeneficiario;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	public Instituciones getInstituciones() {
		return instituciones;
	}

	public void setInstituciones(Instituciones instituciones) {
		this.instituciones = instituciones;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder
				.append(StringUtils.leftPad(String.valueOf(nomRunBeneficiario), 8, "0"))
				.append(nomDvBeneficiario);


		return 	builder.toString();


	}

}