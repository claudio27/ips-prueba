package cl.gob.ips.exencion.exceptions;

/**
 * Created by rodrigo.reyesco@gmail.com on 19-10-2016.
 */
public class DataAccessException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 193382527286472928L;

	public DataAccessException() {
    }

    public DataAccessException(String message) {
        super(message);
    }

    public DataAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataAccessException(Throwable cause) {
        super(cause);
    }

    public DataAccessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
