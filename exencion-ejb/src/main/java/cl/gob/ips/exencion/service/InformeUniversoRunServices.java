package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.InformeUniversoRunDAO;
import cl.gob.ips.exencion.dto.InformeUniversoRunDTO;
import cl.gob.ips.exencion.enums.TipoPFP;
import cl.gob.ips.exencion.enums.TipoPensionado;
import cl.gob.ips.exencion.enums.TipoResidenciaAcreditado;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.model.Pensionados;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alaya on 17/07/17.
 */
public class InformeUniversoRunServices {

    @Inject
    private InformeUniversoRunDAO informeUniversoRunDAO;



    public List<InformeUniversoRunDTO> obtenerInformeUniversoPorRun(Integer periodoDesde, Integer periodoHasta, Long run) throws IpsBussinesException, DataAccessException {

        List<InformeUniversoRunDTO> listaDetalleUniverso = new ArrayList<>();
        List<Pensionados> listaDetalleRunPeriodo = informeUniversoRunDAO.obtenerInformeUniversoPorRun(periodoDesde, periodoHasta, run);


        for (Pensionados pen : listaDetalleRunPeriodo) {
            InformeUniversoRunDTO det = new InformeUniversoRunDTO();

            this.mapeaObjetoBDaDTO(det, pen);

            listaDetalleUniverso.add(det);
        }

        return listaDetalleUniverso;

    }

    private String obtenerTipoPensionado(Integer codigo){
        if (codigo!=null){
            return TipoPensionado.getInstance(codigo).getDescripcion();

        }
        return null;
    }

    private String obtenerTipoResidencia(Integer codigo){
        if (codigo!=null){
            return TipoResidenciaAcreditado.getInstance(codigo).getDescripcion();

        }
        return null;
    }

    private void mapeaObjetoBDaDTO(InformeUniversoRunDTO det, Pensionados pen ){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String tipoPFP="";

        det.setPeriodo(pen.getPeriodo().getPerPeriodo().toString()!=null ? pen.getPeriodo().getPerPeriodo().toString():"");
        det.setEdad(pen.getPenEdadExterna() == null ? pen.getPenEdadExterna() : pen.getPenEdadInterna());
        det.setPfp(pen.getPenPuntajePfpAdm() == null ? pen.getPenPuntajePfp() : pen.getPenPuntajePfpAdm());
        if (pen.getPenPuntajePfpAdm()!=null || pen.getPenPuntajePfp()!=null){
            tipoPFP = pen.getPenPuntajePfpAdm() == null ? TipoPFP.getInstance(2).getDescripcion() : TipoPFP.getInstance(1).getDescripcion();
        }
        det.setTipoPfp(tipoPFP);
        det.setFechaPfp(pen.getPenFechaFocalizacion() != null ? dateFormat.format(pen.getPenFechaFocalizacion()) : "");
        det.setResidencia(obtenerTipoResidencia(pen.getPenTipoResidencia()) !=null ? obtenerTipoResidencia(pen.getPenTipoResidencia()): "");
        det.setFechaResidencia(pen.getPenFechaResidencia() != null ? dateFormat.format(pen.getPenFechaResidencia()) : "");
        det.setTipoPensionado(obtenerTipoPensionado(pen.getPenTipo())!=null ? obtenerTipoPensionado(pen.getPenTipo()):"");
        det.setNombreCompleto(pen.getPenNombres() + " " + pen.getPenApePaterno() + " " + pen.getPenApeMaterno());

    }

}
