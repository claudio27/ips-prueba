package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.FuncionalidadesDAO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Funcionalidades;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.List;

/**
 * Created by practicante on 07-09-17.
 */
@Stateless
@LocalBean
public class FuncionalidadesService {

    @EJB
    FuncionalidadesDAO funcionalidadesDAOService;


    /**
     *
     * @return
     * @throws DataAccessException
     */
    public List<Funcionalidades> getAllFuncionalidadesPerteneceGrupo(final Integer cod) throws DataAccessException {
        return funcionalidadesDAOService.getFuncionalidadesPerteneceGrupo(cod);
    }

    /**
     *
     * @return
     * @throws DataAccessException
     */
    public List<Funcionalidades> getAllFuncionalidadesNoPerteneceGrupo(final Integer cod) throws DataAccessException {
        return funcionalidadesDAOService.getFuncionalidadesNoPerteneceGrupo(cod);
    }


    /**
     *
     * @return
     * @throws DataAccessException
     */
    public Funcionalidades validarFuncionalidadPerteneceGru(String nomFun, final Integer cod) throws DataAccessException {
        return this.funcionalidadesDAOService.validarFuncionalidadPerteneceGru(nomFun, cod);
    }


    /**
     *
     * @return
     * @throws DataAccessException
     */
    public Funcionalidades validarFuncionalidadNoPerteneceGru(String nomFun, final Integer cod) throws DataAccessException {
        return this.funcionalidadesDAOService.validarFuncionalidadNoPerteneceGru(nomFun, cod);
    }

}
