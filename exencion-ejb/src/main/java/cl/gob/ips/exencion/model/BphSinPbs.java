package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * To create ID generator sequence "TB_BPH_SIN_PBS_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_BPH_SIN_PBS_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "BphSinPbs.findAll", query = "select o from BphSinPbs o") })
@Table(name = "TB_BPH_SIN_PBS")
@SequenceGenerator(name = "BphSinPbs_Id_Seq_Gen", sequenceName = "TB_BPH_SIN_PBS_ID_SEQ_GEN", allocationSize = 50,
                   initialValue = 50)
public class BphSinPbs implements Serializable {
    @Id
    @Column(name = "BPH_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BphSinPbs_Id_Seq_Gen")
    private BigDecimal bphId;
    @Column(name = "BPH_ING_BENEFICIO", nullable = false)
    private Integer bphIngBeneficio;
    @Column(name = "BPH_ING_CODIGO", nullable = false)
    private Integer bphIngCodigo;
    @Column(name = "BPH_ING_DING_DVRUNBEN", nullable = false, length = 1)
    private String bphIngDingDvrunben;
    @Column(name = "BPH_ING_DING_RUNBEN", nullable = false)
    private Integer bphIngDingRunben;
    @Column(name = "BPH_ING_ELIMINADO")
    private Integer bphIngEliminado;
    @Temporal(TemporalType.DATE)
    @Column(name = "BPH_ING_FEC_ELIMINADO")
    private Date bphIngFecEliminado;
    @Temporal(TemporalType.DATE)
    @Column(name = "BPH_ING_FECHA_VENCIMIENTO", nullable = false)
    private Date bphIngFechaVencimiento;
    @Temporal(TemporalType.DATE)
    @Column(name = "BPH_ING_FECINIC", nullable = false)
    private Date bphIngFecinic;
    @Column(name = "BPH_ING_MONTO", nullable = false)
    private Integer bphIngMonto;
    @Column(name = "BPH_ING_TIPO_MOVIMIENTO", nullable = false, length = 1)
    private String bphIngTipoMovimiento;
    @Column(name = "BPH_ING_UNIDAD_MEDIDA", nullable = false)
    private Integer bphIngUnidadMedida;
    @Column(name = "BPH_ING_USU_ELIMINADO", length = 20)
    private String bphIngUsuEliminado;
    @ManyToOne
    @JoinColumn(name = "BPH_PERIODO")
    private Periodo tbPeriodo4;

    public BphSinPbs() {
    }

    public BphSinPbs(BigDecimal bphId, Integer bphIngBeneficio, Integer bphIngCodigo, String bphIngDingDvrunben,
                     Integer bphIngDingRunben, Integer bphIngEliminado, Date bphIngFecEliminado,
                     Date bphIngFechaVencimiento, Date bphIngFecinic, Integer bphIngMonto, String bphIngTipoMovimiento,
                     Integer bphIngUnidadMedida, String bphIngUsuEliminado, Periodo tbPeriodo4) {
        this.bphId = bphId;
        this.bphIngBeneficio = bphIngBeneficio;
        this.bphIngCodigo = bphIngCodigo;
        this.bphIngDingDvrunben = bphIngDingDvrunben;
        this.bphIngDingRunben = bphIngDingRunben;
        this.bphIngEliminado = bphIngEliminado;
        this.bphIngFecEliminado = bphIngFecEliminado;
        this.bphIngFechaVencimiento = bphIngFechaVencimiento;
        this.bphIngFecinic = bphIngFecinic;
        this.bphIngMonto = bphIngMonto;
        this.bphIngTipoMovimiento = bphIngTipoMovimiento;
        this.bphIngUnidadMedida = bphIngUnidadMedida;
        this.bphIngUsuEliminado = bphIngUsuEliminado;
        this.tbPeriodo4 = tbPeriodo4;
    }

    public BigDecimal getBphId() {
        return bphId;
    }

    public Integer getBphIngBeneficio() {
        return bphIngBeneficio;
    }

    public void setBphIngBeneficio(Integer bphIngBeneficio) {
        this.bphIngBeneficio = bphIngBeneficio;
    }

    public Integer getBphIngCodigo() {
        return bphIngCodigo;
    }

    public void setBphIngCodigo(Integer bphIngCodigo) {
        this.bphIngCodigo = bphIngCodigo;
    }

    public String getBphIngDingDvrunben() {
        return bphIngDingDvrunben;
    }

    public void setBphIngDingDvrunben(String bphIngDingDvrunben) {
        this.bphIngDingDvrunben = bphIngDingDvrunben;
    }

    public Integer getBphIngDingRunben() {
        return bphIngDingRunben;
    }

    public void setBphIngDingRunben(Integer bphIngDingRunben) {
        this.bphIngDingRunben = bphIngDingRunben;
    }

    public Integer getBphIngEliminado() {
        return bphIngEliminado;
    }

    public void setBphIngEliminado(Integer bphIngEliminado) {
        this.bphIngEliminado = bphIngEliminado;
    }

    public Date getBphIngFecEliminado() {
        return bphIngFecEliminado;
    }

    public void setBphIngFecEliminado(Date bphIngFecEliminado) {
        this.bphIngFecEliminado = bphIngFecEliminado;
    }

    public Date getBphIngFechaVencimiento() {
        return bphIngFechaVencimiento;
    }

    public void setBphIngFechaVencimiento(Date bphIngFechaVencimiento) {
        this.bphIngFechaVencimiento = bphIngFechaVencimiento;
    }

    public Date getBphIngFecinic() {
        return bphIngFecinic;
    }

    public void setBphIngFecinic(Date bphIngFecinic) {
        this.bphIngFecinic = bphIngFecinic;
    }

    public Integer getBphIngMonto() {
        return bphIngMonto;
    }

    public void setBphIngMonto(Integer bphIngMonto) {
        this.bphIngMonto = bphIngMonto;
    }

    public String getBphIngTipoMovimiento() {
        return bphIngTipoMovimiento;
    }

    public void setBphIngTipoMovimiento(String bphIngTipoMovimiento) {
        this.bphIngTipoMovimiento = bphIngTipoMovimiento;
    }

    public Integer getBphIngUnidadMedida() {
        return bphIngUnidadMedida;
    }

    public void setBphIngUnidadMedida(Integer bphIngUnidadMedida) {
        this.bphIngUnidadMedida = bphIngUnidadMedida;
    }

    public String getBphIngUsuEliminado() {
        return bphIngUsuEliminado;
    }

    public void setBphIngUsuEliminado(String bphIngUsuEliminado) {
        this.bphIngUsuEliminado = bphIngUsuEliminado;
    }


    public Periodo getTbPeriodo4() {
        return tbPeriodo4;
    }

    public void setTbPeriodo4(Periodo tbPeriodo4) {
        this.tbPeriodo4 = tbPeriodo4;
    }
}
