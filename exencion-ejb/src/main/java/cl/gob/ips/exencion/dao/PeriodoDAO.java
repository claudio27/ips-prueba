/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Periodo;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Cristian Toledo
 */
@Stateless
@LocalBean
public class PeriodoDAO extends BaseDAO {

    private static final String PARAM_PERIODO = "periodo";

    private static final String PARAM_ID_PERIODO = "periodo_id";

    /**
     * @return Periodo
     * @throws Exception
     */


    public Periodo validarPeriodo(final Integer periodo){
        Periodo periodoValidado = null;

        Query query = super.em.createNativeQuery("select * \n" +
                "from TB_PERIODO p join TB_PR_ETAPAS_PERIODO e\n" +
                "on p.PER_ETAPA_COD = e.ETAPA_PER_COD\n" +
                "where p.PER_PERIODO = ?periodo", Periodo.class).setParameter("periodo", periodo);

        if (!query.getResultList().isEmpty()){
            periodoValidado = (Periodo)query.getSingleResult();
        }

        return periodoValidado;
    }


    public Periodo buscarPeriodoActivoCerrar() throws DataAccessException {
        Periodo p = null;
        super.query = super.em.createNativeQuery("select * \n" +
                "from TB_PERIODO p join TB_PR_ETAPAS_PERIODO e\n" +
                "on p.PER_ETAPA_COD=e.ETAPA_PER_COD\n" +
                "where e.ETAPA_PER_COD = 52", Periodo.class);

        if (!super.query.getResultList().isEmpty()){
            p= (Periodo)super.query.getSingleResult();
        }
        return p;

    }



    public Integer validarEstadoGenerarNominas(final Integer periodo) throws DataAccessException{
        Integer value = 0;

        if (validarPeriodo(periodo)!=null){

            Query periodoNoAptoGenerar = super.em.createNativeQuery("select *\n" +
                    "from TB_PERIODO p join TB_PR_ETAPAS_PERIODO e on p.PER_ETAPA_COD=e.ETAPA_PER_COD\n" +
                    "where p.PER_PERIODO = ?periodo AND e.ETAPA_PER_COD  <> 50 AND e.ETAPA_PER_COD <> 51 " +
                    "AND e.ETAPA_PER_COD <> 52 AND e.ETAPA_PER_COD <> 60 AND e.ETAPA_PER_COD <> 61", Periodo.class).setParameter("periodo", periodo);

            if (!periodoNoAptoGenerar.getResultList().isEmpty()){
                value = 1;
            }
            else if(obtenerPeriodoGenerar(periodo)==null){
                value = 2;
            }
            else if(obtenerPeriodoGenerar(periodo)!=null){
                value = 3;
            }
        }

        return value;

    }

    public Periodo obtenerPeriodoGenerar(final Integer periodo) throws DataAccessException {
        Periodo p = null;

        super.query = super.em.createNativeQuery("select * \n" +
                "from TB_PERIODO p join TB_PR_ETAPAS_PERIODO e\n" +
                "on p.PER_ETAPA_COD = e.ETAPA_PER_COD\n" +
                "where p.PER_PERIODO = ?periodo AND e.ETAPA_PER_COD = 50", Periodo.class).setParameter("periodo", periodo);

        if (!super.query.getResultList().isEmpty()){
            p = (Periodo)super.query.getSingleResult();
        }

        return p;

    }


    public Integer validarEstadoCierrePeriodo(final Integer periodo) throws DataAccessException{
        Integer value = 0;

        if (validarPeriodo(periodo)!=null){

            Query periodoNoAptoCierre = super.em.createNativeQuery("select *\n" +
                    "from TB_PERIODO p join TB_PR_ETAPAS_PERIODO e on p.PER_ETAPA_COD=e.ETAPA_PER_COD\n" +
                    "where p.PER_PERIODO = ?periodo AND e.ETAPA_PER_COD <> 52 AND e.ETAPA_PER_COD <> 60 AND e.ETAPA_PER_COD <> 61", Periodo.class).setParameter("periodo", periodo);

            if (!periodoNoAptoCierre.getResultList().isEmpty()){
                value = 1;
            }
            else if(obtenerPeriodoCerrar(periodo)==null){
                value = 2;
            }

            else if(obtenerPeriodoCerrar(periodo)!=null){
                value = 3;
            }
        }

        return value;

    }

    public Periodo obtenerPeriodoCerrar(final Integer periodo) throws DataAccessException {
        Periodo p = null;

        super.query = super.em.createNativeQuery("select * \n" +
                "from TB_PERIODO p join TB_PR_ETAPAS_PERIODO e\n" +
                "on p.PER_ETAPA_COD = e.ETAPA_PER_COD\n" +
                "where p.PER_PERIODO = ?periodo AND e.ETAPA_PER_COD = 52", Periodo.class).setParameter("periodo", periodo);

        if (!super.query.getResultList().isEmpty()){
            p = (Periodo)super.query.getSingleResult();
        }

        return p;

    }





    /**
     * @param periodo
     * @return Integer
     * @throws DataAccessException
     */
    public Integer obtenerEstadoProceso(final Integer periodo) throws DataAccessException {
        Integer estado = 0;

        /*
        EL PAR�METRO PERIODO DE LA CONSULTA TENIA EL VALOR "201704" EN DURO,SE CAMBIA POR EL PAR�METRO
        RECIBIDO POR EL M�TODO
        */
        super.query = em.createQuery("SELECT per.tbPrEtapasPeriodo.etapaPerCod FROM " + Periodo.class.getName() + " per"
                + " WHERE per.perPeriodo = :periodo " , Integer.class).setParameter(PARAM_PERIODO, periodo);

        if (!super.query.getResultList().isEmpty()) {
            estado = (Integer) super.query.getSingleResult();
        }

        return estado;
    }

    /**
     * @param periodo
     * @return Periodo
     * @throws DataAccessException
     */
    public Periodo buscarPeriodo(Integer periodo) throws DataAccessException {
        Periodo p = null;
        super.query = super.em.createNamedQuery(Periodo.QUERY_FIND_BY_ID).setParameter(PARAM_ID_PERIODO, periodo);

        if (!super.query.getResultList().isEmpty()) {
            p = (Periodo) super.query.getSingleResult();
        }
        return p;
    }

    public Periodo obtenerPeriodoActual(){
        Periodo p =null;

        super.query = super.em.createNativeQuery("select *\n" +
                "from TB_PERIODO \n" +
                "where PER_PERIODO = (SELECT MAX(PER_PERIODO) FROM TB_PERIODO p \n" +
                "JOIN TB_PR_ETAPAS_PERIODO e on p.PER_ETAPA_COD=e.ETAPA_PER_COD " +
                "WHERE e.ETAPA_PER_COD<>61)", Periodo.class);

        if (!super.query.getResultList().isEmpty()){
            p = (Periodo) super.query.getSingleResult();
        }
        return p;
    }


    /**
     * @param periodo
     * @throws Exception
     */

    public void modificarPeriodo(final Periodo periodo) throws DataAccessException {
        super.em.merge(periodo);
    }
}

