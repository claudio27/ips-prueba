package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * To create ID generator sequence "TB_PENSIONADOS_SIN_RSH_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PENSIONADOS_SIN_RSH_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "PensionadosSinRsh.findAll", query = "select o from PensionadosSinRsh o") })
@Table(name = "TB_PENSIONADOS_SIN_RSH")
@SequenceGenerator(name = "PensionadosSinRsh_Id_Seq_Gen", sequenceName = "TB_PENSIONADOS_SIN_RSH_ID_SEQ_GEN",
                   allocationSize = 50, initialValue = 50)
public class PensionadosSinRsh implements Serializable {
    private static final long serialVersionUID = -367499885813034681L;
    @Column(name = "PEN_SIN_ELIMINADO")
    private Integer penSinEliminado;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_SIN_FEC_ELIMINADO")
    private Date penSinFecEliminado;
    @Id
    @Column(name = "PEN_SIN_RSH_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PensionadosSinRsh_Id_Seq_Gen")
    private BigDecimal penSinRshId;
    @Column(name = "PEN_SIN_RSH_RUN", nullable = false)
    private Integer penSinRshRun;
    @Column(name = "PEN_SIN_USU_ELIMINADO", length = 20)
    private String penSinUsuEliminado;
    @ManyToOne
    @JoinColumn(name = "PEN_SIN_RSH_PERIODO")
    private Periodo tbPeriodo10;

    public PensionadosSinRsh() {
    }

    public PensionadosSinRsh(Integer penSinEliminado, Date penSinFecEliminado, BigDecimal penSinRshId,
                             Periodo tbPeriodo10, Integer penSinRshRun, String penSinUsuEliminado) {
        this.penSinEliminado = penSinEliminado;
        this.penSinFecEliminado = penSinFecEliminado;
        this.penSinRshId = penSinRshId;
        this.tbPeriodo10 = tbPeriodo10;
        this.penSinRshRun = penSinRshRun;
        this.penSinUsuEliminado = penSinUsuEliminado;
    }

    public Integer getPenSinEliminado() {
        return penSinEliminado;
    }

    public void setPenSinEliminado(Integer penSinEliminado) {
        this.penSinEliminado = penSinEliminado;
    }

    public Date getPenSinFecEliminado() {
        return penSinFecEliminado;
    }

    public void setPenSinFecEliminado(Date penSinFecEliminado) {
        this.penSinFecEliminado = penSinFecEliminado;
    }

    public BigDecimal getPenSinRshId() {
        return penSinRshId;
    }


    public Integer getPenSinRshRun() {
        return penSinRshRun;
    }

    public void setPenSinRshRun(Integer penSinRshRun) {
        this.penSinRshRun = penSinRshRun;
    }

    public String getPenSinUsuEliminado() {
        return penSinUsuEliminado;
    }

    public void setPenSinUsuEliminado(String penSinUsuEliminado) {
        this.penSinUsuEliminado = penSinUsuEliminado;
    }

    public Periodo getTbPeriodo10() {
        return tbPeriodo10;
    }

    public void setTbPeriodo10(Periodo tbPeriodo10) {
        this.tbPeriodo10 = tbPeriodo10;
    }
}
