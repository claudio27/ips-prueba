package cl.gob.ips.exencion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by alaya on 8/08/17.
 */

@Entity
public class ConsultaNuevos {

    @Id
    @Column(name = "RUN")
    private String run;
    @Column(name = "NOMBRES")
    private String nombres;
    @Column(name = "PATERNO")
    private String aPaterno;
    @Column(name = "MATERNO")
    private String aMaterno;
    @Column(name = "TIPO")
    private Integer tipo;
    @Column(name = "INSTITUCION")
    private String institucion;

    public ConsultaNuevos() {
    }

    public String getRun() {
        return run;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getaPaterno() {
        return aPaterno;
    }

    public void setaPaterno(String aPaterno) {
        this.aPaterno = aPaterno;
    }

    public String getaMaterno() {
        return aMaterno;
    }

    public void setaMaterno(String aMaterno) {
        this.aMaterno = aMaterno;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }
}
