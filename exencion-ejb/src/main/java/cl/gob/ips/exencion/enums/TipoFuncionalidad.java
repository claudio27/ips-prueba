package cl.gob.ips.exencion.enums;

/**
 * Created by alaya on 27/07/17.
 */
public enum TipoFuncionalidad {

    MOVIMIENTOS(1, "Movimientos por RUN"),
    INFORME_GESTION(2, "Informe de Gestión"),
    INFORME_UNIVERSO(3, "Informe del Universo"),
    INFORME_UNIVERSO_RUN(4, "Informe Universo por RUN"),
    RESUMEN_REGISTROS_ARCHIVOS(5,"Resumen registros de Archivos de Entrada"),
    REVISAR_NOMINAS(6, "Revisar Nóminas de un Periodo"),
    EVENTOS_PROCESOS_SISTEMA(7, "Eventos de los procesos de sistema"),
    ATENCION_PENSIONADO(8, "Atención al pensionado"),
    RESULTADO_VALIDACION_ARCHIVOS(9, "Resultado de la validación de archivos"),
    GENERACION_NOMINAS_INSTITUCIONES(10, "Generar nóminas para las instituciones"),
    CARGA_SELECTIVA_ARCHIVOS(11, "Carga selectiva de Archivos"),
    MANTENEDOR_EJECUCION_PROCESO(12, "Mantenedor de ejecución del proceso"),
    MANTENEDOR_INSTITUCION_PAGADORA(13, "Mantenedor institución pagadora"),
    MANTENEDOR_CALENDARIO(14, "Mantenedor de Calendario"),
    MANTENEDOR_GRUPO_USUARIOS(15, "Mantenedor de grupo de usuarios"),
    RESPONSABLE_GRUPOS_ARCHIVOS(16, "Responsable de grupos de archivos"),
    MANTENEDOR_RESPUESTAS(17, "Mantenedor de Respuestas de atención al pensionado")
    ;

    private Integer codigo;
    private String descripcion;

    TipoFuncionalidad(Integer codigo, String descripcion){
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public static TipoFuncionalidad getInstance(Integer codigo){
        for(TipoFuncionalidad itemEnum : values()){
            if(itemEnum.getCodigo() == codigo.intValue()){
                return itemEnum;
            }
        }
        return null;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
