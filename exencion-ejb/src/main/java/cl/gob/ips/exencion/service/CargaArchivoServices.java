package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.CargaArchivosDAO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.ArchivosCargaPeriodo;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 *
 * @author Lenin Pino 13-07-2017
 */
@Stateless
@LocalBean
public class CargaArchivoServices {

	@Inject
	CargaArchivosDAO dao;

	public List<ArchivosCargaPeriodo> buscaPeriodoActivo(final Integer periodo) throws DataAccessException {
		return dao.obtenerArchivosPorPeriodo(periodo);
	}
}
