package cl.gob.ips.exencion.dto;

import java.io.Serializable;
import java.util.Date;

public class MovimientosRutDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3507700076518621276L;

	private String nombreCompleto;
	
	private String periodo;
	
	private String movimiento;
	
	private String fechaDevengamiento;
	
	private Integer pfp;
	
	private String fechaPfp;
	
	private String residencia;
	
	private String fechaResidencia;
	
	private String institucionesInformadas;
	
	private String potencialAps;
	
	private String causalExtincion;
	
	private String requisitosNoAcreditados;

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}

	public String getFechaDevengamiento() {
		return fechaDevengamiento;
	}

	public void setFechaDevengamiento(String fechaDevengamiento) {
		this.fechaDevengamiento = fechaDevengamiento;
	}

	public Integer getPfp() {
		return pfp;
	}

	public void setPfp(Integer pfp) {
		this.pfp = pfp;
	}

	public String getFechaPfp() {
		return fechaPfp;
	}

	public void setFechaPfp(String fechaPfp) {
		this.fechaPfp = fechaPfp;
	}

	public String getResidencia() {
		return residencia;
	}

	public void setResidencia(String residencia) {
		this.residencia = residencia;
	}

	public String getFechaResidencia() {
		return fechaResidencia;
	}

	public void setFechaResidencia(String fechaResidencia) {
		this.fechaResidencia = fechaResidencia;
	}

	public String getInstitucionesInformadas() {
		return institucionesInformadas;
	}

	public void setInstitucionesInformadas(String institucionesInformadas) {
		this.institucionesInformadas = institucionesInformadas;
	}

	public String getPotencialAps() {
		return potencialAps;
	}

	public void setPotencialAps(String potencialAps) {
		this.potencialAps = potencialAps;
	}

	public String getCausalExtincion() {
		return causalExtincion;
	}

	public void setCausalExtincion(String causalExtincion) {
		this.causalExtincion = causalExtincion;
	}

	public String getRequisitosNoAcreditados() {
		return requisitosNoAcreditados;
	}

	public void setRequisitosNoAcreditados(String requisitosNoAcreditados) {
		this.requisitosNoAcreditados = requisitosNoAcreditados;
	}
	
}
