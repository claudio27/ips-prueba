package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQueries({ @NamedQuery(name = "BenregAct.findAll", query = "select o from BenregAct o") })
@Table(name = "TB_BENREG_ACT")
public class BenregAct implements Serializable {
    @Column(name = "BEN_ACT_ARC_ID", nullable = false)
    private Long benActArcId;
    @Column(name = "BEN_ACT_DV", length = 1)
    private String benActDv;
    @Column(name = "BEN_ACT_DV_RUT_ENTIDAD", length = 1)
    private String benActDvRutEntidad;
    @Column(name = "BEN_ACT_ELIMINADO")
    private Integer benActEliminado;
    @Temporal(TemporalType.DATE)
    @Column(name = "BEN_ACT_FEC_ELIMINADO")
    private Date benActFecEliminado;
    @Id
    @Column(name = "BEN_ACT_ID", nullable = false)
    private Long benActId;
    @Column(name = "BEN_ACT_MONTO")
    private Integer benActMonto;
    @Column(name = "BEN_ACT_PERIODO")
    private Integer benActPeriodo;
    @Column(name = "BEN_ACT_REGION")
    private Integer benActRegion;
    @Column(name = "BEN_ACT_RUN", nullable = false)
    private Integer benActRun;
    @Column(name = "BEN_ACT_RUT_ENTIDAD")
    private Integer benActRutEntidad;
    @Column(name = "BEN_ACT_TIPO_REGISTRO", length = 1)
    private String benActTipoRegistro;
    @Column(name = "BEN_ACT_USU_ELIMINADO", length = 20)
    private String benActUsuEliminado;
    @ManyToOne
    @JoinColumn(name = "BEN_ACT_TIPO_BENEFICIO")
    private TipoBeneficio tipoBeneficio;
    @ManyToOne
    @JoinColumn(name = "BEN_PERIODO")
    private Periodo tbPeriodo2;
    @ManyToOne
    @JoinColumn(name = "BEN_ACT_TIPO_ENTIDAD")
    private TipoInstitucion tbPrTipoInstitucion2;

    public BenregAct() {
    }

    public BenregAct(Long benActArcId, String benActDv, String benActDvRutEntidad, Integer benActEliminado,
                     Date benActFecEliminado, Long benActId, Integer benActMonto, Integer benActPeriodo,
                     Integer benActRegion, Integer benActRun, Integer benActRutEntidad, TipoBeneficio tipoBeneficio,
                     TipoInstitucion tbPrTipoInstitucion2, String benActTipoRegistro, String benActUsuEliminado,
                     Periodo tbPeriodo2) {
        this.benActArcId = benActArcId;
        this.benActDv = benActDv;
        this.benActDvRutEntidad = benActDvRutEntidad;
        this.benActEliminado = benActEliminado;
        this.benActFecEliminado = benActFecEliminado;
        this.benActId = benActId;
        this.benActMonto = benActMonto;
        this.benActPeriodo = benActPeriodo;
        this.benActRegion = benActRegion;
        this.benActRun = benActRun;
        this.benActRutEntidad = benActRutEntidad;
        this.tipoBeneficio = tipoBeneficio;
        this.tbPrTipoInstitucion2 = tbPrTipoInstitucion2;
        this.benActTipoRegistro = benActTipoRegistro;
        this.benActUsuEliminado = benActUsuEliminado;
        this.tbPeriodo2 = tbPeriodo2;
    }

    public Long getBenActArcId() {
        return benActArcId;
    }

    public void setBenActArcId(Long benActArcId) {
        this.benActArcId = benActArcId;
    }

    public String getBenActDv() {
        return benActDv;
    }

    public void setBenActDv(String benActDv) {
        this.benActDv = benActDv;
    }

    public String getBenActDvRutEntidad() {
        return benActDvRutEntidad;
    }

    public void setBenActDvRutEntidad(String benActDvRutEntidad) {
        this.benActDvRutEntidad = benActDvRutEntidad;
    }

    public Integer getBenActEliminado() {
        return benActEliminado;
    }

    public void setBenActEliminado(Integer benActEliminado) {
        this.benActEliminado = benActEliminado;
    }

    public Date getBenActFecEliminado() {
        return benActFecEliminado;
    }

    public void setBenActFecEliminado(Date benActFecEliminado) {
        this.benActFecEliminado = benActFecEliminado;
    }

    public Long getBenActId() {
        return benActId;
    }

    public void setBenActId(Long benActId) {
        this.benActId = benActId;
    }

    public Integer getBenActMonto() {
        return benActMonto;
    }

    public void setBenActMonto(Integer benActMonto) {
        this.benActMonto = benActMonto;
    }

    public Integer getBenActPeriodo() {
        return benActPeriodo;
    }

    public void setBenActPeriodo(Integer benActPeriodo) {
        this.benActPeriodo = benActPeriodo;
    }

    public Integer getBenActRegion() {
        return benActRegion;
    }

    public void setBenActRegion(Integer benActRegion) {
        this.benActRegion = benActRegion;
    }

    public Integer getBenActRun() {
        return benActRun;
    }

    public void setBenActRun(Integer benActRun) {
        this.benActRun = benActRun;
    }

    public Integer getBenActRutEntidad() {
        return benActRutEntidad;
    }

    public void setBenActRutEntidad(Integer benActRutEntidad) {
        this.benActRutEntidad = benActRutEntidad;
    }


    public String getBenActTipoRegistro() {
        return benActTipoRegistro;
    }

    public void setBenActTipoRegistro(String benActTipoRegistro) {
        this.benActTipoRegistro = benActTipoRegistro;
    }

    public String getBenActUsuEliminado() {
        return benActUsuEliminado;
    }

    public void setBenActUsuEliminado(String benActUsuEliminado) {
        this.benActUsuEliminado = benActUsuEliminado;
    }


    public TipoBeneficio getTipoBeneficio() {
        return tipoBeneficio;
    }

    public void setTipoBeneficio(TipoBeneficio tbPrTipoPension) {
        this.tipoBeneficio = tbPrTipoPension;
    }

    public Periodo getTbPeriodo2() {
        return tbPeriodo2;
    }

    public void setTbPeriodo2(Periodo tbPeriodo2) {
        this.tbPeriodo2 = tbPeriodo2;
    }

    public TipoInstitucion getTbPrTipoInstitucion2() {
        return tbPrTipoInstitucion2;
    }

    public void setTbPrTipoInstitucion2(TipoInstitucion tbPrTipoInstitucion2) {
        this.tbPrTipoInstitucion2 = tbPrTipoInstitucion2;
    }
}
