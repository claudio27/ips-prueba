package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * To create ID generator sequence "TB_USUARIOS_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_USUARIOS_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({
        @NamedQuery(name = Usuarios.QUERY_FIND_ALL, query = "select o from Usuarios o"),
        @NamedQuery(name = Usuarios.QUERY_FIND_BY_EMAIL, query = "select o from Usuarios o WHERE o.usuCorreo = :usuarioCorreo"),
        @NamedQuery(name = Usuarios.QUERY_FIND_BY_NOMUSER, query = "select o from Usuarios o WHERE o.usuUsuario = :usuarioNomUsuario"),
        @NamedQuery(name = Usuarios.QUERY_FIND_USUARIO_GRUPO, query = "select o from Usuarios o JOIN o.tbGrupusuUsuList1 g where o.usuUsuario = :nombre_usuario AND g.tbGrupoUsuarios1.gruUsuCod = :gru_usu_cod"),
        @NamedQuery(name = Usuarios.QUERY_FIND_USUARIO_NO_GRUPO, query = "select a from Usuarios a where a.usuCod not in (select b.tbUsuarios.usuCod from GrupoUsuarioUsuarios b where b.tbGrupoUsuarios1.gruUsuCod = :gru_usu_cod) AND a.usuUsuario = :nombre_usuario"),
        @NamedQuery(name = Usuarios.QUERY_FIND_USUARIO_PERTENCE_GRUPO, query = "select o from Usuarios o JOIN o.tbGrupusuUsuList1 g where g.tbGrupoUsuarios1.gruUsuCod = :gru_usu_cod"),
        @NamedQuery(name = Usuarios.QUERY_FIND_USUARIO_NO_PERTENECE_GRUPO, query = "select a from Usuarios a where a.usuCod not in (\n" +
                "select b.tbUsuarios.usuCod from GrupoUsuarioUsuarios b where b.tbGrupoUsuarios1.gruUsuCod = :gru_usu_cod)")
})
@Table(name = "TB_USUARIOS")
@SequenceGenerator(name = "Usuarios_Id_Seq_Gen", sequenceName = "TB_USUARIOS_ID_SEQ_GEN", allocationSize = 50,
        initialValue = 50)
public class Usuarios implements Serializable {
    private static final long serialVersionUID = 6172872475430544746L;

    public static final String QUERY_FIND_BY_EMAIL = "Usuarios.findByEmail";

    public static final String QUERY_FIND_ALL = "Usuarios.findAll";

    public static final String QUERY_FIND_BY_NOMUSER = "Usuarios.findByNomUser";

    public static final String QUERY_FIND_USUARIO_GRUPO = "GrupoUsuarioUsuarios.findUserGroup";

    public static final String QUERY_FIND_USUARIO_NO_GRUPO = "GrupoUsuarioUsuarios.findUserNoGroup";

    public static final String QUERY_FIND_USUARIO_PERTENCE_GRUPO = "GrupoUsuarioUsuarios.findByGruUsuCod";

    public static final String QUERY_FIND_USUARIO_NO_PERTENECE_GRUPO = "GrupoUsuarioUsuarios.findBy!GruUsuCod";

    @Column(name = "USU_APE_MAT", length = 20)
    private String usuApeMat;
    @Column(name = "USU_APE_PAT", length = 20)
    private String usuApePat;
    @Id
    @Column(name = "USU_COD", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Usuarios_Id_Seq_Gen")
    private Integer usuCod;
    @Column(name = "USU_CORREO", length = 50)
    private String usuCorreo;
    @Column(name = "USU_ESTADO", nullable = false)
    private Integer usuEstado;
    @Column(name = "USU_NOMBRE", length = 30)
    private String usuNombre;
    @Column(name = "USU_USUARIO", nullable = false, length = 20)
    private String usuUsuario;
    @OneToMany(mappedBy = "tbUsuarios", cascade = { CascadeType.ALL, CascadeType.ALL })
    private List<GrupoUsuarioUsuarios> tbGrupusuUsuList1;
    @OneToMany(mappedBy = "tbUsuarios1", cascade = { CascadeType.ALL, CascadeType.ALL })
    private List<UsuarioGrupo> tbUsuGrupoList1;

    public Usuarios() {
    }

    public Usuarios(String usuApeMat, String usuApePat, Integer usuCod, String usuCorreo, Integer usuEstado,
                    String usuNombre, String usuUsuario) {
        this.usuApeMat = usuApeMat;
        this.usuApePat = usuApePat;
        this.usuCod = usuCod;
        this.usuCorreo = usuCorreo;
        this.usuEstado = usuEstado;
        this.usuNombre = usuNombre;
        this.usuUsuario = usuUsuario;
    }

    public String getUsuApeMat() {
        return usuApeMat;
    }

    public void setUsuApeMat(String usuApeMat) {
        this.usuApeMat = usuApeMat;
    }

    public String getUsuApePat() {
        return usuApePat;
    }

    public void setUsuApePat(String usuApePat) {
        this.usuApePat = usuApePat;
    }

    public Integer getUsuCod() {
        return usuCod;
    }

    public String getUsuCorreo() {
        return usuCorreo;
    }

    public void setUsuCorreo(String usuCorreo) {
        this.usuCorreo = usuCorreo;
    }

    public Integer getUsuEstado() {
        return usuEstado;
    }

    public void setUsuEstado(Integer usuEstado) {
        this.usuEstado = usuEstado;
    }

    public String getUsuNombre() {
        return usuNombre;
    }

    public void setUsuNombre(String usuNombre) {
        this.usuNombre = usuNombre;
    }

    public String getUsuUsuario() {
        return usuUsuario;
    }

    public void setUsuUsuario(String usuUsuario) {
        this.usuUsuario = usuUsuario;
    }

    public List<GrupoUsuarioUsuarios> getTbGrupusuUsuList1() {
        return tbGrupusuUsuList1;
    }

    public void setTbGrupusuUsuList1(List<GrupoUsuarioUsuarios> tbGrupusuUsuList1) {
        this.tbGrupusuUsuList1 = tbGrupusuUsuList1;
    }

    public GrupoUsuarioUsuarios addGrupoUsuarioUsuarios(GrupoUsuarioUsuarios grupoUsuarioUsuarios) {
        getTbGrupusuUsuList1().add(grupoUsuarioUsuarios);
        grupoUsuarioUsuarios.setTbUsuarios(this);
        return grupoUsuarioUsuarios;
    }

    public GrupoUsuarioUsuarios removeGrupoUsuarioUsuarios(GrupoUsuarioUsuarios grupoUsuarioUsuarios) {
        getTbGrupusuUsuList1().remove(grupoUsuarioUsuarios);
        grupoUsuarioUsuarios.setTbUsuarios(null);
        return grupoUsuarioUsuarios;
    }

    public List<UsuarioGrupo> getTbUsuGrupoList1() {
        return tbUsuGrupoList1;
    }

    public void setTbUsuGrupoList1(List<UsuarioGrupo> tbUsuGrupoList1) {
        this.tbUsuGrupoList1 = tbUsuGrupoList1;
    }

    public UsuarioGrupo addUsuarioGrupo(UsuarioGrupo usuarioGrupo) {
        getTbUsuGrupoList1().add(usuarioGrupo);
        usuarioGrupo.setTbUsuarios1(this);
        return usuarioGrupo;
    }

    public UsuarioGrupo removeUsuarioGrupo(UsuarioGrupo usuarioGrupo) {
        getTbUsuGrupoList1().remove(usuarioGrupo);
        usuarioGrupo.setTbUsuarios1(null);
        return usuarioGrupo;
    }
}
