package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.PeriodoDAO;
import cl.gob.ips.exencion.dto.PeriodoDTO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.model.Periodo;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.swing.*;
import java.text.SimpleDateFormat;

/**
 *
 * @author Cristian Toledo 19-12-2016
 */
@Stateless
@LocalBean
public class PeriodoServices {

	@Inject
	PeriodoDAO dao;

	public Periodo buscaPeriodoActivo() throws DataAccessException {
		return dao.buscarPeriodoActivoCerrar();
	}

	public Periodo obtenerPeriodoCerrar(final Integer periodo) throws DataAccessException {
		return dao.obtenerPeriodoCerrar(periodo);
	}

	public Periodo obtenerPeriodoGenerar(final Integer periodo) throws DataAccessException {
		return dao.obtenerPeriodoGenerar(periodo);
	}
	
	public Integer obtenerEstadoProceso(final Integer periodo) throws DataAccessException {
		return dao.obtenerEstadoProceso(periodo);
	}

	public void modificarPeriodo(final Periodo periodo) throws DataAccessException {
		dao.modificarPeriodo(periodo);
	}

	public Integer validarPeriodoCerrar(final Integer periodo) throws DataAccessException{
		return dao.validarEstadoCierrePeriodo(periodo);
	}

	public Integer validarPeriodoGenerar(final Integer periodo) throws DataAccessException{
		return dao.validarEstadoGenerarNominas(periodo);
	}

	public Periodo obtenerPeriodoActual(){
		return dao.obtenerPeriodoActual();
	}

	public PeriodoDTO obtenerPeriodoActualDTO() throws IpsBussinesException, DataAccessException {
		PeriodoDTO periodoDTO =null;

		Periodo periodoActual = dao.obtenerPeriodoActual();

		if (periodoActual!=null){
			periodoDTO = new PeriodoDTO();
			this.mapeaObjetoBDaDTO(periodoDTO, periodoActual);
		}

		return periodoDTO;

	}

	private void mapeaObjetoBDaDTO(PeriodoDTO det, Periodo per){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		det.setPerFecCierre(per.getPerUsuCierre() != null ? dateFormat.format(per.getPerFecCierre()) : "(No disponible)");
		det.setPerFecCreacion(per.getPerFecCreacion() !=null ? dateFormat.format(per.getPerFecCreacion()) :"(No disponible)");
        det.setPerPeriodo(per.getPerPeriodo().toString() != null ? per.getPerPeriodo().toString() : "(No disponible)");
		det.setPerUsuCierre(per.getPerUsuCierre() != null ? per.getPerUsuCierre() : "(No disponible)");
		det.setNivelActual(per.getNivelActual() != null ? per.getNivelActual() : 0);
		det.setEtapaPeriodoCod(per.getTbPrEtapasPeriodo().getEtapaPerCod() !=null ? per.getTbPrEtapasPeriodo().getEtapaPerCod() : 0);
		det.setEtapaPeriodoGlosa(per.getTbPrEtapasPeriodo().getEtapaPerGlosa() !=null ? per.getTbPrEtapasPeriodo().getEtapaPerGlosa() : "(No disponible)");

	}



}
