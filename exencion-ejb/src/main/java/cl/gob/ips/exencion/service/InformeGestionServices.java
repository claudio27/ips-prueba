package cl.gob.ips.exencion.service;

import cl.gob.ips.exencion.dao.InformeGestionDAO;
import cl.gob.ips.exencion.dto.InformeGestionDTO;
import cl.gob.ips.exencion.model.InformeGestionExtinto;
import cl.gob.ips.exencion.model.InformeGestionMantienen;
import cl.gob.ips.exencion.model.InformeGestionNuevos;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.model.InformeGestionVer;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by alaya on 4/08/17.
 */
public class InformeGestionServices {

    @Inject
    private InformeGestionDAO informeGestionDAO;

    public List<InformeGestionDTO> getInformeGestion(Integer periodoDesde, Integer periodoHasta, Integer codTipoPeriodo, Integer codPresentadoPor) throws DataAccessException {
        return informeGestionDAO.obtenerInformeGestion(periodoDesde, periodoHasta, codPresentadoPor, codPresentadoPor);
    }

    public List<InformeGestionExtinto> getInformeExtintos(Integer periodo, Integer codTipoPeriodo) throws DataAccessException,IpsBussinesException{
        return informeGestionDAO.obtenerInformeExtintos(periodo, codTipoPeriodo);
    }

    public List<InformeGestionNuevos> getInformeNuevos(Integer periodo, Integer codTipoPeriodo) throws DataAccessException, IpsBussinesException {
        return informeGestionDAO.getInformeNuevos(periodo, codTipoPeriodo);
    }

    public List<InformeGestionMantienen> getInformeMantienen(Integer periodo, Integer codTipoPeriodo) throws DataAccessException, IpsBussinesException {
        return informeGestionDAO.getInformeMantienen(periodo, codTipoPeriodo);
    }

    public List<InformeGestionVer> obtenerDatosVerPorTipoPeriodo(Integer periodo, Integer tipoPeriodo) throws DataAccessException, IpsBussinesException {
        return informeGestionDAO.obtenerDatosVerPorTipoPeriodo(periodo, tipoPeriodo);
    }

    public List<InformeGestionNuevos> obtenerDatosNuevos(Integer periodo) throws DataAccessException, IpsBussinesException {
        return informeGestionDAO.obtenerDatosNuevos(periodo);
    }

    public List<InformeGestionNuevos> obtenerDatosVerNuevos(Integer periodo, String institucion) throws DataAccessException, IpsBussinesException {
        return informeGestionDAO.obtenerDatosVerNuevos(periodo, institucion);
    }
}
