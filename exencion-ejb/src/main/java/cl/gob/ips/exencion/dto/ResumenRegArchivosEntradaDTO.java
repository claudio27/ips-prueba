package cl.gob.ips.exencion.dto;

import java.io.Serializable;

public class ResumenRegArchivosEntradaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4378385773283525006L;

	private String periodo;
	private String archivoCarga;
	private Integer cantidadRegistros;

	public ResumenRegArchivosEntradaDTO() {
		super();
	}

	public ResumenRegArchivosEntradaDTO(String periodo, String archivoCarga,
			Integer cantidadRegistros) {
		super();
		this.periodo = periodo;
		this.archivoCarga = archivoCarga;
		this.cantidadRegistros = cantidadRegistros;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getArchivoCarga() {
		return archivoCarga;
	}

	public void setArchivoCarga(String archivoCarga) {
		this.archivoCarga = archivoCarga;
	}

	public Integer getCantidadRegistros() {
		return cantidadRegistros;
	}

	public void setCantidadRegistros(Integer cantidadRegistros) {
		this.cantidadRegistros = cantidadRegistros;
	}

}
