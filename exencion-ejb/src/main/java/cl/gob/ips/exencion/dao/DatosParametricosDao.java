package cl.gob.ips.exencion.dao;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.GruposArchivo;
import cl.gob.ips.exencion.model.NivelResponsabilidad;
import cl.gob.ips.exencion.model.TipoInstitucion;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by rodrigo.reyesco@gmail.com on 06-10-2016.
 */
@Stateless
@LocalBean
public class DatosParametricosDao extends BaseDAO {

    /**
     * Dao que obtiene registros de la tabla TB_PR_TIPO_INSTITUCION
     * @return List<TipoInstitucion>
     * @throws Exception
     */
    public List<TipoInstitucion> getTiposInstitucion() throws DataAccessException {
        return (List<TipoInstitucion>) super.em.createNamedQuery(TipoInstitucion.QUERY_FIND_ALL)
                .getResultList();
    }

    /**
     *
     * @return
     * @throws DataAccessException
     */
    public List<GruposArchivo> getGruposArchivo() throws DataAccessException {
        return (List<GruposArchivo>) super.em.createNamedQuery(GruposArchivo.QUERY_FIND_ALL)
                .getResultList();
    }

    /**
     * Dao que obtiene registros de la tabla TB_PR_NIVEL_RESPONSABILIDAD
     * @return
     * @throws DataAccessException
     */
    public List<NivelResponsabilidad> getNivelesResponsabilidad() throws DataAccessException {
        return (List<NivelResponsabilidad>) super.em.createNamedQuery(NivelResponsabilidad.QUERY_FIND_ALL)
                .getResultList();
    }
}
