package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@NamedQueries({ @NamedQuery(name = "ArchivosCargaPeriodo.findAll", query = "select o from ArchivosCargaPeriodo o") })
@Table(name = "TB_ARCHIVOS_CARGA_PERIODO")
public class ArchivosCargaPeriodo implements Serializable {
    @Column(name = "ARC_CANT_REG_CARGADOS")
    private Integer arcCantRegCargados;
    @Column(name = "ARC_CANT_REG_ILEGIBLES")
    private Integer arcCantRegIlegibles;
    @Column(name = "ARC_CANT_REG_INFO")
    private Integer arcCantRegInfo;
    @Column(name = "ARC_CANT_REG_SIN_DATOS")
    private Integer arcCantRegSinDatos;
    @Column(name = "ARC_ESTRUCTURA", length = 3)
    private String arcEstructura;
    @Temporal(TemporalType.DATE)
    @Column(name = "ARC_FECHA_CARGAR")
    private Date arcFechaCargar;
    @Temporal(TemporalType.DATE)
    @Column(name = "ARC_FECHA_ELIMINACION")
    private Date arcFechaEliminacion;
    @Column(name = "ARC_FORMATO_NOMBRE", length = 3)
    private String arcFormatoNombre;
    @Id
    @Column(name = "ARC_ID", nullable = false)
    private Long arcId;
    @Column(name = "ARC_NOMBRE_ARCHIVO_CARGADO", length = 50)
    private String arcNombreArchivoCargado;
    @Column(name = "ARC_NOMBRE_ARCHIVO")
    private String archNombreArchivo;
    @Column(name = "ARC_USU_ELIMINACION", length = 20)
    private String arcUsuEliminacion;

    @Column(name = "ARC_MENSAJE_ERROR")
    private String arcMensajeError;

    @OneToMany(mappedBy = "archivosCargaPeriodo", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<BenregHis> tbBenregHisList;
    @OneToMany(mappedBy = "archivosCargaPeriodo1", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<SuspencionesHistorico> tbSuspencionesHisList;
    @ManyToOne
    @JoinColumn(name = "ARC_PERIODO")
    private Periodo tbPeriodo1;
    @ManyToOne
    @JoinColumn(name = "ARC_EST_CAR_ID")
    private EstadoCarga tbPrEstCarga;
    @OneToMany(mappedBy = "archivosCargaPeriodo2", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<PdiHistoricos> tbPdiHisList;
    @ManyToOne
    @JoinColumn(name = "ARC_INI_ID")
    private ArchivosIniciales tbPrArchivosIniciales;
    @OneToMany(mappedBy = "archivosCargaPeriodo3", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<BphHistorico> tbBphHisList;
    @OneToMany(mappedBy = "archivosCargaPeriodo4", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private List<ArchEstandarHis> tbArchEstandarHisList1;

    public ArchivosCargaPeriodo() {
    }

    public ArchivosCargaPeriodo(Integer arcCantRegCargados, Integer arcCantRegIlegibles, Integer arcCantRegInfo,
                                Integer arcCantRegSinDatos, EstadoCarga tbPrEstCarga, String arcEstructura,
                                Date arcFechaCargar, Date arcFechaEliminacion, String arcFormatoNombre, Long arcId,
                                ArchivosIniciales tbPrArchivosIniciales, String arcNombreArchivoCargado, String arcNombreArchivo,
                                Periodo tbPeriodo1, String arcUsuEliminacion, String arcMensajeError) {
        this.arcCantRegCargados = arcCantRegCargados;
        this.arcCantRegIlegibles = arcCantRegIlegibles;
        this.arcCantRegInfo = arcCantRegInfo;
        this.arcCantRegSinDatos = arcCantRegSinDatos;
        this.tbPrEstCarga = tbPrEstCarga;
        this.arcEstructura = arcEstructura;
        this.arcFechaCargar = arcFechaCargar;
        this.arcFechaEliminacion = arcFechaEliminacion;
        this.arcFormatoNombre = arcFormatoNombre;
        this.arcId = arcId;
        this.tbPrArchivosIniciales = tbPrArchivosIniciales;
        this.arcNombreArchivoCargado = arcNombreArchivoCargado;
        this.archNombreArchivo = arcNombreArchivo;
        this.tbPeriodo1 = tbPeriodo1;
        this.arcUsuEliminacion = arcUsuEliminacion;
        this.arcMensajeError = arcMensajeError;
    }

    public Integer getArcCantRegCargados() {
        return arcCantRegCargados;
    }

    public void setArcCantRegCargados(Integer arcCantRegCargados) {
        this.arcCantRegCargados = arcCantRegCargados;
    }

    public Integer getArcCantRegIlegibles() {
        return arcCantRegIlegibles;
    }

    public void setArcCantRegIlegibles(Integer arcCantRegIlegibles) {
        this.arcCantRegIlegibles = arcCantRegIlegibles;
    }

    public Integer getArcCantRegInfo() {
        return arcCantRegInfo;
    }

    public void setArcCantRegInfo(Integer arcCantRegInfo) {
        this.arcCantRegInfo = arcCantRegInfo;
    }

    public Integer getArcCantRegSinDatos() {
        return arcCantRegSinDatos;
    }

    public void setArcCantRegSinDatos(Integer arcCantRegSinDatos) {
        this.arcCantRegSinDatos = arcCantRegSinDatos;
    }


    public String getArcEstructura() {
        return arcEstructura;
    }

    public void setArcEstructura(String arcEstructura) {
        this.arcEstructura = arcEstructura;
    }

    public Date getArcFechaCargar() {
        return arcFechaCargar;
    }

    public void setArcFechaCargar(Date arcFechaCargar) {
        this.arcFechaCargar = arcFechaCargar;
    }

    public Date getArcFechaEliminacion() {
        return arcFechaEliminacion;
    }

    public void setArcFechaEliminacion(Date arcFechaEliminacion) {
        this.arcFechaEliminacion = arcFechaEliminacion;
    }

    public String getArcFormatoNombre() {
        return arcFormatoNombre;
    }

    public void setArcFormatoNombre(String arcFormatoNombre) {
        this.arcFormatoNombre = arcFormatoNombre;
    }

    public Long getArcId() {
        return arcId;
    }

    public void setArcId(Long arcId) {
        this.arcId = arcId;
    }


    public String getArcNombreArchivoCargado() {
        return arcNombreArchivoCargado;
    }

    public void setArcNombreArchivoCargado(String arcNombreArchivoCargado) {
        this.arcNombreArchivoCargado = arcNombreArchivoCargado;
    }

    public String getArchNombreArchivo() {
        return archNombreArchivo;
    }

    public void setArchNombreArchivo(String archNombreArchivo) {
        this.archNombreArchivo = archNombreArchivo;
    }

    public String getArcUsuEliminacion() {
        return arcUsuEliminacion;
    }

    public void setArcUsuEliminacion(String arcUsuEliminacion) {
        this.arcUsuEliminacion = arcUsuEliminacion;
    }

    public String getArcMensajeError() {
        return arcMensajeError;
    }

    public void setArcMensajeError(String arcMensajeError) {
        this.arcMensajeError = arcMensajeError;
    }

    public List<BenregHis> getTbBenregHisList() {
        return tbBenregHisList;
    }

    public void setTbBenregHisList(List<BenregHis> tbBenregHisList) {
        this.tbBenregHisList = tbBenregHisList;
    }

    public BenregHis addBenregHis(BenregHis benregHis) {
        getTbBenregHisList().add(benregHis);
        benregHis.setArchivosCargaPeriodo(this);
        return benregHis;
    }

    public BenregHis removeBenregHis(BenregHis benregHis) {
        getTbBenregHisList().remove(benregHis);
        benregHis.setArchivosCargaPeriodo(null);
        return benregHis;
    }

    public List<SuspencionesHistorico> getTbSuspencionesHisList() {
        return tbSuspencionesHisList;
    }

    public void setTbSuspencionesHisList(List<SuspencionesHistorico> tbSuspencionesHisList) {
        this.tbSuspencionesHisList = tbSuspencionesHisList;
    }

    public SuspencionesHistorico addSuspencionesHistorico(SuspencionesHistorico suspencionesHistorico) {
        getTbSuspencionesHisList().add(suspencionesHistorico);
        suspencionesHistorico.setArchivosCargaPeriodo1(this);
        return suspencionesHistorico;
    }

    public SuspencionesHistorico removeSuspencionesHistorico(SuspencionesHistorico suspencionesHistorico) {
        getTbSuspencionesHisList().remove(suspencionesHistorico);
        suspencionesHistorico.setArchivosCargaPeriodo1(null);
        return suspencionesHistorico;
    }

    public Periodo getTbPeriodo1() {
        return tbPeriodo1;
    }

    public void setTbPeriodo1(Periodo tbPeriodo1) {
        this.tbPeriodo1 = tbPeriodo1;
    }

    public EstadoCarga getTbPrEstCarga() {
        return tbPrEstCarga;
    }

    public void setTbPrEstCarga(EstadoCarga tbPrEstCarga) {
        this.tbPrEstCarga = tbPrEstCarga;
    }

    public List<PdiHistoricos> getTbPdiHisList() {
        return tbPdiHisList;
    }

    public void setTbPdiHisList(List<PdiHistoricos> tbPdiHisList) {
        this.tbPdiHisList = tbPdiHisList;
    }

    public PdiHistoricos addPdiHistoricos(PdiHistoricos pdiHistoricos) {
        getTbPdiHisList().add(pdiHistoricos);
        pdiHistoricos.setArchivosCargaPeriodo2(this);
        return pdiHistoricos;
    }

    public PdiHistoricos removePdiHistoricos(PdiHistoricos pdiHistoricos) {
        getTbPdiHisList().remove(pdiHistoricos);
        pdiHistoricos.setArchivosCargaPeriodo2(null);
        return pdiHistoricos;
    }

    public ArchivosIniciales getTbPrArchivosIniciales() {
        return tbPrArchivosIniciales;
    }

    public void setTbPrArchivosIniciales(ArchivosIniciales tbPrArchivosIniciales) {
        this.tbPrArchivosIniciales = tbPrArchivosIniciales;
    }

    public List<BphHistorico> getTbBphHisList() {
        return tbBphHisList;
    }

    public void setTbBphHisList(List<BphHistorico> tbBphHisList) {
        this.tbBphHisList = tbBphHisList;
    }

    public BphHistorico addBphHistorico(BphHistorico bphHistorico) {
        getTbBphHisList().add(bphHistorico);
        bphHistorico.setArchivosCargaPeriodo3(this);
        return bphHistorico;
    }

    public BphHistorico removeBphHistorico(BphHistorico bphHistorico) {
        getTbBphHisList().remove(bphHistorico);
        bphHistorico.setArchivosCargaPeriodo3(null);
        return bphHistorico;
    }

    public List<ArchEstandarHis> getTbArchEstandarHisList1() {
        return tbArchEstandarHisList1;
    }

    public void setTbArchEstandarHisList1(List<ArchEstandarHis> tbArchEstandarHisList1) {
        this.tbArchEstandarHisList1 = tbArchEstandarHisList1;
    }

    public ArchEstandarHis addArchEstandarHis(ArchEstandarHis archEstandarHis) {
        getTbArchEstandarHisList1().add(archEstandarHis);
        archEstandarHis.setArchivosCargaPeriodo4(this);
        return archEstandarHis;
    }

    public ArchEstandarHis removeArchEstandarHis(ArchEstandarHis archEstandarHis) {
        getTbArchEstandarHisList1().remove(archEstandarHis);
        archEstandarHis.setArchivosCargaPeriodo4(null);
        return archEstandarHis;
    }
}
