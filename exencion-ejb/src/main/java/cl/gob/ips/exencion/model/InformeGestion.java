package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by HancacuraB on 04-08-2017
 */
@Entity
@Table(name = "TB_INFORME_GESTION")
@NamedStoredProcedureQuery(
        name = "PRC_GENERAR_INFORME_GESTION",
        procedureName = "PRC_GENERAR_INFORME_GESTION",
        parameters = {
                @StoredProcedureParameter(name = "fechaDesde", type = Integer.class, mode = ParameterMode.IN),
                @StoredProcedureParameter(name = "fechaHasta", type = Integer.class, mode = ParameterMode.IN),
                @StoredProcedureParameter(name = "tipoPeriodo", type = Integer.class, mode = ParameterMode.IN),
                @StoredProcedureParameter(name = "presentadoPor", type = Integer.class, mode = ParameterMode.IN)
        }
)
public class InformeGestion implements Serializable {

    private static final long serialVersionUID = 3811660713451779064L;

    @Id
    @Column(name = "INF_PERIODO")
    private Integer periodo;

    @Column(name = "INF_IPS")
    private Integer ips;

    @Column(name = "INF_ISL")
    private Integer isl;

    @Column(name = "INF_BPH")
    private Integer bph;

    @Column(name = "INF_TOTAL_INTERNOS")
    private Integer totalInternos;

    @Column(name = "INF_AFP")
    private Integer afp;

    @Column(name = "INF_CSV")
    private Integer csv;

    @Column(name = "INF_MUTUAL")
    private Integer mutual;

    @Column(name = "INF_TOTAL_EXTERNOS")
    private Integer totalExternos;

    @Column(name = "INF_TOTAL")
    private Integer total;

    @Column(name = "INF_NUEVOS")
    private Integer nuevos;

    @Column(name = "INF_EXTINCIONES")
    private Integer extinciones;

    @Column(name = "INF_MANTIENEN")
    private Integer mantienen;

    public InformeGestion() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Integer periodo) {
        this.periodo = periodo;
    }

    public Integer getIps() {
        return ips;
    }

    public void setIps(Integer ips) {
        this.ips = ips;
    }

    public Integer getIsl() {
        return isl;
    }

    public Integer getBph() {
        return bph;
    }

    public void setBph(Integer bph) {
        this.bph = bph;
    }

    public void setIsl(Integer isl) {
        this.isl = isl;
    }

    public Integer getTotalInternos() {
        return totalInternos;
    }

    public void setTotalInternos(Integer totalInternos) {
        this.totalInternos = totalInternos;
    }

    public Integer getAfp() {
        return afp;
    }

    public void setAfp(Integer afp) {
        this.afp = afp;
    }

    public Integer getCsv() {
        return csv;
    }

    public void setCsv(Integer csv) {
        this.csv = csv;
    }

    public Integer getMutual() {
        return mutual;
    }

    public void setMutual(Integer mutual) {
        this.mutual = mutual;
    }

    public Integer getTotalExternos() {
        return totalExternos;
    }

    public void setTotalExternos(Integer totalExternos) {
        this.totalExternos = totalExternos;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getNuevos() {
        return nuevos;
    }

    public void setNuevos(Integer nuevos) {
        this.nuevos = nuevos;
    }

    public Integer getExtinciones() {
        return extinciones;
    }

    public void setExtinciones(Integer extinciones) {
        this.extinciones = extinciones;
    }

    public Integer getMantienen() {
        return mantienen;
    }

    public void setMantienen(Integer mantienen) {
        this.mantienen = mantienen;
    }
}
