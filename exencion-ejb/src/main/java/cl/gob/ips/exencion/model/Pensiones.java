package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * To create ID generator sequence "TB_PENSIONES_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PENSIONES_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@NamedQueries({ @NamedQuery(name = "Pensiones.findAll", query = "select o from Pensiones o") })
@Table(name = "TB_PENSIONES")
@SequenceGenerator(name = "Pensiones_Id_Seq_Gen", sequenceName = "TB_PENSIONES_ID_SEQ_GEN", allocationSize = 50,
                   initialValue = 50)
public class Pensiones implements Serializable {
    private static final long serialVersionUID = -5038653450966521313L;
    @Column(name = "PNS_BPH")
    private Integer pnsBph;
    @Column(name = "PNS_COD_INSTITUCION")
    private Long pnsCodInstitucion;
    @Column(name = "PNS_COD_ORIGEN_PENSION")
    private Integer pnsCodOrigenPension;
    @Column(name = "PNS_DV", length = 1)
    private String pnsDv;
    @Column(name = "PNS_ELIMINADO")
    private Integer pnsEliminado;
    @Temporal(TemporalType.DATE)
    @Column(name = "PNS_FEC_ELIMINADO")
    private Date pnsFecEliminado;
    @Id
    @Column(name = "PNS_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Pensiones_Id_Seq_Gen")
    private BigDecimal pnsId;
    @Column(name = "PNS_NUM_COTIZADO")
    private Long pnsNumCotizado;
    @Column(name = "PNS_RUN", nullable = false)
    private Long pnsRun;
    @Column(name = "PNS_TPR_COD")
    private Integer pnsTprCod;
    @Column(name = "PNS_USU_ELIMINADO", length = 20)
    private String pnsUsuEliminado;
    @ManyToOne
    @JoinColumn(name = "PNS_TBL_ORI_COD")
    private Origen tbPrTblOrigen;
    @ManyToOne
    @JoinColumn(name = "PNS_TIP_PEN_COD")
    private TipoBeneficio tipoBeneficio;
    @ManyToOne
    @JoinColumn(name = "PNS_PROC_INST_COD")
    private ProcedenciaInst tbPrProcedenciaInst;
    @ManyToOne
    @JoinColumn(name = "PNS_PERIODO")
    private Periodo tbPeriodo;
    @ManyToOne
    @JoinColumn(name = "PNS_RUT_INSTITUCION")
    private Instituciones tbPrInstituciones;

    public Pensiones() {
    }

    public Pensiones(Integer pnsBph, Long pnsCodInstitucion, Integer pnsCodOrigenPension, String pnsDv,
                     Integer pnsEliminado, Date pnsFecEliminado, BigDecimal pnsId, Long pnsNumCotizado,
                     Periodo tbPeriodo, ProcedenciaInst tbPrProcedenciaInst, Long pnsRun,
                     Instituciones tbPrInstituciones, Origen tbPrTblOrigen, TipoBeneficio tipoBeneficio,
                     Integer pnsTprCod, String pnsUsuEliminado) {
        this.pnsBph = pnsBph;
        this.pnsCodInstitucion = pnsCodInstitucion;
        this.pnsCodOrigenPension = pnsCodOrigenPension;
        this.pnsDv = pnsDv;
        this.pnsEliminado = pnsEliminado;
        this.pnsFecEliminado = pnsFecEliminado;
        this.pnsId = pnsId;
        this.pnsNumCotizado = pnsNumCotizado;
        this.tbPeriodo = tbPeriodo;
        this.tbPrProcedenciaInst = tbPrProcedenciaInst;
        this.pnsRun = pnsRun;
        this.tbPrInstituciones = tbPrInstituciones;
        this.tbPrTblOrigen = tbPrTblOrigen;
        this.tipoBeneficio = tipoBeneficio;
        this.pnsTprCod = pnsTprCod;
        this.pnsUsuEliminado = pnsUsuEliminado;
    }

    public Integer getPnsBph() {
        return pnsBph;
    }

    public void setPnsBph(Integer pnsBph) {
        this.pnsBph = pnsBph;
    }

    public Long getPnsCodInstitucion() {
        return pnsCodInstitucion;
    }

    public void setPnsCodInstitucion(Long pnsCodInstitucion) {
        this.pnsCodInstitucion = pnsCodInstitucion;
    }

    public Integer getPnsCodOrigenPension() {
        return pnsCodOrigenPension;
    }

    public void setPnsCodOrigenPension(Integer pnsCodOrigenPension) {
        this.pnsCodOrigenPension = pnsCodOrigenPension;
    }

    public String getPnsDv() {
        return pnsDv;
    }

    public void setPnsDv(String pnsDv) {
        this.pnsDv = pnsDv;
    }

    public Integer getPnsEliminado() {
        return pnsEliminado;
    }

    public void setPnsEliminado(Integer pnsEliminado) {
        this.pnsEliminado = pnsEliminado;
    }

    public Date getPnsFecEliminado() {
        return pnsFecEliminado;
    }

    public void setPnsFecEliminado(Date pnsFecEliminado) {
        this.pnsFecEliminado = pnsFecEliminado;
    }

    public BigDecimal getPnsId() {
        return pnsId;
    }

    public Long getPnsNumCotizado() {
        return pnsNumCotizado;
    }

    public void setPnsNumCotizado(Long pnsNumCotizado) {
        this.pnsNumCotizado = pnsNumCotizado;
    }


    public Long getPnsRun() {
        return pnsRun;
    }

    public void setPnsRun(Long pnsRun) {
        this.pnsRun = pnsRun;
    }


    public Integer getPnsTprCod() {
        return pnsTprCod;
    }

    public void setPnsTprCod(Integer pnsTprCod) {
        this.pnsTprCod = pnsTprCod;
    }

    public String getPnsUsuEliminado() {
        return pnsUsuEliminado;
    }

    public void setPnsUsuEliminado(String pnsUsuEliminado) {
        this.pnsUsuEliminado = pnsUsuEliminado;
    }

    public Origen getTbPrTblOrigen() {
        return tbPrTblOrigen;
    }

    public void setTbPrTblOrigen(Origen tbPrTblOrigen) {
        this.tbPrTblOrigen = tbPrTblOrigen;
    }

    public TipoBeneficio getTipoBeneficio() {
        return tipoBeneficio;
    }

    public void setTipoBeneficio(TipoBeneficio tipoBeneficio) {
        this.tipoBeneficio = tipoBeneficio;
    }

    public ProcedenciaInst getTbPrProcedenciaInst() {
        return tbPrProcedenciaInst;
    }

    public void setTbPrProcedenciaInst(ProcedenciaInst tbPrProcedenciaInst) {
        this.tbPrProcedenciaInst = tbPrProcedenciaInst;
    }

    public Periodo getTbPeriodo() {
        return tbPeriodo;
    }

    public void setTbPeriodo(Periodo tbPeriodo7) {
        this.tbPeriodo = tbPeriodo7;
    }

    public Instituciones getTbPrInstituciones() {
        return tbPrInstituciones;
    }

    public void setTbPrInstituciones(Instituciones tbPrInstituciones2) {
        this.tbPrInstituciones = tbPrInstituciones2;
    }
}
