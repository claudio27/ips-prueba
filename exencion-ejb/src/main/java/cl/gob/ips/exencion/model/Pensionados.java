package cl.gob.ips.exencion.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * To create ID generator sequence "TB_PENSIONADOS_ID_SEQ_GEN":
 * CREATE SEQUENCE "TB_PENSIONADOS_ID_SEQ_GEN" INCREMENT BY 50 START WITH 50;
 */
@Entity
@Cacheable(false)
@NamedQueries({ 
	@NamedQuery(name = "Pensionados.findAll", query = "select o from Pensionados o"),
	@NamedQuery(name = "Pensionados.findRunAndBetweenPeriodo", query = "select p from Pensionados p where p.penRun = :run and  p.periodo.perPeriodo between :periodoDesde and :periodoHasta"),
    @NamedQuery(name = "Pensionados.findRun", query = "select p from Pensionados p where p.penRun = :penRun"), @NamedQuery(name = "Pensionados.findRun", query = "select p from Pensionados p where p.penRun = :penRun")

})
@Table(name = "TB_PENSIONADOS")
public class Pensionados implements Serializable {
//p.penDv = :dv and
    private static final long serialVersionUID = 1515066718783612923L;

    public static final String QUERY_FIND_BY_RUN = "Pensionados.findRun";
    public static final String QUERY_FIND_ALL = "Pensionados.findAll";
    public static final String QUERY_FIND_BY_RUN_PERIODO = "Pensionados.findRunAndBetweenPeriodo";

    @EmbeddedId
    private PensionadosPK id;

    @Column(name = "PEN_APE_MATERNO", nullable = false, length = 20)
    private String penApeMaterno;
    @Column(name = "PEN_APE_PATERNO", length = 20)
    private String penApePaterno;
    @Column(name = "PEN_BENIFICIARIO_GLOSA", length = 50)
    private String penBenificiarioGlosa;
    @Column(name = "PEN_CON_RSH")
    private Integer penConRsh;
    @Column(name = "PEN_DESC_FOCALIZACION", length = 50)
    private String penDescFocalizacion;
    @Column(name = "PEN_DV", length = 1)
    private String penDv;
    @Column(name = "PEN_EDAD_EXTERNA")
    private Integer penEdadExterna;
    @Column(name = "PEN_EDAD_INTERNA")
    private Integer penEdadInterna;
    @Column(name = "PEN_ELIMINADO")
    private Integer penEliminado;
    @Column(name = "PEN_EXTINGUIDO")
    private Integer penExtinguido;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_FEC_COTIZACIONES")
    private Date penFecCotizaciones;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_FEC_DEFUNCION")
    private Date penFecDefuncion;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_FEC_DEVENGAMIENTO")
    private Date penFecDevengamiento;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_FEC_ELIMINADO")
    private Date penFecEliminado;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_FEC_NACIMIENTO")
    private Date penFecNacimiento;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_FEC_PDI")
    private Date penFecPdi;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_FEC_PRESENTACION_SOL")
    private Date penFecPresentacionSol;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_FEC_RECEPCION_DJ")
    private Date penFecRecepcionDj;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_FEC_RESI_ACRED_ANTER")
    private Date penFecResiAcredAnter;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_FECHA_FOCALIZACION")
    private Date penFechaFocalizacion;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_FECHA_RESIDENCIA")
    private Date penFechaResidencia;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_FECHA_RSH")
    private Date penFechaRsh;
    @Temporal(TemporalType.DATE)
    @Column(name = "PEN_FECHA_RSH_ADM")
    private Date penFechaRshAdm;
    @Column(name = "PEN_FOCALIZACION", nullable = false)
    private Integer penFocalizacion;
    @Column(name = "PEN_NOMBRES", nullable = false, length = 50)
    private String penNombres;
    @Column(name = "PEN_PERIODO_BONIFICACION")
    private Integer penPeriodoBonificacion;
    @Column(name = "PEN_POT_BENEFICIARIO")
    private Integer penPotBeneficiario;
    @Column(name = "PEN_POTENCIAL_APS")
    private BigDecimal penPotencialAps;
    @Column(name = "PEN_PUNTAJE_RSH")
    private BigDecimal penPuntajeRsh;
    @Column(name = "PEN_PUNTAJE_RSH_ADM")
    private BigDecimal penPuntajeRshAdm;
    @Column(name = "PEN_RESIDENCIA", nullable = false)
    private Integer penResidencia;
    @Column(name = "PEN_RUN", nullable = false)
    private Long penRun;
    @Column(name = "PEN_SEXO", length = 1)
    private String penSexo;
    @Column(name = "PEN_SPS", nullable = false)
    private Integer penSps;
    @Column(name = "PEN_SUSPENDIDO")
    private Integer penSuspendido;
    @Column(name = "PEN_TIPO", nullable = false)
    private Integer penTipo;
    @Column(name = "PEN_TIPO_RESIDENCIA")
    private Integer penTipoResidencia;
    @Column(name = "PEN_USU_ELIMINADO", length = 20)
    private String penUsuEliminado;
    @Column(name = "PEN_V_ELEGIBILIDAD", length = 60)
    private String penVElegibilidad;
    @Column(name = "PEN_VIVO", length = 1)
    private String penVivo;
    @ManyToOne
    @JoinColumn(name = "PEN_PERIODO")
    private Periodo periodo;

    @Column(name = "PEN_POT_APS")
    private BigDecimal penPotAps;

    @Column(name = "PEN_BENEFICIARIO")
    private Integer penBeneficiario;

    @Column(name = "PEN_PUNTAJE_PFP")
    private Integer penPuntajePfp;

    @Column(name = "PEN_PUNTAJE_PFP_ADM")
    private Integer penPuntajePfpAdm;

    public Pensionados() {
    }

    public Pensionados(PensionadosPK id, String penApeMaterno, String penApePaterno, String penBenificiarioGlosa,
                       Integer penConRsh, String penDescFocalizacion, String penDv, Integer penEdadExterna,
                       Integer penEdadInterna, Integer penEliminado, Integer penExtinguido, Date penFecCotizaciones,
                       Date penFecDefuncion, Date penFecDevengamiento, Date penFecEliminado, Date penFecNacimiento,
                       Date penFecPdi, Date penFecPresentacionSol, Date penFecRecepcionDj, Date penFecResiAcredAnter,
                       Date penFechaFocalizacion, Date penFechaResidencia, Date penFechaRsh, Date penFechaRshAdm,
                       Integer penFocalizacion, String penNombres, Integer penPeriodoBonificacion,
                       Integer penPotBeneficiario, BigDecimal penPotencialAps, BigDecimal penPuntajeRsh,
                       BigDecimal penPuntajeRshAdm, Integer penResidencia, Long penRun, String penSexo,
                       Integer penSps, Integer penSuspendido, Integer penTipo, Integer penTipoResidencia,
                       String penUsuEliminado, String penVElegibilidad, String penVivo, Periodo periodo,
                       BigDecimal penPotAps, Integer penBeneficiario, Integer penPuntajePfp, Integer penPuntajePfpAdm) {
        this.id = id;
        this.penApeMaterno = penApeMaterno;
        this.penApePaterno = penApePaterno;
        this.penBenificiarioGlosa = penBenificiarioGlosa;
        this.penConRsh = penConRsh;
        this.penDescFocalizacion = penDescFocalizacion;
        this.penDv = penDv;
        this.penEdadExterna = penEdadExterna;
        this.penEdadInterna = penEdadInterna;
        this.penEliminado = penEliminado;
        this.penExtinguido = penExtinguido;
        this.penFecCotizaciones = penFecCotizaciones;
        this.penFecDefuncion = penFecDefuncion;
        this.penFecDevengamiento = penFecDevengamiento;
        this.penFecEliminado = penFecEliminado;
        this.penFecNacimiento = penFecNacimiento;
        this.penFecPdi = penFecPdi;
        this.penFecPresentacionSol = penFecPresentacionSol;
        this.penFecRecepcionDj = penFecRecepcionDj;
        this.penFecResiAcredAnter = penFecResiAcredAnter;
        this.penFechaFocalizacion = penFechaFocalizacion;
        this.penFechaResidencia = penFechaResidencia;
        this.penFechaRsh = penFechaRsh;
        this.penFechaRshAdm = penFechaRshAdm;
        this.penFocalizacion = penFocalizacion;
        this.penNombres = penNombres;
        this.periodo = periodo;
        this.penPeriodoBonificacion = penPeriodoBonificacion;
        this.penPotBeneficiario = penPotBeneficiario;
        this.penPotencialAps = penPotencialAps;
        this.penPuntajeRsh = penPuntajeRsh;
        this.penPuntajeRshAdm = penPuntajeRshAdm;
        this.penResidencia = penResidencia;
        this.penRun = penRun;
        this.penSexo = penSexo;
        this.penSps = penSps;
        this.penSuspendido = penSuspendido;
        this.penTipo = penTipo;
        this.penTipoResidencia = penTipoResidencia;
        this.penUsuEliminado = penUsuEliminado;
        this.penVElegibilidad = penVElegibilidad;
        this.penVivo = penVivo;
        this.periodo = periodo;
        this.penPotAps = penPotAps;
        this.penBeneficiario = penBeneficiario;
        this.penPuntajePfp = penPuntajePfp;
        this.penPuntajePfpAdm = penPuntajePfpAdm;
    }

    public String getPenApeMaterno() {
        return penApeMaterno;
    }

    public void setPenApeMaterno(String penApeMaterno) {
        this.penApeMaterno = penApeMaterno;
    }

    public String getPenApePaterno() {
        return penApePaterno;
    }

    public void setPenApePaterno(String penApePaterno) {
        this.penApePaterno = penApePaterno;
    }

    public String getPenBenificiarioGlosa() {
        return penBenificiarioGlosa;
    }

    public void setPenBenificiarioGlosa(String penBenificiarioGlosa) {
        this.penBenificiarioGlosa = penBenificiarioGlosa;
    }

    public Integer getPenConRsh() {
        return penConRsh;
    }

    public void setPenConRsh(Integer penConRsh) {
        this.penConRsh = penConRsh;
    }

    public String getPenDescFocalizacion() {
        return penDescFocalizacion;
    }

    public void setPenDescFocalizacion(String penDescFocalizacion) {
        this.penDescFocalizacion = penDescFocalizacion;
    }

    public String getPenDv() {
        return penDv;
    }

    public void setPenDv(String penDv) {
        this.penDv = penDv;
    }

    public Integer getPenEdadExterna() {
        return penEdadExterna;
    }

    public void setPenEdadExterna(Integer penEdadExterna) {
        this.penEdadExterna = penEdadExterna;
    }

    public Integer getPenEdadInterna() {
        return penEdadInterna;
    }

    public void setPenEdadInterna(Integer penEdadInterna) {
        this.penEdadInterna = penEdadInterna;
    }

    public Integer getPenEliminado() {
        return penEliminado;
    }

    public void setPenEliminado(Integer penEliminado) {
        this.penEliminado = penEliminado;
    }

    public Integer getPenExtinguido() {
        return penExtinguido;
    }

    public void setPenExtinguido(Integer penExtinguido) {
        this.penExtinguido = penExtinguido;
    }

    public Date getPenFecCotizaciones() {
        return penFecCotizaciones;
    }

    public void setPenFecCotizaciones(Date penFecCotizaciones) {
        this.penFecCotizaciones = penFecCotizaciones;
    }

    public Date getPenFecDefuncion() {
        return penFecDefuncion;
    }

    public void setPenFecDefuncion(Date penFecDefuncion) {
        this.penFecDefuncion = penFecDefuncion;
    }

    public Date getPenFecDevengamiento() {
        return penFecDevengamiento;
    }

    public void setPenFecDevengamiento(Date penFecDevengamiento) {
        this.penFecDevengamiento = penFecDevengamiento;
    }

    public Date getPenFecEliminado() {
        return penFecEliminado;
    }

    public void setPenFecEliminado(Date penFecEliminado) {
        this.penFecEliminado = penFecEliminado;
    }

    public Date getPenFecNacimiento() {
        return penFecNacimiento;
    }

    public void setPenFecNacimiento(Date penFecNacimiento) {
        this.penFecNacimiento = penFecNacimiento;
    }

    public Date getPenFecPdi() {
        return penFecPdi;
    }

    public void setPenFecPdi(Date penFecPdi) {
        this.penFecPdi = penFecPdi;
    }

    public Date getPenFecPresentacionSol() {
        return penFecPresentacionSol;
    }

    public void setPenFecPresentacionSol(Date penFecPresentacionSol) {
        this.penFecPresentacionSol = penFecPresentacionSol;
    }

    public Date getPenFecRecepcionDj() {
        return penFecRecepcionDj;
    }

    public void setPenFecRecepcionDj(Date penFecRecepcionDj) {
        this.penFecRecepcionDj = penFecRecepcionDj;
    }

    public Date getPenFecResiAcredAnter() {
        return penFecResiAcredAnter;
    }

    public void setPenFecResiAcredAnter(Date penFecResiAcredAnter) {
        this.penFecResiAcredAnter = penFecResiAcredAnter;
    }

    public Date getPenFechaFocalizacion() {
        return penFechaFocalizacion;
    }

    public void setPenFechaFocalizacion(Date penFechaFocalizacion) {
        this.penFechaFocalizacion = penFechaFocalizacion;
    }

    public Date getPenFechaResidencia() {
        return penFechaResidencia;
    }

    public void setPenFechaResidencia(Date penFechaResidencia) {
        this.penFechaResidencia = penFechaResidencia;
    }

    public Date getPenFechaRsh() {
        return penFechaRsh;
    }

    public void setPenFechaRsh(Date penFechaRsh) {
        this.penFechaRsh = penFechaRsh;
    }

    public Date getPenFechaRshAdm() {
        return penFechaRshAdm;
    }

    public void setPenFechaRshAdm(Date penFechaRshAdm) {
        this.penFechaRshAdm = penFechaRshAdm;
    }

    public Integer getPenFocalizacion() {
        return penFocalizacion;
    }

    public void setPenFocalizacion(Integer penFocalizacion) {
        this.penFocalizacion = penFocalizacion;
    }

    public String getPenNombres() {
        return penNombres;
    }

    public void setPenNombres(String penNombres) {
        this.penNombres = penNombres;
    }


    public Integer getPenPeriodoBonificacion() {
        return penPeriodoBonificacion;
    }

    public void setPenPeriodoBonificacion(Integer penPeriodoBonificacion) {
        this.penPeriodoBonificacion = penPeriodoBonificacion;
    }

    public Integer getPenPotBeneficiario() {
        return penPotBeneficiario;
    }

    public void setPenPotBeneficiario(Integer penPotBeneficiario) {
        this.penPotBeneficiario = penPotBeneficiario;
    }

    public BigDecimal getPenPotencialAps() {
        return penPotencialAps;
    }

    public void setPenPotencialAps(BigDecimal penPotencialAps) {
        this.penPotencialAps = penPotencialAps;
    }

    public BigDecimal getPenPuntajeRsh() {
        return penPuntajeRsh;
    }

    public void setPenPuntajeRsh(BigDecimal penPuntajeRsh) {
        this.penPuntajeRsh = penPuntajeRsh;
    }

    public BigDecimal getPenPuntajeRshAdm() {
        return penPuntajeRshAdm;
    }

    public void setPenPuntajeRshAdm(BigDecimal penPuntajeRshAdm) {
        this.penPuntajeRshAdm = penPuntajeRshAdm;
    }

    public Integer getPenResidencia() {
        return penResidencia;
    }

    public void setPenResidencia(Integer penResidencia) {
        this.penResidencia = penResidencia;
    }

    public Long getPenRun() {
        return penRun;
    }

    public void setPenRun(Long penRun) {
        this.penRun = penRun;
    }

    public String getPenSexo() {
        return penSexo;
    }

    public void setPenSexo(String penSexo) {
        this.penSexo = penSexo;
    }

    public Integer getPenSps() {
        return penSps;
    }

    public void setPenSps(Integer penSps) {
        this.penSps = penSps;
    }

    public Integer getPenSuspendido() {
        return penSuspendido;
    }

    public void setPenSuspendido(Integer penSuspendido) {
        this.penSuspendido = penSuspendido;
    }

    public Integer getPenTipo() {
        return penTipo;
    }

    public void setPenTipo(Integer penTipo) {
        this.penTipo = penTipo;
    }

    public Integer getPenTipoResidencia() {
        return penTipoResidencia;
    }

    public void setPenTipoResidencia(Integer penTipoResidencia) {
        this.penTipoResidencia = penTipoResidencia;
    }

    public String getPenUsuEliminado() {
        return penUsuEliminado;
    }

    public void setPenUsuEliminado(String penUsuEliminado) {
        this.penUsuEliminado = penUsuEliminado;
    }

    public String getPenVElegibilidad() {
        return penVElegibilidad;
    }

    public void setPenVElegibilidad(String penVElegibilidad) {
        this.penVElegibilidad = penVElegibilidad;
    }

    public String getPenVivo() {
        return penVivo;
    }

    public void setPenVivo(String penVivo) {
        this.penVivo = penVivo;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo tbPeriodo6) {
        this.periodo = tbPeriodo6;
    }

    public BigDecimal getPenPotAps() {
        return this.penPotAps;
    }

    public void setPenPotAps(BigDecimal penPotAps) {
        this.penPotAps = penPotAps;
    }

    public Integer getPenBeneficiario() {
        return penBeneficiario;
    }

    public void setPenBeneficiario(Integer penBeneficiario) {
        this.penBeneficiario = penBeneficiario;
    }

    public Integer getPenPuntajePfp() {
        return penPuntajePfp;
    }

    public void setPenPuntajePfp(Integer penPuntajePfp) {
        this.penPuntajePfp = penPuntajePfp;
    }

    public Integer getPenPuntajePfpAdm() {
        return penPuntajePfpAdm;
    }

    public void setPenPuntajePfpAdm(Integer penPuntajePfpAdm) {
        this.penPuntajePfpAdm = penPuntajePfpAdm;
    }

    @Override
    public String toString() {
        return "Pensionados{" +
                "id=" + id +
                ", penApeMaterno='" + penApeMaterno + '\'' +
                ", penApePaterno='" + penApePaterno + '\'' +
                ", penBenificiarioGlosa='" + penBenificiarioGlosa + '\'' +
                ", penConRsh=" + penConRsh +
                ", penDescFocalizacion='" + penDescFocalizacion + '\'' +
                ", penDv='" + penDv + '\'' +
                ", penEdadExterna=" + penEdadExterna +
                ", penEdadInterna=" + penEdadInterna +
                ", penEliminado=" + penEliminado +
                ", penExtinguido=" + penExtinguido +
                ", penFecCotizaciones=" + penFecCotizaciones +
                ", penFecDefuncion=" + penFecDefuncion +
                ", penFecDevengamiento=" + penFecDevengamiento +
                ", penFecEliminado=" + penFecEliminado +
                ", penFecNacimiento=" + penFecNacimiento +
                ", penFecPdi=" + penFecPdi +
                ", penFecPresentacionSol=" + penFecPresentacionSol +
                ", penFecRecepcionDj=" + penFecRecepcionDj +
                ", penFecResiAcredAnter=" + penFecResiAcredAnter +
                ", penFechaFocalizacion=" + penFechaFocalizacion +
                ", penFechaResidencia=" + penFechaResidencia +
                ", penFechaRsh=" + penFechaRsh +
                ", penFechaRshAdm=" + penFechaRshAdm +
                ", penFocalizacion=" + penFocalizacion +
                ", penNombres='" + penNombres + '\'' +
                ", penPeriodoBonificacion=" + penPeriodoBonificacion +
                ", penPotBeneficiario=" + penPotBeneficiario +
                ", penPotencialAps=" + penPotencialAps +
                ", penPuntajeRsh=" + penPuntajeRsh +
                ", penPuntajeRshAdm=" + penPuntajeRshAdm +
                ", penResidencia=" + penResidencia +
                ", penRun=" + penRun +
                ", penSexo='" + penSexo + '\'' +
                ", penSps=" + penSps +
                ", penSuspendido=" + penSuspendido +
                ", penTipo=" + penTipo +
                ", penTipoResidencia=" + penTipoResidencia +
                ", penUsuEliminado='" + penUsuEliminado + '\'' +
                ", penVElegibilidad='" + penVElegibilidad + '\'' +
                ", penVivo='" + penVivo + '\'' +
                ", periodo=" + periodo +
                ", penPotAps=" + penPotAps +
                ", penBeneficiario=" + penBeneficiario +
                ", penPuntajePfp=" + penPuntajePfp +
                ", penPuntajePfpAdm=" + penPuntajePfpAdm +
                '}';
    }
}
