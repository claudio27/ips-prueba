package cl.gob.ips.exencion.informes.view;

import cl.gob.ips.exencion.dto.DetalleInformeUniversoDTO;
import cl.gob.ips.exencion.dto.InformeUniversoDTO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Pensionados;
import cl.gob.ips.exencion.service.InformeUniversoServices;
import cl.gob.ips.exencion.util.CreadorArchivosUtil;
import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.util.Reportes;
import cl.gob.ips.exencion.util.Utils;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import net.bootsfaces.component.button.Button;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Alaya
 * @since 15-03-2017.
 */
@ManagedBean(name = "informeUniversoView")
@ViewScoped
public class InformeUniversoView extends BaseControllerView implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 331345600770868691L;
	private static final Logger LOGGER = Logger.getLogger(InformeUniversoView.class);

	private Date periodoDesde;
	private Date periodoHasta;
	private Boolean mostrarGrilla;
	private Boolean btnCerrarModal;
	private List<InformeUniversoDTO> respuestaGrilla;
	private List<DetalleInformeUniversoDTO> respuestaGrillaVer;
	private Button boton;

	@Inject
	private InformeUniversoServices informeServices;

	@PostConstruct
	private void init() {
		this.mostrarGrilla = Boolean.FALSE;
		this.btnCerrarModal = Boolean.TRUE;
		boton = new Button();
        this.boton.setDisabled(true);
	}
	
	public void buscar() {
		LOGGER.debug("periodoDesde: " + this.periodoDesde);
		LOGGER.debug("periodoHasta: " + this.periodoHasta);
		
		if (null != this.periodoDesde && null != this.periodoHasta ) {
			if (Utils.validarPeriodoDesdeHasta(this.periodoDesde, this.periodoHasta)) {

                try {
                    respuestaGrilla = informeServices.obtenerInformeUniverso(
                            obtenerPeriodoEntero(this.periodoDesde),
                            obtenerPeriodoEntero(this.periodoHasta));
                } catch (EJBException | DataAccessException e) {
                    super.muestraErrorDeAccesoaDatos(e);
                    return;
                }

                if(respuestaGrilla == null || respuestaGrilla.isEmpty()){
                    MensajesUtil.setMensajeErrorContexto("validaciones.informe.universo.sin.informacion");
                    super.mostrarMensajeError();
                    LOGGER.info("No hay datos para el rango de periodos consultado");
                    limpiar();
                    return;
                }

                this.mostrarGrilla = Boolean.TRUE;

            } else {
				limpiar();
				MensajesUtil.setMensajeErrorContexto("validacion.movimientos.rut.periodosDesdeHasta");
				super.mostrarMensajeError();
				LOGGER.debug("is validation");
			}

		} else {
			MensajesUtil.setMensajeErrorContexto("validacion.test");
			super.mostrarMensajeError();
			LOGGER.debug("is error");
		}
	}
	
	public void verResultados(Integer periodo) {
		this.respuestaGrillaVer = new ArrayList<>();

		try {

            respuestaGrillaVer = this.mapeaResutadoaDTO(
                    informeServices.obtenerInformeUniversoPorRun(periodo)
            );

        } catch (EJBException | DataAccessException e) {
            super.muestraErrorDeAccesoaDatos(e);
        }
    }
	
	public void obtenerExcelVer(Integer periodo) throws IOException {

	    try {
			obtenerDatosConsultaVer(periodo);
		    File archivo = Reportes.obtenerInformeUniversoVer(this.respuestaGrillaVer);
			super.generarExcel(archivo);
		} catch (FileNotFoundException e) {
			LOGGER.error("Archivo no encontrado: " + e.getMessage());
	    } catch (IOException e) {
	    	LOGGER.error("Ha ocurrido un error: " + e.getMessage());
	    }
	}
	
	public void limpiar() {
		this.periodoDesde = null;
		this.periodoHasta = null;
		this.mostrarGrilla = Boolean.FALSE;
	}

	public String redirectUniversoRun() {
        return "informe-del-universo-run?faces-redirect=true&includeViewParams=true";
	}
	
	public void obtenerExcelGeneral(){

	    try {
		    File archivo = Reportes.obtenerInformeUniverso(this.respuestaGrilla);
			super.generarExcel(archivo);
		} catch (FileNotFoundException e) {
			LOGGER.error("Archivo no encontrado", e);
			super.mostrarMensajeError();
	    } catch (IOException e) {
	    	LOGGER.error("Ha ocurrido un error", e);
			super.mostrarMensajeError();
	    }
	}

	public Date getPeriodoDesde() {
		return periodoDesde;
	}

	public void setPeriodoDesde(Date periodoDesde) {
		this.periodoDesde = periodoDesde;
	}

	public Date getPeriodoHasta() {
		return periodoHasta;
	}

	public void setPeriodoHasta(Date periodoHasta) {
		this.periodoHasta = periodoHasta;
	}

	public Boolean getMostrarGrilla() {
		return mostrarGrilla;
	}

	public void setMostrarGrilla(Boolean mostrarGrilla) {
		this.mostrarGrilla = mostrarGrilla;
	}

	public List<InformeUniversoDTO> getRespuestaGrilla() {
		return respuestaGrilla;
	}

	public void setRespuestaGrilla(List<InformeUniversoDTO> respuestaGrilla) {
		this.respuestaGrilla = respuestaGrilla;
	}

	public List<DetalleInformeUniversoDTO> getRespuestaGrillaVer() {
		return respuestaGrillaVer;
	}

	public void setRespuestaGrillaVer(
			List<DetalleInformeUniversoDTO> respuestaGrillaVer) {
		this.respuestaGrillaVer = respuestaGrillaVer;
	}


    public void exportarDirecto(Integer periodo) throws IOException{
        //this.respuestaGrillaVer = new ArrayList<>();

		obtenerDatosConsultaVer(periodo);

        LOGGER.debug("Periodo obtenido " + periodo );

        //CreadorArchivosUtil ca = new CreadorArchivosUtil();
        //ca.setPeriodo(periodo);
        //ca.setInformeServices(informeServices);
        //ca.start();
		File archivo = Reportes.obtenerInformeUniversoVer(this.respuestaGrillaVer);
		super.generarExcel(archivo);

        LOGGER.debug("Excel OK "  );
	}


	public void obtenerDatosConsultaVer(Integer periodo){

		this.respuestaGrillaVer = new ArrayList<>();

		try {
			this.respuestaGrillaVer = mapeaResutadoaDTO(
					this.informeServices.obtenerInformeUniversoPorRun(periodo)
			);
		}catch (EJBException | DataAccessException e) {
			LOGGER.error("Fallo obtencion de datos", e);
		}

		LOGGER.debug("Cantidad de datos ver por run " + this.respuestaGrillaVer.size());
	}


    public Boolean getBtnCerrarModal() {
        return btnCerrarModal;
    }

    public void setBtnCerrarModal(Boolean btnCerrarModal) {
        this.btnCerrarModal = btnCerrarModal;
    }

    public Button getBoton() {
        return boton;
    }

    public void setBoton(Button boton) {
        this.boton = boton;
    }

    public static List<DetalleInformeUniversoDTO> mapeaResutadoaDTO(List<Pensionados> listaDetalleRunPeriodo){

	    List<DetalleInformeUniversoDTO> listaDetalleUniverso = new ArrayList<>();

	    for (Pensionados pen : listaDetalleRunPeriodo) {
            DetalleInformeUniversoDTO det = new DetalleInformeUniversoDTO();

            try {
                det.setRun( Utils.formatearRUT(pen.getPenRun() + "-" + pen.getPenDv()) );
                det.setNombre(pen.getPenNombres()!=null ? pen.getPenNombres():"");
                det.setApellidoPaterno(pen.getPenApePaterno()!=null ? pen.getPenApePaterno():"");
                det.setApellidoMaterno(pen.getPenApeMaterno()!=null ? pen.getPenApeMaterno():"");
                det.setVivo(pen.getPenVivo() != null ? Integer.valueOf(pen.getPenVivo()) == 1 ? "Si" : "No" : "No");
                det.setRsh(pen.getPenConRsh() != null ? pen.getPenConRsh() == 1 ? "Si" : "No" : "No");
                det.setFocalizacion(pen.getPenFocalizacion() != null ? pen.getPenFocalizacion() == 1 ? "Si" : "No" : "No");
                det.setResidencia(pen.getPenResidencia() != null ? pen.getPenResidencia() == 1 ? "Si" : "No" : "No");
                det.setCumpleRequisitos(pen.getPenBeneficiario() != null ? pen.getPenBeneficiario() == 1 ? "Si" : "No" : "No");

                listaDetalleUniverso.add(det);

            }catch (NumberFormatException e){
                LOGGER.error("Error al formatear campo numerico", e);
            }
        }

        return listaDetalleUniverso;
    }

}
