package cl.gob.ips.exencion.view.base;

import cl.alaya.component.utils.DateUtil;
import cl.gob.ips.exencion.service.util.UserUtil;
import cl.gob.ips.exencion.util.MensajesUtil;
import org.apache.log4j.Logger;
import org.apache.poi.util.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.StreamedContent;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.naming.directory.InitialDirContext;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import static javax.faces.context.FacesContext.getCurrentInstance;

@ManagedBean(name = "baseControllerView")
public class BaseControllerView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4016273817413758331L;

	private static final Logger LOGGER = Logger.getLogger(BaseControllerView.class);

	protected String username;
	protected String password;
	protected boolean errorAutentica;
	protected String fechaLimite;

	protected transient InitialDirContext ldapContext;

	@Inject
	protected UserUtil userUtil;

	protected StreamedContent downloadFile = null;

	public void mostrarMensajeError() {
		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("showErrorSistema()");
	}

	public StreamedContent getDownloadFile() {
		return downloadFile;
	}

	public void setDownloadFile(StreamedContent downloadFile) {
		this.downloadFile = downloadFile;
	}

	protected Long obtenerRun(String run) {
		Long runAux = Long.valueOf(0);
		String rut = null;

		try {
			rut = run.replace(".", "").replace("-", "");
			runAux = Long.parseLong(rut.substring(0, rut.length() - 1));
		} catch (Exception e) {
			LOGGER.error("Error al obtener RUN: ", e);
		}

		return runAux;
	}

	public void validarMaxPeriodoActual(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		String strDate = sdf.format(c.getTime());
		this.setFechaLimite(strDate);
	}

	protected Integer obtenerPeriodoAnterior() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.MONTH, -1);
		String strDate = sdf.format(c.getTime());
		strDate = strDate.replace("-", "");
		return Integer.valueOf(strDate);
	}
	
	protected Integer obtenerPeriodoActual() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		Date now = new Date();
		String strDate = sdf.format(now);
		strDate = strDate.replace("-", "");
		return Integer.valueOf(strDate);
	}

	protected String obtenerFechaFormato(Date fecha) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		return sdf.format(fecha);
	}

	protected Integer obtenerPeriodoEntero(Date periodo) {
		String strDate = DateUtil.formatoFecha(periodo, "yyyy/MM");
		strDate = strDate.replace("/", "");
		return Integer.valueOf(strDate);
	}

	protected Integer obtenerPeriodoInput(Date periodo){
        SimpleDateFormat sdf = new SimpleDateFormat("YYYYMM");
        Integer periodoConvertido = Integer.valueOf(sdf.format(periodo));
        return periodoConvertido;
    }

	public void redirectError() {
		try {
			getCurrentInstance().getExternalContext().dispatch("error.jsf");
		} catch (IOException e) {
			LOGGER.error("Error al redireccionar: ", e);
		}
	}

	public void muestraErrorDeAccesoaDatos(Throwable e){
		LOGGER.error("Fallo el acceso a los datos", e);
		MensajesUtil.setMensajeErrorContexto("error.general");
		this.mostrarMensajeError();
	}

	protected void generarExcel(File archivo) throws IOException {
		OutputStream output = null;
		InputStream input = null;
		FacesContext fc = null;
		ExternalContext ec = null;

		try {
			fc = getCurrentInstance();
			ec = fc.getExternalContext();

			ec.responseReset();
			ec.setResponseHeader("Content-Disposition", "attachment; filename=" + archivo.getName());
			ec.setResponseContentType("application/vnd.ms-excel");

			output = ec.getResponseOutputStream();
			input = new FileInputStream(archivo);

			IOUtils.copy(input, output);

			fc.responseComplete();
		} catch (IOException e) {
			LOGGER.error("Error al generar excel: ", e);
		} finally {
			IOUtils.closeQuietly(output);
			IOUtils.closeQuietly(input);
		}
	}

	protected void descargarArchivoValidado(File archivo) throws IOException {
		OutputStream output = null;
		InputStream input = null;
		FacesContext fc = null;
		ExternalContext ec = null;

		try {
			fc = getCurrentInstance();
			ec = fc.getExternalContext();

			ec.responseReset();
			ec.setResponseHeader("Content-Disposition", "attachment; filename=" + archivo.getName());
			ec.setResponseContentType("text/plain");

			output = ec.getResponseOutputStream();
			input = new FileInputStream(archivo);

			IOUtils.copy(input, output);

			fc.responseComplete();
		} catch (IOException e) {
			LOGGER.error("Error al descargar archivo validado", e);
			MensajesUtil.setMensajeErrorContexto("No se encuentra el archivo solicitado");
			this.mostrarMensajeError();
		} finally {
			IOUtils.closeQuietly(output);
			IOUtils.closeQuietly(input);
		}
	}

	public void validarUsuario() {
		try {
			if (null == this.userUtil.getUsername() || "".equals(this.userUtil.getUsername())) {
				int cod = (int) (Math.random() * 999 + 2);
		 		ExternalContext ec = getCurrentInstance().getExternalContext();
		    	ec.redirect(ec.getRequestContextPath() + "/" + "login.jsf?faces-redirect=true&auth=" + cod);
		 		return;
		  }
		} catch (IOException e) {
			LOGGER.error("Error al validar usuario: ", e);
			MensajesUtil.setMensajeErrorContexto("error.general");
			this.mostrarMensajeError();
		}
	}

	public void logout() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			ExternalContext ec = getCurrentInstance().getExternalContext();
			ec.redirect(ec.getRequestContextPath() + "/" + "login.jsf");
		} catch (IOException e) {
			LOGGER.info("Error al cerrar sessi�n:", e);
			MensajesUtil.setMensajeErrorContexto("error.general");
			this.mostrarMensajeError();
		}
	}

	public UserUtil getSessionBean() {
		if (userUtil == null)
			userUtil =  new UserUtil();
		return userUtil;
	}

	public void guardarUserUtil(String username, String password, String token, Date fechaCreacion) {
		getSessionBean().setFechaToken(fechaCreacion);
		getSessionBean().setPassword(password);
		getSessionBean().setToken(token);
		getSessionBean().setUsername(username);
	}

	public Date obtenerFecha(String periodo) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = sdf.parse(periodo);
		return date;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isErrorAutentica() {
		return errorAutentica;
	}

	public void setErrorAutentica(boolean errorAutentica) {
		this.errorAutentica = errorAutentica;
	}

	public String getFechaLimite() {
		return fechaLimite;
	}

	public void setFechaLimite(String fechaLimite) {
		this.fechaLimite = fechaLimite;
	}
}
