package cl.gob.ips.exencion.informes.view;

import cl.alaya.component.utils.DateUtil;
import cl.gob.ips.exencion.dto.EventosProcesosSistemaDTO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.service.BitacoraServices;
import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.util.Reportes;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@ManagedBean(name = "eventosProcesosSistemaView")
@ViewScoped
public class EventosProcesosSistemaView extends BaseControllerView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2263632496088404183L;

	private static final Logger LOGGER = Logger.getLogger(EventosProcesosSistemaView.class);
	
	private Date periodo;

	private Boolean mostrarGrilla;

	private Boolean disableDescarga;

	private List<EventosProcesosSistemaDTO> respuestaGrilla;

	@Inject
	private BitacoraServices bitacoraServices;
	
	@PostConstruct
	private void init() {
		this.mostrarGrilla = Boolean.FALSE;
		this.disableDescarga = Boolean.TRUE;
	}
	
	public void buscar() {
		this.respuestaGrilla = null;
		Integer periodoConsulta = null;
		try {
			if (this.periodo == null) {
				throw new IpsBussinesException("El periodo a buscar es Null!");
			}

        	periodoConsulta = super.obtenerPeriodoEntero(this.periodo);

			if(this.bitacoraServices.obtenerBitacoraPorPeriodo(periodoConsulta)==null){
				this.mostrarGrilla = Boolean.FALSE;
				MensajesUtil.setMensajeErrorContexto("validaciones.informe.eventos.del.sistema.sin.informacion");
				super.mostrarMensajeError();
				LOGGER.info("No hay notificaciones para este periodo " + periodoConsulta );
				return;
			}

			this.respuestaGrilla = this.bitacoraServices.obtenerBitacoraPorPeriodo(periodoConsulta);
			this.mostrarGrilla = Boolean.TRUE;

		} catch (DataAccessException e) {
			super.muestraErrorDeAccesoaDatos(e);
		}
	}

	public void limpiar() {
		this.periodo = null;
		this.mostrarGrilla = Boolean.FALSE;
	}
	
	public void obtenerExcel() throws IOException {
		File archivoExcel = null;
	    try {
			if (this.periodo == null) {
				throw new IpsBussinesException("El periodo no puede ser Null!");
			}
			archivoExcel = Reportes.obtenerEventosProcesosSistema(this.respuestaGrilla,
					DateUtil.formatoFecha(this.periodo, DateUtil.sdfFechaAnioMesGuionesStandar));
			super.generarExcel(archivoExcel);
		} catch (FileNotFoundException e) {
			LOGGER.error("Archivo no encontrado: " + e.getMessage());
	    } catch (IOException e) {
			LOGGER.error("Ha ocurrido un error: " + e.getMessage());
		}
	}

	public Date getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Date periodo) {
		this.periodo = periodo;
	}

	public Boolean getMostrarGrilla() {
		return mostrarGrilla;
	}

	public void setMostrarGrilla(Boolean mostrarGrilla) {
		this.mostrarGrilla = mostrarGrilla;
	}

	public List<EventosProcesosSistemaDTO> getRespuestaGrilla() {
		return respuestaGrilla;
	}

	public void setRespuestaGrilla(
			List<EventosProcesosSistemaDTO> respuestaGrilla) {
		this.respuestaGrilla = respuestaGrilla;
	}

	public Boolean getDisableDescarga() {
		return disableDescarga;
	}

	public void setDisableDescarga(Boolean disableDescarga) {
		this.disableDescarga = disableDescarga;
	}
}
