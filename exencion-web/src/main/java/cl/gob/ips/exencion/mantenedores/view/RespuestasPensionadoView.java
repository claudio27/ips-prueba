package cl.gob.ips.exencion.mantenedores.view;

import cl.gob.ips.exencion.model.TipoRespuesta;
import cl.gob.ips.exencion.service.RespuestasPensionadoServices;
import cl.gob.ips.exencion.util.FacesUtils;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

/**
 * Created by rodrigo.reyesco@gmail.com on 07-10-2016.
 */
@ManagedBean(name = "respuestasPensionadoView")
@ViewScoped
public class RespuestasPensionadoView extends BaseControllerView implements Serializable {

    public static final String FORMULARIO_BUSQUEDA_BTN_BUSCAR = "formulario_busqueda:btn_buscar";
    public static final String TITULO_ERROR = "Error";
    public static final String MSG_ERROR_SELECCION = "Debe seleccionar un tipo de respuesta";
    public static final String FORM_MODAL_EDITAR_BTN_EDITAR = "form_modal_editar:btn_editar";
    public static final String TITULO_ACTUALIZACION_EXITOSA = "Actualización Exitosa";
    public static final String MSG_EXITO_ACTUALIZAR = "La respuesta al pensionado se ha guardado con éxito";
    public static final String MSG_ERROR_ACTUALIZAR = "Se ha producido un error al editar respuesta al pensionado";
    public static final String MSG_ERROR_BUSCAR = "Se ha producido un error al buscar respuesta al pensionado";
    public static final String MSG_ERROR_INIT = "Se ha producido un error al inicializar la pagina";
    public static final String MSG_SELECCIONE = "Seleccione";
    private final transient Logger logger = Logger.getLogger(this.getClass());


    @EJB
    private transient RespuestasPensionadoServices respuestasPensionadoServices;

    private List<TipoRespuesta> tipoRespuestaList;
    private TipoRespuesta tipoRespuestaSearch;
    private TipoRespuesta tipoRespuesta;
    private String strDisabled; 


    /**
     * metodo que se ejecuta al cargar la vista para los datos necesarios en el formulario
     */
    @PostConstruct
    public void init() {
        try {
            this.initFields();
            
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_INIT);
            logger.error(MSG_ERROR_INIT, e);
        }
    }
   
    private void initFields() throws NullPointerException {
        this.tipoRespuestasBuscar(); //carga lista con los tipos de respuesta tecnica
        this.setStrDisabled("0");
    }

    /**
     * realiza busqueda de los tipos de respuestas para cargar combo
     */
    public void tipoRespuestasBuscar() {
        logger.debug("tipoRespuestasBuscar");
        try {
            this.setTipoRespuestaList(respuestasPensionadoServices.buscarTiposRespuesta());
            this.getTipoRespuestaList().add(0, new TipoRespuesta(0, MSG_SELECCIONE));
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_BUSCAR);
            logger.error(MSG_ERROR_BUSCAR, e);
        }
    }

    public void buscarRespuestaSeleccionada(){
        logger.debug("buscarRespuestaSeleccionada");
        try{
            if (this.getTipoRespuestaSearch().getTipRespCod() != 0){
                this.setTipoRespuesta(this.getTipoRespuestaSearch());               
                this.setStrDisabled("1");
            }else{
                FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_SELECCION);   
            }
         
        }catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_BUSCAR);
            logger.error(MSG_ERROR_BUSCAR, e);
        }

           
    }

     /**
     * Metodo encargado de manejar la accion desde el boton de grabar los datos en la ventana modal de edicion
     */
    public void doGuardaTipoRespuesta() {
        try {
            logger.debug("doGuardaTipoRespuesta");
            this.getTipoRespuesta().setTipRespUsuarioModifi("Cangulo");
            Calendar c1 = Calendar.getInstance();
            this.getTipoRespuesta().setTipRespFecModifi(c1.getTime());
            respuestasPensionadoServices.actualizarTipoRespuesta(this.getTipoRespuesta());
            this.limpiarCriteriosBusqueda();
            FacesUtils.showSuccesMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ACTUALIZACION_EXITOSA, MSG_EXITO_ACTUALIZAR);
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_ACTUALIZAR);
            logger.error(MSG_ERROR_ACTUALIZAR, e);
        }
    } 
    
    /**
     * Metodo para limpiar opciones de busqueda del formulario
     */
    public void limpiarCriteriosBusqueda() {
        logger.debug("limpiarCriteriosBusqueda");
        try {

            this.setTipoRespuestaSearch(null);
            this.setTipoRespuesta(null);  
            this.setStrDisabled("0");
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_BUSCAR);
            logger.error(MSG_ERROR_BUSCAR, e);
        }
    }

        /**
     * Metodo para limpiar opciones de busqueda del formulario
     */
    public void limpiarRespuesta() {
        logger.debug("limpiarRespuesta");
        try {

            this.setTipoRespuesta(null);
            this.setStrDisabled("0");
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_BUSCAR);
            logger.error(MSG_ERROR_BUSCAR, e);
        }
    }

    public List<TipoRespuesta> getTipoRespuestaList() {
        return tipoRespuestaList;
    }

    public void setTipoRespuestaList(List<TipoRespuesta> tipoRespuestaList) {
        this.tipoRespuestaList = tipoRespuestaList;
    }

    public TipoRespuesta getTipoRespuestaSearch() {
        return tipoRespuestaSearch;
    }

    public void setTipoRespuestaSearch(TipoRespuesta tipoRespuestaSearch) {
        this.tipoRespuestaSearch = tipoRespuestaSearch;
    }

    public TipoRespuesta getTipoRespuesta() {
        return tipoRespuesta;
    }

    public void setTipoRespuesta(TipoRespuesta tipoRespuesta) {
        this.tipoRespuesta = tipoRespuesta;
    }
    
    public String getStrDisabled() {
        return strDisabled;
    }

    public void setStrDisabled(String strDisabled) {
        this.strDisabled = strDisabled;
    }    
}
