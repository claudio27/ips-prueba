package cl.gob.ips.exencion.mantenedores.view;

import cl.gob.ips.exencion.model.Calendario;
import cl.gob.ips.exencion.service.MantenedorCalendarioService;
import cl.gob.ips.exencion.util.FacesUtils;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by senshi.cristian@gmail.com on 17-10-2016.
 */
@ManagedBean(name = "mantenedorCalendarioView")
@ViewScoped
public class MantenedorCalendarioView extends BaseControllerView implements Serializable {

    public static final String FORMULARIO_BUSQUEDA_BTN_BUSCAR = "formulario_busqueda:btn_buscar";
    public static final String TITULO_ERROR = "Error";
    public static final String MSG_ERROR_ELIMINAR = "Se ha producido un error al eliminar día";
    public static final String TITULO_ELIMINACION_EXITOSA = "Eliminación Exitosa";
    public static final String MSG_EXITO_ELIMINAR = "Los días se han eliminado con éxito";
    public static final String TITULO_CREACION_EXITOSA = "Creación Exitosa";
    public static final String MSG_EXITO_CREAR = "El día inhábil se han creado con éxito";
    public static final String MSG_ERROR_CREAR = "Se ha producido un error al crear día inhábil";
    public static final String MSG_ERROR_BUSCAR_DIA = "Día a crear ya existe";
    public static final String FORM_MODAL_EDITAR_BTN_EDITAR = "form_modal_editar:btn_editar";
    public static final String FORM_MODAL_CREAR_BTN_CREAR = "form_modal_crear:btn_crear";
    public static final String TITULO_ACTUALIZACION_EXITOSA = "Actualización Exitosa";
    public static final String MSG_EXITO_ACTUALIZAR = "El día inhábil se ha actualizado con éxito";
    public static final String MSG_ERROR_ACTUALIZAR = "Se ha producido un error al editar día inhábil";
    public static final String MSG_ERROR_BUSCAR = "Se ha producido un error al buscar día inhábil";
    public static final String MSG_ERROR_INIT = "Se ha producido un error al inicializar la pagina";
    public static final String MSG_SELECCIONE = "Seleccione";
    private final transient Logger logger = Logger.getLogger(this.getClass());

    @Inject
    MantenedorCalendarioService mantenedorCalendarioService;

    private List<Calendario> calendario;
    private List<Integer> yearsList;
    private List<Calendario> calendariosDelete;
    private Calendario calendarioEdit;
    private Calendario calendarioCreate;
    private Calendario calendarioSearchCriteria;
    private Integer yearSearch;
    private String fechaEdit;
    private String descripcionEdit;


    private Map<Calendario, Boolean> checked;

    /**
     * metodo que se ejecuta al cargar la vista para los datos necesarios en el formulario
     */
    @PostConstruct
    public void init() {
        try {
            this.initFields();
            
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_INIT);
            logger.error(MSG_ERROR_INIT, e);
        }
    }
   
    private void initFields() throws NullPointerException {
        checked = new HashMap<>();
        calendarioEdit = new Calendario();
        calendarioCreate = new Calendario();
        calendariosDelete = new ArrayList<>();

        this.listaYears(); //carga lista con los annos            
    }
      
    
    /**
     * carga lista con annos
     */
    public void listaYears() {
        Calendar c1 = Calendar.getInstance();
        List<Integer> listaYears = new ArrayList<Integer>();
        Integer yearActual = c1.get(Calendar.YEAR);
        this.setYearSearch(yearActual);
        if (yearActual >= 1990){
            for(Integer i = 1990; i <= yearActual + 1; i++){
                listaYears.add(i);
            }
        }
        this.setYearsList(listaYears);
    } 
    
    /**
     * realiza busqueda de dias feriados del calendario por los criterios ingresados en el formulario
     *
     * 
     */
    public void buscarCalendario() {
        try {
            logger.debug("Buscando dias feriados");
            
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            
            String dateIniString = "01/01/" + this.getYearSearch();
            String dateFinString = "31/12/" + this.getYearSearch();
            logger.debug("string1:" + dateIniString );
            //logger.debug("string2:" + dateFinString );
            
            Date dateIni = null;
            Date dateFin = null;
            
            try {

                dateIni = formatter.parse(dateIniString);
                dateFin = formatter.parse(dateFinString);
                
                //logger.debug("date1:" + dateIni );
                //logger.debug("date2:" + dateFin );                
                
            } catch (ParseException e) {
                FacesMessage msg = new FacesMessage("Error", "Se ha producido un error al buscar fechas");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext.getCurrentInstance().addMessage("formulario_busqueda:btn_buscar", msg);
                logger.error(e);
            }

            logger.debug("date1:" + dateIni );
            //logger.debug("date2:" + dateFin );             
            this.setCalendario(mantenedorCalendarioService.buscarDiasFeriadosPorCriterio(dateIni, dateFin));
            
        } catch (Exception e) {
            FacesMessage msg = new FacesMessage("Error", "Se ha producido un error al buscar fechas");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage("formulario_busqueda:btn_buscar", msg);
            logger.error(e);
        }
        
        
    }

    /**
     * Metodo para limpiar opciones de busqueda del formulario
     */
    public void limpiarCriteriosBusqueda() {
        try {
            //Limpia criterios de busqueda
            Calendar c1 = Calendar.getInstance();
            Integer yearActual = c1.get(Calendar.YEAR);
            this.setYearSearch(yearActual);
            //limpia grilla
            this.setCalendario(null);
            //limpiar otros
            this.setCalendariosDelete(null);
            this.setCalendarioEdit(null);
            this.setCalendarioCreate(null);
            this.setFechaEdit(null);
            this.setDescripcionEdit(null);
            
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_BUSCAR);
            logger.error(MSG_ERROR_BUSCAR, e);
        }
    }
    
    
        /**
     * @return
     */
    public String crearCalendario() {
        logger.debug("crearCalendario()");
        this.setCalendarioCreate(new Calendario());
        this.setFechaEdit(null);
        this.setDescripcionEdit(null);
        return "crearCalendario";
    }
    
    /**
     * 
     */
    public void doCrearCalendario() {
        logger.debug("Crea dia feriado - doCrearCalendario() ");
        try {
            //this.buildCalendarioCreate();
            int year =  Integer.parseInt(this.getFechaEdit().substring(6, 10));// a�o
            int month = Integer.parseInt(this.getFechaEdit().substring(3, 5));// mes [1,...,12]
            int dayOfMonth = Integer.parseInt(this.getFechaEdit().substring(0, 2));// d�a [1,...,31]

            if (year < 1900) {
                throw new IllegalArgumentException("A�o inv�lido.");
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setLenient(false);
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month - 1); // [0,...,11]
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            Date date = calendar.getTime();
            
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");            
            //Setea nueva fecha 
            logger.debug("Setea nueva fecha " + sdf.format(date));          
            
            this.getCalendarioCreate().setCalFecFeriado(date);
            this.getCalendarioCreate().setCalDescripcion(this.getDescripcionEdit());
            
            if (mantenedorCalendarioService.buscarDiasFeriadosPorDia(this.getCalendarioCreate().getCalFecFeriado()).isEmpty()){
                //crea fecha
                logger.debug("crear fecha"); 
                mantenedorCalendarioService.creaCalendario(this.getCalendarioCreate());
                
                //limpiar valores
                this.setFechaEdit(null);
                this.setDescripcionEdit(null);                
                this.setCalendarioCreate( new Calendario());
                
                //Busca calendario
                this.buscarCalendario(); 
                
                //this.showInsertSuccesMessage();       
                FacesUtils.showSuccesMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_CREACION_EXITOSA, MSG_EXITO_CREAR);
                FacesUtils.executeJavascriptOnView("$('#modalCrear').modal('toggle'); scrollToTop();");                
            }else{
                FacesUtils.showErrorMessage(FORM_MODAL_CREAR_BTN_CREAR, TITULO_ERROR, MSG_ERROR_BUSCAR_DIA);  
            }
            

        } catch (Exception e) {
            logger.error(MSG_ERROR_CREAR, e);
            FacesUtils.showErrorMessage(FORM_MODAL_CREAR_BTN_CREAR, TITULO_ERROR, MSG_ERROR_CREAR);           
        }
    }
    
    /*
    private void showInsertSuccesMessage() {
        FacesUtils.showSuccesMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_CREACION_EXITOSA, MSG_EXITO_CREAR);
        FacesUtils.executeJavascriptOnView("$('#modalCrear').modal('toggle'); scrollToTop();");
    }
        
    private void buildCalendarioCreate() throws FormatException {
        logger.debug("Construye nuevo dia feriado - buildCalendarioCreate() ");
        logger.debug("fecha feriado:" + this.getFechaEdit());
        try{
            //format a fecha crear
            //SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");            
            //String dateIniString = this.getFechaEdit().substring(0, 2) +  "/" + this.getFechaEdit().substring(3, 5) + "/" + this.getFechaEdit().substring(6, 10);
            //logger.debug("fecha Strig:" + dateIniString );
            //this.getCalendarioCreate().setCalFecFeriado(formatter.parse(dateIniString));
            
            ///
            int year =  Integer.parseInt(this.getFechaEdit().substring(6, 10));// a�o
            int month = Integer.parseInt(this.getFechaEdit().substring(3, 5));// mes [1,...,12]
            int dayOfMonth = Integer.parseInt(this.getFechaEdit().substring(0, 2));// d�a [1,...,31]

            if (year < 1900) {
                throw new IllegalArgumentException("A�o inv�lido.");
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setLenient(false);
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month - 1); // [0,...,11]
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            Date date = calendar.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            System.out.println(sdf.format(date)); // 01/01/2016            
            
            
            ///
            this.getCalendarioCreate().setCalFecFeriado(date);
            
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORM_MODAL_CREAR_BTN_CREAR, TITULO_ERROR, MSG_ERROR_CREAR);
            logger.error(MSG_ERROR_CREAR, e);
        }

        //Descripcion
        this.getCalendarioCreate().setCalDescripcion(this.getDescripcionEdit());
            
        this.getCalendarioCreate().setCalUsuCreacion("cangulo");    
        Calendar c1 = Calendar.getInstance();
        this.getCalendarioCreate().setCalFecCreacion(c1.getTime());
        
    }
*/
    /**
     * Metodo que se encarga de la carga de los datos del dia seleccionado para editar en la ventana Modal.
     * @param calendario
     * @return
     */
    public String editarCalendario(Calendario calendario) {
        logger.debug("Entro a editarCalendario");
        logger.debug("Fecha a editar:" + calendario.getCalFecFeriado());
        this.setCalendarioEdit(calendario);
        
        DateFormat formatoFechaDia = new SimpleDateFormat("dd/");
        DateFormat formatoFechaMes = new SimpleDateFormat("MM/");
        DateFormat formatoFechaYear = new SimpleDateFormat("yyyy");
        String fehaFF = formatoFechaDia.format(calendario.getCalFecFeriado())+formatoFechaMes.format(calendario.getCalFecFeriado())+formatoFechaYear.format(calendario.getCalFecFeriado());
        this.setFechaEdit(fehaFF);
        
        this.setDescripcionEdit(calendario.getCalDescripcion());
        return "editarCalendario";
    }    
    /**
     * Metodo encargado de manejar la accion desde el boton de grabar los datos en la ventana modal de edicion
     */
    public void doEditarCalendario() {
        logger.debug("doEditarCalendario");          
        try {
            logger.debug("fecha modificacion:" + this.getFechaEdit());
            logger.debug("desc modificacion:" + this.getDescripcionEdit());
            logger.debug("id modificacion:" + this.getCalendarioEdit().getCalId());
            
            //format a fecha modificada
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");            
            String dateIniString = this.getFechaEdit().substring(0, 2) +  "/" + this.getFechaEdit().substring(3, 5) + "/" + this.getFechaEdit().substring(6, 10);
            logger.debug("string1:" + dateIniString );
            Date fechaMody = formatter.parse(dateIniString);
            logger.debug("fehca:" + fechaMody );
            
            ///
            int year =  Integer.parseInt(this.getFechaEdit().substring(6, 10));// a�o
            int month = Integer.parseInt(this.getFechaEdit().substring(3, 5));// mes [1,...,12]
            int dayOfMonth = Integer.parseInt(this.getFechaEdit().substring(0, 2));// d�a [1,...,31]

            if (year < 1900) {
                throw new IllegalArgumentException("A�o inv�lido.");
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setLenient(false);
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month - 1); // [0,...,11]
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            Date date = calendar.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            System.out.println(sdf.format(date)); // 01/01/2016            
            
            
            ///
            this.getCalendarioEdit().setCalFecFeriado(date);
            
            //Descripcion
            this.getCalendarioEdit().setCalDescripcion(this.getDescripcionEdit());
            
            //seteo usuario y fecha modificacion
            this.getCalendarioEdit().setCalUsuCreacion("cangulo2");    
            Calendar c1 = Calendar.getInstance();
            this.getCalendarioEdit().setCalFecCreacion(c1.getTime()); 
            
            //Actualiza fecha
            mantenedorCalendarioService.actualizarCalendario(this.getCalendarioEdit());           
            
            FacesUtils.showSuccesMessage(FORM_MODAL_EDITAR_BTN_EDITAR, TITULO_ACTUALIZACION_EXITOSA, MSG_EXITO_ACTUALIZAR);
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORM_MODAL_EDITAR_BTN_EDITAR, TITULO_ERROR, MSG_ERROR_ACTUALIZAR);
            logger.error(MSG_ERROR_ACTUALIZAR, e);
        }
    }    
    
    /**
     * @param calendario
     */
    public void cambiaEstadoCalendario(Calendario calendario) {
        logger.debug("entro a cambiaEstadoCalendario");
        if (this.getChecked().get(calendario)) {
            calendario.setCalUsuEliminacion("eliminado");    
            Calendar c1 = Calendar.getInstance();
            calendario.setCalFecEliminacion(c1.getTime());
            this.getCalendariosDelete().add(calendario);
        } else {            
            this.getCalendariosDelete().remove(calendario);
        }
    }
    
    public void doEliminaCalendario() {
        try {
            logger.debug("Eliminando dia feriado");
            mantenedorCalendarioService.actualizaCalendarios(this.getCalendariosDelete());
            this.getCalendariosDelete().clear();
            this.getCalendario().clear();
            this.buscarCalendario();
            FacesUtils.showSuccesMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ELIMINACION_EXITOSA, MSG_EXITO_ELIMINAR);
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_ELIMINAR);
            logger.error(MSG_ERROR_ELIMINAR, e);
        }
    }


    public Calendario getCalendarioSearchCriteria() {
        return calendarioSearchCriteria;
    }

    public void setCalendarioSearchCriteria(Calendario calendarioSearchCriteria) {
        this.calendarioSearchCriteria = calendarioSearchCriteria;
    }
    

    public List<Calendario> getCalendario() {
        return calendario;
    }

    public void setCalendario(List<Calendario> calendario) {
        this.calendario = calendario;
    }

    public List<Integer> getYearsList() {
        return yearsList;
    }

    public void setYearsList(List<Integer> yearsList) {
        this.yearsList = yearsList;
    }
    public Integer getYearSearch() {
        return yearSearch;
    }

    public void setYearSearch(Integer yearSearch) {
        this.yearSearch = yearSearch;
    }  
    
    public Calendario getCalendarioEdit() {
        return calendarioEdit;
    }

    public void setCalendarioEdit(Calendario calendarioEdit) {
        this.calendarioEdit = calendarioEdit;
    }

    public Map<Calendario, Boolean> getChecked() {
        return checked;
    }

    public void setChecked(Map<Calendario, Boolean> checked) {
        this.checked = checked;
    }
    
    public List<Calendario> getCalendariosDelete() {
        return calendariosDelete;
    }

    public void setCalendariosDelete(List<Calendario> calendariosDelete) {
        this.calendariosDelete = calendariosDelete;
    }
    
    public Calendario getCalendarioCreate() {
        return calendarioCreate;
    }

    public void setCalendarioCreate(Calendario calendarioCreate) {
        this.calendarioCreate = calendarioCreate;
    }    

    public String getFechaEdit() {
        return fechaEdit;
    }

    public void setFechaEdit(String fechaEdit) {
        this.fechaEdit = fechaEdit;
    }

    public String getDescripcionEdit() {
        return descripcionEdit;
    }

    public void setDescripcionEdit(String descripcionEdit) {
        this.descripcionEdit = descripcionEdit;
    }
    
}
