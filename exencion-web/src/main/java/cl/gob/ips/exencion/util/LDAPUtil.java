package cl.gob.ips.exencion.util;

import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;
import java.util.Hashtable;
import java.util.ResourceBundle;

/**
 * Created by alaya on 22-08-2017.
 */
public class LDAPUtil {

    private static final Logger LOG = Logger.getLogger(LDAPUtil.class);

    private static transient ResourceBundle properties;

    public static void getLDAPProperties() {
        properties = ResourceBundle.getBundle("ldap");

        if (properties == null) {
            throw new RuntimeException("No existe archivo de propiedades en el classpath");
        }
    }

    public static InitialDirContext inciarSesionLDAPAD(String usuario, String pass) throws Exception{
        InitialDirContext ldapContext = null;
        Hashtable<String, String> ldapEnv = null;

        try {
            getLDAPProperties();

            ldapEnv = new Hashtable<String, String>();

            ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY,
                    properties.getString("ldap.initial.context.factory"));
            ldapEnv.put(Context.PROVIDER_URL, properties.getString("ldap.ip.nodo")
                    + ":" + properties.getString("ldap.puerto.nodo"));
            ldapEnv.put(Context.SECURITY_AUTHENTICATION, properties.getString("ldap.security.authentication"));
            ldapEnv.put(Context.SECURITY_PRINCIPAL, usuario + properties.getString("ldap.domain.ips"));
            ldapEnv.put(Context.SECURITY_CREDENTIALS, pass);

            ldapContext = new InitialDirContext(ldapEnv);
            LOG.debug("Conexión exitosa para el usuario: " + usuario);
        } catch (NamingException e) {
            LOG.error("Error al inciar sesión con LDAP", e);
        }

        return ldapContext;
    }
}
