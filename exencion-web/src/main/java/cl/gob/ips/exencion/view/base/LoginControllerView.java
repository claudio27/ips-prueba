package cl.gob.ips.exencion.view.base;

import cl.gob.ips.exencion.enums.TipoGrupo;
import cl.gob.ips.exencion.service.UsuariosServices;
import cl.gob.ips.exencion.util.LDAPUtil;
import cl.gob.ips.exencion.util.MensajesUtil;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static javax.faces.context.FacesContext.getCurrentInstance;

/**
 * Created by lpino on 26-07-17.
 */
@ManagedBean(name = "loginControllerView")
@SessionScoped
public class LoginControllerView extends BaseControllerView implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4016273866413758331L;

    private static final Logger LOGGER = Logger.getLogger(LoginControllerView.class);

    private boolean perfilGestion;
    private boolean perfilNegocio;
    private boolean perfilProd;
    private boolean perfilSistema;
    private boolean perfilAdministrador;

    private int identificador = 0;
    private int identificadorSession = 0;
    private int cont = 0;


    @Inject
    private UsuariosServices usuariosServices;

    @PostConstruct
    public void init() {
        //Validar Sesion
          if(identificador != identificadorSession) {
              identificadorSession = identificador;
              cont=0;
          }

        if (identificador > 1 && cont==0) {

            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(
                            FacesMessage.SEVERITY_WARN, " ", "¡Alerta! Usuario no autenticado para realizar esta acci\u00f3n."));
            cont=1;
        }
    }

    public void login() {
        List<BigDecimal> perfiles = null;

        try {
            super.ldapContext = LDAPUtil.inciarSesionLDAPAD(this.getUsername(), this.getPassword());

            if (super.ldapContext == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, " ",
                        "¡Error! Su nombre de usuario o contrase\u00F1a no es v\u00E1lido."));
                return;
            }

            super.guardarUserUtil(this.username, this.password, String.valueOf(Math.random() * 9999999 + 1), new Date());

            if (null != super.userUtil.getUsername() || !"".equals(super.userUtil.getPassword())) {
                perfiles = this.usuariosServices.obtenerGrupos(this.username);

                if (null != perfiles) {
                    if (perfiles.size() > 0){
                        for (BigDecimal perfil : perfiles) {
                            if (TipoGrupo.ADMINISTRACION.getCodigo() == perfil.intValue()) {
                                this.perfilAdministrador = true;
                            } else if (TipoGrupo.GESTION_BENEFICIOS.getCodigo() == perfil.intValue()) {
                                this.perfilGestion = true;
                            } else if (TipoGrupo.PRODUCCION.getCodigo() == perfil.intValue()) {
                                this.perfilProd = true;
                            } else if (TipoGrupo.RESPONSABLE_NEGOCIO.getCodigo() == perfil.intValue()) {
                                this.perfilNegocio = true;
                            }
                        }
                    }
                } else {
                    FacesContext.getCurrentInstance().
                            addMessage(null, new FacesMessage(
                                    FacesMessage.SEVERITY_WARN, " ", "¡Alerta! Usuario no tiene privilegios para acceder al sistema."));
                    return;
                }
            } else {
                FacesContext.getCurrentInstance().
                        addMessage(null, new FacesMessage(
                                FacesMessage.SEVERITY_WARN, " ", "¡Error! Su nombre de usuario o contrase\u00F1a no es v\u00E1lido."));
            }

            FacesContext.getCurrentInstance().getExternalContext().redirect("default.jsf");
        } catch (Exception e) {
            LOGGER.error("Error al comprobar usuario", e);
            FacesContext.getCurrentInstance().
                    addMessage(null, new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, " ", "¡Alerta! No se puede acceder al sistema."));
        }
    }

    public boolean isPerfilGestion() {
        return perfilGestion;
    }

    public boolean isPerfilNegocio() {
        return perfilNegocio;
    }

    public boolean isPerfilProd() {
        return perfilProd;
    }

    public boolean isPerfilSistema() {
        return perfilSistema;
    }

    public boolean isPerfilAdministrador() {
        return perfilAdministrador;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
}
