package cl.gob.ips.exencion.procesos.view;

import cl.gob.ips.exencion.model.Bitacora;
import cl.gob.ips.exencion.model.EtapasPeriodo;
import cl.gob.ips.exencion.model.Periodo;
import cl.gob.ips.exencion.model.TipoEventos;
import cl.gob.ips.exencion.service.BitacoraServices;
import cl.gob.ips.exencion.service.PeriodoServices;
import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by practicante
 */
@ManagedBean(name = "cierrePeriodoView")
@ViewScoped
public class CierrePeriodoView extends BaseControllerView implements Serializable {

    private static final long serialVersionUID = 3428153268516734489L;
    private static final Logger LOGGER = Logger.getLogger(CierrePeriodoView.class);

    @Inject
    private transient PeriodoServices periodoServices;

    @Inject
    private transient BitacoraServices bitacoraServices;

    private Periodo periodo;
    private Periodo periodoEditar;
    private Bitacora bitacoraCrear;
    private Date periodoCerrar;

    @PostConstruct
    public void init() {
        try {
            this.initFields();
        } catch (Exception e) {
            LOGGER.error("Error, "+e);
        }
    }


    private void initFields() throws NullPointerException {
        periodo = new Periodo();
        periodoEditar = new Periodo();
        bitacoraCrear = new Bitacora();
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public Periodo getPeriodoEditar() {
        return periodoEditar;
    }

    public void setPeriodoEditar(Periodo periodoEditar) {
        this.periodoEditar = periodoEditar;
    }

    public Date getPeriodoCerrar() {
        return periodoCerrar;
    }

    public void setPeriodoCerrar(Date periodoCerrar) {
        this.periodoCerrar = periodoCerrar;
    }

    public Bitacora getBitacoraCrear() {
        return bitacoraCrear;
    }

    public void setBitacoraCrear(Bitacora bitacoraCrear) {
        this.bitacoraCrear = bitacoraCrear;
    }

    public void cerrarPeriodo() {
        try{
            if (this.periodoCerrar!=null){

                Integer per = this.obtenerPeriodoEntero(this.periodoCerrar);

                switch(this.periodoServices.validarPeriodoCerrar(per)){
                    case 0:
                        //validar periodo existe en sistema
                        MensajesUtil.setMensajeErrorContexto("procesos.cerrar.periodo.validar.periodo");
                        super.mostrarMensajeError();
                        LOGGER.error("El periodo ya no cuenta con información en la base de datos");
                        break;
                    case 1:
                        //validar periodo apto para posible cierre
                        MensajesUtil.setMensajeErrorContexto("procesos.cerrar.periodo.validar.candidato");
                        super.mostrarMensajeError();
                        LOGGER.error("Faltan procesos para cerrar este periodo");
                        break;
                    case 2:
                        //validar periodo ya cerrado
                        MensajesUtil.setMensajeErrorContexto("procesos.cerrar.periodo.cerrado");
                        super.mostrarMensajeError();
                        LOGGER.error("Periodo ya fué cerrado" );
                        break;
                    case 3:
                        //cerrar periodo
                        String bitResponsable = super.getSessionBean().getUsername();
                        this.setPeriodoEditar(this.periodoServices.obtenerPeriodoCerrar(per));
                        EtapasPeriodo estapaCierre = new EtapasPeriodo(60, "Notificando Cierre");
                        this.periodoEditar.setPerUsuCierre(bitResponsable);
                        this.periodoEditar.setPerFecCierre(new Date());
                        this.periodoEditar.setTbPrEtapasPeriodo(estapaCierre);
                        this.bitacoraCrear.setTbPeriodo11(this.periodoEditar);
                        this.bitacoraCrear.setBitDescripcionEvento("Cierre en demanda del periodo");
                        this.bitacoraCrear.setBitFechaEvento(new Date());
                        this.bitacoraCrear.setBitResponsable(bitResponsable);
                        this.bitacoraCrear.setTbPrTipoEventos(new TipoEventos(2, "Proceso de periodo"));
                        this.periodoServices.modificarPeriodo(this.periodoEditar);
                        this.bitacoraServices.creaBitacora(this.bitacoraCrear);
                        RequestContext requestContext = RequestContext.getCurrentInstance();
                        requestContext.execute("showCerrarConfirm()");
                        break;
                }


            }else{
                MensajesUtil.setMensajeErrorContexto("error.general");
                super.mostrarMensajeError();
                LOGGER.error("El periodo es null");
            }

        }
        catch(Exception e){
            MensajesUtil.setMensajeErrorContexto("error.general");
            super.mostrarMensajeError();
            LOGGER.error("Error al cerrar periodo, razón: ", e);
        }

    }

    public void facesRedirect() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect("cierre-periodo?seccion=3?faces-redirect=true");
    }


}