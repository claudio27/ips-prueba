package cl.gob.ips.exencion.procesos.view;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Bitacora;
import cl.gob.ips.exencion.model.EtapasPeriodo;
import cl.gob.ips.exencion.model.Periodo;
import cl.gob.ips.exencion.service.NominasInstitucionesServices;
import cl.gob.ips.exencion.service.PeriodoServices;
import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by lpino on 07-07-17.
 */

@ViewScoped
@ManagedBean(name = "nominasInstitucionesView")
public class NominasInstitucionesView extends BaseControllerView implements
		Serializable {

	private static final Logger LOGGER = Logger
			.getLogger(NominasInstitucionesView.class);
	private static final long serialVersionUID = -6672900366316925777L;

	private static final int ESTADO_GENERAR_NOMINAS = 5;

	private static final int EMITIR_NOMINA_VALIDACION = 1;

	private Date periodo;

	private Integer estadoPeriodo;

	private boolean disabledGenerar;

	private Periodo periodoEditar;

	@Inject
	private NominasInstitucionesServices nominasServices;

	@Inject
	private PeriodoServices periodoServices;

	@PostConstruct
	public void init() {
		try {
			this.initFields();
		} catch (Exception e) {
			LOGGER.error("Error, " + e);
		}
	}

	private void initFields() throws NullPointerException {
		this.periodoEditar = new Periodo();
	}

	public void generar() throws DataAccessException{
		try {

			LOGGER.debug("periodo  " + this.periodo);

			if (this.periodo != null) {

				Integer per = this.obtenerPeriodoEntero(this.periodo);

				switch(this.periodoServices.validarPeriodoGenerar(per)){

					case 0:
						//validar periodo existe en sistema
						MensajesUtil.setMensajeErrorContexto("El periodo ya no cuenta con información en la base de datos");
						super.mostrarMensajeError();
						LOGGER.error("El periodo ya no cuenta con información en la base de datos");
						break;

					case 1:
						//validar periodo apto para generar nóminas
						MensajesUtil.setMensajeErrorContexto("Faltan procesos para generar las nóminas de este periodo");
						super.mostrarMensajeError();
						LOGGER.error("Faltan procesos para generar las nóminas de este periodo");
						break;

					case 2:
						//validar periodo ya generó nóminas
						MensajesUtil.setMensajeErrorContexto("Periodo ya generó nóminas");
						super.mostrarMensajeError();
						LOGGER.error("Periodo ya generó nóminas");
						break;

					case 3:
						//generar nóminas periodo
						EtapasPeriodo etapaGenerarNomina = new EtapasPeriodo(51, "Generando  Nóminas");
						this.setPeriodoEditar(this.periodoServices.obtenerPeriodoGenerar(per));
						this.periodoEditar.setTbPrEtapasPeriodo(etapaGenerarNomina);
						this.periodoServices.modificarPeriodo(this.periodoEditar);
						RequestContext requestContext = RequestContext.getCurrentInstance();
						requestContext.execute("showGenerarConfirm()");
						break;
				}

			} else {
				MensajesUtil.setMensajeErrorContexto("error.general");
				super.mostrarMensajeError();
				LOGGER.error("El periodo es null");
			}


		} catch (DataAccessException ex) {
			LOGGER.info(ex.getMessage());
		}
	}

	public Date getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Date periodo) {
		this.periodo = periodo;
	}

	public Integer getEstadoPeriodo() {
		return estadoPeriodo;
	}

	public void setEstadoPeriodo(Integer estadoPeriodo) {
		this.estadoPeriodo = estadoPeriodo;
	}

	public Periodo getPeriodoEditar() {
		return periodoEditar;
	}

	public void setPeriodoEditar(Periodo periodoEditar) {
		this.periodoEditar = periodoEditar;
	}
}
