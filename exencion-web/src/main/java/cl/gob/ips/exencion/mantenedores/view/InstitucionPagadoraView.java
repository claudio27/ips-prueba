package cl.gob.ips.exencion.mantenedores.view;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.FormatException;
import cl.gob.ips.exencion.model.EstadoInstitucion;
import cl.gob.ips.exencion.model.Instituciones;
import cl.gob.ips.exencion.model.Rut;
import cl.gob.ips.exencion.model.TipoInstitucion;
import cl.gob.ips.exencion.service.DatosParametricosServices;
import cl.gob.ips.exencion.service.InstitucionPagadoraServices;
import cl.gob.ips.exencion.util.FacesUtils;
import cl.gob.ips.exencion.util.Utils;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rodrigo.reyesco@gmail.com on 07-10-2016.
 */
@ManagedBean(name = "institucionPagadoraView")
@ViewScoped
public class InstitucionPagadoraView extends BaseControllerView implements Serializable {

    public static final String FORMULARIO_BUSQUEDA_BTN_BUSCAR = "formulario_busqueda:btn_buscar";
    public static final String TITULO_ERROR = "Error";
    public static final String MSG_ERROR_ELIMINAR = "Se ha producido un error al eliminar la instituciòn pagadora";
    public static final String TITULO_ELIMINACION_EXITOSA = "Eliminación Exitosa";
    public static final String MSG_EXITO_ELIMINAR = "Las instituciónes pagadoras se han eliminado con éxito";
    public static final String TITULO_CREACION_EXITOSA = "Creación Exitosa";
    public static final String MSG_EXITO_CREAR = "Las institución pagadoras se han creado con éxito";
    public static final String MSG_ERROR_CREAR = "Se ha producido un error al crear la institución pagadora";
    public static final String FORM_MODAL_EDITAR_BTN_EDITAR = "form_modal_editar:btn_editar";
    public static final String FORM_MODAL_CREAR_BTN_CREAR = "form_modal_crear:btn_crear";
    public static final String TITULO_ACTUALIZACION_EXITOSA = "Actualización Exitosa";
    public static final String MSG_EXITO_ACTUALIZAR = "La institución pagadora se ha actualizado con éxito";
    public static final String MSG_ERROR_ACTUALIZAR = "Se ha producido un error al editar la instituciòn pagadora";
    public static final String MSG_ERROR_BUSCAR = "Se ha producido un error al buscar la institución pagadora";
    public static final String MSG_ERROR_INIT = "Se ha producido un error al inicializar la pagina";
    public static final String MSG_SELECCIONE = "Seleccione";
    private static final String MSG_ERROR_RUT_EXISTE = "El Rut que intenta crear ya existe";
    private final transient Logger logger = Logger.getLogger(this.getClass());

    @Inject
    private transient DatosParametricosServices datosParametricosServices;

    @Inject
    private transient InstitucionPagadoraServices institucionPagadoraServices;

    private List<TipoInstitucion> tipoInstitucionList;
    private List<Instituciones> instituciones;
    private List<Instituciones> institucionesDelete;
    private Instituciones institucionEdit;
    private Instituciones institucionCreate;
    private Instituciones institucionSearchCriteria;

    private String rutInstitucion;
    private String nombreInstitucion;
    private TipoInstitucion tipoInstitucion;

    private String rutInstitucionSearch;
    private String nombreInstitucionSearch;

    private TipoInstitucion tipoInstitucionSearch;
    private Instituciones institucionSearch;

    private Map<Instituciones, Boolean> checked;

    /**
     * metodo que se ejecuta al cargar la vista para los datos necesarios en el formulario
     */
    @PostConstruct
    public void init() {
        try {
            this.initFields();
            this.buildTipoInstitucionData();
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_INIT);
            logger.error(MSG_ERROR_INIT, e);
        }
    }

    private void initFields() throws NullPointerException {
        institucionEdit = new Instituciones(new TipoInstitucion());
        institucionCreate = new Instituciones(new TipoInstitucion());
        checked = new HashMap<>();
        institucionesDelete = new ArrayList<>();
        rutInstitucionSearch = null;
        nombreInstitucionSearch = null;
        tipoInstitucionSearch = null;
    }

    private void buildTipoInstitucionData() throws DataAccessException {
        this.setTipoInstitucionList(datosParametricosServices.getTiposInstitucion());
        this.getTipoInstitucionList().add(0, new TipoInstitucion(0, MSG_SELECCIONE));
    }

    /**
     * valida si una Institucion existe en la base de datos antes de crear
     */
    public void validaInstitucionExiste(){
        try {
            if( institucionPagadoraServices.findInstitucionByRut(this.getInstitucionCreate()) == null ){
                FacesMessage msg = new FacesMessage(TITULO_ERROR, MSG_ERROR_RUT_EXISTE);
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext.getCurrentInstance().addMessage(FORM_MODAL_CREAR_BTN_CREAR, msg);
                throw new ValidatorException(msg);
            }
        } catch (DataAccessException e) {
            FacesUtils.showErrorMessage(FORM_MODAL_CREAR_BTN_CREAR, TITULO_ERROR, MSG_ERROR_RUT_EXISTE);
            logger.error(MSG_ERROR_RUT_EXISTE, e);
        }
    }

    /**
     * realiza busqueda de Instituciones por los criterios ingresados en el formulario
     */
    public void buscarInstitucionPagadora() {
        try {
            this.buildSearchOptions();
            this.setInstituciones(institucionPagadoraServices.buscarInstitucionesPorCriterio(this.getInstitucionSearch()));
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_BUSCAR);
            logger.error(MSG_ERROR_BUSCAR, e);
        }
    }

    private void buildAndResetSearchOptions() throws FormatException {
        this.setRutInstitucionSearch(StringUtils.EMPTY);
        Rut rut = Utils.separateRut(this.getRutInstitucionSearch());
        this.setInstitucionSearch(new Instituciones(rut.getRut(), rut.getDv(), StringUtils.EMPTY, new TipoInstitucion(0, MSG_SELECCIONE)));
    }

    private void buildSearchOptions() throws FormatException {
        Rut rut = Utils.separateRut(this.getRutInstitucionSearch());
        this.setInstitucionSearch(new Instituciones(rut.getRut(), rut.getDv(), this.getNombreInstitucionSearch(), this.getTipoInstitucionSearch()));
    }

    /**
     * Metodo para limpiar opciones de busqueda del formulario
     */
    public void limpiarCriteriosBusqueda() {
        try {
            this.setRutInstitucionSearch(StringUtils.EMPTY);
            this.setNombreInstitucionSearch(StringUtils.EMPTY);
            this.setTipoInstitucionSearch(null);
            this.setInstitucionSearch(null);
            this.setInstituciones(null);
            this.setInstitucionesDelete(null);
            this.setInstitucionCreate(null);
            this.setInstitucionEdit(null);
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_BUSCAR);
            logger.error(MSG_ERROR_BUSCAR, e);
        }
    }

    /**
     * Metodo que se encarga de la carga de los datos de la institucion seleccionada para editar en la ventana Modal.
     * @param institucion
     * @return
     */
    public String editarInstitucionPagadora(Instituciones institucion) {
        this.setInstitucionEdit(institucion);
        return "editarInstitucionPagadora";
    }


    /**
     * Metodo encargado de manejar la accion desde el boton de grabar los datos en la ventana modal de edicion
     * @return
     */
    public void doEditarInstitucionPagadora() {
        try {
            institucionPagadoraServices.actualizarIntitucion(this.getInstitucionEdit());
            FacesUtils.showSuccesMessage(FORM_MODAL_EDITAR_BTN_EDITAR, TITULO_ACTUALIZACION_EXITOSA, MSG_EXITO_ACTUALIZAR);
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORM_MODAL_EDITAR_BTN_EDITAR, TITULO_ERROR, MSG_ERROR_ACTUALIZAR);
            logger.error(MSG_ERROR_ACTUALIZAR, e);
        }
    }

    /**
     * Metodo que ejecuta la accion de eliminar un set de instituciones pagadoras
     */
    public void doEliminaInstitucionesPagadoras() {
        try {
            institucionPagadoraServices.actualizaInstituciones(this.getInstitucionesDelete());
            this.getInstitucionesDelete().clear();
            this.getInstituciones().clear();
            this.reloadTableData();
            FacesUtils.showSuccesMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ELIMINACION_EXITOSA, MSG_EXITO_ELIMINAR);
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_ELIMINAR);
            logger.error(MSG_ERROR_ELIMINAR, e);
        }
    }

    /**
     * @return
     */
    public String crearInstitucionPagadora() {
        this.setInstitucionCreate(new Instituciones(new TipoInstitucion()));
        return "crearInstitucionPagadora";
    }

    /**
     * Metodo que ejecuta la creacion de una nueva institucion
     */
    public void doCrearInstitucionPagadora() {
        try {
            this.validaInstitucionExiste();
            this.buildInstitucionCreate();
            institucionPagadoraServices.creaInstitucion(this.getInstitucionCreate());
            institucionCreate = new Instituciones(new TipoInstitucion());
            this.setRutInstitucion(null);
            this.buildAndResetSearchOptions();
            this.reloadTableData();
            this.showInsertSuccesMessage();
        } catch (Exception e) {
            if (! (e instanceof ValidatorException) ) {
                FacesUtils.showErrorMessage(FORM_MODAL_CREAR_BTN_CREAR, TITULO_ERROR, MSG_ERROR_CREAR);
                logger.error(MSG_ERROR_CREAR, e);
            }
        }
    }

    private void showInsertSuccesMessage() {
        FacesUtils.showSuccesMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_CREACION_EXITOSA, MSG_EXITO_CREAR);
        FacesUtils.executeJavascriptOnView("$('#modalCrear').modal('toggle'); scrollToTop();");
    }

    private void reloadTableData() throws DataAccessException {
        this.setInstituciones(institucionPagadoraServices.buscarInstitucionesPorCriterio(this.getInstitucionSearch()));
    }

    private void buildInstitucionCreate() throws FormatException {
        Rut rut = Utils.separateRut(this.getRutInstitucion());
        this.getInstitucionCreate().setInstRut(rut.getRut());
        this.getInstitucionCreate().setInstDv(rut.getDv());
        this.getInstitucionCreate().setInstNombreCorto(StringUtils.SPACE);
        this.getInstitucionCreate().setTbPrEstInstitucion(new EstadoInstitucion(1));
    }

    /**
     * @param institucion
     */
    public void cambiaEstadoInstitucion(Instituciones institucion) {
        if (this.getChecked().get(institucion)) {
            institucion.getTbPrEstInstitucion().setEstInstCod(2);
            this.getInstitucionesDelete().add(institucion);
        } else {
            institucion.getTbPrEstInstitucion().setEstInstCod(1);
            this.getInstitucionesDelete().remove(institucion);
        }
    }


    public Instituciones getInstitucionSearchCriteria() {
        return institucionSearchCriteria;
    }

    public void setInstitucionSearchCriteria(Instituciones institucionSearchCriteria) {
        this.institucionSearchCriteria = institucionSearchCriteria;
    }

    public List<TipoInstitucion> getTipoInstitucionList() {
        return tipoInstitucionList;
    }

    public void setTipoInstitucionList(List<TipoInstitucion> tipoInstitucionList) {
        this.tipoInstitucionList = tipoInstitucionList;
    }

    public List<Instituciones> getInstituciones() {
        return instituciones;
    }

    public void setInstituciones(List<Instituciones> instituciones) {
        this.instituciones = instituciones;
    }

    public String getRutInstitucion() {
        return rutInstitucion;
    }

    public void setRutInstitucion(String rutInstitucion) {
        this.rutInstitucion = rutInstitucion;
    }

    public String getNombreInstitucion() {
        return nombreInstitucion;
    }

    public void setNombreInstitucion(String nombreInstitucion) {
        this.nombreInstitucion = nombreInstitucion;
    }

    public TipoInstitucion getTipoInstitucion() {
        return tipoInstitucion;
    }

    public void setTipoInstitucion(TipoInstitucion tipoInstitucion) {
        this.tipoInstitucion = tipoInstitucion;
    }

    public Instituciones getInstitucionEdit() {
        return institucionEdit;
    }

    public void setInstitucionEdit(Instituciones institucionEdit) {
        this.institucionEdit = institucionEdit;
    }

    public Map<Instituciones, Boolean> getChecked() {
        return checked;
    }

    public void setChecked(Map<Instituciones, Boolean> checked) {
        this.checked = checked;
    }

    public List<Instituciones> getInstitucionesDelete() {
        return institucionesDelete;
    }

    public void setInstitucionesDelete(List<Instituciones> institucionesDelete) {
        this.institucionesDelete = institucionesDelete;
    }

    public Instituciones getInstitucionCreate() {
        return institucionCreate;
    }

    public void setInstitucionCreate(Instituciones institucionCreate) {
        this.institucionCreate = institucionCreate;
    }

    public String getRutInstitucionSearch() {
        return rutInstitucionSearch;
    }

    public void setRutInstitucionSearch(String rutInstitucionSearch) {
        this.rutInstitucionSearch = rutInstitucionSearch;
    }

    public String getNombreInstitucionSearch() {
        return nombreInstitucionSearch;
    }

    public void setNombreInstitucionSearch(String nombreInstitucionSearch) {
        this.nombreInstitucionSearch = nombreInstitucionSearch;
    }

    public TipoInstitucion getTipoInstitucionSearch() {
        return tipoInstitucionSearch;
    }

    public void setTipoInstitucionSearch(TipoInstitucion tipoInstitucionSearch) {
        this.tipoInstitucionSearch = tipoInstitucionSearch;
    }

    public Instituciones getInstitucionSearch() {
        return institucionSearch;
    }

    public void setInstitucionSearch(Instituciones institucionSearch) {
        this.institucionSearch = institucionSearch;
    }
}
