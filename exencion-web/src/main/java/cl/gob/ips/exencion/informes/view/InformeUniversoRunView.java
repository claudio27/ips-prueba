package cl.gob.ips.exencion.informes.view;

import cl.gob.ips.exencion.dto.InformeUniversoRunDTO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.service.InformeUniversoRunServices;
import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.util.Reportes;
import cl.gob.ips.exencion.util.Utils;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Alaya
 * @since 15-03-2017.
 */
@ManagedBean(name = "informeUniversoRunView")
@ViewScoped
public class InformeUniversoRunView extends BaseControllerView implements Serializable {

	private static final long serialVersionUID = 331345600770868691L;
	private static final Logger LOGGER = Logger.getLogger(InformeUniversoRunView.class);

	@Inject
	private InformeUniversoRunServices informeRunServices;

	private String rut;
	private Date periodoDesde;
	private Date periodoHasta;
	private Boolean mostrarGrilla;
	private List<InformeUniversoRunDTO> respuestaGrilla;
	private String nombreCompleto;


	@PostConstruct
	private void init() {

		try{
			this.mostrarGrilla = Boolean.FALSE;
			Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

			if (params.containsKey("rut")&&params.containsKey("pDesde")&&params.containsKey("pHasta")) {
				this.rut = params.get("rut");
				this.periodoDesde = obtenerFecha(params.get("pDesde"));
				this.periodoHasta = obtenerFecha(params.get("pHasta"));

				if (this.rut != null && this.periodoDesde != null && this.periodoHasta != null){

					buscar();

				}

			}
		}catch (ParseException e) {
			LOGGER.error("Al convertir parametros de fechas", e);
		}catch (NullPointerException e){
			LOGGER.error("Los datos no vienen desde MovimientosRunView", e);
		}

	}


	public void buscar() {
		LOGGER.debug("rut:" + this.rut);
		LOGGER.debug("periodoDesde: " + this.periodoDesde);
		LOGGER.debug("periodoHasta: " + this.periodoHasta);

		if (!this.rut.equalsIgnoreCase("") && null != this.periodoDesde && null != this.periodoHasta ) {

			if (Utils.validarPeriodoDesdeHasta(this.periodoDesde, this.periodoHasta)) {

				try {

					this.respuestaGrilla = informeRunServices.obtenerInformeUniversoPorRun(
							obtenerPeriodoEntero(this.periodoDesde),
							obtenerPeriodoEntero(this.periodoHasta),
							obtenerRun(this.rut));

				} catch (EJBException | DataAccessException e) {

					super.muestraErrorDeAccesoaDatos(e);

				}catch (NullPointerException e){
					LOGGER.error("Existen valores nulos en los datos", e);
				}

				if(this.respuestaGrilla == null || this.respuestaGrilla.size() == 0){
					MensajesUtil.setMensajeErrorContexto("validaciones.informe.universo.run.sin.informacion");
					super.mostrarMensajeError();
					limpiar();
					return;
				}

				this.setNombreCompleto(this.respuestaGrilla.get(0).getNombreCompleto());

				this.mostrarGrilla = Boolean.TRUE;

			} else {
				limpiar();
				MensajesUtil.setMensajeErrorContexto("validacion.movimientos.rut.periodosDesdeHasta");
				super.mostrarMensajeError();
			}

		} else {
			MensajesUtil.setMensajeErrorContexto("validacion.movimientos.rut.completar.campos");
			super.mostrarMensajeError();
			LOGGER.debug("is error");
		}
	}

	public void limpiar() {
		this.rut = null;
		this.periodoDesde = null;
		this.periodoHasta = null;
		this.mostrarGrilla = Boolean.FALSE;
	}

	public String redirectUniversoRun() {
		Integer pDesde = obtenerPeriodoEntero(this.periodoDesde);
		Integer pHasta = obtenerPeriodoEntero(this.periodoHasta);

		StringBuilder sb = new StringBuilder();
		sb.append("&rut=").append(rut).append("&pDesde=").append(pDesde+"01").append("&pHasta=").append(pHasta+"01");

		return "movimiento-por-run?faces-redirect=true" + sb.toString();
	}

	public void obtenerExcelGeneral() throws IOException {

		try {
			File archivo = Reportes.obtenerInformeUniversoRun(this.respuestaGrilla);
			super.generarExcel(archivo);
		} catch (FileNotFoundException e) {
			LOGGER.error("Archivo no encontrado: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("Ha ocurrido un error: " + e.getMessage());
		}
	}

	public String redirectConsultarMovimientos() {

		Integer pDesde = obtenerPeriodoEntero(periodoDesde);
		Integer pHasta = obtenerPeriodoEntero(periodoHasta);

		StringBuilder sb = new StringBuilder();
		sb.append("&rut=").append(rut).append("&pDesde=").append(pDesde).append("&pHasta=").append(pHasta);

		return "movimiento-por-run?faces-redirect=true" + sb.toString();
	}

	public Date getPeriodoDesde() {
		return periodoDesde;
	}

	public void setPeriodoDesde(Date periodoDesde) {
		this.periodoDesde = periodoDesde;
	}

	public Date getPeriodoHasta() {
		return periodoHasta;
	}

	public void setPeriodoHasta(Date periodoHasta) {
		this.periodoHasta = periodoHasta;
	}

	public Boolean getMostrarGrilla() {
		return mostrarGrilla;
	}

	public void setMostrarGrilla(Boolean mostrarGrilla) {
		this.mostrarGrilla = mostrarGrilla;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public List<InformeUniversoRunDTO> getRespuestaGrilla() {
		return respuestaGrilla;
	}

	public void setRespuestaGrilla(List<InformeUniversoRunDTO> respuestaGrilla) {
		this.respuestaGrilla = respuestaGrilla;
	}


	public String getNombreCompleto() {return nombreCompleto;}

	public void setNombreCompleto(String nombreCompleto) {this.nombreCompleto = nombreCompleto;}
}
