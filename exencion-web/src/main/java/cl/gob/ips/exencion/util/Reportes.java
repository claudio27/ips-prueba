package cl.gob.ips.exencion.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cl.gob.ips.exencion.dto.*;
import cl.gob.ips.exencion.enums.TipoReporte;
import cl.gob.ips.exencion.model.InformeGestionExtinto;
import cl.gob.ips.exencion.model.InformeGestionMantienen;
import cl.gob.ips.exencion.model.InformeGestionNuevos;
import org.apache.log4j.Logger;

import cl.alaya.component.utils.ExcelUtil;
import cl.alaya.component.utils.PdfUtil;

import com.itextpdf.text.Document;

public final class Reportes {

	private static final Logger logger = Logger.getLogger(Reportes.class);

	public static File obtenerMovimientosRun(List<MovimientosRutDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.MOVIMIENTOS_RUN_TEMPLATE.getCodigo());
			int fila = 1;

			for (MovimientosRutDTO mr : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, mr.getPeriodo());
				ExcelUtil.escribirEnCelda(fila, 1, mr.getMovimiento());
				ExcelUtil.escribirEnCelda(fila, 2, mr.getFechaDevengamiento());
				ExcelUtil.escribirEnCelda(fila, 3, mr.getPfp());
				ExcelUtil.escribirEnCelda(fila, 4, mr.getFechaPfp());
				ExcelUtil.escribirEnCelda(fila, 5, mr.getResidencia());
				ExcelUtil.escribirEnCelda(fila, 6, mr.getFechaResidencia());
				ExcelUtil.escribirEnCelda(fila, 7, mr.getInstitucionesInformadas());
				ExcelUtil.escribirEnCelda(fila, 8, mr.getPotencialAps());
				ExcelUtil.escribirEnCelda(fila, 9, mr.getCausalExtincion());
				ExcelUtil.escribirEnCelda(fila, 10, mr.getRequisitosNoAcreditados());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.MOVIMIENTOS_RUN.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerInformeGestion(List<InformeGestionDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.INFORME_GESTION_TEMPLATE.getCodigo());

			int fila = 1;

			for (InformeGestionDTO informe : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, informe.getPeriodo());
				ExcelUtil.escribirEnCelda(fila, 1, informe.getIps());
				ExcelUtil.escribirEnCelda(fila, 2, informe.getIsl());
				ExcelUtil.escribirEnCelda(fila, 3, informe.getBph());
				ExcelUtil.escribirEnCelda(fila, 4, informe.getTotalInternos());
				ExcelUtil.escribirEnCelda(fila, 5, informe.getAfp());
				ExcelUtil.escribirEnCelda(fila, 6, informe.getCsv());
				ExcelUtil.escribirEnCelda(fila, 7, informe.getMutual());
				ExcelUtil.escribirEnCelda(fila, 8, informe.getTotalExternos());
				ExcelUtil.escribirEnCelda(fila, 9, informe.getTotal());
				ExcelUtil.escribirEnCelda(fila, 10, informe.getNuevos());
				ExcelUtil.escribirEnCelda(fila, 11, informe.getExtinciones());
				ExcelUtil.escribirEnCelda(fila, 12, informe.getMantienen());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.INFORME_GESTION.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerInformeGestionNuevos(List<InformeGestionNuevosDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.INFORME_GESTION_NUEVOS_TEMPLATE.getCodigo());

			int fila = 1;

			for (InformeGestionNuevosDTO informe : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, informe.getRun());
				ExcelUtil.escribirEnCelda(fila, 1, informe.getNombre());
				ExcelUtil.escribirEnCelda(fila, 2, informe.getApellidoPaterno());
				ExcelUtil.escribirEnCelda(fila, 3, informe.getApellidoMaterno());
				ExcelUtil.escribirEnCelda(fila, 4, informe.getTipoPensionado());
				ExcelUtil.escribirEnCelda(fila, 5, informe.getInstitucion());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.INFORME_GESTION_NUEVOS.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerInformeGestionExtinciones(List<InformeGestionExtintoDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.INFORME_GESTION_EXTINCION_TEMPLATE.getCodigo());

			int fila = 1;

			for (InformeGestionExtintoDTO informe : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, informe.getRun());
				ExcelUtil.escribirEnCelda(fila, 1, informe.getNombre());
				ExcelUtil.escribirEnCelda(fila, 2, informe.getApellidoPaterno());
				ExcelUtil.escribirEnCelda(fila, 3, informe.getApellidoMaterno());
				ExcelUtil.escribirEnCelda(fila, 4, informe.getTipoPensionado());
				ExcelUtil.escribirEnCelda(fila, 5, informe.getInstitucion());
				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.INFORME_GESTION_EXTINCION.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerInformeGestionMantienen(List<InformeGestionMantienenDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.INFORME_GESTION_MANTIENEN_TEMPLATE.getCodigo());

			int fila = 1;

			for (InformeGestionMantienenDTO informe : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, informe.getRun());
				ExcelUtil.escribirEnCelda(fila, 1, informe.getNombre());
				ExcelUtil.escribirEnCelda(fila, 2, informe.getApellidoPaterno());
				ExcelUtil.escribirEnCelda(fila, 3, informe.getApellidoMaterno());
				ExcelUtil.escribirEnCelda(fila, 4, informe.getTipoPensionado());
				ExcelUtil.escribirEnCelda(fila, 5, informe.getInstitucion());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.INFORME_GESTION_MANTIENEN.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerInformeGestionVer(List<InformeGestionDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.INFORME_GESTION_VER_TEMPLATE.getCodigo());

			int fila = 1;

			for (InformeGestionDTO informe : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, informe.getInstitucion());
				ExcelUtil.escribirEnCelda(fila, 1, informe.getTipoEntidad());
				ExcelUtil.escribirEnCelda(fila, 2, informe.getTotal());
				ExcelUtil.escribirEnCelda(fila, 3, informe.getNuevos());
				ExcelUtil.escribirEnCelda(fila, 4, informe.getExtinciones());
				ExcelUtil.escribirEnCelda(fila, 5, informe.getMantienen());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.INFORME_GESTION_VER.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerInformeUniverso(List<InformeUniversoDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.INFORME_UNIVERSO_TEMPLATE.getCodigo());

			int fila = 1;

			for (InformeUniversoDTO informe : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, informe.getPeriodo());
				ExcelUtil.escribirEnCelda(fila, 1, informe.getVivos());
				ExcelUtil.escribirEnCelda(fila, 2, informe.getSinSps());
				ExcelUtil.escribirEnCelda(fila, 3, informe.getSinRsh());
				ExcelUtil.escribirEnCelda(fila, 4, informe.getSinFocalizacion());
				ExcelUtil.escribirEnCelda(fila, 5, informe.getSinResidencia());
				ExcelUtil.escribirEnCelda(fila, 6, informe.getCumplenRequisitos());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.INFORME_UNIVERSO.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerInformeUniversoVer(List<DetalleInformeUniversoDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.INFORME_UNIVERSO_VER_TEMPLATE.getCodigo());

			int fila = 1;

			for (DetalleInformeUniversoDTO informe : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, informe.getRun());
				ExcelUtil.escribirEnCelda(fila, 1, informe.getNombre());
				ExcelUtil.escribirEnCelda(fila, 2, informe.getApellidoPaterno());
				ExcelUtil.escribirEnCelda(fila, 3, informe.getApellidoMaterno());
				ExcelUtil.escribirEnCelda(fila, 4, informe.getVivo());
				ExcelUtil.escribirEnCelda(fila, 5, informe.getRsh());
				ExcelUtil.escribirEnCelda(fila, 6, informe.getFocalizacion());
				ExcelUtil.escribirEnCelda(fila, 7, informe.getResidencia());
				ExcelUtil.escribirEnCelda(fila, 8, informe.getCumpleRequisitos());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.INFORME_UNIVERSO_VER.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerInformeUniversoRun(List<InformeUniversoRunDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.INFORME_UNIVERSO_RUN_TEMPLATE.getCodigo());

			int fila = 1;

			for (InformeUniversoRunDTO informe : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, informe.getPeriodo());
				ExcelUtil.escribirEnCelda(fila, 1, informe.getEdad());
				ExcelUtil.escribirEnCelda(fila, 2, informe.getPfp());
				ExcelUtil.escribirEnCelda(fila, 3, informe.getTipoPfp());
				ExcelUtil.escribirEnCelda(fila, 4, informe.getFechaPfp());
				ExcelUtil.escribirEnCelda(fila, 5, informe.getResidencia());
				ExcelUtil.escribirEnCelda(fila, 6, informe.getFechaResidencia());
				ExcelUtil.escribirEnCelda(fila, 7, informe.getTipoPensionado());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.INFORME_UNIVERSO_RUN.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerResumenRegArchivosEntrada(List<ResumenRegArchivosEntradaDTO> dto) {
		try {
			ExcelUtil.inicializarHoja(TipoReporte.RESUMEN_REG_ARCHIVOS_ENTRADA_TEMPLATE.getCodigo());

			int fila = 1;

			for (ResumenRegArchivosEntradaDTO informe : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, informe.getPeriodo());
				ExcelUtil.escribirEnCelda(fila, 1, informe.getArchivoCarga());
				ExcelUtil.escribirEnCelda(fila, 2, informe.getCantidadRegistros());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.RESUMEN_REG_ARCHIVOS_ENTRADA.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerResultadoValidacionArchivos(List<ResultadoValidacionArchivosDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.RESULTADO_VALIDACION_ARCHIVOS_TEMPLATE.getCodigo());

			int fila = 1;

			for (ResultadoValidacionArchivosDTO informe : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, informe.getNombre());
				ExcelUtil.escribirEnCelda(fila, 1, informe.getFechaCarga());
				ExcelUtil.escribirEnCelda(fila, 2, informe.getRegistrosInformados());
				ExcelUtil.escribirEnCelda(fila, 3, informe.getRegistrosCargados());
				ExcelUtil.escribirEnCelda(fila, 4, informe.getFilasSinDatos());
				ExcelUtil.escribirEnCelda(fila, 5, informe.getRegistrosIlegibles());
				ExcelUtil.escribirEnCelda(fila, 6, informe.getFormatoNombre());
				ExcelUtil.escribirEnCelda(fila, 7, informe.getEstructura());
				ExcelUtil.escribirEnCelda(fila, 8, informe.getResultadoCarga());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.RESULTADO_VALIDACION_ARCHIVOS.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerEventosProcesosSistema(List<EventosProcesosSistemaDTO> dto, String periodo) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.EVENTOS_PROCESOS_SISTEMA_TEMPLATE.getCodigo());

			ExcelUtil.escribirEnCelda(0, 3, periodo);

			int fila = 2;

			for (EventosProcesosSistemaDTO informe : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, informe.getFecha());
				ExcelUtil.escribirEnCelda(fila, 1, informe.getDescripcion());
				ExcelUtil.escribirEnCelda(fila, 2, informe.getResponsables());
				ExcelUtil.escribirEnCelda(fila, 3, informe.getTipoEvento());
				ExcelUtil.escribirEnCelda(fila, 4, informe.getDestinatarioNotificacion());
				ExcelUtil.escribirEnCelda(fila, 5, informe.getEstadoNotificacion());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.EVENTOS_PROCESOS_SISTEMA.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerAtencionPensionado(List<AtencionPensionadoDTO> dto) {

		try {
			//TODO CAMBIAR TEMPLATE
			ExcelUtil.inicializarHoja(TipoReporte.ATENCION_PENSIONADO_TEMPLATE.getCodigo());
			int fila = 1;

			for (AtencionPensionadoDTO atencionPensionadoDTO : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, atencionPensionadoDTO.getFecha());
				ExcelUtil.escribirEnCelda(fila, 1, atencionPensionadoDTO.getPeriodo());
				ExcelUtil.escribirEnCelda(fila, 2, atencionPensionadoDTO.getRespuesta());
				ExcelUtil.escribirEnCelda(fila, 3, atencionPensionadoDTO.getObservacion());
				ExcelUtil.escribirEnCelda(fila, 4, atencionPensionadoDTO.getInstitucionPagadora());
				ExcelUtil.escribirEnCelda(fila, 5, atencionPensionadoDTO.getUsuario());

				fila++;
			}

			//TODO AGREGAR TIPO AL ENUM
			return ExcelUtil.construirArchivo(TipoReporte.ATENCION_PENSIONADO.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerNominaBeneficiarios(List<NominasPeriodoDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.NOMINA_BENEFICIARIO_TEMPLATE.getCodigo());

			int fila = 1;

			for (NominasPeriodoDTO nomina : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, nomina.getPeriodo());
				ExcelUtil.escribirEnCelda(fila, 1, nomina.getFechaDevengamientoBeneficio());
				ExcelUtil.escribirEnCelda(fila, 2, nomina.getRutEntidad()+"-"+nomina.getDvEntidad());
				ExcelUtil.escribirEnCelda(fila, 3, nomina.getRunBeneficiario()+"-"+nomina.getDvBeneficiario());
				ExcelUtil.escribirEnCelda(fila, 4, nomina.getNombres());
				ExcelUtil.escribirEnCelda(fila, 5, nomina.getApellidoPaterno());
				ExcelUtil.escribirEnCelda(fila, 6, nomina.getApellidoMaterno());
				ExcelUtil.escribirEnCelda(fila, 7, nomina.getTipoPension());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.NOMINA_BENEFICIARIO.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerNominaConsultaPdi(List<NominaPeriodoPdiDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.NOMINA_CONSULTAPDI_TEMPLATE.getCodigo());

			int fila = 1;

			for (NominaPeriodoPdiDTO nomina : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, nomina.getRun());
				ExcelUtil.escribirEnCelda(fila, 1, nomina.getApellidoPaterno());
				ExcelUtil.escribirEnCelda(fila, 2, nomina.getApellidoMaterno());
				ExcelUtil.escribirEnCelda(fila, 3, nomina.getNombres());
				ExcelUtil.escribirEnCelda(fila, 4, nomina.getSexo());
				ExcelUtil.escribirEnCelda(fila, 5, nomina.getFechaNacimiento());
				ExcelUtil.escribirEnCelda(fila, 6, nomina.getDesdeUno());
				ExcelUtil.escribirEnCelda(fila, 7, nomina.getHastaUno());
				ExcelUtil.escribirEnCelda(fila, 8, nomina.getDesdeDos());
				ExcelUtil.escribirEnCelda(fila, 9, nomina.getHastaDos());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.NOMINA_CONSULTAPDI.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerNominaPotBeneficiarios(List<NominasPeriodoDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.NOMINA_CONSULTA_POTBENEF_TEMPLATE.getCodigo());

			int fila = 1;

			for (NominasPeriodoDTO nomina : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, nomina.getPeriodo());
				ExcelUtil.escribirEnCelda(fila, 1, nomina.getRutEntidad()+"-"+nomina.getDvEntidad());
				ExcelUtil.escribirEnCelda(fila, 2, nomina.getRunBeneficiario()+"-"+nomina.getDvBeneficiario());
				ExcelUtil.escribirEnCelda(fila, 3, nomina.getNombres());
				ExcelUtil.escribirEnCelda(fila, 4, nomina.getApellidoPaterno());
				ExcelUtil.escribirEnCelda(fila, 5, nomina.getApellidoMaterno());
				ExcelUtil.escribirEnCelda(fila, 6, nomina.getTipoPension());
				ExcelUtil.escribirEnCelda(fila, 7, nomina.getRequisitosNoAcreaditados());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.NOMINA_CONSULTA_POTBENEF.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerNominaSinPbs(List<NominaPeriodoSinPbsDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.NOMINA_CONSULTA_SINPBS_TEMPLATE.getCodigo());

			int fila = 1;

			for (NominaPeriodoSinPbsDTO nomina : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, nomina.getTipoMovimiento());
				ExcelUtil.escribirEnCelda(fila, 1, nomina.getTipoBeneficio());
				ExcelUtil.escribirEnCelda(fila, 2, nomina.getRunBeneficiario()+"-"+nomina.getDvBeneficiario());
				ExcelUtil.escribirEnCelda(fila, 3, nomina.getMonto());
				ExcelUtil.escribirEnCelda(fila, 4, nomina.getFechaInicioPago());
				ExcelUtil.escribirEnCelda(fila, 5, nomina.getFechaVencimiento());
				ExcelUtil.escribirEnCelda(fila, 6, nomina.getCodigoHaber());
				ExcelUtil.escribirEnCelda(fila, 7, nomina.getUnidadMedida());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.NOMINA_CONSULTA_SINPBS.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerNominaBenefeciariosAps(List<NominasPeriodoDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.NOMINA_CONSULTA_BENEFAPS_TEMPLATE.getCodigo());

			int fila = 1;

			for (NominasPeriodoDTO nomina : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, nomina.getPeriodo());
				ExcelUtil.escribirEnCelda(fila, 1, nomina.getRutEntidad()+"-"+nomina.getDvEntidad());
				ExcelUtil.escribirEnCelda(fila, 2, nomina.getRunBeneficiario()+"-"+nomina.getDvBeneficiario());
				ExcelUtil.escribirEnCelda(fila, 3, nomina.getNombres());
				ExcelUtil.escribirEnCelda(fila, 4, nomina.getApellidoPaterno());
				ExcelUtil.escribirEnCelda(fila, 5, nomina.getApellidoMaterno());
				ExcelUtil.escribirEnCelda(fila, 6, nomina.getIndicadorFps());

				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.NOMINA_CONSULTA_BENEFAPS.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerNominaSinFicha(List<NominasPeriodoDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.NOMINA_CONSULTA_SINFICHA_TEMPLATE.getCodigo());

			int fila = 1;

			for (NominasPeriodoDTO nomina : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, nomina.getRunBeneficiario()+"-"+nomina.getDvBeneficiario());
				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.NOMINA_CONSULTA_SINFICHA.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static File obtenerNominaRebaja(List<NominasPeriodoDTO> dto) {

		try {
			ExcelUtil.inicializarHoja(TipoReporte.NOMINA_CONSULTA_REBAJA_TEMPLATE.getCodigo());

			int fila = 1;

			for (NominasPeriodoDTO nomina : dto) {
				ExcelUtil.escribirEnCelda(fila, 0, nomina.getRunBeneficiario());
				ExcelUtil.escribirEnCelda(fila, 1, nomina.getEstado());
				fila++;
			}

			return ExcelUtil.construirArchivo(TipoReporte.NOMINA_CONSULTA_REBAJA.getCodigo());
		} catch (Exception e) {
			logger.error("No se pudo cargar el contenido de la planilla excel. "
					+ e.getMessage());
			return null;
		}
	}

	public static Document obtenerPdf() {

		try {
			Map<String, List<String>> map = new HashMap<String, List<String>>();

			List<String> titulos = new ArrayList<String>();
			List<String> valores = new ArrayList<String>();
			for (int i = 0; i < 10; i++) {
				titulos.add("titulo " + i);
				valores.add("valores " + i);
			}

			map.put("titulos", titulos);
			map.put("valores", valores);

			Document documento = PdfUtil.generarPdf(map, "prueba");

			return documento;
		} catch (Exception e) {
			logger.equals(e);
		}

		return null;
	}
}
