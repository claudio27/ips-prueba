package cl.gob.ips.exencion.util;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 * Clase utilitaria para el manejo de mensajes y etiquetas de JSF.
 * 
 * Registro de versiones:
 * <ul>
 * <li>1.0, 20/03/2017 Cristian Toledo O. : version inicial.</li>
 * </ul>
 * 
 */
public final class MensajesUtil {

    /**
     * Constante que representa el código de status.
     */
    public static final String STATUS_CODE = "EXE_STATUS";

    /**
     * Constante que representa el código de error.
     */
    public static final String ERROR_CODE = "ERROR";

    /**
     * Log de la clase.
     */
    private static final Logger LOGGER = Logger.getLogger(MensajesUtil.class);

    /**
     * Nombre del archivo de etiquetas.
     */
    private static final String LABEL_RESOURCE_KEY = "lbl";

    /**
     * Nombre del archivo de mensajes.
     */
    private static final String MESSAGE_RESOURCE_KEY = "val";

    /**
     * Constructor privado de clase utilitaria.
     */
    private MensajesUtil() {
    }

    /**
     * Permite obtener la etiqueta con parámetros asociada a la clave.
     * 
     * @param msgKey
     *            Clave de la etiqueta.
     * @param msgParams
     *            Parámetros de la etiqueta.
     * @return La etiqueta obtenida.
     */
    public static String getLabel(String msgKey, Object[] msgParams) {
        return getText(LABEL_RESOURCE_KEY, msgKey, msgParams);

    }

    /**
     * Permite obtener la etiqueta asociada a la clave.
     * 
     * @param msgKey
     *            Clave de la etiqueta.
     * 
     * @return La etiqueta obtenida.
     */
    public static String getLabel(String msgKey) {
        return getText(LABEL_RESOURCE_KEY, msgKey, new Object[] {});

    }

    /**
     * Permite obtener el mensaje con parámetros asociado a la clave.
     * 
     * @param msgKey
     *            Clave del mensaje.
     * @param msgParams
     *            Parámetros del mensaje.
     * @return El mensaje obtenido.
     */
    public static String getMessage(String msgKey, Object[] msgParams) {
        return getText(MESSAGE_RESOURCE_KEY, msgKey, msgParams);
    }

    /**
     * Permite obtener el mensaje asociado a la clave.
     * 
     * @param msgKey
     *            Clave del mensaje.
     * 
     * @return El mensaje obtenido.
     */
    public static String getMessage(String msgKey) {
        return getText(MESSAGE_RESOURCE_KEY, msgKey, new Object[] {});
    }

    /**
     * Método que permite setear un mensaje en el contexto JSF para un
     * componente específico, según la clave del mensaje y su severidad.
     * 
     * @param keyMensaje
     *            clave del mensaje
     * @param idComponente
     *            id del componente
     * @param sev
     *            severidad del mensaje
     */
    public static void setMensajeContexto(String keyMensaje, Severity sev, String idComponente) {

        FacesMessage men = getFacesMessage(keyMensaje);
        men.setSeverity(sev);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(idComponente, men);

        updateContextStatus(sev);
    }

    /**
     * Método que permite setear un mensaje en el contexto JSF según la clave
     * del mensaje, los parámetros del mismo, y su severidad.
     * 
     * @param keyMensaje
     *            clave del mensaje
     * @param msgParams
     *            parametros del mensaje
     * @param idComponente
     *            id del componente
     * @param sev
     *            severidad del mensaje
     */
    public static void setMensajeContexto(String keyMensaje, Object[] msgParams, 
        Severity sev, String idComponente) {

        FacesMessage men = getFacesMessage(keyMensaje, msgParams);
        FacesContext fc = FacesContext.getCurrentInstance();
        men.setSeverity(sev);
        fc.addMessage(idComponente, men);

        updateContextStatus(sev);
    }

    /**
     * Método que permite setear un mensaje en el contexto JSF, según la clave
     * del mensaje y su severidad.
     * 
     * @param keyMensaje
     *            clave del mensaje
     * @param sev
     *            severidad del mensaje
     */
    public static void setMensajeContexto(String keyMensaje, Severity sev) {

        FacesMessage men = getFacesMessage(keyMensaje);
        men.setSeverity(sev);
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, men);

        updateContextStatus(sev);
    }

    /**
     * Método que permite setear un mensaje en el contexto JSF, según la clave
     * del mensaje, los parámetros del mismo, y su severidad.
     * 
     * @param keyMensaje
     *            clave del mensaje
     * @param msgParams
     *            parametros del mensaje
     * @param sev
     *            severidad del mensaje
     */
    public static void setMensajeContexto(String keyMensaje, Object[] msgParams, Severity sev) {

        FacesMessage men = getFacesMessage(keyMensaje, msgParams);
        FacesContext fc = FacesContext.getCurrentInstance();
        men.setSeverity(sev);
        fc.addMessage(null, men);

        updateContextStatus(sev);
    }

    /**
     * Método que setea un mensaje con la severidad de error.
     * 
     * @param keyMensaje clave del mensaje.
     */
    public static void setMensajeErrorContexto(String keyMensaje) {
        setMensajeContexto(keyMensaje, FacesMessage.SEVERITY_ERROR, null);

        if (RequestContext.getCurrentInstance() != null) {
            RequestContext.getCurrentInstance().addCallbackParam(STATUS_CODE,
                ERROR_CODE);
        }
    }

    /**
     * Método que setea un mensaje con la severidad de error.
     * 
     * @param keyMensaje clave del mensaje.
     * @param msgParams parametros del mensaje.
     */
    public static void setMensajeErrorContexto(String keyMensaje,
        Object[] msgParams) {
        setMensajeContexto(keyMensaje, msgParams, FacesMessage.SEVERITY_ERROR,
            null);
    }

    /**
     * Actualiza el status del mensaje, y dá la instrucción para el renderizado
     * del mismo.
     * 
     * @param sev
     *            Severidad del mensaje.
     */
    private static void updateContextStatus(Severity sev) {
        if (RequestContext.getCurrentInstance() != null) {
            if (sev.equals(FacesMessage.SEVERITY_ERROR) || sev.equals(FacesMessage.SEVERITY_FATAL)) {
                RequestContext.getCurrentInstance().addCallbackParam(STATUS_CODE, ERROR_CODE);
            }
        }
    }

    /**
     * Permite obtener el FacesMessage asociado a la clave.
     * 
     * @param msgKey
     *            Clave del mensaje.
     * @param msgParams
     *            Parámetros del mensaje.
     * @return El FacesMessage obtenido.
     */
    public static FacesMessage getFacesMessage(String msgKey, Object[] msgParams) {
        return new FacesMessage(getMessage(msgKey, msgParams));
    }

    /**
     * Permite obtener el FacesMessage asociado a la clave y sus par�metros.
     * 
     * @param msgKey
     *            Clave del mensaje.
     * 
     * @return El FacesMessage obtenido.
     */
    public static FacesMessage getFacesMessage(String msgKey) {
        return new FacesMessage(getMessage(msgKey));
    }

    /**
     * Permite obtener el texto asociado a la clave incorporando parámetros, en
     * un archivo de recursos especificado.
     * 
     * @param resourceKey
     *            Clave del archivo de recursos.
     * @param msgKey
     *            La clave a buscar.
     * @param msgParams
     *            Parametros a pasar al texto obtenido.
     * @return Texto obtenido.
     */
    private static String getText(String resourceKey, String msgKey, Object[] msgParams) {

        FacesContext fc = FacesContext.getCurrentInstance();
        ResourceBundle bundle = fc.getApplication().getResourceBundle(fc, resourceKey);
        String etiqueta = msgKey;

        try {
            etiqueta = MessageFormat.format(bundle.getString(msgKey), msgParams);
        }
        catch (MissingResourceException e) {
            LOGGER.error("key not found: " + msgKey + " resource: " + resourceKey);
        }

        return etiqueta;
    }

    /**
     * Permite obtener el texto asociado a la clave en un archivo de recursos
     * especificado.
     * 
     * @param msgKey
     *            Clave de la etiqueta.
     * @param resource
     *            Alias del archivo de recursos.
     * @return El texto paramétrico obtenido.
     */
    public static String getText(String msgKey, String resource) {

        return getText(resource, msgKey, new Object[] {});

    }

    /**
     * <p>Returna la peropiedad <code>label</code> desde el componente especificado.</p>
     *
     * @param context   - Instancia de <code>FacesContext</code> para la solicitud actual.
     * @param component - El componente a utilizar.
     *
     * @return El label del componente, retorna el id en caso contrario.
     */
    public static Object getLabel(FacesContext context, UIComponent component) {

        Object o = component.getAttributes().get("label");
        if (o == null || (o instanceof String && ((String) o).length() == 0)) {
            o = component.getValueExpression("label");
        }
        if (o == null) {
            o = component.getClientId(context);
        }
        return o;
    }
}
