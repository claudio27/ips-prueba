package cl.gob.ips.exencion.util;

import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Created by rodrigo.reyesco@gmail.com on 20-10-2016.
 */
public class FacesUtils {

    private static final String CSS_SELECTOR_ALERT_ERROR = ".alert fadein alert-danger";
    private static final String CSS_SELECTOR_ALERT_INFO = ".alert fadein alert-info";

    private FacesUtils() {
    }

    /**
     *
     * @param title
     * @param message
     * @param severity
     * @return
     */
    public static FacesMessage generateMessage(String title, String message, FacesMessage.Severity severity){
        FacesMessage msg = new FacesMessage(title, message);
        msg.setSeverity(severity);
        return msg;
    }

    /**
     * Muestra un FacesMessage con nivel ERROR
     * @param idComponent
     * @param title
     * @param message
     */
    public static void showErrorMessage(String idComponent, String title, String message) {
        FacesMessage msg = new FacesMessage(title, message);
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(idComponent, msg);
        executeJavascriptOnView("scrollTo('" + CSS_SELECTOR_ALERT_ERROR + "');");
    }

    /**
     * Muestra un FacesMessage con nivel INFO
     * @param idComponent
     * @param title
     * @param message
     */
    public static void showSuccesMessage(String idComponent, String title, String message){
        FacesMessage msg = new FacesMessage(title, message);
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(idComponent, msg);
    }

    /**
     * Ejecuta una funcion javascript en la vista
     * @param function
     */
    public static void executeJavascriptOnView(String function){
        RequestContext.getCurrentInstance().execute(function);
    }
}
