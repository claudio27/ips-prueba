package cl.gob.ips.exencion.informes.view;

import cl.gob.ips.exencion.dto.MovimientosRutDTO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Pensionados;
import cl.gob.ips.exencion.service.ConsultaBeneficioServices;
import cl.gob.ips.exencion.service.PensionadosServices;
import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.util.Reportes;
import cl.gob.ips.exencion.util.Utils;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author alaya
 * @since 15-03-2017.
 */
@ManagedBean(name = "movimientosRunView")
@ViewScoped
public class MovimientosRunView extends BaseControllerView implements Serializable {

	private static final long serialVersionUID = 618185454005258476L;
	private static final Logger LOGGER = Logger.getLogger(MovimientosRunView.class);

	private String rut;
	private Date periodoDesde;
	private Date periodoHasta;
	private List<MovimientosRutDTO> respuestaGrilla;

	private Boolean mostrarGrilla;

	@Inject
	private PensionadosServices pensionadosServices;

	@Inject
	private ConsultaBeneficioServices consultaBeneficioServices;

	@PostConstruct
	private void init() {
		try{

			this.rut = !"".equals(rut) ? this.rut : "";
			this.mostrarGrilla = Boolean.FALSE;

			Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

			if (params.containsKey("rut")&&params.containsKey("pDesde")&&params.containsKey("pHasta")){
				this.rut = params.get("rut");
				this.periodoDesde = obtenerFecha(params.get("pDesde"));
				this.periodoHasta = obtenerFecha(params.get("pHasta"));

				if (this.periodoDesde != null && this.periodoHasta != null && this.rut != null){

					buscarMovimientos();
				}
			}
		}catch (ParseException e) {
			LOGGER.error("Al convertir parametros de fechas", e);
		}catch (NullPointerException e){
			LOGGER.error("Los datos no vienen desde InformeUniversoRunView run", e);
		}

	}

	public String getMovimiento(Pensionados pen){
		String retorno = "";
		int beneficiario = isNullObject(pen.getPenBeneficiario())? 0:pen.getPenBeneficiario();
		int potencialBeneficiario = isNullObject(pen.getPenPotBeneficiario()) ? 0:pen.getPenPotBeneficiario();
		int extinguido = isNullObject(pen.getPenExtinguido())? 0:pen.getPenExtinguido();

		if (beneficiario == 1) {
			retorno = "Beneficiario";
		} else if (potencialBeneficiario == 1) {
			retorno = "Potencial Beneficiario";
		} else if (extinguido == 1) {
			retorno = "Extinción";
		}

		return retorno;
	}

	public String getIntitucionesInformadas(int run, int periodo) throws DataAccessException {
		String retorno = "";
		List<String> instituciones = consultaBeneficioServices.obtenerNombreInstituciones(run,periodo);

		if (instituciones!=null){
			for(String nombreInstitucion : instituciones){
				retorno = retorno + nombreInstitucion+", ";
			}
			return  retorno.substring(0, retorno.length()-2);
		}

		return null;
	}

	public String getTipoResidencia(int tipoResidencia ){
		String retorno ;

		switch (tipoResidencia){
			case 1:
				retorno = "Acreditado proceso anterior";
				break;
			case 2:
				retorno = "Solicitud";
				break;
			case 3:
				retorno = "Declaración Jurada";
				break;
			case 4:
				retorno = "Cotizaciones";
				break;
			case 5:
				retorno = "PDI";
				break;
			default:
				retorno = " ";
		}

		return retorno;
	}

	public String getPotencialAPS(int potencialAPS) {
		String retorno = "";

		if (potencialAPS == 1) {
			retorno = "SI";
		} else {
			retorno = "NO";
		}
		return retorno;
	}

	public String getCausalExtincion(int causalExtincion){
		String retorno;

		switch (causalExtincion){
			case 0:
				retorno = "No Extinguido";
				break;
			case 1:
				retorno = "Fallecimiento";
				break;
			case 2:
				retorno = "Dejo de recibir pensión";
				break;
			case 3:
				retorno = "Beneficiario SPS";
				break;
			case 4:
				retorno = "PFP Superior";
				break;
			default:
				retorno = " ";
		}
		return retorno;
	}

	public String getRequisitoNoAcreditado (int potencialBeneficiario){
		String retorno = "";
		if( potencialBeneficiario == 0 ){
			retorno = "SI";
		}else{
			retorno = "NO";
		}
		return retorno;
	}

	public boolean isNullObject(Object toCompare){
		boolean ret = true;

		if(null == toCompare){
			ret = true;
		}else{
			ret = false;
		}

		return ret;
	}

	public void buscarMovimientos() {
		LOGGER.debug("rut:" + this.rut);
		LOGGER.debug("periodoDesde: " + this.periodoDesde);
		LOGGER.debug("periodoHasta: " + this.periodoHasta);
		//RequestContext.getCurrentInstance().reset("inputRutTest");
		if (!"".equalsIgnoreCase(this.rut) && null != this.periodoDesde && null != this.periodoHasta ) {

			if (Utils.validarPeriodoDesdeHasta(this.periodoDesde, this.periodoHasta)) {
				String rut3 = rut.substring(0,rut.length()-2);
				Long run = Long.parseLong((rut3.replace("-","")).replace(".",""));

				try {
				    if (this.pensionadosServices.getPensionadoPorRun(run)!=null){
                        //List <Pensionados> pensionados = pensionadosServices.getPensionadoPorRun(run);
                        List<Pensionados> pensionados = pensionadosServices.getPensionadoPorRunPeriodo(run, super.obtenerPeriodoEntero(this.getPeriodoDesde()), super.obtenerPeriodoEntero(this.getPeriodoHasta()));

                        if(pensionados.isEmpty()){
                            MensajesUtil.setMensajeErrorContexto("validacion.movimientos.pensionados.resultado");
                            super.mostrarMensajeError();
                            LOGGER.debug("is validation");
							this.mostrarGrilla = Boolean.FALSE;
                            return;
                        }

                        this.respuestaGrilla = new ArrayList<MovimientosRutDTO>();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        for (Pensionados pen : pensionados) {
                            //Validación del nombre del pensionado
                            StringBuilder nombreCompleto = new StringBuilder();
                            nombreCompleto.append(pen.getPenNombres());
                            if (pen.getPenApePaterno() != null)
                                nombreCompleto.append(" " + pen.getPenApePaterno());
                            if (pen.getPenApePaterno() != null)
                                nombreCompleto.append(" " + pen.getPenApeMaterno());

                            MovimientosRutDTO m1 = new MovimientosRutDTO();
                            m1.setCausalExtincion(!isNullObject(pen.getPenExtinguido()) ? getCausalExtincion(pen.getPenExtinguido()) : "");
                            m1.setMovimiento(getMovimiento(pen));
                            m1.setFechaDevengamiento(pen.getPenFecDevengamiento() != null ? dateFormat.format(pen.getPenFecDevengamiento()) : "");
                            m1.setFechaPfp(pen.getPenFechaFocalizacion() != null ? dateFormat.format(pen.getPenFechaFocalizacion()) : "");
                            m1.setFechaResidencia(pen.getPenFechaResidencia() != null ? dateFormat.format(pen.getPenFechaResidencia()) : "");
                            m1.setInstitucionesInformadas(isNullObject(getIntitucionesInformadas(pen.getPenRun().intValue(), pen.getPeriodo().getPerPeriodo())) ? "" : getIntitucionesInformadas(pen.getPenRun().intValue(), pen.getPeriodo().getPerPeriodo()));
                            m1.setNombreCompleto(nombreCompleto.toString());
                            m1.setPeriodo(pen.getPeriodo().getPerPeriodo().toString()!=null?pen.getPeriodo().getPerPeriodo().toString():"");
                            m1.setPfp(pen.getPenPuntajePfpAdm() == null ? pen.getPenPuntajePfp() : pen.getPenPuntajePfpAdm());
                            m1.setRequisitosNoAcreditados(pen.getPenPotBeneficiario() != null ? getRequisitoNoAcreditado(pen.getPenPotBeneficiario()) : " ");   // DONDE OBTENGO REQUISITOS NO ACREDITADOS
                            m1.setPotencialAps(isNullObject(pen.getPenPotencialAps()) ? "" : getPotencialAPS(pen.getPenPotencialAps().intValue()));
                            m1.setResidencia(isNullObject(pen.getPenTipoResidencia()) ? "" : getTipoResidencia(pen.getPenTipoResidencia()));
                            this.respuestaGrilla.add(m1);
                            LOGGER.debug("is OK");
                        }
                        this.mostrarGrilla = Boolean.TRUE;

                    }else{
                        MensajesUtil.setMensajeErrorContexto("validacion.run.pensionado");
                        super.mostrarMensajeError();
                        LOGGER.error("Run no posee registros");
                        this.mostrarGrilla = Boolean.FALSE;

                    }

				} catch (DataAccessException e) {
					e.printStackTrace();
				}

				//TODO validacion en caso de que no se obtengan datos "El Run no dispone de información en los periodos ingresados"
				//TODO validacion de respaldo historico "El RUN ingresado no posee Información histórica"
			} else {
				limpiar();
				MensajesUtil.setMensajeErrorContexto("validacion.movimientos.rut.periodosDesdeHasta");
				super.mostrarMensajeError();
				LOGGER.debug("is validation");
			}

		} else {
			MensajesUtil.setMensajeErrorContexto("validacion.movimientos.rut.completar.campos");
			super.mostrarMensajeError();
			LOGGER.debug("is error");
		}
	}


	public void obtenerExcel() throws IOException {
		try {
			File archivo = Reportes.obtenerMovimientosRun(this.respuestaGrilla);
			super.generarExcel(archivo);
		} catch (FileNotFoundException e) {
			LOGGER.error("Archivo no encontrado: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("Ha ocurrido un error: " + e.getMessage());
		}
	}

	public void limpiar() {
		this.respuestaGrilla = null;
		this.rut = "";
		this.periodoDesde = null;
		this.periodoHasta = null;
		this.mostrarGrilla = Boolean.FALSE;
	}

	public String redirectUniversoRun() {
		Integer pDesde = obtenerPeriodoEntero(this.periodoDesde);
		Integer pHasta = obtenerPeriodoEntero(this.periodoHasta);

		StringBuilder sb = new StringBuilder();
		sb.append("&rut=").append(rut).append("&pDesde=").append(pDesde+"01").append("&pHasta=").append(pHasta+"01");

		return "informe-del-universo-run?faces-redirect=true" + sb.toString();
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public Date getPeriodoDesde() {
		return periodoDesde;
	}

	public void setPeriodoDesde(Date periodoDesde) {
		this.periodoDesde = periodoDesde;
	}

	public Date getPeriodoHasta() {
		return periodoHasta;
	}

	public void setPeriodoHasta(Date periodoHasta) {
		this.periodoHasta = periodoHasta;
	}

	public Boolean getMostrarGrilla() {
		return mostrarGrilla;
	}

	public void setMostrarGrilla(Boolean mostrarGrilla) {
		this.mostrarGrilla = mostrarGrilla;
	}

	public List<MovimientosRutDTO> getRespuestaGrilla() {
		return respuestaGrilla;
	}

	public void setRespuestaGrilla(List<MovimientosRutDTO> respuestaGrilla) {
		this.respuestaGrilla = respuestaGrilla;
	}



}
