package cl.gob.ips.exencion.util;

import cl.gob.ips.exencion.dto.DetalleInformeUniversoDTO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.service.InformeUniversoServices;
import org.apache.log4j.Logger;

import javax.ejb.EJBException;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import static cl.gob.ips.exencion.informes.view.InformeUniversoView.mapeaResutadoaDTO;

/**
 * Created by alaya on 7/08/17.
 */
public class CreadorArchivosUtil extends Thread {

    private static final Logger LOGGER = Logger.getLogger(CreadorArchivosUtil.class);

    private File archivoOrigen;

    private File archivoDestino;

    private List<DetalleInformeUniversoDTO> respuestaGrillaVer;

    private Integer periodo;

    private InformeUniversoServices informeServices;

    public CreadorArchivosUtil(){
        super();
    }

    @Override
    public void run(){
        try {

            LOGGER.debug("Iniciando creacion de archivo");
            this.obtenerDatosConsulta();
            LOGGER.debug("Datos obtenidos [OK]");
            this.crearArchivo();
            LOGGER.debug("Archivo Listo [OK]");

        }catch (Exception e) {
            LOGGER.error("run thread ", e);
        }
    }

    private void crearArchivo(){

        this.archivoOrigen = Reportes.obtenerInformeUniversoVer(this.respuestaGrillaVer);

        LOGGER.debug("Creando archivo en ruta . " +  obtenerNombreArchivo());

        this.archivoDestino = new File(obtenerNombreArchivo());

        try {
            this.copiarArchivo(this.archivoOrigen, this.archivoDestino);
        } catch (IOException e) {
            LOGGER.error("Error al copiar archivo: ", e);
        }
    }

    private void copiarArchivo(File input, File output) throws IOException {
        InputStream in = null;
        OutputStream out = null;
        int length;
        byte[] buff = null;

        try {
            in = new FileInputStream(input);
            out = new FileOutputStream(output);

            buff = new byte[1024];

            while ((length = in.read(buff)) > 0){
                out.write(buff, 0, length);
            }

            input.delete();

            LOGGER.debug("Copiado [OK]");
        } finally {
            close(in);
            close(out);
        }
    }

    public static void close(Closeable c) {
        try {
            if (c != null) {
                c.close();
            }
        } catch (IOException e) {
            LOGGER.error("Erro al cerrar Streem", e);
        }
    }

    public void obtenerDatosConsulta(){

        this.respuestaGrillaVer = new ArrayList<>();

        try {
            this.respuestaGrillaVer = mapeaResutadoaDTO(
                this.informeServices.obtenerInformeUniversoPorRun(periodo)
            );
        }catch (EJBException | DataAccessException e) {
            LOGGER.error("Fallo obtencion de datos", e);
        }

        LOGGER.debug("Cantidad de datos ver por run " + this.respuestaGrillaVer.size());
    }


    private String obtenerNombreArchivo(){

        ResourceBundle rb = ResourceBundle.getBundle("ruta-creacion-excels");

        SimpleDateFormat sdf = new SimpleDateFormat("_yyyy-MM-dd_HH:mm:ss_");
        String fecha = sdf.format(new Date());

        String nombreArchivo = this.archivoOrigen.getName();
        int i = nombreArchivo.lastIndexOf('.');
        String extension = nombreArchivo.substring(i, nombreArchivo.length());
        nombreArchivo = nombreArchivo.substring(0, i);

        return rb.getString("ruta.creacion.archivos.excels") + nombreArchivo + fecha + extension;
    }


    public void setPeriodo(Integer periodo) {
        this.periodo = periodo;
    }

    public void setInformeServices(InformeUniversoServices informeServices) {
        this.informeServices = informeServices;
    }
}
