package cl.gob.ips.exencion.procesos.view;

import cl.gob.ips.exencion.dto.AtencionPensionadoDTO;
import cl.gob.ips.exencion.enums.TipoRespuestaEnum;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.ConsultaBeneficio;
import cl.gob.ips.exencion.model.Pensionados;
import cl.gob.ips.exencion.model.TipoRespuesta;
import cl.gob.ips.exencion.service.ConsultaBeneficioServices;
import cl.gob.ips.exencion.service.PensionadosServices;
import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.util.Reportes;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by lpino on 03-07-17.
 */
@ManagedBean(name = "atencionPensionadoView")
@ViewScoped
public class AtencionPensionadoView extends BaseControllerView implements
		Serializable {

	private static final Logger LOGGER = Logger
			.getLogger(AtencionPensionadoView.class);
	private static final long serialVersionUID = 3428153268516734489L;

	private String run;
	private String fechaNacimiento;
	private String edad;
	private String estado;
	private boolean mostrarGrilla;
	private String institucionPagadora;
	private String respuesta;
	private String observacion;
	private String nombre;
	private List<AtencionPensionadoDTO> atencionPensionadoDTO;
	private Integer codigoRespuesta;

	@Inject
	private PensionadosServices pensionadosServices;

	@Inject
	private ConsultaBeneficioServices consultaBeneficioServices;

	@PostConstruct
	private void init() {
		this.mostrarGrilla = false;
		this.run = "";
		//this.respuesta = "Es beneficiario de Exención Parcial de Salud";
		atencionPensionadoDTO = new ArrayList<AtencionPensionadoDTO>();
	}

	public void buscarPensionado() {
		LOGGER.debug("rut:" + this.run);
		Long runAux = Long.valueOf(0);
		try {
			if (null != this.atencionPensionadoDTO) {
				this.atencionPensionadoDTO.clear();
			} else {
				this.atencionPensionadoDTO = new ArrayList<>();
			}
			if (super.obtenerRun(this.run)!=null) {
				runAux = super.obtenerRun(this.run);
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				List<Pensionados> pensionado = pensionadosServices.getPensionadoPorRun(runAux);

				if (null != pensionado && pensionado.size() > 0) {
					this.fechaNacimiento = pensionado.get(0).getPenFecNacimiento() != null ? dateFormat.format(pensionado.get(0).getPenFecNacimiento()) : "";
					this.edad = pensionado.get(0).getPenEdadInterna().toString();

					if ((!"".equals(pensionado.get(0).getPenNombres()) && null != pensionado.get(0).getPenNombres())
							&& (!"".equals(pensionado.get(0).getPenApePaterno())) && null != pensionado.get(0).getPenNombres()) {
						this.nombre = pensionado.get(0).getPenNombres() + " " + pensionado.get(0).getPenApePaterno();
					} else {
						this.nombre = "-";
					}

					if (1 == Integer.valueOf(pensionado.get(0).getPenVivo())){
						this.estado = "Vivo";
					} else {
						this.estado = "Fallecido";
					}

					if (null != pensionado.get(0)) {
						if (!"1".equals(pensionado.get(0).getPenVivo())) {
							this.respuesta = "Fallecido";
							this.codigoRespuesta = TipoRespuestaEnum.FALLECIDO.getCodigo();
						} else if (1 == pensionado.get(0).getPenBeneficiario()) {
							this.respuesta = "Beneficiario de Exención";
							this.codigoRespuesta = TipoRespuestaEnum.BENIFICIARIO_EXENCION.getCodigo();
						} else if (0 == pensionado.get(0).getPenResidencia()) {
							this.respuesta = "No cumple residencia";
							this.codigoRespuesta = TipoRespuestaEnum.NO_CUMPLE_RESIDENCIA.getCodigo();
						} else if (0 == pensionado.get(0).getPenFocalizacion()) {
							this.respuesta = "No focaliza";
							this.codigoRespuesta = TipoRespuestaEnum.NO_FOCALIZA.getCodigo();
						} else {
							this.respuesta = "-";
							this.codigoRespuesta = TipoRespuestaEnum.NO_ESTA_PENSIONADO.getCodigo();
						}
					}

					List<String> institucionesPagadoras =  consultaBeneficioServices.obtenerNombreInstituciones(runAux.intValue());

					if (null != institucionesPagadoras && institucionesPagadoras.size() > 0) {
						StringBuilder instituciones = new StringBuilder();
						for (String nombre : institucionesPagadoras) {
							instituciones.append(nombre);
							instituciones.append(",");
						}

						this.institucionPagadora = instituciones.toString();

						if (!"".equals(this.institucionPagadora)) {
							this.institucionPagadora = this.institucionPagadora.substring(0, this.institucionPagadora.length() - 1);
						} else {
							this.institucionPagadora = "-";
						}
					}

					List<ConsultaBeneficio> consultaBeneficios = consultaBeneficioServices.getConsultaBeneficio(runAux.intValue());
					if (null != consultaBeneficios) {
						SimpleDateFormat dateFormatCon = new SimpleDateFormat("dd/MM/yyyy");
						for (ConsultaBeneficio c : consultaBeneficios) {
							AtencionPensionadoDTO atencionPensionado = new AtencionPensionadoDTO();
							atencionPensionado.setFecha(c.getConBenFecConsulta() != null ? dateFormat.format(c.getConBenFecConsulta()):"");
							atencionPensionado.setInstitucionPagadora(c.getCanBenInstituciones());
							atencionPensionado.setObservacion(c.getConBenObservacionUsu());
							atencionPensionado.setPeriodo(c.getConBenPeriodo().toString());
							for (TipoRespuesta tp : c.getTipoRespuestas()) {
								atencionPensionado.setRespuesta(tp.getTipRespDescTecnica());
							}
							if ("".equals(atencionPensionado.getRespuesta()) || null == atencionPensionado.getRespuesta()) {
								atencionPensionado.setRespuesta("-");
							}
							atencionPensionado.setUsuario(c.getCanBenUsuario());
							this.atencionPensionadoDTO.add(atencionPensionado);
						}
					}
					this.mostrarGrilla = true;

				} else {
					MensajesUtil.setMensajeErrorContexto("validacion.atencion.pensionado.rut.noexiste");
					super.mostrarMensajeError();
					this.limpiar();
				}
			} else {
				MensajesUtil
						.setMensajeErrorContexto("validacion.movimientos.rut.completar.campos");
				super.mostrarMensajeError();
			}
		} catch (DataAccessException ex) {
			LOGGER.info(ex.getMessage());
		}
	}

	public void recargarRespuestas() {
		LOGGER.debug("rut:" + this.run);
		Long runAux = Long.valueOf(0);
		runAux = super.obtenerRun(this.run);
		if (!"".equalsIgnoreCase(this.run)){
			try {
				this.atencionPensionadoDTO.clear();
				List<ConsultaBeneficio> consultaBeneficios = consultaBeneficioServices.getConsultaBeneficio(runAux.intValue());
				if (null != consultaBeneficios) {
					SimpleDateFormat dateFormatCon = new SimpleDateFormat("dd/MM/yyyy");
					for (ConsultaBeneficio c : consultaBeneficios) {
						AtencionPensionadoDTO atencionPensionado = new AtencionPensionadoDTO();
						atencionPensionado.setFecha(c.getConBenFecConsulta() != null ? dateFormatCon.format(c.getConBenFecConsulta()) : "");
						atencionPensionado.setInstitucionPagadora(c.getCanBenInstituciones());
						atencionPensionado.setObservacion(c.getConBenObservacionUsu());
						atencionPensionado.setPeriodo(c.getConBenPeriodo().toString());
						for (TipoRespuesta tp : c.getTipoRespuestas()) {
							atencionPensionado.setRespuesta(tp.getTipRespDescTecnica());
						}
						if ("".equals(atencionPensionado.getRespuesta()) || null == atencionPensionado.getRespuesta()) {
							atencionPensionado.setRespuesta("-");
						}
						atencionPensionado.setUsuario(c.getCanBenUsuario());
						this.atencionPensionadoDTO.add(atencionPensionado);
					}
				}
				this.mostrarGrilla = true;
			}
			catch (DataAccessException ex) {
				LOGGER.info(ex.getMessage());
			}

		}
		else {
			MensajesUtil
					.setMensajeErrorContexto("validacion.movimientos.rut.completar.campos");
			super.mostrarMensajeError();
		}

	}

	public void guardar() {
		Long runAux;
		String dv;
		Integer periodoActual;
		try {
			periodoActual = super.obtenerPeriodoActual();
			runAux = super.obtenerRun(this.run);
			String usuario = super.getSessionBean().getUsername();
			dv = this.run.substring(this.run.length() - 1);
			if (!"".equals(this.observacion)&&this.observacion.trim().length()>0){
                consultaBeneficioServices.guardarConsultaBeneficio(runAux.intValue(), dv,
                        periodoActual, this.observacion,
						usuario, this.institucionPagadora, this.codigoRespuesta);
                RequestContext requestContext = RequestContext.getCurrentInstance();
                requestContext.execute("showComentario()");
                this.observacion=null;
                this.recargarRespuestas();


			}else{
                MensajesUtil.setMensajeErrorContexto("validacion.atencion.pensionado.respuesta.vacia");
                super.mostrarMensajeError();
            }

		} catch (DataAccessException ex) {
			LOGGER.info(ex.getMessage());
		}

	}

	public void obtenerExcel() throws IOException {

		try {
			File archivo = Reportes.obtenerAtencionPensionado(this.atencionPensionadoDTO);
			super.generarExcel(archivo);
		} catch (FileNotFoundException e) {
			LOGGER.error("Archivo no encontrado: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("Ha ocurrido un error: " + e.getMessage());
		}
	}

	public void limpiar() {
		this.atencionPensionadoDTO = null;
		this.run = "";
		this.mostrarGrilla = Boolean.FALSE;
	}

	public String getRun() {
		return run;
	}

	public void setRun(String run) {
		this.run = run;
	}

	public List<AtencionPensionadoDTO> getAtencionPensionadoDTO() {
		return atencionPensionadoDTO;
	}

	public void setAtencionPensionadoDTO(List<AtencionPensionadoDTO> atencionPensionadoDTO) {
		this.atencionPensionadoDTO = atencionPensionadoDTO;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public boolean isMostrarGrilla() {
		return mostrarGrilla;
	}

	public void setMostrarGrilla(boolean mostrarGrilla) {
		this.mostrarGrilla = mostrarGrilla;
	}

	public String getInstitucionPagadora() {
		return institucionPagadora;
	}

	public void setInstitucionPagadora(String institucionPagadora) {
		this.institucionPagadora = institucionPagadora;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Integer getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(Integer codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
