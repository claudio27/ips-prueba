package cl.gob.ips.exencion.administracion.view.converter;

import cl.gob.ips.exencion.model.Usuarios;
import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import java.util.List;

/**
 * Created by practicante on 21-08-17.
 */
@FacesConverter("usuariosConverter")
public class UsuariosConverter implements Converter {

    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        return getObjectFromUIPickListComponent(component,value);
    }

    public String getAsString(FacesContext context, UIComponent component, Object object) {
        String string;
        if(object == null){
            string="";
        }else{
            try{
                string = String.valueOf(((Usuarios)object).getUsuCod());
            }catch(ClassCastException cce){
                throw new ConverterException();
            }
        }
        return string;
    }

    @SuppressWarnings("unchecked")
    private Usuarios getObjectFromUIPickListComponent(UIComponent component, String value) {
        final DualListModel<Usuarios> dualList;
        try{
            dualList = (DualListModel<Usuarios>) ((PickList)component).getValue();
            Usuarios usuarios = getObjectFromList(dualList.getSource(),Integer.valueOf(value));
            if(usuarios==null){
                usuarios = getObjectFromList(dualList.getTarget(),Integer.valueOf(value));
            }

            return usuarios;
        }catch(ClassCastException cce){
            throw new ConverterException();
        }catch(NumberFormatException nfe){
            throw new ConverterException();
        }
    }

    private Usuarios getObjectFromList(final List<?> list, final Integer identifier) {
        for(final Object object:list){
            final Usuarios usuarios = (Usuarios) object;
            if(usuarios.getUsuCod().equals(identifier)){
                return usuarios;
            }
        }
        return null;
    }
    }



