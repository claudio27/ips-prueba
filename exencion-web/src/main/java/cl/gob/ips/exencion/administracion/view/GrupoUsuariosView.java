package cl.gob.ips.exencion.administracion.view;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.model.*;
import cl.gob.ips.exencion.service.*;
import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by practicante
 */
@ManagedBean(name = "grupoUsuariosView")
@ViewScoped
public class GrupoUsuariosView extends BaseControllerView implements Serializable {

    private static final long serialVersionUID = 3428153268516734489L;
    private static final Logger LOGGER = Logger.getLogger(GrupoUsuariosView.class);


    @Inject
    private transient GrupoUsuariosService grupoUsuariosService;

    @Inject
    private transient GrupoUsuarioUsuariosService grupoUsuarioUsuariosService;

    @Inject
    private transient GrupoUsuarioFuncionesService grupoUsuarioFuncionesService;

    @Inject
    private transient UsuariosServices usuariosServices;

    @Inject
    private transient FuncionalidadesService funcionalidadesService;


    private String descripcionUltimoGrupo;

    private String nombreUsuarioBuscar;

    private GrupoUsuarios grupoUsuariosCreate;
    private GrupoUsuarios grupoUsuariosDelete;
    private GrupoUsuarios grupoUsuarios;
    private List<GrupoUsuarios> listaGruposUsuarios;

    private GrupoUsuarioUsuarios grupoUsuarioUsuariosCreate;

    private DualListModel<Usuarios> usuarios;
    private DualListModel<Funcionalidades> funcionalidades;

    private List<Usuarios> listaUsuariosAuxAdd;
    private List<Usuarios> listaUsuariosAuxRemove;
    private List<Funcionalidades> listaFuncionalidadesAuxAdd;
    private List<Funcionalidades> listaFuncionalidadesAuxRemove;

    @PostConstruct
    public void init() {
        this.initFields();
    }

    private void initFields() {
        try{
            grupoUsuarios = new GrupoUsuarios();
            grupoUsuariosCreate = new GrupoUsuarios();
            grupoUsuariosDelete = new GrupoUsuarios();
            grupoUsuarioUsuariosCreate = new GrupoUsuarioUsuarios(new GrupoUsuarios(), new Usuarios());
            usuarios = new DualListModel<Usuarios>();
            funcionalidades = new DualListModel<Funcionalidades>();
            listaUsuariosAuxAdd=new ArrayList<Usuarios>();
            listaUsuariosAuxRemove=new ArrayList<Usuarios>();
            listaFuncionalidadesAuxAdd = new ArrayList<Funcionalidades>();
            listaFuncionalidadesAuxRemove = new ArrayList<Funcionalidades>();
            listaGruposUsuarios = grupoUsuariosService.getAllGrupos();
        }catch(DataAccessException e){
            LOGGER.error("Error en el acceso a datos");
        }

    }

    public GrupoUsuarios getGrupoUsuariosCreate() {
        return grupoUsuariosCreate;
    }

    public void setGrupoUsuariosCreate(GrupoUsuarios grupoUsuariosCreate) {
        this.grupoUsuariosCreate = grupoUsuariosCreate;
    }

    public GrupoUsuarios getGrupoUsuariosDelete() {
        return grupoUsuariosDelete;
    }

    public void setGrupoUsuariosDelete(GrupoUsuarios grupoUsuariosDelete) {
        this.grupoUsuariosDelete = grupoUsuariosDelete;
    }

    public GrupoUsuarioUsuarios getGrupoUsuarioUsuariosCreate() {
        return grupoUsuarioUsuariosCreate;
    }

    public void setGrupoUsuarioUsuariosCreate(GrupoUsuarioUsuarios grupoUsuarioUsuariosCreate) {
        this.grupoUsuarioUsuariosCreate = grupoUsuarioUsuariosCreate;
    }

    public GrupoUsuarios getGrupoUsuarios() {
        return grupoUsuarios;
    }

    public void setGrupoUsuarios(GrupoUsuarios grupoUsuarios) {
        this.grupoUsuarios = grupoUsuarios;
    }

    public String getDescripcionUltimoGrupo() {
        return descripcionUltimoGrupo;
    }

    public void setDescripcionUltimoGrupo(String descripcionUltimoGrupo) {
        this.descripcionUltimoGrupo = descripcionUltimoGrupo;
    }

    public String getNombreUsuarioBuscar() {
        return nombreUsuarioBuscar;
    }

    public void setNombreUsuarioBuscar(String nombreUsuarioBuscar) {
        this.nombreUsuarioBuscar = nombreUsuarioBuscar;
    }


    public DualListModel<Usuarios> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(DualListModel<Usuarios> usuarios) {
        this.usuarios = usuarios;
    }

    public DualListModel<Funcionalidades> getFuncionalidades() {
        return funcionalidades;
    }

    public void setFuncionalidades(DualListModel<Funcionalidades> funcionalidades) {
        this.funcionalidades = funcionalidades;
    }

    public List<Usuarios> getListaUsuariosAuxAdd() {
        return listaUsuariosAuxAdd;
    }

    public void setListaUsuariosAuxAdd(List<Usuarios> listaUsuariosAuxAdd) {
        this.listaUsuariosAuxAdd = listaUsuariosAuxAdd;
    }

    public List<Usuarios> getListaUsuariosAuxRemove() {
        return listaUsuariosAuxRemove;
    }

    public void setListaUsuariosAuxRemove(List<Usuarios> listaUsuariosAuxRemove) {
        this.listaUsuariosAuxRemove = listaUsuariosAuxRemove;
    }

    public List<Funcionalidades> getListaFuncionalidadesAuxAdd() {
        return listaFuncionalidadesAuxAdd;
    }

    public void setListaFuncionalidadesAuxAdd(List<Funcionalidades> listaFuncionalidadesAuxAdd) {
        this.listaFuncionalidadesAuxAdd = listaFuncionalidadesAuxAdd;
    }

    public List<Funcionalidades> getListaFuncionalidadesAuxRemove() {
        return listaFuncionalidadesAuxRemove;
    }

    public void setListaFuncionalidadesAuxRemove(List<Funcionalidades> listaFuncionalidadesAuxRemove) {
        this.listaFuncionalidadesAuxRemove = listaFuncionalidadesAuxRemove;
    }


    public List<GrupoUsuarios> getListaGruposUsuarios() {
        return listaGruposUsuarios;
    }

    public void setListaGruposUsuarios(List<GrupoUsuarios> listaGruposUsuarios) {
        this.listaGruposUsuarios = listaGruposUsuarios;
    }

    public String grupoUsuariosEliminar(GrupoUsuarios item){
        this.setGrupoUsuariosDelete(item);
        return "grupo-usuarios";
    }

    public void eliminarGrupoUsuarios() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        try {

            this.grupoUsuariosService.eliminarGrupoUsuarios(this.getGrupoUsuariosDelete());
            this.listaGruposUsuarios = grupoUsuariosService.getAllGrupos();
            requestContext.execute("$('.modalBlockUI').modal('hide');$('.modalPseudoClass3').modal()");

        } catch (Exception e) {
            requestContext.execute("$('.modalBlockUI').modal('hide');");
            MensajesUtil.setMensajeErrorContexto("validaciones.grupo.usuarios.eliminar.grupo");
            super.mostrarMensajeError();
            LOGGER.error("Para eliminar el grupo de usuarios, este no debe tener asociaciones con usuarios o funcionalidades");
        }
    }

    public void doCrearGrupoUsuarios() {
        RequestContext requestContext = RequestContext.getCurrentInstance();

        try {

            if (this.grupoUsuariosCreate == null) {
                throw new IpsBussinesException("el grupo de usuario es null");
            }

            if (this.grupoUsuariosCreate.getGruUsuDescripcion() != null
                    && !"".equals(this.grupoUsuariosCreate.getGruUsuDescripcion()) && this.grupoUsuariosCreate.getGruUsuDescripcion().trim().length()>0) {

                if (this.grupoUsuariosService.buscarGrupoUsuario(this.grupoUsuariosCreate.getGruUsuDescripcion()) != null) {
                    requestContext.execute("$('.modalBlockUI').modal('hide');");
                    MensajesUtil.setMensajeErrorContexto("validacion.grupo.usuarios.agregar.grupo");
                    super.mostrarMensajeError();
                    LOGGER.error("Error, el grupo ya existe");
                    requestContext.execute("$('#paso1-modal-nuevogrupo').modal('show');");
                    return;
                }

                this.grupoUsuariosService.crearGrupoUsuarios(this.getGrupoUsuariosCreate());
                this.listaGruposUsuarios = this.grupoUsuariosService.getAllGrupos();
                this.setDescripcionUltimoGrupo(this.getGrupoUsuariosCreate().getGruUsuDescripcion());
                requestContext.execute("$('.modalBlockUI').modal('hide');showVer();");
                LOGGER.info("Se ha registrado el grupo de usuarios exitosamente");

                this.grupoUsuariosCreate = new GrupoUsuarios();
            } else {
                requestContext.execute("$('.modalBlockUI').modal('hide');");
                MensajesUtil.setMensajeErrorContexto("validacion.campos.texto");
                super.mostrarMensajeError();
                requestContext.execute("$('#paso1-modal-nuevogrupo').modal('show');");
                return;
            }

            requestContext.execute("$('#modal-nuevogrupo').modal('hide');$('#paso1-modal-nuevogrupo').modal('hide');");

            this.grupoUsuariosCreate.setGruUsuDescripcion("");
        } catch (DataAccessException | IpsBussinesException e) {
            MensajesUtil.setMensajeErrorContexto("error.general");
            super.mostrarMensajeError();
            LOGGER.error("Error al crear grupos de usuario, razón: ", e);
        }
    }

    public void doCrearGrupoUsuarioUsuarios() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        try {
            if (this.grupoUsuarioUsuariosCreate == null){
                throw new IpsBussinesException("El objeto GrupoUsuarioUsuarios es null");
            }

            if (!"".equals(this.nombreUsuarioBuscar)
                    && this.usuariosServices.encontrarUsuario(this.nombreUsuarioBuscar) != null) {

                if (this.usuariosServices.validarUsuarioPerteneceGru(this.nombreUsuarioBuscar, this.grupoUsuarios.getGruUsuCod()) !=null) {
                    requestContext.execute("$('.modalBlockUI').modal('hide');");
                    MensajesUtil.setMensajeErrorContexto("validacion.grupo.usuarios.agregar.usuarios");
                    super.mostrarMensajeError();
                    requestContext.execute("$('#user-admin').modal('show');$('#modal-agrega-usuario').modal('hide');");

                    requestContext.execute("$('#modal-agrega-usuario').modal('show');");

                    LOGGER.error("Usuario ya se encuentra registrado en este grupo");

                    return;
                }

                this.getGrupoUsuarioUsuariosCreate().setTbGrupoUsuarios1(
                        this.grupoUsuariosService.buscarGrupoUsuario(this.grupoUsuarios.getGruUsuDescripcion()));
                this.getGrupoUsuarioUsuariosCreate().setTbUsuarios(this.usuariosServices.encontrarUsuario(this.nombreUsuarioBuscar));

                this.grupoUsuarioUsuariosService.createGrupoUsuarioUsuarios(this.getGrupoUsuarioUsuariosCreate());

                requestContext.execute("$('.modalBlockUI').modal('hide');showAgregarUsuPaso2();");
                LOGGER.info("Se ha registrado el usuario en el grupo exitosamente");
                this.grupoUsuarioUsuariosCreate = new GrupoUsuarioUsuarios();

            } else {
                requestContext.execute("$('.modalBlockUI').modal('hide');");
                MensajesUtil.setMensajeErrorContexto("validacion.grupo.usuarios.agregar.usuarios.!existe");
                super.mostrarMensajeError();
                requestContext.execute("$('#user-admin').modal('show');$('#modal-agrega-usuario').modal('hide');");

                requestContext.execute("$('#modal-agrega-usuario').css({\n" +
                        "                        'display' : 'initial'\n" +
                        "                    });");

                requestContext.execute("$('#modal-agrega-usuario').modal('show');");

                LOGGER.error("Usuario no existe");
            }

            //requestContext.execute("$('#user-admin').modal('show');$('#modal-agrega-usuario').modal('show');");

        } catch (DataAccessException | IpsBussinesException e) {
            MensajesUtil.setMensajeErrorContexto("validacion.grupo.usuarios.agregar.usuarios");
            super.mostrarMensajeError();
            LOGGER.error("Error al registrar usuario en grupo: ", e);
        }
    }

    /**
     * @param grupoUsuariosTabla
     * @return
     */
    public String listaUsuariosGrupos(GrupoUsuarios grupoUsuariosTabla) throws DataAccessException {
        this.setGrupoUsuarios(grupoUsuariosTabla);
        this.usuarios.setSource(this.usuariosServices.getAllNoUsuariosNoPerteneceGrupo(this.grupoUsuarios.getGruUsuCod()));
        this.usuarios.setTarget(this.usuariosServices.getAllUsuariosPerteneceGrupo(this.grupoUsuarios.getGruUsuCod()));
        return "listaUsuariosGrupo";
    }

    /**
     * @param grupoUsuariosTabla
     * @return
     */
    public String listaFuncionesGrupos(GrupoUsuarios grupoUsuariosTabla) throws DataAccessException {
        this.setGrupoUsuarios(grupoUsuariosTabla);
        this.funcionalidades.setSource(this.funcionalidadesService.getAllFuncionalidadesNoPerteneceGrupo(this.grupoUsuarios.getGruUsuCod()));
        this.funcionalidades.setTarget(this.funcionalidadesService.getAllFuncionalidadesPerteneceGrupo(this.grupoUsuarios.getGruUsuCod()));
        return "listaFuncionesGrupos";
    }

    public void onTransferUsers(TransferEvent event) {

        for (Object item : event.getItems()) {
            Usuarios usu = (Usuarios) item;
            if (event.isAdd()) {
                if (!this.listaUsuariosAuxAdd.contains(usu)){
                    this.listaUsuariosAuxAdd.add(usu);
                    if (this.listaUsuariosAuxRemove.contains(usu)){
                        this.listaUsuariosAuxRemove.remove(usu);
                    }
                }

            } else if (event.isRemove()) {
                if (!this.listaUsuariosAuxRemove.contains(usu)){
                    this.listaUsuariosAuxRemove.add(usu);
                    if (this.listaUsuariosAuxAdd.remove(usu)){
                        this.listaUsuariosAuxAdd.remove(usu);
                    }
                }
            }
        }
    }

    public void onTransferFunctions(TransferEvent event) {
        for (Object item : event.getItems()) {
            Funcionalidades fun = (Funcionalidades) item;
            if (event.isAdd()) {
                if (!this.listaFuncionalidadesAuxAdd.contains(fun)){
                    this.listaFuncionalidadesAuxAdd.add(fun);
                    if (this.listaFuncionalidadesAuxRemove.contains(fun)){
                        this.listaFuncionalidadesAuxRemove.remove(fun);
                    }
                }
            } else if (event.isRemove()) {
                if (!this.listaFuncionalidadesAuxRemove.contains(fun)){
                    this.listaFuncionalidadesAuxRemove.add(fun);
                    if (this.listaFuncionalidadesAuxAdd.remove(fun)){
                        this.listaFuncionalidadesAuxAdd.remove(fun);
                    }
                }
            }
        }
    }

    public void moverUsuariosGrupo() {
        try{
            if (!this.listaUsuariosAuxAdd.isEmpty()){
                for (Usuarios uAdd : this.listaUsuariosAuxAdd){
                    GrupoUsuarioUsuarios gUAdd = new GrupoUsuarioUsuarios(new GrupoUsuarios(), new Usuarios());
                    gUAdd.setTbGrupoUsuarios1(this.grupoUsuariosService.buscarGrupoUsuario(this.grupoUsuarios.getGruUsuDescripcion()));

                    gUAdd.setTbUsuarios(uAdd);

                    if (this.usuariosServices.validarUsuarioPerteneceGru(gUAdd.getTbUsuarios().getUsuUsuario(), gUAdd.getTbGrupoUsuarios1().getGruUsuCod())==null){
                        this.grupoUsuarioUsuariosService.createGrupoUsuarioUsuarios(gUAdd);

                    }
                }
                this.listaUsuariosAuxAdd = new ArrayList<Usuarios>();
            }
            if (!this.listaUsuariosAuxRemove.isEmpty()){
                for (Usuarios uRemove : this.listaUsuariosAuxRemove){
                    GrupoUsuarioUsuarios gURemove = new GrupoUsuarioUsuarios(new GrupoUsuarios(), new Usuarios());
                    gURemove.setTbGrupoUsuarios1(this.grupoUsuariosService.buscarGrupoUsuario(this.grupoUsuarios.getGruUsuDescripcion()));
                    gURemove.setTbUsuarios(uRemove);

                    if (this.usuariosServices.validarUsuarioNoPerteneceGru(gURemove.getTbUsuarios().getUsuUsuario(), gURemove.getTbGrupoUsuarios1().getGruUsuCod())==null){
                        this.grupoUsuarioUsuariosService.eliminarGrupoUsuarioUsuarios(gURemove);

                    }

                }
                this.listaUsuariosAuxRemove = new ArrayList<Usuarios>();
            }
        }catch(DataAccessException e){
            LOGGER.error("Error al mover usuarios: ", e);
            MensajesUtil.setMensajeErrorContexto("error.general");
            super.mostrarMensajeError();
        }
    }

    public void moverFuncionesGrupo() {
        try {
            if (!this.listaFuncionalidadesAuxAdd.isEmpty()){
                for (Funcionalidades fAdd : this.listaFuncionalidadesAuxAdd) {
                    GrupoUsuarioFunciones gFAdd = new GrupoUsuarioFunciones(new Funcionalidades(), new GrupoUsuarios());
                    gFAdd.setTbPrFuncionalidades(fAdd);
                    gFAdd.setTbGrupoUsuarios(this.grupoUsuariosService.buscarGrupoUsuario(this.grupoUsuarios.getGruUsuDescripcion()));
                    if (this.funcionalidadesService.validarFuncionalidadPerteneceGru(gFAdd.getTbPrFuncionalidades().getFuncDescripcion(), gFAdd.getTbGrupoUsuarios().getGruUsuCod())==null){
                        this.grupoUsuarioFuncionesService.createGrupoUsuarioFunciones(gFAdd);

                    }
                }
                this.listaFuncionalidadesAuxAdd = new ArrayList<Funcionalidades>();
            }
            if (!this.listaFuncionalidadesAuxRemove.isEmpty()){
                for (Funcionalidades fRemove : this.listaFuncionalidadesAuxRemove){
                    GrupoUsuarioFunciones gFRemove = new GrupoUsuarioFunciones(new Funcionalidades(), new GrupoUsuarios());
                    gFRemove.setTbGrupoUsuarios(this.grupoUsuariosService.buscarGrupoUsuario(this.grupoUsuarios.getGruUsuDescripcion()));
                    gFRemove.setTbPrFuncionalidades(fRemove);
                    if (this.funcionalidadesService.validarFuncionalidadNoPerteneceGru(gFRemove.getTbPrFuncionalidades().getFuncDescripcion(), gFRemove.getTbGrupoUsuarios().getGruUsuCod())==null){
                        this.grupoUsuarioFuncionesService.eliminarGrupoUsuarioFunciones(gFRemove);

                    }
                }

                this.listaFuncionalidadesAuxRemove = new ArrayList<Funcionalidades>();
            }
        }catch(DataAccessException e){
            LOGGER.error("Error al mover funciones: ", e);
            MensajesUtil.setMensajeErrorContexto("error.general");
            super.mostrarMensajeError();
        }
    }



}