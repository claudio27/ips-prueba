package cl.gob.ips.exencion.informes.view;

import cl.gob.ips.exencion.dto.ResultadoValidacionArchivosDTO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.ArchivosCargaPeriodo;
import cl.gob.ips.exencion.service.PropiedadesService;
import cl.gob.ips.exencion.service.ResultadoValidacionArchivosServices;
import cl.gob.ips.exencion.service.util.SeleccionadorDeNominas;
import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.util.Reportes;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static javax.faces.context.FacesContext.getCurrentInstance;

@ManagedBean(name = "resultadoValidacionArchivosView")
@ViewScoped
public class ResultadoValidacionArchivosView extends BaseControllerView implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 192940131497711844L;

	private static final Logger LOGGER = Logger.getLogger(ResultadoValidacionArchivosView.class);

	@Inject
	private ResultadoValidacionArchivosServices validacionArchivosServices;

	@Inject
	private SeleccionadorDeNominas seleccionadorDeNominas;

	@Inject
	private PropiedadesService propiedadesService;

	private Date periodo;

	private Boolean mostrarGrilla;

	private Boolean disableDescarga;

	private List<ResultadoValidacionArchivosDTO> respuestaGrilla;

	private String mensajeErrorCargaArchivo;

	@PostConstruct
	private void init(){
		try{
			this.mostrarGrilla = Boolean.FALSE;
			this.disableDescarga = Boolean.TRUE;

			Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
			if (params.containsKey("periodo")){
				String period = params.get("periodo");
				this.periodo = obtenerFecha(period);

				if (this.periodo!=null){
					this.buscar();
				}
			}

		}catch(ParseException e){
			LOGGER.error("Al convertir parametros de fechas", e);

		}

	}

	public void archivoSelectedTable(String mensaje){
		this.setMensajeErrorCargaArchivo(mensaje);
	}

	public Date obtenerFecha(String periodo) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = sdf.parse(periodo);
		return date;
	}

	public void buscar() {
		this.respuestaGrilla = new ArrayList<ResultadoValidacionArchivosDTO>();
		LOGGER.info("Consultando periodo : " + obtenerPeriodoEntero(periodo));

		List<ArchivosCargaPeriodo> listaArchivosValidados = new ArrayList<>();

		try{

			listaArchivosValidados = this.validacionArchivosServices.
					obtenerResultadoValidacionArchivosDAO(obtenerPeriodoEntero(this.periodo));

		}catch (EJBException | DataAccessException e){
			super.muestraErrorDeAccesoaDatos(e);
			return;
		}

		this.mapeoDto(listaArchivosValidados, this.respuestaGrilla);

		if(this.respuestaGrilla == null || this.respuestaGrilla.isEmpty()){
			MensajesUtil.setMensajeErrorContexto("resultado.validacion.archivos.no.registros");
			super.mostrarMensajeError();
			LOGGER.info("No hay datos para el rango de periodos consultado");
			limpiar();
			return;
		}

		this.mostrarGrilla = Boolean.TRUE;
	}

	public void limpiar() {
		this.periodo = null;
		this.mostrarGrilla = Boolean.FALSE;
	}

	public void obtenerExcel() {
		try {
			File archivo = Reportes.obtenerResultadoValidacionArchivos(this.respuestaGrilla);
			super.generarExcel(archivo);
		} catch (FileNotFoundException e) {
			LOGGER.error("Archivo no encontrado: ", e);
		} catch (IOException e) {
			LOGGER.error("Ha ocurrido un error: ", e);
		}
	}

	public Date getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Date periodo) {
		this.periodo = periodo;
	}

	public Boolean getMostrarGrilla() {
		return mostrarGrilla;
	}

	public void setMostrarGrilla(Boolean mostrarGrilla) {
		this.mostrarGrilla = mostrarGrilla;
	}

	public List<ResultadoValidacionArchivosDTO> getRespuestaGrilla() {
		return respuestaGrilla;
	}

	public void setRespuestaGrilla(
			List<ResultadoValidacionArchivosDTO> respuestaGrilla) {
		this.respuestaGrilla = respuestaGrilla;
	}

	public Boolean getDisableDescarga() {
		return disableDescarga;
	}

	public void setDisableDescarga(Boolean disableDescarga) {
		this.disableDescarga = disableDescarga;
	}

	public String getMensajeErrorCargaArchivo() {
		return mensajeErrorCargaArchivo;
	}

	public void setMensajeErrorCargaArchivo(String mensajeErrorCargaArchivo) {
		this.mensajeErrorCargaArchivo = mensajeErrorCargaArchivo;
	}

	private void mapeoDto(List<ArchivosCargaPeriodo> listaArchivosValidados, List<ResultadoValidacionArchivosDTO> respuestaGrilla){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		for (ArchivosCargaPeriodo res : listaArchivosValidados) {
			ResultadoValidacionArchivosDTO obj = new ResultadoValidacionArchivosDTO();
			Boolean flag = false;

			obj.setId(res.getArcId() != null ? res.getArcId():0);
			obj.setNombre(res.getArchNombreArchivo() != null ? res.getArchNombreArchivo():"");
			obj.setNombreArchivoCargado(res.getArcNombreArchivoCargado() != null ? res.getArcNombreArchivoCargado():"");
			obj.setFechaCarga(res.getArcFechaCargar()!=null?obtenerFechaFormato(res.getArcFechaCargar()):"");
			obj.setRegistrosInformados(res.getArcCantRegInfo()!=null ? res.getArcCantRegInfo():0);
			obj.setRegistrosCargados(res.getArcCantRegCargados() != null ? res.getArcCantRegCargados():0);
			obj.setFilasSinDatos(res.getArcCantRegSinDatos()!=null ? res.getArcCantRegSinDatos():0);
			obj.setRegistrosIlegibles(res.getArcCantRegIlegibles()!=null ? res.getArcCantRegIlegibles():0);
			obj.setFormatoNombre(res.getArcFormatoNombre()!=null ? res.getArcFormatoNombre():"");
			obj.setEstructura(res.getArcEstructura()!=null ? res.getArcEstructura():"");
			obj.setResultadoCarga(res.getTbPrEstCarga().getEstCarDescripcion()!=null?res.getTbPrEstCarga().getEstCarDescripcion():"");

			if(res.getTbPrEstCarga().getEstCarId()== 3){

				flag = true;
			}

			validarDescargaArchivo(obj);

			obj.setFallido(flag);
			obj.setNombreArchInicial(res.getTbPrArchivosIniciales().getArcIniNombre()!=null?res.getTbPrArchivosIniciales().getArcIniNombre():""); // todo echarsela
			obj.setMensajeError(res.getArcMensajeError()!=null?res.getArcMensajeError():"");

			this.respuestaGrilla.add(obj);
		}
	}

	public void descargarArchivoValidado(String nombreArchivoCargado,String resultadoCarga){ // todo determinar si fue exitoso o no

		LOGGER.info("descargarArchivoValidado ( " + obtenerPeriodoEntero(this.periodo) + ", " + nombreArchivoCargado + " )");
		FacesContext fc;
		ExternalContext ec;
		OutputStream output;


		String user;
		String pass;
		String server;
		String carpeta;
		String remotePath;

		if(resultadoCarga.indexOf("Exitoso")>=0){
			user = this.propiedadesService.propiedadesFTP("FTP_USUARIO_EXITOSOS").getPropValor();
			pass = this.propiedadesService.propiedadesFTP("FTP_CLAVE_EXITOSOS").getPropValor();
			server = this.propiedadesService.propiedadesFTP("FTP_URL_EXITOSOS").getPropValor();
			carpeta = this.propiedadesService.propiedadesFTP("FTP_CARPETA_EXITOSOS").getPropValor();
			remotePath = carpeta + "/" + nombreArchivoCargado;
		}else{
			user = this.propiedadesService.propiedadesFTP("FTP_USUARIO_FALLIDOS").getPropValor();
			pass = this.propiedadesService.propiedadesFTP("FTP_CLAVE_FALLIDOS").getPropValor();
			server = this.propiedadesService.propiedadesFTP("FTP_URL_FALLIDOS").getPropValor();
			carpeta = this.propiedadesService.propiedadesFTP("FTP_CARPETA_FALLIDOS").getPropValor();
			remotePath =  carpeta + "/" + nombreArchivoCargado;
		}

		try {

			URL url = new URL("ftp://" + user + ":" + pass + "@" + server + remotePath + ";type=i");
			URLConnection urlCx = url.openConnection();
			InputStream is = urlCx.getInputStream();

			fc = getCurrentInstance();
			ec = fc.getExternalContext();

			ec.responseReset();
			ec.setResponseHeader("Content-Disposition", "attachment; filename=" + nombreArchivoCargado);
			ec.setResponseContentType("text/plain");
			output = ec.getResponseOutputStream();

			int c;
			while ((c = is.read()) != -1) {
				output.write(c);
			}

			output.flush();
			fc.responseComplete();
			is.close();

		} catch (FileNotFoundException e){
			MensajesUtil.setMensajeErrorContexto("Archivo no disponible para ser descargado");
			super.mostrarMensajeError();
			LOGGER.error("El archivo ya no existe en la carpeta del servidor, detalle: ",e);
		}
		catch (IOException e) {
			LOGGER.error("descargarArchivoValidado",e);
			super.muestraErrorDeAccesoaDatos(e);
		} finally {
			//IOUtils.closeQuietly(output);
			//IOUtils.closeQuietly(input);
		}
	}





	public Boolean validarDescargaArchivo(ResultadoValidacionArchivosDTO archivoGrilla){
		boolean flag = false;
		if (archivoGrilla.getResultadoCarga().equals("Fallido")||archivoGrilla.getResultadoCarga().equals("Exitoso")
				||archivoGrilla.getResultadoCarga().equals("Cargado")){
			flag = true;
		}
		return flag;
	}


	/*
	private void ss(){

		try {
			fc = getCurrentInstance();
			ec = fc.getExternalContext();

			ec.responseReset();
			ec.setResponseHeader("Content-Disposition", "attachment; filename=" + archivo.getName());
			ec.setResponseContentType("text/plain");

			output = ec.getResponseOutputStream();
			IOUtils.copy(input, output);
			fc.responseComplete();
		} catch (IOException e) {
			LOGGER.error("Error al descargar archivo validado", e);
			MensajesUtil.setMensajeErrorContexto("No se encuentra el archivo solicitado");
			this.mostrarMensajeError();
		} finally {
			IOUtils.closeQuietly(output);
			IOUtils.closeQuietly(input);
		}
	}

	public static boolean downloadFile(String server, String user, String pass, String localPath, String remotePath) {
		try {
			URL url = new URL("ftp://" + user + ":" + pass + "@" + server + remotePath + ";type=i");
			URLConnection urlc = url.openConnection();
			InputStream is = urlc.getInputStream();
			BufferedWriter bw = new BufferedWriter(new FileWriter(localPath));
			int c;
			while ((c = is.read()) != -1) {
				bw.write(c);

			}
			is.close();
			bw.flush();
			bw.close();
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
			return false;}
	}
	*/

}

