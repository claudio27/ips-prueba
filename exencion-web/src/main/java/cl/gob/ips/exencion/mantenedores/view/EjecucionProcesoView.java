package cl.gob.ips.exencion.mantenedores.view;


import cl.gob.ips.exencion.dto.PeriodoDTO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.*;
import cl.gob.ips.exencion.service.BitacoraServices;
import cl.gob.ips.exencion.service.EjecucionProcesoServices;
import cl.gob.ips.exencion.service.PeriodoServices;
import org.apache.log4j.Logger;


import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.view.base.BaseControllerView;

import cl.gob.ips.exencion.util.FacesUtils;



import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;


/**
 * Created by senshi.cristian@gmail.com on 22-11-2016.
 */
@ManagedBean(name = "ejecucionProcesoView")
@ViewScoped
public class EjecucionProcesoView extends BaseControllerView implements Serializable {

    public static final String FORMULARIO_PARAMETROS = "formulario_parametros";
    public static final String TITULO_ERROR = "Error";
    public static final String TITULO_ACTUALIZACION_EXITOSA = "Actualización Exitosa";
    public static final String MSG_EXITO_ACTUALIZAR = "Parametro de ejecución de proceso se ha actualizado con éxito";
    public static final String MSG_ERROR_ACTUALIZAR = "Se ha producido un error al actualizar parametro de ejecución de proceso";
    public static final String MSG_ERROR_BUSCAR = "Se ha producido un error al buscar los parametros de ejecución de proceso";
    public static final String MSG_ERROR_INIT = "Se ha producido un error al inicializar la pagina";

    private final transient Logger logger = Logger.getLogger(this.getClass());

    @Inject
    private transient EjecucionProcesoServices ejecucionProcesoService;
    @Inject
    private transient BitacoraServices bitacoraServices;
    @Inject
    private transient PeriodoServices periodoServices;

    private PeriodoDTO periodoInfo;

    private MantencionEjecucion ejecucionParametro;
    private List<MantencionEjecucion> ejecucionParametros;
    private MantencionEjecucion ejecucionParametro0;
    private MantencionEjecucion ejecucionParametro1;
    private MantencionEjecucion ejecucionParametro2;
    private MantencionEjecucion ejecucionParametro3;
    private MantencionEjecucion ejecucionParametro4;
    private MantencionEjecucion ejecucionParametro5;
    private MantencionEjecucion ejecucionParametro6;
    private Boolean checked;

    private Boolean changeParametro0;
    private Boolean changeParametro1;
    private Boolean changeParametro2;
    private Boolean changeParametro3;
    private Boolean changeParametro4;
    private Boolean changeParametro5;
    private Boolean changeParametro6;

    /**
     * metodo que se ejecuta al cargar la vista para los datos necesarios en el formulario
     */

    @PostConstruct
    public void init() {
        try {
            //inicializo
            this.setChangeParametro0(false);
            this.setChangeParametro1(false);
            this.setChangeParametro2(false);
            this.setChangeParametro3(false);
            this.setChangeParametro4(false);
            this.setChangeParametro5(false);
            this.setChangeParametro6(false);

            //Busco parametros
            this.buscarMantencionEjecucion();

            this.periodoInfo = this.periodoServices.obtenerPeriodoActualDTO();

        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_PARAMETROS, TITULO_ERROR, MSG_ERROR_INIT);
            logger.error(MSG_ERROR_INIT, e);
        }
    }

    /**
     * realiza busqueda de los parametros de ejecucion
     */
    public void buscarMantencionEjecucion() {
        logger.debug("buscarMantencionEjecucion");
        try {
            this.setEjecucionParametros(ejecucionProcesoService.findParametrosEjecucion());

            //cuenta cantidad de parametros
            if (this.getEjecucionParametros().size() == 7){
                this.setEjecucionParametro0(this.getEjecucionParametros().get(0));
                this.setEjecucionParametro1(this.getEjecucionParametros().get(1));
                this.setEjecucionParametro2(this.getEjecucionParametros().get(2));
                this.setEjecucionParametro3(this.getEjecucionParametros().get(3));
                this.setEjecucionParametro4(this.getEjecucionParametros().get(4));
                this.setEjecucionParametro5(this.getEjecucionParametros().get(5));
                this.setEjecucionParametro6(this.getEjecucionParametros().get(6));

                if (this.getEjecucionParametro6().getMantEjecValIni1() != null){
                    if (this.getEjecucionParametro6().getMantEjecValIni1() == 0){
                        this.setChecked(false);
                    }else{
                        this.setChecked(true);
                    }
                }else{
                    this.setChecked(false);
                }

                logger.debug(this.getEjecucionParametro0().getMantEjecDescripcion());
                logger.debug(this.getEjecucionParametro1().getMantEjecDescripcion());
                logger.debug(this.getEjecucionParametro2().getMantEjecDescripcion());
                logger.debug(this.getEjecucionParametro3().getMantEjecDescripcion());
                logger.debug(this.getEjecucionParametro4().getMantEjecDescripcion());
                logger.debug(this.getEjecucionParametro5().getMantEjecDescripcion());
                logger.debug(this.getEjecucionParametro6().getMantEjecDescripcion());
            }else{
                logger.error("Error al cargar parametros cantidad");
            }
        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_PARAMETROS, TITULO_ERROR, MSG_ERROR_BUSCAR);
            logger.error(MSG_ERROR_BUSCAR, e);
        }
    }

    /**
     * Metodo encargado de manejar la accion desde el boton de grabar los datos en la ventana modal de edicion
     * @param valChange
     */
    public void changeParametro(Integer valChange ) {
        logger.debug("changeParametro:" + valChange);
        switch(valChange){
            case 0 : {this.setChangeParametro0(true);
                logger.debug("ChangeParametro0:" + this.getChangeParametro0());
                break;}
            case 1 : {this.setChangeParametro1(true);
                logger.debug("ChangeParametro1:" + this.getChangeParametro1());
                break;}
            case 2 : {this.setChangeParametro2(true);
                logger.debug("ChangeParametro2:" + this.getChangeParametro2());
                break;}
            case 3 : {this.setChangeParametro3(true);
                logger.debug("ChangeParametro3:" + this.getChangeParametro3());
                break;}
            case 4 : {this.setChangeParametro4(true);
                logger.debug("ChangeParametro4:" + this.getChangeParametro4());
                break;}
            case 5 : {this.setChangeParametro5(true);
                logger.debug("ChangeParametro5:" + this.getChangeParametro5());
                break;}
            case 6 : {this.setChangeParametro6(true);
                logger.debug("ChangeParametro6:" + this.getChangeParametro6());
                break;}
        }

    }

    public void guardarParametroBitacora(MantencionEjecucion mantencionEjecucion) {
        logger.debug("guardarParametroBitacora");
        try {
            //seteo usuario y fecha modificacion

            Calendar c1 = Calendar.getInstance();

            mantencionEjecucion.setMantEjecUsuModifica("cangulo2"); //Cambiar por usuario logeado
            mantencionEjecucion.setMantEjecFecModificacion(c1.getTime());
            ejecucionProcesoService.actualizarParametroEjecucion(mantencionEjecucion);

            Bitacora bitacora = new Bitacora();

            TipoEventos tipoEventos = new TipoEventos(3);

            Periodo periodo = periodoServices.buscaPeriodoActivo();

            bitacora.setTbPrTipoEventos(tipoEventos);
            bitacora.setTbPeriodo11(periodo);
            bitacora.setBitFechaEvento(c1.getTime());

            bitacoraServices.creaBitacora(bitacora);

        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_PARAMETROS, TITULO_ERROR, MSG_ERROR_ACTUALIZAR);
            logger.error(MSG_ERROR_ACTUALIZAR, e);
        }
    }
    /**
     * Metodo encargado de manejar la accion desde el boton de grabar los datos
     */
    public void guardarParametros() {
        logger.debug("guardarParametros");
        try {
            //seteo usuario y fecha modificacion

            Calendar c1 = Calendar.getInstance();

            if (this.getChangeParametro0()){
                logger.debug("Actualiza parametro[0]");
                //this.getEjecucionParametro0().setMantEjecUsuModifica("cangulo2");
                //this.getEjecucionParametro0().setMantEjecFecModificacion(c1.getTime());                  
                //ejecucionProcesoService.actualizarParametroEjecucion(this.getEjecucionParametro0());
                this.guardarParametroBitacora(this.getEjecucionParametro0());
            }
            if (this.getChangeParametro1()){
                logger.debug("Actualiza parametro[1]");
                //this.getEjecucionParametro1().setMantEjecUsuModifica("cangulo2");
                //this.getEjecucionParametro1().setMantEjecFecModificacion(c1.getTime());                
                //ejecucionProcesoService.actualizarParametroEjecucion(this.getEjecucionParametro1());
                this.guardarParametroBitacora(this.getEjecucionParametro1());
            }
            if (this.getChangeParametro2()){
                logger.debug("Actualiza parametro[2]");
                //this.getEjecucionParametro2().setMantEjecUsuModifica("cangulo2");
                //this.getEjecucionParametro2().setMantEjecFecModificacion(c1.getTime());                
                //ejecucionProcesoService.actualizarParametroEjecucion(this.getEjecucionParametro2());
                this.guardarParametroBitacora(this.getEjecucionParametro2());
            }
            if (this.getChangeParametro3()){
                logger.debug("Actualiza parametro[3]");
                //this.getEjecucionParametro3().setMantEjecUsuModifica("cangulo2");
                //this.getEjecucionParametro3().setMantEjecFecModificacion(c1.getTime());                
                //ejecucionProcesoService.actualizarParametroEjecucion(this.getEjecucionParametro3());
                this.guardarParametroBitacora(this.getEjecucionParametro3());
            }
            if (this.getChangeParametro4()){
                logger.debug("Actualiza parametro[4]");
                //this.getEjecucionParametro4().setMantEjecUsuModifica("cangulo2");
                //this.getEjecucionParametro4().setMantEjecFecModificacion(c1.getTime());                
                //ejecucionProcesoService.actualizarParametroEjecucion(this.getEjecucionParametro4());
                this.guardarParametroBitacora(this.getEjecucionParametro4());
            }
            if (this.getChangeParametro5()){
                logger.debug("Actualiza parametro[5]");
                //this.getEjecucionParametro5().setMantEjecUsuModifica("cangulo2");
                //this.getEjecucionParametro5().setMantEjecFecModificacion(c1.getTime());
                //ejecucionProcesoService.actualizarParametroEjecucion(this.getEjecucionParametro5());
                this.guardarParametroBitacora(this.getEjecucionParametro5());
            }
            if (this.getChangeParametro6()){
                logger.debug("Actualiza parametro[6]");
                if (this.getChecked()){
                    this.getEjecucionParametro6().setMantEjecValIni1(1);
                }else{
                    this.getEjecucionParametro6().setMantEjecValIni1(0);
                }
                //this.getEjecucionParametro6().setMantEjecUsuModifica("cangulo2");
                //this.getEjecucionParametro6().setMantEjecFecModificacion(c1.getTime());                
                //ejecucionProcesoService.actualizarParametroEjecucion(this.getEjecucionParametro6());
                this.guardarParametroBitacora(this.getEjecucionParametro6());
            }

            //inicializo 
            this.setChangeParametro0(false);
            this.setChangeParametro1(false);
            this.setChangeParametro2(false);
            this.setChangeParametro3(false);
            this.setChangeParametro4(false);
            this.setChangeParametro5(false);
            this.setChangeParametro6(false);
            FacesUtils.showSuccesMessage(FORMULARIO_PARAMETROS, TITULO_ACTUALIZACION_EXITOSA, MSG_EXITO_ACTUALIZAR);

        } catch (Exception e) {
            FacesUtils.showErrorMessage(FORMULARIO_PARAMETROS, TITULO_ERROR, MSG_ERROR_ACTUALIZAR);
            logger.error(MSG_ERROR_ACTUALIZAR, e);
        }
    }

    public void reprocesarNivel3() {
        try
        {
            Periodo p = this.periodoServices.obtenerPeriodoActual();

            if (p!=null){
                if (p.getNivelActual()==0||p.getNivelActual()==1||p.getNivelActual()==2){
                    MensajesUtil.setMensajeErrorContexto("mantenedores.ejecucion.proceso.estado.notificacion");
                    super.mostrarMensajeError();
                    return;
                }
                p.setNivelActual(2);
                periodoServices.modificarPeriodo(p);
                p.setNivelActual(3);
                periodoServices.modificarPeriodo(p);
                RequestContext requestContext = RequestContext.getCurrentInstance();
                requestContext.execute("showReprocesar()");

            }else{
                MensajesUtil.setMensajeErrorContexto("mantenedores.ejecucion.proceso.no.periodo.pendiente");
                super.mostrarMensajeError();
                logger.error("No hay periodos en curso");
            }

        } catch(DataAccessException e){

            logger.error("Ha ocurido un error, detalle: "+e.getMessage());
        }

    }

    public void reiniciarProcesoConcesion() {
        try
        {
            Periodo p = this.periodoServices.obtenerPeriodoActual();

            if (p!=null){
                EtapasPeriodo etapaCargandoArchivos = new EtapasPeriodo(10, "Cargando Archivos");
                p.setTbPrEtapasPeriodo(etapaCargandoArchivos);
                periodoServices.modificarPeriodo(p);
                RequestContext requestContext = RequestContext.getCurrentInstance();
                requestContext.execute("showReiniciarProceso()");

            }else{
                MensajesUtil.setMensajeErrorContexto("mantenedores.ejecucion.proceso.no.periodo.pendiente");
                super.mostrarMensajeError();
                logger.error("No hay periodos en curso");
            }

        } catch(DataAccessException e){

            logger.error("Ha ocurido un error, detalle: "+e.getMessage());
        }

    }


    public MantencionEjecucion getEjecucionParametro() {
        return ejecucionParametro;
    }

    public void setEjecucionParametro(MantencionEjecucion ejecucionParametro) {
        this.ejecucionParametro = ejecucionParametro;
    }

    public List<MantencionEjecucion> getEjecucionParametros() {
        return ejecucionParametros;
    }

    public void setEjecucionParametros(List<MantencionEjecucion> ejecucionParametros) {
        this.ejecucionParametros = ejecucionParametros;
    }

    public MantencionEjecucion getEjecucionParametro0() {
        return ejecucionParametro0;
    }

    public void setEjecucionParametro0(MantencionEjecucion ejecucionParametro0) {
        this.ejecucionParametro0 = ejecucionParametro0;
    }

    public MantencionEjecucion getEjecucionParametro1() {
        return ejecucionParametro1;
    }

    public void setEjecucionParametro1(MantencionEjecucion ejecucionParametro1) {
        this.ejecucionParametro1 = ejecucionParametro1;
    }

    public MantencionEjecucion getEjecucionParametro2() {
        return ejecucionParametro2;
    }

    public void setEjecucionParametro2(MantencionEjecucion ejecucionParametro2) {
        this.ejecucionParametro2 = ejecucionParametro2;
    }

    public MantencionEjecucion getEjecucionParametro3() {
        return ejecucionParametro3;
    }

    public void setEjecucionParametro3(MantencionEjecucion ejecucionParametro3) {
        this.ejecucionParametro3 = ejecucionParametro3;
    }

    public MantencionEjecucion getEjecucionParametro4() {
        return ejecucionParametro4;
    }

    public void setEjecucionParametro4(MantencionEjecucion ejecucionParametro4) {
        this.ejecucionParametro4 = ejecucionParametro4;
    }

    public MantencionEjecucion getEjecucionParametro5() {
        return ejecucionParametro5;
    }

    public void setEjecucionParametro5(MantencionEjecucion ejecucionParametro5) {
        this.ejecucionParametro5 = ejecucionParametro5;
    }

    public MantencionEjecucion getEjecucionParametro6() {
        return ejecucionParametro6;
    }

    public void setEjecucionParametro6(MantencionEjecucion ejecucionParametro6) {
        this.ejecucionParametro6 = ejecucionParametro6;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Boolean getChangeParametro0() {
        return changeParametro0;
    }

    public void setChangeParametro0(Boolean changeParametro0) {
        this.changeParametro0 = changeParametro0;
    }

    public Boolean getChangeParametro1() {
        return changeParametro1;
    }

    public void setChangeParametro1(Boolean changeParametro1) {
        this.changeParametro1 = changeParametro1;
    }

    public Boolean getChangeParametro2() {
        return changeParametro2;
    }

    public void setChangeParametro2(Boolean changeParametro2) {
        this.changeParametro2 = changeParametro2;
    }

    public Boolean getChangeParametro3() {
        return changeParametro3;
    }

    public void setChangeParametro3(Boolean changeParametro3) {
        this.changeParametro3 = changeParametro3;
    }

    public Boolean getChangeParametro4() {
        return changeParametro4;
    }

    public void setChangeParametro4(Boolean changeParametro4) {
        this.changeParametro4 = changeParametro4;
    }

    public Boolean getChangeParametro5() {
        return changeParametro5;
    }

    public void setChangeParametro5(Boolean changeParametro5) {
        this.changeParametro5 = changeParametro5;
    }

    public Boolean getChangeParametro6() {
        return changeParametro6;
    }

    public void setChangeParametro6(Boolean changeParametro6) {
        this.changeParametro6 = changeParametro6;
    }

    public PeriodoDTO getPeriodoInfo() {
        return periodoInfo;
    }

    public void setPeriodoInfo(PeriodoDTO periodoInfo) {
        this.periodoInfo = periodoInfo;
    }
}
