package cl.gob.ips.exencion.informes.view;

import cl.gob.ips.exencion.model.PersonaNatural;
import cl.gob.ips.exencion.service.PersonaNaturalServices;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 * @author rereyes@emergya.com
 * @since 18-08-2016.
 */
@ManagedBean(name="personaNaturalView")
@RequestScoped
public class PersonaNaturalView extends BaseControllerView implements Serializable {

    private final transient Logger LOGGER = Logger.getLogger(PersonaNaturalView.class);

    @Inject
    private transient PersonaNaturalServices personaNaturalServices;

    private List<PersonaNatural> personas;
    private String apellidoPaterno;

    /*@PostConstruct
    public void init() {
    }*/

    /**
     * @since 18-08-2016
     */
    public void buscarPorApellidoPaterno(){
        try{
            this.setPersonas(personaNaturalServices.getPersonasByApellidoPaterno(this.getApellidoPaterno()));
        }catch(Exception e){
            LOGGER.error(e);
        }
    }

    public List<PersonaNatural> getPersonas() {
        return personas;
    }

    public void setPersonas(List<PersonaNatural> personas) {
        this.personas = personas;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }
}
