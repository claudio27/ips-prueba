package cl.gob.ips.exencion.common.validator;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * @author rereyes@emergya.com
 * @since 18-08-2016.
 */
@FacesValidator("cl.gob.ips.exencion.common.validator.ApellidoValidator")
public class ApellidoValidator implements Validator, Serializable {
    
    private final transient Logger logger = Logger.getLogger(this.getClass());

    public void validate(FacesContext facesContext, UIComponent component, Object value) throws ValidatorException {
        if(StringUtils.isEmpty(value.toString())){
            FacesMessage msg = new FacesMessage("Se encontraron los siguientes errores", "El apellido es obligatorio");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            logger.info("Apellido es vacio");
            throw new ValidatorException(msg);
        }
        if (!StringUtils.isAllUpperCase(value.toString())) {
            FacesMessage msg = new FacesMessage("Se encontraron los siguientes errores", "El apellido no tiene formato correcto");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            logger.info("Apellido no es mayuscula");
            throw new ValidatorException(msg);
        }
    }
}
