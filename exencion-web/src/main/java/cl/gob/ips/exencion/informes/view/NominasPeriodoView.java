package cl.gob.ips.exencion.informes.view;

import cl.gob.ips.exencion.dto.NominaPeriodoPdiDTO;
import cl.gob.ips.exencion.dto.NominaPeriodoSinPbsDTO;
import cl.gob.ips.exencion.dto.NominasPeriodoDTO;
import cl.gob.ips.exencion.enums.TipoNomina;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.*;
import cl.gob.ips.exencion.service.InstitucionPagadoraServices;
import cl.gob.ips.exencion.service.NominasInstitucionesServices;
import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.util.Reportes;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJBException;
import javax.ejb.EJBTransactionRolledbackException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;
import java.util.List;

@ManagedBean(name = "nominasPeriodoView")
@ViewScoped
public class NominasPeriodoView extends BaseControllerView implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8283812469845961588L;

	private static final Integer RUT_PDI = 60506000;

	private static final Integer RUT_IPS = 61979440;

	private static final Logger LOGGER = Logger.getLogger(NominasPeriodoView.class);

	private Date periodo;
	private Integer codNomina;
	private Integer rutInstitucion;
	private List<NominasPeriodoDTO> resultado;
	private List<NominaPeriodoPdiDTO> resultadoPdi;
	private List<NominaPeriodoSinPbsDTO> resultadoPbs;
	private List<Instituciones> instituciones;
	private TipoNomina[] tpNomina;
	private Boolean mostrarPrimeraTabla;
	private Boolean mostrarSegundaTabla;
	private Boolean mostrarTablaPdi;
	private Boolean mostrarTablaPbs;
	private Boolean mostrarTablaSinFicha;
	private int tipoReporte;
	private boolean btnBuscar;

	private boolean mostrarInstitucion = Boolean.TRUE;

	@Inject
	private InstitucionPagadoraServices institucionPagadoraServices;

	@Inject
	private NominasInstitucionesServices nominasServices;

	@PostConstruct
	private void init() {
		this.mostrarPrimeraTabla = Boolean.FALSE;
		this.mostrarSegundaTabla = Boolean.FALSE;
		this.mostrarTablaPdi= Boolean.FALSE;
		this.tpNomina = TipoNomina.values();
		this.resultado = new ArrayList<NominasPeriodoDTO>();

		try {
			this.instituciones = this.institucionPagadoraServices.buscarInstitucionesHabilitadas();
		} catch (EJBTransactionRolledbackException e) {
			this.btnBuscar = Boolean.TRUE;
			MensajesUtil.setMensajeErrorContexto("validaciones.general.filtros.busqueda");
			super.mostrarMensajeError();
		}
	}

	public void validarPeriodoSinInformacion(){
		MensajesUtil.setMensajeErrorContexto("validacion.nominas.periodo");
		super.mostrarMensajeError();
		this.mostrarPrimeraTabla = Boolean.FALSE;
		this.mostrarSegundaTabla = Boolean.FALSE;
		this.mostrarTablaPdi = Boolean.FALSE;
		this.mostrarTablaPbs = Boolean.FALSE;
		this.mostrarTablaSinFicha = Boolean.FALSE;
		LOGGER.error("Periodo sin información");
	}

	public void validarMostrarGrilla(int codigoNomina){
		switch (codigoNomina){
			case 0:
				this.mostrarPrimeraTabla = Boolean.TRUE;
				this.mostrarSegundaTabla = Boolean.FALSE;
				this.mostrarTablaPdi = Boolean.FALSE;
				this.mostrarTablaPbs = Boolean.FALSE;
				this.mostrarTablaSinFicha = Boolean.FALSE;
				break;

			case 1:
				this.mostrarPrimeraTabla = Boolean.TRUE;
				this.mostrarSegundaTabla = Boolean.FALSE;
				this.mostrarTablaPdi = Boolean.FALSE;
				this.mostrarTablaPbs = Boolean.FALSE;
				this.mostrarTablaSinFicha = Boolean.FALSE;
				break;

			case 2:
				this.mostrarPrimeraTabla = Boolean.FALSE;
				this.mostrarSegundaTabla = Boolean.FALSE;
				this.mostrarTablaPdi = Boolean.FALSE;
				this.mostrarTablaPbs = Boolean.TRUE;
				this.mostrarTablaSinFicha = Boolean.FALSE;
				break;

			case 3:
				this.mostrarPrimeraTabla = Boolean.TRUE;
				this.mostrarSegundaTabla = Boolean.FALSE;
				this.mostrarTablaPdi = Boolean.FALSE;
				this.mostrarTablaPbs = Boolean.FALSE;
				this.mostrarTablaSinFicha = Boolean.FALSE;
				break;

			case 4:
				this.mostrarPrimeraTabla = Boolean.FALSE;
				this.mostrarSegundaTabla = Boolean.FALSE;
				this.mostrarTablaPdi = Boolean.FALSE;
				this.mostrarTablaPbs = Boolean.FALSE;
				this.mostrarTablaSinFicha = Boolean.TRUE;
				break;

			case 5:
				this.mostrarPrimeraTabla = Boolean.FALSE;
				this.mostrarSegundaTabla = Boolean.FALSE;
				this.mostrarTablaPdi = Boolean.TRUE;
				this.mostrarTablaPbs = Boolean.FALSE;
				this.mostrarTablaSinFicha = Boolean.FALSE;
				break;

			case 6:
				this.mostrarPrimeraTabla = Boolean.FALSE;
				this.mostrarSegundaTabla = Boolean.TRUE;
				this.mostrarTablaPdi = Boolean.FALSE;
				this.mostrarTablaPbs = Boolean.FALSE;
				this.mostrarTablaSinFicha = Boolean.FALSE;
				break;
			default:
				break;
		}
	}

	public void buscarNominas() {
		LOGGER.debug("periodo: " + this.periodo);
		LOGGER.debug("codNomina: " + this.codNomina);
		LOGGER.debug("rutInstitucion: " + this.rutInstitucion);
		NominasPeriodoDTO dto;
		NominaPeriodoPdiDTO dtoPdi;
		NominaPeriodoSinPbsDTO dtoSinPbs;
		this.resultado = new ArrayList<NominasPeriodoDTO>();

		try {
			if (null == this.rutInstitucion) {
				if (this.codNomina == 0 || this.codNomina == 1
						|| this.codNomina == 3) {
					MensajesUtil.setMensajeErrorContexto("nominas.periodo.rutinstitucion");
					super.mostrarMensajeError();
					return;
				}
			}

			switch (this.codNomina) {
				case 0:
					this.tipoReporte = 0;
					List<NominaBeneficiario> nominaBeneficiarios = this.nominasServices.obtenerNominaBeneficiario(
							super.obtenerPeriodoEntero(this.periodo), this.rutInstitucion);

					if (nominaBeneficiarios.isEmpty()){
						validarPeriodoSinInformacion();
						return;
					}

					for (NominaBeneficiario nom : nominaBeneficiarios) {
						dto = new NominasPeriodoDTO();
						dto.setPeriodo(null == nom.getPeriodo() ? "-" : nom.getPeriodo().getPerPeriodo().toString());
						dto.setFechaDevengamientoBeneficio(null == nom.getNomFechaDevBenef() ? null : nom.getNomFechaDevBenef());
						dto.setRutEntidad(null == nom.getInstituciones() ? 0 : nom.getInstituciones().getInstRut().intValue());
						dto.setDvEntidad(null == nom.getNomDvInstitucion() ? "-" : nom.getNomDvInstitucion());
						dto.setRunBeneficiario(null == nom.getNomRunBeneficiario() ? 0 : nom.getNomRunBeneficiario());
						dto.setDvBeneficiario(null == nom.getNomDvBeneficiario() ? "-" : nom.getNomDvBeneficiario());
						dto.setApellidoPaterno(null == nom.getNomApePaterno() ? "-" : nom.getNomApePaterno());
						dto.setApellidoMaterno(null == nom.getNomApeMaterno() ? "-" : nom.getNomApeMaterno());
						dto.setNombres(null == nom.getNomNombres() ? "-" : nom.getNomNombres());
						dto.setTipoPension(null == nom.getNomTipoPension() ? "-" : nom.getNomTipoPension());
						this.resultado.add(dto);
					}

					validarMostrarGrilla(0);
					break;

				case 1:
					this.tipoReporte = 1;
					List<NominaPotBeneficiario> nominaPotBeneficiarios = this.nominasServices.obtenerNominaPotBeneficiario(
							super.obtenerPeriodoEntero(this.periodo), this.rutInstitucion);

					if (nominaPotBeneficiarios.isEmpty()){
						validarPeriodoSinInformacion();
						return;
					}

					for (NominaPotBeneficiario nom: nominaPotBeneficiarios) {
						dto = new NominasPeriodoDTO();
						dto.setPeriodo(null == nom.getNomRunBeneficiario() ? "-" : nom.getPeriodo().getPerPeriodo().toString() );
						dto.setRutEntidad(null == nom.getInstituciones().getInstRut() ? 0 : nom.getInstituciones().getInstRut().intValue());
						dto.setDvEntidad(null == nom.getNomDvInstitucion() ? "-" : nom.getNomDvInstitucion());
						dto.setRunBeneficiario(null == nom.getNomRunBeneficiario() ? 0 : nom.getNomRunBeneficiario());
						dto.setDvBeneficiario(null == nom.getNomDvBeneficiario() ? "-" : nom.getNomDvBeneficiario());
						dto.setApellidoPaterno(null == nom.getNomApellidoPat() ? "-" : nom.getNomApellidoPat());
						dto.setApellidoMaterno(null == nom.getNomApellidoMat() ? "-" : nom.getNomApellidoMat());
						dto.setNombres(null == nom.getNomNombres() ? "-" : nom.getNomNombres());
						dto.setTipoPension(null == nom.getNomTipoPension() ? "-" : nom.getNomTipoPension().toString());
						dto.setRequisitosNoAcreaditados(null == nom.getNomReqNoAcredita() ? "-" : nom.getNomReqNoAcredita());
						this.resultado.add(dto);
					}
					validarMostrarGrilla(1);
					break;

				case 2:
					this.tipoReporte = 2;
					this.resultadoPbs = new ArrayList<NominaPeriodoSinPbsDTO>();

					List<NominaBphSinPbs> nominasBphSinPbs = this.nominasServices.obtenerNominaBphSinPbs(
							super.obtenerPeriodoEntero(this.periodo), RUT_IPS);

					if (nominasBphSinPbs.isEmpty()){
						validarPeriodoSinInformacion();
						return;
					}

					for (NominaBphSinPbs nom: nominasBphSinPbs) {
						dtoSinPbs = new NominaPeriodoSinPbsDTO();
						dtoSinPbs.setTipoMovimiento(null == nom.getNomTipoMovimiento() ? "-" : nom.getNomTipoMovimiento());
						dtoSinPbs.setTipoBeneficio(null == nom.getNomTipoBeneficio() ? 0 : nom.getNomTipoBeneficio());
						dtoSinPbs.setRunBeneficiario(null == nom.getNomRunBeneficiario() ? 0 : nom.getNomRunBeneficiario());
						dtoSinPbs.setDvBeneficiario(null == nom.getNomDvBeneficiario() ? "-" : nom.getNomDvBeneficiario());
						dtoSinPbs.setMonto(null == nom.getNomMonto() ? 0 : nom.getNomMonto());
						dtoSinPbs.setFechaInicioPago(null == nom.getNomFechaInic() ? null : super.obtenerFechaFormato(nom.getNomFechaInic()));
						dtoSinPbs.setFechaVencimiento(null == nom.getNomFechaVencimiento() ? null : super.obtenerFechaFormato(nom.getNomFechaVencimiento()));
						dtoSinPbs.setCodigoHaber(null == nom.getNomCodigoHaber() ? "-" : nom.getNomCodigoHaber());
						dtoSinPbs.setUnidadMedida(null == nom.getNomUnidadMedida() ? "-" : nom.getNomUnidadMedida());
						this.resultadoPbs.add(dtoSinPbs);
					}
					validarMostrarGrilla(2);
					break;

				case 3:
					this.tipoReporte = 3;
					List<NominaPotencialesAps> nominaPotencialesAps = this.nominasServices.obtenerNominaPotencialesAps(
							super.obtenerPeriodoEntero(this.periodo), this.rutInstitucion);

					if (nominaPotencialesAps.isEmpty()){
						validarPeriodoSinInformacion();
						return;
					}

					for (NominaPotencialesAps nom: nominaPotencialesAps) {
						dto = new NominasPeriodoDTO();
						dto.setPeriodo(null == nom.getPeriodo() ? "-" : nom.getPeriodo().getPerPeriodo().toString());
						dto.setRutEntidad(null == nom.getInstituciones() ? 0 : nom.getInstituciones().getInstRut().intValue());
						dto.setDvEntidad(null == nom.getNomDvInstitucion() ? "-" : nom.getNomDvInstitucion());
						dto.setRunBeneficiario(null == nom.getNomRunBeneficiario() ? 0 : nom.getNomRunBeneficiario());
						dto.setDvBeneficiario(null == nom.getNomDvBeneficiario() ? "-" : nom.getNomDvBeneficiario());
						dto.setApellidoPaterno(null == nom.getNomApellidoPat() ? "-" : nom.getNomApellidoPat());
						dto.setApellidoMaterno(null == nom.getNomApellidoMat() ? "-" : nom.getNomApellidoMat());
						dto.setNombres(null == nom.getNomNombres() ? "-" : nom.getNomNombres());
						dto.setIndicadorFps(null == nom.getNomIndicadorFps() ? "-" : nom.getNomIndicadorFps().toString());
						this.resultado.add(dto);
					}
					validarMostrarGrilla(3);
					break;
				case 4:
					this.tipoReporte = 4;
					List<NominaPenSinFicha> nominaPenSinFichas = this.nominasServices.obtenerNominaPenSinFicha(
							super.obtenerPeriodoEntero(this.periodo));

					if (nominaPenSinFichas.isEmpty()){
						validarPeriodoSinInformacion();
						return;
					}

					for (NominaPenSinFicha nom: nominaPenSinFichas) {
						dto = new NominasPeriodoDTO();
						dto.setRunBeneficiario(null == nom.getNomRunBeneficiario() ? 0 : nom.getNomRunBeneficiario());
						dto.setDvBeneficiario(null == nom.getNomDvBeneficiario() ? "-" : nom.getNomDvBeneficiario());
						this.resultado.add(dto);
					}

					validarMostrarGrilla(4);
					break;

				case 5:
					this.tipoReporte = 5;
					this.resultadoPdi = new ArrayList<NominaPeriodoPdiDTO>();

					List<NominaConsultaPdi> nominaConsultaPdi = this.nominasServices.obtenerNominaConsultaPdi(
							super.obtenerPeriodoEntero(this.periodo), RUT_PDI);

					if (nominaConsultaPdi.isEmpty()){
						validarPeriodoSinInformacion();
						return;
					}

					for (NominaConsultaPdi nom: nominaConsultaPdi) {
						dtoPdi = new NominaPeriodoPdiDTO();
						dtoPdi.setRun(null == nom.getNomRunBeneficiario() ? 0 : nom.getNomRunBeneficiario());
						dtoPdi.setApellidoPaterno(null == nom.getNomApellidoPat() ? "-" : nom.getNomApellidoPat());
						dtoPdi.setApellidoMaterno(null == nom.getNomApellidoMat() ? "-" : nom.getNomApellidoMat());
						dtoPdi.setNombres(null == nom.getNomNombres() ? "-" : nom.getNomNombres());
						dtoPdi.setSexo(null == nom.getNomSexo() ? "-" : nom.getNomSexo());
						dtoPdi.setFechaNacimiento(null == nom.getNomFechaNac() ? "-" : nom.getNomFechaNac());
						dtoPdi.setDesdeUno(null == nom.getNomDesde1() ? "-" : nom.getNomDesde1().toString());
						dtoPdi.setHastaUno(null == nom.getNomHasta1() ? "-" : nom.getNomHasta1().toString());
						dtoPdi.setDesdeDos(null == nom.getNomDesde2() ? "-" : nom.getNomDesde2().toString());
						dtoPdi.setHastaDos(null == nom.getNomHasta2() ? "-" : nom.getNomHasta2().toString());
						this.resultadoPdi.add(dtoPdi);
					}

					validarMostrarGrilla(5);
					break;

				case 6:
					this.tipoReporte = 6;
					List<NominaRebaja> nominaRebajas = this.nominasServices.obtenerNominaRebaja(super.obtenerPeriodoEntero(this.periodo));

					if (nominaRebajas.isEmpty()){
						validarPeriodoSinInformacion();
						return;
					}

					for (NominaRebaja nom: nominaRebajas) {
						dto = new NominasPeriodoDTO();
						dto.setRunBeneficiario(null == nom.getNomRunBeneficiario() ? 0 : nom.getNomRunBeneficiario());
						dto.setEstado(null == nom.getNomEstado() ? 0 : nom.getNomEstado());
						this.resultado.add(dto);
					}

					validarMostrarGrilla(6);
					break;

				default:
					break;
			}
		} catch (EJBException | DataAccessException e) {
			MensajesUtil.setMensajeErrorContexto("error.general");
			super.mostrarMensajeError();
		}
	}

	public void generarPdf() {

		Map<String, List<String>> map = new HashMap<String, List<String>>();

		List<String> titulos = new ArrayList<String>();
		List<String> valores = new ArrayList<String>();
		String tituloPDF="";

		switch (this.tipoReporte) {
			case 0:
				titulos.add("Periodo");
				titulos.add("Fecha Devengamiento");
				titulos.add("Rut Entidad");
				titulos.add("Run Benef.");
				titulos.add("Nombres");
				titulos.add("Apellido Paterno");
				titulos.add("Apellido Materno");
				titulos.add("Tipo Pension");

				for (NominasPeriodoDTO data : this.resultado) {
					valores.add(null == data.getPeriodo() ? "-" : data.getPeriodo());
					valores.add(null == data.getFechaDevengamientoBeneficio() ? "-" : super.obtenerFechaFormato(data.getFechaDevengamientoBeneficio()));
					valores.add(null == data.getRutEntidad() ? "-" : data.getRutEntidad().toString()+"-"+data.getDvEntidad().toString());
					valores.add(null == data.getRunBeneficiario() ? "-" : data.getRunBeneficiario().toString()+"-"+data.getDvBeneficiario().toString());
					valores.add(null == data.getNombres() ? "-" : data.getNombres());
					valores.add(null == data.getApellidoPaterno() ? "-" : data.getApellidoPaterno());
					valores.add(null == data.getApellidoMaterno() ? "-" : data.getApellidoMaterno());
					valores.add(null == data.getTipoPension() ? "-" : data.getTipoPension());
				}
				tituloPDF = "Beneficiarios Exención de Salud";
				break;
			case 1:
				titulos.add("Periodo");
				titulos.add("Rut Entidad");
				titulos.add("Run Benef.");
				titulos.add("Nombres");
				titulos.add("Apellido Paterno");
				titulos.add("Apellido Materno");
				titulos.add("Tipo Pension");
				titulos.add("Requisitos No Acreditados");

				for (NominasPeriodoDTO data : this.resultado) {
					valores.add(data.getPeriodo());
					valores.add(data.getRutEntidad().toString()+"-"+data.getDvEntidad().toString());
					valores.add(data.getRunBeneficiario().toString()+"-"+data.getDvBeneficiario().toString());
					valores.add(data.getNombres());
					valores.add(data.getApellidoPaterno());
					valores.add(data.getApellidoMaterno());
					valores.add(data.getTipoPension());
					valores.add(data.getRequisitosNoAcreaditados());
				}
				tituloPDF = "Potenciales Beneficiarios Exención de Salud";
				break;
			case 2:
				titulos.add("Tipo Mov");
				titulos.add("Tipo Benef.");
				titulos.add("Run Benef.");
				titulos.add("Monto");
				titulos.add("F. Inicio Pago");
				titulos.add("F. Vencimiento");
				titulos.add("Cód Haber");
				titulos.add("U. Medida");

				for (NominaPeriodoSinPbsDTO data : this.resultadoPbs) {
					valores.add(data.getTipoMovimiento());
					valores.add(data.getTipoBeneficio().toString());
					valores.add(data.getRunBeneficiario().toString()+"-"+data.getDvBeneficiario().toString());
					valores.add(data.getMonto().toString());
					valores.add(data.getFechaInicioPago());
					valores.add(data.getFechaVencimiento());
					valores.add(data.getCodigoHaber());
					valores.add(data.getUnidadMedida());
				}
				tituloPDF = "Bono por Hijo sin PBS";
				break;
			case 3:
				titulos.add("Periodo");
				titulos.add("Rut Entidad");
				titulos.add("Run Benef.");
				titulos.add("Nombres");
				titulos.add("Apellido Paterno");
				titulos.add("Apellido Materno");
				titulos.add("Indicador FPS");

				for (NominasPeriodoDTO data : this.resultado) {
					valores.add(data.getPeriodo());
					valores.add(data.getRutEntidad().toString()+"-"+data.getDvEntidad().toString());
					valores.add(data.getRunBeneficiario().toString()+"-"+data.getDvBeneficiario().toString());
					valores.add(data.getNombres());
					valores.add(data.getApellidoPaterno());
					valores.add(data.getApellidoMaterno());
					valores.add(data.getIndicadorFps());
				}
				tituloPDF = "Potenciales Beneficiarios de APS";
				break;
			case 4:
				titulos.add("Run Beneficiario");

				for (NominasPeriodoDTO data : this.resultado) {
					valores.add(data.getRunBeneficiario().toString()+"-"+data.getDvBeneficiario().toString());
				}
				tituloPDF = "Pensionados Sin Ficha";
				break;
			case 5:
				titulos.add("Run");
				titulos.add("Apellido Paterno");
				titulos.add("Apellido Materno");
				titulos.add("Nombres");
				titulos.add("Sexo");
				titulos.add("Fecha Nacimiento");
				titulos.add("Desde 1");
				titulos.add("Hasta 1");
				titulos.add("Desde 2");
				titulos.add("Hasta 2");

				for (NominaPeriodoPdiDTO data : this.resultadoPdi) {
					valores.add(data.getRun().toString());
					valores.add(data.getApellidoPaterno());
					valores.add(data.getApellidoMaterno());
					valores.add(data.getNombres());
					valores.add(data.getSexo());
					valores.add(data.getFechaNacimiento());
					valores.add(data.getDesdeUno());
					valores.add(data.getHastaUno());
					valores.add(data.getDesdeDos());
					valores.add(data.getHastaDos());
				}
				tituloPDF = "Consulta PDI";
				break;
			case 6:
				titulos.add("Run Beneficiario");
				titulos.add("Estado");

				for (NominasPeriodoDTO data : this.resultado) {
					valores.add(data.getRunBeneficiario().toString());
					valores.add(data.getEstado().toString());
				}
				tituloPDF = "Rebaja";
				break;
		}

		map.put("titulos", titulos);
		map.put("valores", valores);

		Document pdf = new Document(PageSize.A4.rotate());


		if (valores.size()<=3) {
			pdf=new Document();

		}



		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {

			PdfWriter.getInstance(pdf, baos);
			pdf.open();
			Paragraph headerPDF = new Paragraph(tituloPDF);
			headerPDF.setAlignment(Element.ALIGN_CENTER);
			pdf.add(headerPDF);
			PdfPTable table = new PdfPTable(titulos.size()); // numero de columnas
			table.setWidthPercentage(100); // ancho en porcentaje
			table.setSpacingBefore(10f); // espacio antes de la tabla
			table.setSpacingAfter(10f); // espacio despues de la tabla

			// Set Column widths
			float[] columnWidths = new float[titulos.size()];

			for (int i = 0; i < titulos.size(); i++) {
				columnWidths[i] = 1f;
			}

			table.setWidths(columnWidths);

			for (int i = 0; i < titulos.size(); i++) {
				PdfPCell cell = new PdfPCell(new Paragraph(titulos.get(i)));
				cell.setBorderColor(BaseColor.BLACK);
				cell.setPaddingLeft(10);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//				cell.setPhrase(new Phrase(titulos.get(i), new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD)));
				table.addCell(cell);
			}

			pdf.add(table);

			PdfPTable tablaValores = new PdfPTable(titulos.size()); //columnas.
			tablaValores.setWidthPercentage(100); // ancho %

			// Set Column widths
			tablaValores.setWidths(columnWidths);

			int cont = 0;

			int diferencia = valores.size() / titulos.size();

			for (int i = 0; i < diferencia; i++) {
				for (int j = 0; j < titulos.size(); j++) {
					PdfPCell cell = new PdfPCell(new Paragraph(valores.get(cont)));
					cell.setBorderColor(BaseColor.BLACK);
					cell.setPaddingLeft(10);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					tablaValores.addCell(cell);
					cont++;
				}
			}

			pdf.add(tablaValores);
			pdf.close();

			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			response.setContentType("Application/x-pdf");
			response.setHeader("Content-Disposition", "attachment; filename="+ "archivo.pdf");
			response.getOutputStream().write(baos.toByteArray());
			response.getOutputStream().flush();
			response.getOutputStream().close();
			context.responseComplete();


		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new
					FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Error al generar el PDF"));
		}
	}

	public void ocultarInstitucion() {
		if (this.codNomina == 5 || this.codNomina == 2
				|| this.codNomina == 4 || this.codNomina == 6) {
			this.mostrarInstitucion = Boolean.FALSE;
		} else {
			this.mostrarInstitucion = Boolean.TRUE;
		}

		this.mostrarPrimeraTabla = Boolean.FALSE;
		this.mostrarSegundaTabla = Boolean.FALSE;
		this.mostrarTablaPdi = Boolean.FALSE;
		this.mostrarTablaPbs = Boolean.FALSE;
		this.mostrarTablaSinFicha = Boolean.FALSE;
	}

	public void obtenerExcelGeneral() throws IOException {

		File archivo = null;
		try {

			switch (this.tipoReporte) {
				case 0:
					archivo = Reportes.obtenerNominaBeneficiarios(this.resultado);
					break;
				case 1:
					archivo = Reportes.obtenerNominaPotBeneficiarios(this.resultado);
					break;
				case 2:
					archivo = Reportes.obtenerNominaSinPbs(this.resultadoPbs);
					break;
				case 3:
					archivo = Reportes.obtenerNominaBenefeciariosAps(this.resultado);
					break;
				case 4:
					archivo = Reportes.obtenerNominaSinFicha(this.resultado);
					break;
				case 5:
					archivo = Reportes.obtenerNominaConsultaPdi(this.resultadoPdi);
					break;
				case 6:
					archivo = Reportes.obtenerNominaRebaja(this.resultado);
					break;
				default:
					break;
			}

			super.generarExcel(archivo);

		} catch (FileNotFoundException e) {
			LOGGER.error("Archivo no encontrado: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.error("Ha ocurrido un error: " + e.getMessage());
		}
	}

	public void limpiar() {
		this.codNomina = null;
		this.periodo = null;
		this.rutInstitucion = null;
		this.mostrarPrimeraTabla = Boolean.FALSE;
		this.mostrarSegundaTabla = Boolean.FALSE;
		this.mostrarTablaPbs = Boolean.FALSE;
		this.mostrarTablaPdi = Boolean.FALSE;
		this.mostrarTablaSinFicha = Boolean.FALSE;

	}

	public Date getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Date periodo) {
		this.periodo = periodo;
	}

	public Integer getCodNomina() {
		return codNomina;
	}

	public void setCodNomina(Integer codNomina) {
		this.codNomina = codNomina;
	}

	public Integer getRutInstitucion() {
		return rutInstitucion;
	}

	public void setRutInstitucion(Integer codInstitucion) {
		this.rutInstitucion = codInstitucion;
	}

	public List<NominasPeriodoDTO> getResultado() {
		return resultado;
	}

	public void setResultado(List<NominasPeriodoDTO> resultado) {
		this.resultado = resultado;
	}

	public List<Instituciones> getInstituciones() {
		return instituciones;
	}

	public void setInstituciones(List<Instituciones> instituciones) {
		this.instituciones = instituciones;
	}

	public TipoNomina[] getTpNomina() {
		return tpNomina;
	}

	public void setTpNomina(TipoNomina[] tpNomina) {
		this.tpNomina = tpNomina;
	}

	public Boolean getMostrarPrimeraTabla() {
		return mostrarPrimeraTabla;
	}

	public void setMostrarPrimeraTabla(Boolean mostrarPrimeraTabla) {
		this.mostrarPrimeraTabla = mostrarPrimeraTabla;
	}

	public Boolean getMostrarSegundaTabla() {
		return mostrarSegundaTabla;
	}

	public void setMostrarSegundaTabla(Boolean mostrarSegundaTabla) {
		this.mostrarSegundaTabla = mostrarSegundaTabla;
	}

	public Boolean getMostrarTablaPdi() {
		return mostrarTablaPdi;
	}

	public void setMostrarTablaPdi(Boolean mostrarTablaPdi) {
		this.mostrarTablaPdi = mostrarTablaPdi;
	}

	public List<NominaPeriodoPdiDTO> getResultadoPdi() {
		return resultadoPdi;
	}

	public void setResultadoPdi(List<NominaPeriodoPdiDTO> resultadoPdi) {
		this.resultadoPdi = resultadoPdi;
	}

	public boolean isMostrarInstitucion() {
		return mostrarInstitucion;
	}

	public List<NominaPeriodoSinPbsDTO> getResultadoPbs() {
		return resultadoPbs;
	}

	public void setResultadoPbs(List<NominaPeriodoSinPbsDTO> resultadoPbs) {
		this.resultadoPbs = resultadoPbs;
	}

	public Boolean getMostrarTablaPbs() {
		return mostrarTablaPbs;
	}

	public void setMostrarTablaPbs(Boolean mostrarTablaPbs) {
		this.mostrarTablaPbs = mostrarTablaPbs;
	}

	public Boolean getMostrarTablaSinFicha() {
		return mostrarTablaSinFicha;
	}

	public void setMostrarTablaSinFicha(Boolean mostrarTablaSinFicha) {
		this.mostrarTablaSinFicha = mostrarTablaSinFicha;
	}

	public int getTipoReporte() {
		return tipoReporte;
	}

	public void setTipoReporte(int tipoReporte) {
		this.tipoReporte = tipoReporte;
	}

	public boolean isBtnBuscar() {
		return btnBuscar;
	}

	public void setBtnBuscar(boolean btnBuscar) {
		this.btnBuscar = btnBuscar;
	}
}
