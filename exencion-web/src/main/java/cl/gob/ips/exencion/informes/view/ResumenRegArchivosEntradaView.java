package cl.gob.ips.exencion.informes.view;

import cl.gob.ips.exencion.dto.ResumenRegArchivosEntradaDTO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.ArchivosCargaPeriodo;
import cl.gob.ips.exencion.service.CargaArchivoServices;
import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.util.Reportes;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ManagedBean(name = "resumenRegArchivosEntradaView")
@ViewScoped
public class ResumenRegArchivosEntradaView extends BaseControllerView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2550164658327289746L;

	private static final Logger LOGGER = Logger.getLogger(ResumenRegArchivosEntradaView.class);
	
	private Date periodo;
	private Boolean mostrarGrilla;
	private List<ResumenRegArchivosEntradaDTO> respuestaGrilla;

	@Inject
	private CargaArchivoServices cargaArchivoServices;

	private List<ArchivosCargaPeriodo> archivosCargaPeriodoList;
	
	@PostConstruct
	private void init() {
		this.mostrarGrilla = Boolean.FALSE;
	}
	
	public void buscar() {

		try {

			this.archivosCargaPeriodoList = this.cargaArchivoServices.buscaPeriodoActivo(super.obtenerPeriodoEntero(this.periodo));

			if (null != this.archivosCargaPeriodoList && !this.archivosCargaPeriodoList.isEmpty()) {
				this.respuestaGrilla = new ArrayList<ResumenRegArchivosEntradaDTO>();

				mapeoDto(this.archivosCargaPeriodoList, this.respuestaGrilla);


			} else {
				this.mostrarGrilla = Boolean.FALSE;
				MensajesUtil.setMensajeErrorContexto("resumen.archivos.entrada.no.registros");
				super.mostrarMensajeError();
				return;
			}

			this.mostrarGrilla = Boolean.TRUE;
		} catch (DataAccessException ex) {
			LOGGER.error("Error al buscar: ", ex);
			MensajesUtil.setMensajeErrorContexto("validacion.error.carga.datos");
			super.mostrarMensajeError();
		}

	}

	public void mapeoDto(List<ArchivosCargaPeriodo> listaArchivos, List<ResumenRegArchivosEntradaDTO> listaDto){

		for (ArchivosCargaPeriodo archivo : listaArchivos) {
			ResumenRegArchivosEntradaDTO resumenRegArchivosEntradaDTO = new ResumenRegArchivosEntradaDTO();
			resumenRegArchivosEntradaDTO.setPeriodo(archivo.getTbPeriodo1().getPerPeriodo().toString() != null ? archivo.getTbPeriodo1().getPerPeriodo().toString() : "");
			resumenRegArchivosEntradaDTO.setArchivoCarga(archivo.getArcNombreArchivoCargado() != null ? archivo.getArcNombreArchivoCargado() : "");
			resumenRegArchivosEntradaDTO.setCantidadRegistros(archivo.getArcCantRegCargados() != null ? archivo.getArcCantRegCargados() : 0);
			listaDto.add(resumenRegArchivosEntradaDTO);
		}
	}
	
	public String redirectResultadoValidacion() {

		Integer period = obtenerPeriodoEntero(periodo);


		StringBuilder sb = new StringBuilder();
		sb.append("&periodo=").append(period+"01");

		return "resultado-validacion-archivos?faces-redirect=true" + sb.toString();

	}
	
	public void limpiar() {
		this.periodo = null;
		this.mostrarGrilla = Boolean.FALSE;
	}
	
	public void obtenerExcel() throws IOException {
	    try {
		    File archivo = Reportes.obtenerResumenRegArchivosEntrada(this.respuestaGrilla);
			super.generarExcel(archivo);
		} catch (IOException e) {
	    	LOGGER.error("Error al obtener excel: ", e);
			MensajesUtil.setMensajeErrorContexto("validacion.error.carga.datos");
			super.mostrarMensajeError();
	    }
	}

	public Date getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Date periodo) {
		this.periodo = periodo;
	}

	public Boolean getMostrarGrilla() {
		return mostrarGrilla;
	}

	public void setMostrarGrilla(Boolean mostrarGrilla) {
		this.mostrarGrilla = mostrarGrilla;
	}

	public List<ResumenRegArchivosEntradaDTO> getRespuestaGrilla() {
		return respuestaGrilla;
	}

	public void setRespuestaGrilla(
			List<ResumenRegArchivosEntradaDTO> respuestaGrilla) {
		this.respuestaGrilla = respuestaGrilla;
	}

	public List<ArchivosCargaPeriodo> getArchivosCargaPeriodoList() {
		return archivosCargaPeriodoList;
	}

	public void setArchivosCargaPeriodoList(List<ArchivosCargaPeriodo> archivosCargaPeriodoList) {
		this.archivosCargaPeriodoList = archivosCargaPeriodoList;
	}
}
