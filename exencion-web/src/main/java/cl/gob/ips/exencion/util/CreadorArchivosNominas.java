package cl.gob.ips.exencion.util;

import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by alaya on 1/09/17.
 */
public class CreadorArchivosNominas {

    private static String rutaTemp = System.getProperty("java.io.tmpdir");
    private static final Logger LOGGER = Logger.getLogger(CreadorArchivosNominas.class);

    public static <T> File crearNomina(List<T> lista){

        // todo renombrar archivo segun tipo de objeto de entrada
        BufferedWriter bw = null;
        FileWriter fw = null;
        String rutaArchivo = rutaTemp + "/out.txt";

        try{

            fw = new FileWriter(rutaArchivo);
            bw = new BufferedWriter(fw);


            for (T linea : lista) {

                bw.write(linea.toString() + "\n" );
            }

        }catch (IOException e){
            LOGGER.error("Fallo creacion de nomina", e);
        }finally {
            cerrarBuffers(bw, fw);
        }


        return new File(rutaArchivo);
    }



    private static void cerrarBuffers(BufferedWriter bw, FileWriter fw){
        try{
            if(bw != null)
                bw.close();

            if(fw != null)
                fw.close();

        }catch (IOException e){
            LOGGER.error("Fallo cierre buffers nomina", e);
        }

    }

}
