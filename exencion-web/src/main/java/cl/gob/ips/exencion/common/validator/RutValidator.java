package cl.gob.ips.exencion.common.validator;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import java.io.Serializable;
import java.util.ResourceBundle;

/**
 * Created by rreyes on 07-10-2016.
 */
@FacesValidator("cl.gob.ips.exencion.common.validator.RutValidator")
public class RutValidator implements Validator, Serializable {

    private final transient Logger logger = Logger.getLogger(this.getClass());


    public void validate(FacesContext facesContext, UIComponent component, Object value) throws ValidatorException {
        logger.debug("rut a validar: " + value.toString());

        ResourceBundle rb = ResourceBundle.getBundle("validaciones");

        if (!"".equals(value.toString()) && !this.isValidRut(value.toString())) {
            FacesMessage msg = new FacesMessage("Se encontraron los siguientes errores", rb.getString("validacion.rut.invalido"));
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            logger.info("Rut es invalido");
            RequestContext requestContext = RequestContext.getCurrentInstance();
            requestContext.execute("limpiarMensajesGenerales()");
            throw new ValidatorException(msg);
        }
    }

    private boolean isValidRut(String rut) {
        return this.validarRut(rut);
    }

    /**
     * Metodo que valida un rut
     *
     * @param rut
     * @return
     */
    public boolean validarRut(String rut) {
        boolean validacion = false;
        try {
            rut = StringUtils.upperCase(rut);
            rut = StringUtils.replace(rut, ".", StringUtils.EMPTY);
            rut = StringUtils.replace(rut, "-", StringUtils.EMPTY);
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));
            char dv = rut.charAt(rut.length() - 1);
            int m = 0;
            int s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }
        } catch (java.lang.NumberFormatException e) {
            logger.error("Error en validacion de rut ", e);
        } catch (Exception e) {
            logger.error("Error en validacion de rut ", e);
        }
        return validacion;
    }
}
