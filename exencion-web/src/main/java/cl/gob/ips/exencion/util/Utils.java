package cl.gob.ips.exencion.util;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import cl.gob.ips.exencion.exceptions.FormatException;
import cl.gob.ips.exencion.model.Rut;

/**
 * Created by rreyes on 17-10-2016.
 */
public class Utils {

    private Utils() {

    }

    /**
     *
     * @param rutStr
     * @return
     * @throws Exception
     */
    public static Rut separateRut(String rutStr) throws FormatException {
        if(!StringUtils.isEmpty(rutStr)) {
            Rut rut = new Rut();
            rutStr.toUpperCase().replace(".", "").replace("-", "");
            rut.setRut(new Long(rutStr.substring(0, rutStr.length() - 1)));
            rut.setDv(String.valueOf(rutStr.charAt(rutStr.length() - 1)));
            return rut;
        }else{
            return new Rut();
        }
    }
    
    /**
     * Metodo para comparar periodos desde hasta, donde el periodo hasta no puede ser menos al periodo desde.
     * 
     * @param periodoDesde periodo desde.
     * @param periodoHasta periodo Hasta.
     * @return boolean indicando si el periodo hasta es mayor al periodo desde o no.
     * @since 1.0
     */
	public static Boolean validarPeriodoDesdeHasta (Date periodoDesde, Date periodoHasta) {
		try {
			Calendar calendarDesde = Calendar.getInstance();
			Calendar calendarHasta = Calendar.getInstance();
			
//			SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
			
//			Date dateDesde = formateador.parse(periodoDesde.toString().substring(NUMEROCERO, NUMEROCUATRO) + "-" 
//												+ periodoDesde.toString().substring(NUMEROCUATRO, NUMEROSEIS) + "-" + CEROUNO);
//			Date dateHasta = formateador.parse(periodoHasta.toString().substring(NUMEROCERO, NUMEROCUATRO) + "-" 
//					+ periodoHasta.toString().substring(NUMEROCUATRO, NUMEROSEIS) + "-" + CEROUNO);
			
			calendarDesde.setTime(periodoDesde);
			calendarHasta.setTime(periodoHasta);
			
			if (calendarHasta.after(calendarDesde) || calendarHasta.equals(calendarDesde)) {
				return Boolean.TRUE;
			} else {
				return Boolean.FALSE;
			}
			
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}

    /**
     * @param rut Un rut digito verificador incluido
     * @return rut con formato 11.111.111-1
     */

    public static String formatearRUT(String rut) {

        int cont = 0;
        String format;
        rut = rut.replace(".", "");
        rut = rut.replace("-", "");
        format = "-" + rut.substring(rut.length() - 1);
        for (int i = rut.length() - 2; i >= 0; i--) {
            format = rut.substring(i, i + 1) + format;
            cont++;
            if (cont == 3 && i != 0) {
                format = "." + format;
                cont = 0;
            }
        }
        return format;
    }
}
