package cl.gob.ips.exencion.administracion.view.converter;

import cl.gob.ips.exencion.model.Funcionalidades;
import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import java.util.List;

/**
 * Created by practicante on 21-08-17.
 */
@FacesConverter("funcionalidadesConverter")
public class FuncionalidadesConverter implements Converter{

    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        return getObjectFromUIPickListComponent(component,value);
    }

    public String getAsString(FacesContext context, UIComponent component, Object object) {
        String string;
        if(object == null){
            string="";
        }else{
            try{
                string = String.valueOf(((Funcionalidades)object).getFuncCod());
            }catch(ClassCastException cce){
                throw new ConverterException();
            }
        }
        return string;
    }

    @SuppressWarnings("unchecked")
    private Funcionalidades getObjectFromUIPickListComponent(UIComponent component, String value) {
        final DualListModel<Funcionalidades> dualList;
        try{
            dualList = (DualListModel<Funcionalidades>) ((PickList)component).getValue();
            Funcionalidades funcionalidades = getObjectFromList(dualList.getSource(),Integer.valueOf(value));
            if(funcionalidades==null){
                funcionalidades = getObjectFromList(dualList.getTarget(),Integer.valueOf(value));
            }

            return funcionalidades;
        }catch(ClassCastException cce){
            throw new ConverterException();
        }catch(NumberFormatException nfe){
            throw new ConverterException();
        }
    }

    private Funcionalidades getObjectFromList(final List<?> list, final Integer identifier) {
        for(final Object object:list){
            final Funcionalidades funcionalidades = (Funcionalidades) object;
            if(funcionalidades.getFuncCod().equals(identifier)){
                return funcionalidades;
            }
        }
        return null;
    }
}
