package cl.gob.ips.exencion.procesos.view;

import cl.alaya.component.utils.DateUtil;
import cl.gob.ips.exencion.service.CargaArchivoServices;
import cl.gob.ips.exencion.view.base.BaseControllerView;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by alaya on 29-08-2017.
 */

@ViewScoped
@ManagedBean(name = "cargaSelectivaArchivosView")
public class CargaSelectivaArchivosView extends BaseControllerView implements Serializable {

    private Date periodoCarga;

    @Inject
    private CargaArchivoServices cargaArchivoServices;

    public CargaSelectivaArchivosView() {
        super();
    }

    @PostConstruct
    public void initCargarSelectivaArchivos() {
        this.periodoCarga = new Date();
    }

    public String getPeriodoCarga() {
        return DateUtil.formatoFecha(this.periodoCarga, DateUtil.sdfFechaAnioMesGuionesStandar);
    }

    public void setPeriodoCarga(Date periodoCarga) {
        this.periodoCarga = periodoCarga;
    }
}
