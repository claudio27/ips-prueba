package cl.gob.ips.exencion.mantenedores.view;

import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.GruposArchivo;
import cl.gob.ips.exencion.model.NivelResponsabilidad;
import cl.gob.ips.exencion.model.UsuarioGrupo;
import cl.gob.ips.exencion.model.Usuarios;
import cl.gob.ips.exencion.service.DatosParametricosServices;
import cl.gob.ips.exencion.service.ResponsableGrupoArchivoServices;
import cl.gob.ips.exencion.util.FacesUtils;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by rodrigo.reyesco@gmail.com on 27-10-2016.
 */
@ManagedBean(name = "responsableGrupoArchivoView")
@ViewScoped
public class ResponsableGrupoArchivoView extends BaseControllerView implements Serializable {

    private static final String MSG_SELECCIONE = "Seleccione";
    public static final String FORMULARIO_BUSQUEDA_BTN_BUSCAR = "formulario_busqueda:btn_buscar";
    public static final String TITULO_ERROR = "Error";
    public static final String MSG_ERROR_INIT = "Se ha producido un error al inicializar la pagina";
    private static final String MSG_ERROR_BUSCAR = "Error al buscar responsables";
    private static final String MSG_ERROR_VALIDAR_USUARIO = "Error al Validar los datos de Usuario";
    private static final String FORM_MODAL_CREAR_BTN_CREAR = "form_modal_crear:btn_crear";
    private static final String MSG_USUARIO_NO_EXISTE = "El Usuario Ingresado no existe";
    private static final String MSG_ERROR_CREAR = "Error al Crear el responsable por Grupo de Archivo, El Usuario ya tiene ese nivel de responsabilidad asociado";
    private static final String MSG_ERROR_CREAR_GENERICO = "Error al Crear el responsable por Grupo de Archivo.";
    private static final String MSG_EXITO_CREAR = "El Responsable por Grupo de Archivo se han creado con éxito";
    private static final String TITULO_CRECION_EXITOSA = "Creción Exitosa";

    private final transient Logger logger = Logger.getLogger(this.getClass());

    @Inject
    private transient DatosParametricosServices datosParametricosServices;

    @Inject
    private transient ResponsableGrupoArchivoServices grupoArchivoService;

    private List<GruposArchivo> gruposArchivoList;
    private List<NivelResponsabilidad> nivelResponsabilidadList;
    private List<UsuarioGrupo> usuarioGrupoList;
    private List<UsuarioGrupo> usuarioGrupoDelete;
    private GruposArchivo grupoArchivoSearch;
    private UsuarioGrupo usuarioGrupoCreate;
    private Map<UsuarioGrupo, Boolean> checked;

    @PostConstruct
    public void init() {
        try {
            this.buildGrupoArchivoData();
            this.buildNivelResponsabilidadData();
            this.initFields();
        } catch (DataAccessException e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_INIT);
            logger.error(MSG_ERROR_INIT, e);
        }
    }

    private void initFields() throws NullPointerException {
        usuarioGrupoCreate = new UsuarioGrupo(new NivelResponsabilidad(), new Usuarios(), new GruposArchivo());
        usuarioGrupoDelete = new ArrayList<>();
    }


    private void buildGrupoArchivoData() throws DataAccessException {
        this.setGruposArchivoList(datosParametricosServices.getGruposArchivo());
        this.getGruposArchivoList().add(0, new GruposArchivo(new Integer(0), MSG_SELECCIONE));
    }

    private void buildNivelResponsabilidadData() throws DataAccessException {
        this.setNivelResponsabilidadList(datosParametricosServices.getNivelesResponsabilidad());
    }

    /**
     * valida si un usuario existe en la base de datos antes de crear
     */
    public void validateUsuarioExiste(){
        try {
            if( grupoArchivoService.findUsuarioByEmail(this.getUsuarioGrupoCreate().getTbUsuarios1().getUsuCorreo()) == null ){
                FacesUtils.showErrorMessage(FORM_MODAL_CREAR_BTN_CREAR, TITULO_ERROR, MSG_USUARIO_NO_EXISTE);
            }
        } catch (DataAccessException e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_VALIDAR_USUARIO);
            logger.error(MSG_ERROR_VALIDAR_USUARIO, e);
        }
    }


    /**
     *
     * Metodo encargado de manejar la accion de busqueda
     */
    public void buscarResponsableGrupoArchivo() {
        try {
            this.setUsuarioGrupoList(grupoArchivoService.getUsuarioGrupoByGrupoArchivo(this.getGrupoArchivoSearch()));
        } catch (DataAccessException e) {
            FacesUtils.showErrorMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_ERROR, MSG_ERROR_BUSCAR);
            logger.error(MSG_ERROR_BUSCAR, e);
        }
    }


    /**
     *
     * Accion encargada de manejar la accion que abre el modal de creacion de nuevo responsable por grupo de Archivo
     */
    public void crearResponsableGrupoArchivo() {
        this.setUsuarioGrupoCreate(new UsuarioGrupo(new NivelResponsabilidad(), new Usuarios(), new GruposArchivo()));
    }

    /**
     * Accion encardade de crear la instancia de la entidad usuario grupo desde la vista hacia la base de datos.
     */
    public void doCrearResponsableGrupoArchivo() {
        try {
            this.getUsuarioGrupoCreate().setTbPrGruposArchivo1(this.getGrupoArchivoSearch());
            this.getUsuarioGrupoCreate().setTbUsuarios1(grupoArchivoService.findUsuarioByEmail(this.getUsuarioGrupoCreate().getTbUsuarios1().getUsuCorreo()));
            this.getUsuarioGrupoCreate().setTbPrNivelResponsabilidad(this.getUsuarioGrupoCreate().getTbPrNivelResponsabilidad());
            grupoArchivoService.creaUsuarioGrupoArchivo(this.getUsuarioGrupoCreate());
            this.setUsuarioGrupoList(grupoArchivoService.getUsuarioGrupoByGrupoArchivo(this.getGrupoArchivoSearch()));
            this.setUsuarioGrupoCreate(new UsuarioGrupo(new NivelResponsabilidad(), new Usuarios(), new GruposArchivo()));
            this.showInsertSuccesMessage();
        } catch (DataAccessException e) {
            FacesUtils.showErrorMessage(FORM_MODAL_CREAR_BTN_CREAR, TITULO_ERROR, MSG_ERROR_CREAR_GENERICO);
            logger.error(MSG_ERROR_CREAR_GENERICO, e);
        } catch (Exception e ) {
            FacesUtils.showErrorMessage(FORM_MODAL_CREAR_BTN_CREAR, TITULO_ERROR, MSG_ERROR_CREAR);
            logger.error(MSG_ERROR_CREAR, e);
        }
    }

    private void showInsertSuccesMessage() {
        FacesUtils.showSuccesMessage(FORMULARIO_BUSQUEDA_BTN_BUSCAR, TITULO_CRECION_EXITOSA, MSG_EXITO_CREAR);
        FacesUtils.executeJavascriptOnView("$('#modalCrear').modal('toggle'); scrollToTop();");
    }

    public void cambiaEstado(UsuarioGrupo usuarioGrupo) {
        if (this.getChecked().get(usuarioGrupo)) {
            this.getUsuarioGrupoDelete().add(usuarioGrupo);
        } else {
            this.getUsuarioGrupoDelete().remove(usuarioGrupo);
        }
    }

    public List<GruposArchivo> getGruposArchivoList() {
        return gruposArchivoList;
    }

    public void setGruposArchivoList(List<GruposArchivo> gruposArchivoList) {
        this.gruposArchivoList = gruposArchivoList;
    }

    public GruposArchivo getGrupoArchivoSearch() {
        return grupoArchivoSearch;
    }

    public void setGrupoArchivoSearch(GruposArchivo grupoArchivoSearch) {
        this.grupoArchivoSearch = grupoArchivoSearch;
    }

    public List<UsuarioGrupo> getUsuarioGrupoList() {
        return usuarioGrupoList;
    }

    public void setUsuarioGrupoList(List<UsuarioGrupo> usuarioGrupoList) {
        this.usuarioGrupoList = usuarioGrupoList;
    }

    public UsuarioGrupo getUsuarioGrupoCreate() {
        return usuarioGrupoCreate;
    }

    public void setUsuarioGrupoCreate(UsuarioGrupo usuarioGrupoCreate) {
        this.usuarioGrupoCreate = usuarioGrupoCreate;
    }

    public List<NivelResponsabilidad> getNivelResponsabilidadList() {
        return nivelResponsabilidadList;
    }

    public void setNivelResponsabilidadList(List<NivelResponsabilidad> nivelResponsabilidadList) {
        this.nivelResponsabilidadList = nivelResponsabilidadList;
    }

    public Map<UsuarioGrupo, Boolean> getChecked() {
        return checked;
    }

    public void setChecked(Map<UsuarioGrupo, Boolean> checked) {
        this.checked = checked;
    }

    public List<UsuarioGrupo> getUsuarioGrupoDelete() {
        return usuarioGrupoDelete;
    }

    public void setUsuarioGrupoDelete(List<UsuarioGrupo> usuarioGrupoDelete) {
        this.usuarioGrupoDelete = usuarioGrupoDelete;
    }
}
