package cl.gob.ips.exencion.informes.view;

import cl.gob.ips.exencion.dto.InformeGestionDTO;
import cl.gob.ips.exencion.dto.InformeGestionExtintoDTO;
import cl.gob.ips.exencion.dto.InformeGestionMantienenDTO;
import cl.gob.ips.exencion.dto.InformeGestionNuevosDTO;
import cl.gob.ips.exencion.model.InformeGestionExtinto;
import cl.gob.ips.exencion.model.InformeGestionMantienen;
import cl.gob.ips.exencion.model.InformeGestionNuevos;
import cl.gob.ips.exencion.enums.PresentadoPor;
import cl.gob.ips.exencion.enums.TipoPeriodo;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.model.InformeGestionVer;
import cl.gob.ips.exencion.service.ConsultaBeneficioServices;
import cl.gob.ips.exencion.service.InformeGestionServices;
import cl.gob.ips.exencion.service.PensionadosServices;
import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.util.Reportes;
import cl.gob.ips.exencion.util.Utils;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.log4j.Logger;
import org.apache.poi.util.IOUtils;

import javax.annotation.PostConstruct;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ManagedBean(name = "informeGestionView")
@ViewScoped
public class InformeGestionView extends BaseControllerView implements Serializable {

    private static final long serialVersionUID = 621706724945777647L;
    private static final Logger LOGGER = Logger.getLogger(InformeGestionView.class);
    private Date periodoDesde;
    private Date periodoHasta;
    private Boolean mostrarGrilla;
    private List<InformeGestionDTO> respuestaGrilla;
    private List<InformeGestionNuevosDTO> respuestaGrillaNuevos;
    private List<InformeGestionNuevosDTO> respuestaGrillaNuevosDos;
    private List<InformeGestionExtintoDTO> respuestaGrillaExtinciones;
    private List<InformeGestionMantienenDTO> respuestaGrillaMantienen;
    private List<InformeGestionExtinto> respuestaGrillaExtintos;
    private List<InformeGestionDTO> respuestaGrillaPrueba;
    private List<InformeGestionDTO> respuestaGrillaVer;
    private Integer codPresentadoPor;
    private PresentadoPor[] presentadoPor;
    private Integer codTipoPeriodo;
    private TipoPeriodo[] tipoPeriodo;
    private static Integer periodoSeleccionado;

    @Inject
    private PensionadosServices pensionadosServices;

    @Inject
    private ConsultaBeneficioServices consultaBeneficioServices;

    @Inject
    private InformeGestionServices informeGestionServices;


    @PostConstruct
    private void init() {
        this.mostrarGrilla = Boolean.FALSE;
        this.presentadoPor = PresentadoPor.values();
        this.tipoPeriodo = TipoPeriodo.values();
        this.codTipoPeriodo = 0;
        this.codPresentadoPor = 0;
        this.respuestaGrillaExtintos = new ArrayList<>();

    }

    public Integer parseDateToInteger(Date fecha) {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
        String sFecha = dateFormat.format(fecha);
        Integer retorno = Integer.parseInt(sFecha);
        return retorno;
    }

    public void changeTipoPeriodo(Integer codTipoPeriodoSelected){
        this.codTipoPeriodo = codTipoPeriodoSelected;
        LOGGER.debug("Codigo Capturado: " + codTipoPeriodoSelected);
        LOGGER.debug("Tipo Periodo: " + tipoPeriodo);
        LOGGER.debug("Codigo tipoPeriodo: " + codTipoPeriodo);
    }

    public void changePresentadoPor(Integer codPresentadoPor){
        this.codPresentadoPor = codPresentadoPor;
        LOGGER.debug("Codigo Capturado: " + codPresentadoPor);
        LOGGER.debug("Presentado Por: " + tipoPeriodo);
        LOGGER.debug("Codigo Tipo Periodo: " + codTipoPeriodo);
    }

    public void buscar() throws DataAccessException {
        LOGGER.debug("buscar()");
        LOGGER.debug("periodoDesde: " + this.periodoDesde);
        LOGGER.debug("periodoHasta:  " + this.periodoHasta);

        if (null != this.periodoDesde && null != this.periodoHasta) {
            if (Utils.validarPeriodoDesdeHasta(this.periodoDesde, this.periodoHasta)) {
                List<InformeGestionDTO> informeDTO = new ArrayList<>();
                this.respuestaGrilla = new ArrayList<InformeGestionDTO>();

                if (null != codTipoPeriodo && null != codPresentadoPor) {
                    informeDTO = informeGestionServices.getInformeGestion(parseDateToInteger(periodoDesde), parseDateToInteger(periodoHasta), codTipoPeriodo, codPresentadoPor);

                    if (informeDTO==null){
                        MensajesUtil.setMensajeErrorContexto("validacion.informe.gestion.no.informacion");
                        super.mostrarMensajeError();
                        LOGGER.error("No se encontró información dentro de los periodos ingresados");
                        this.mostrarGrilla = Boolean.FALSE;
                        return;
                    }

                    this.respuestaGrilla = informeDTO;
                    this.mostrarGrilla = Boolean.TRUE;

                    //TODO - realizar busqueda de los datos
                }

            } else {
                limpiar();
                MensajesUtil.setMensajeErrorContexto("validacion.movimientos.rut.periodosDesdeHasta");
                super.mostrarMensajeError();
                LOGGER.debug("is validation");
            }

        } else {
            MensajesUtil.setMensajeErrorContexto("validacion.test");
            super.mostrarMensajeError();
            LOGGER.debug("is error");
        }
        LOGGER.debug("Fin buscar()");
    }

    public void cargarNuevosVer(Integer periodo) throws DataAccessException, IpsBussinesException{
        LOGGER.debug("cargarNuevos()" );
        LOGGER.debug("periodo: " + periodo);
        LOGGER.debug("codTipoPeriodo: " + this.codTipoPeriodo);

        try{

//            List<InformeGestionMantienen> resultMantienen = informeGestionServices.getInformeMantienen(periodo, this.codTipoPeriodo);
//            this.respuestaGrillaMantienen = resultMantienen;


            this.respuestaGrillaNuevosDos = new ArrayList<InformeGestionNuevosDTO>();
            List<InformeGestionNuevos> resultados = informeGestionServices.getInformeNuevos(periodo, this.codTipoPeriodo);
            this.mapeaNuevos(resultados);
        } catch (EJBException | DataAccessException e) {
            super.muestraErrorDeAccesoaDatos(e);
            return;
        }
        //LOGGER.debug("Prueba respuestaGrillaNuevosDos" + this.respuestaGrillaNuevos.get(0).getNombre());
        LOGGER.debug("Fin cargarNuevos()");
    }

    public void limpiar() {
        this.periodoDesde = null;
        this.periodoHasta = null;
        this.mostrarGrilla = Boolean.FALSE;
    }

    public void obtenerExcelGeneral() throws IOException {

        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();

        OutputStream output = null;
        InputStream input = null;

        try {
            File archivo = Reportes.obtenerInformeGestion(this.respuestaGrilla);

            ec.responseReset();
            ec.setResponseContentType("application/vnd.ms-excel");
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + archivo.getName() + "\"");

            output = ec.getResponseOutputStream();
            input = new FileInputStream(archivo);

            IOUtils.copy(input, output);
        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado: " + e.getMessage());
        } catch (IOException e) {
            LOGGER.error("Ha ocurrido un error: " + e.getMessage());
        } finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(input);
        }

        fc.responseComplete();
    }

    public void obtenerExcelNuevos() throws IOException {

        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();

        OutputStream output = null;
        InputStream input = null;

        try {
            File archivo = Reportes.obtenerInformeGestionNuevos(this.respuestaGrillaNuevos);

            ec.responseReset();
            ec.setResponseContentType("application/vnd.ms-excel");
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + archivo.getName() + "\"");

            output = ec.getResponseOutputStream();
            input = new FileInputStream(archivo);

            IOUtils.copy(input, output);
        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado: " + e.getMessage());
        } catch (IOException e) {
            LOGGER.error("Ha ocurrido un error: " + e.getMessage());
        } finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(input);
        }

        fc.responseComplete();
    }

    public void cargarNuevos( Integer periodo ) throws DataAccessException {
        this.respuestaGrillaNuevos = new ArrayList<>();
        List<InformeGestionNuevos> informeGestionNuevos = this.informeGestionServices.getInformeNuevos(periodo, this.codTipoPeriodo);
        mapeaNuevos(informeGestionNuevos);
    }

    public void cargarExtinciones( Integer periodo ) throws DataAccessException {
        this.respuestaGrillaExtinciones = new ArrayList<>();
        List<InformeGestionExtinto> informeGestionExtintos = this.informeGestionServices.getInformeExtintos(periodo, this.codTipoPeriodo);
        mapeaExtintos(informeGestionExtintos);
    }

    public void cargarMantienen(Integer periodo) throws DataAccessException, IpsBussinesException{
        this.respuestaGrillaMantienen = new ArrayList<>();
        List<InformeGestionMantienen> informeGestionMantienen = this.informeGestionServices.getInformeMantienen(periodo, this.codTipoPeriodo);
        mapeaMantienen(informeGestionMantienen);
    }

    public List<InformeGestionExtinto> listaInformeGestionExtinto() throws DataAccessException {
        return informeGestionServices.getInformeExtintos(201703, 1);
    }


    public void obtenerExcelMantienen() throws IOException {

        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();

        OutputStream output = null;
        InputStream input = null;

        try {
            System.out.println(this.respuestaGrillaMantienen.size());
            File archivo = Reportes.obtenerInformeGestionMantienen(this.respuestaGrillaMantienen);

            ec.responseReset();
            ec.setResponseContentType("application/vnd.ms-excel");
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + archivo.getName() + "\"");

            output = ec.getResponseOutputStream();
            input = new FileInputStream(archivo);

            IOUtils.copy(input, output);
        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado: " + e.getMessage());
        } catch (IOException e) {
            LOGGER.error("Ha ocurrido un error: " + e.getMessage());
        } finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(input);
        }

        fc.responseComplete();
    }

    public String obtenerPdf() throws DocumentException {

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Document documento = Reportes.obtenerPdf();
            documento.close();

            PdfWriter.getInstance(documento, baos);

            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.setContentType("Application/x-pdf");
            response.setHeader("Content-Disposition", "attachment; filename=" + "archivo");
            response.getOutputStream().write(baos.toByteArray());
            response.getOutputStream().flush();
            response.getOutputStream().close();
            context.responseComplete();


//			InputStream is = new ByteArrayInputStream(baos.toByteArray());
//			super.downloadFile = new DefaultStreamedContent(is, "Application/x-pdf", "archivo");

        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado: " + e.getMessage());
        } catch (IOException e) {
            LOGGER.error("Ha ocurrido un error: " + e.getMessage());
        }
        return "";
    }

    public void cargarNuevosPorInstitucion(String institucion) {
        LOGGER.debug("cargando datos nuevos " + periodoSeleccionado);
        //TODO Determinar el tipo de periodo, si es periodo de concecion o bonficacion
        //this.respuestaGrillaNuevos = new ArrayList<InformeGestionNuevos>();
        try {
            List<InformeGestionNuevos> resultados = informeGestionServices.obtenerDatosVerNuevos(periodoSeleccionado, institucion);
            this.mapea(resultados);


        } catch (DataAccessException e) {
            LOGGER.error("Fallo la obtencion de datos", e);
        } catch (Throwable e) {
            LOGGER.error("Fallo la obtencion de datos", e);
        }

    }

    public void obtenerExcelExtinciones() throws IOException {

        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();

        OutputStream output = null;
        InputStream input = null;

        try {

            if (this.respuestaGrillaExtintos==null){
                MensajesUtil.setMensajeErrorContexto("La grilla no contiene datos para exportar");
                super.mostrarMensajeError();
                LOGGER.error("La grilla es null");
                return;
            }
            File archivo = Reportes.obtenerInformeGestionExtinciones(this.respuestaGrillaExtinciones);

            ec.responseReset();
            ec.setResponseContentType("application/vnd.ms-excel");
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + archivo.getName() + "\"");

            output = ec.getResponseOutputStream();
            input = new FileInputStream(archivo);

            IOUtils.copy(input, output);
        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado: " + e.getMessage());
        } catch (IOException e) {
            LOGGER.error("Ha ocurrido un error: " + e.getMessage());
        } finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(input);
        }

        fc.responseComplete();
    }

    public void cargarVer(String periodo) {

        LOGGER.debug("cargarVer()");
        LOGGER.debug("periodo : " + periodo);
        LOGGER.debug("this.getCodTipoPeriodo() : " + this.getCodTipoPeriodo());


        periodoSeleccionado = Integer.valueOf(periodo);
        this.respuestaGrillaVer = new ArrayList<InformeGestionDTO>(); //TODO tomar el periodo de la consulta
        try {
            List<InformeGestionVer> informe = informeGestionServices.obtenerDatosVerPorTipoPeriodo(Integer.valueOf(periodo), this.getCodTipoPeriodo());

            for (InformeGestionVer ver : informe) {
                InformeGestionDTO informeDTODos = new InformeGestionDTO();
                informeDTODos.setInstitucion(ver.getInstitucion()!=null ? ver.getInstitucion():"");
                informeDTODos.setTipoEntidad(ver.getTipoEntidad()!=null ? ver.getTipoEntidad():"");
                informeDTODos.setTotal(ver.getTotal()!=null ? ver.getTotal():0);
                informeDTODos.setNuevos(ver.getNuevos()!=null ? ver.getNuevos():0);
                informeDTODos.setExtinciones(ver.getExtintos()!=null ? ver.getExtintos():0);
                informeDTODos.setMantienen(ver.getMantienen()!=null ? ver.getMantienen():0);
                this.respuestaGrillaVer.add(informeDTODos);
            }
        } catch (EJBException | DataAccessException e) {
            super.muestraErrorDeAccesoaDatos(e);
            return;
        }

        LOGGER.debug("Fin Ver()");

    }

    public void obtenerExcelVer() throws IOException {
        try {
            File archivo = Reportes.obtenerInformeGestionVer(this.respuestaGrillaVer);
            super.generarExcel(archivo);
        } catch (FileNotFoundException e) {
            LOGGER.error("Archivo no encontrado: " + e.getMessage());
        } catch (IOException e) {
            LOGGER.error("Ha ocurrido un error: " + e.getMessage());
        }
    }

    public Date getPeriodoDesde() {
        return periodoDesde;
    }

    public void setPeriodoDesde(Date periodoDesde) {
        this.periodoDesde = periodoDesde;
    }

    public Date getPeriodoHasta() {
        return periodoHasta;
    }

    public void setPeriodoHasta(Date periodoHasta) {
        this.periodoHasta = periodoHasta;
    }

    public Boolean getMostrarGrilla() {
        return mostrarGrilla;
    }

    public void setMostrarGrilla(Boolean mostrarGrilla) {
        this.mostrarGrilla = mostrarGrilla;
    }

    public List<InformeGestionDTO> getRespuestaGrilla() {
        return respuestaGrilla;
    }

    public void setRespuestaGrilla(List<InformeGestionDTO> respuestaGrilla) {
        this.respuestaGrilla = respuestaGrilla;
    }

    public List<InformeGestionNuevosDTO> getRespuestaGrillaNuevosDTO() {
        return respuestaGrillaNuevos;
    }

    public void setRespuestaGrillaNuevosDTO(
            List<InformeGestionNuevosDTO> respuestaGrillaNuevos) {
        this.respuestaGrillaNuevos = respuestaGrillaNuevos;
    }

    public List<InformeGestionExtintoDTO> getRespuestaGrillaExtincionesDTO() {
        return respuestaGrillaExtinciones;
    }

    public void setRespuestaGrillaExtincionesDTO(
            List<InformeGestionExtintoDTO> respuestaGrillaExtinciones) {
        this.respuestaGrillaExtinciones = respuestaGrillaExtinciones;
    }

    public List<InformeGestionExtinto> getRespuestaGrillaExtintos() {
        return respuestaGrillaExtintos;
    }

    public void setRespuestaGrillaExtintos(List<InformeGestionExtinto> respuestaGrillaExtintos) {
        this.respuestaGrillaExtintos = respuestaGrillaExtintos;
    }

    public List<InformeGestionMantienenDTO> getRespuestaGrillaMantienenDTO() {
        return respuestaGrillaMantienen;
    }

    public void setRespuestaGrillaMantienenDTO(
            List<InformeGestionMantienenDTO> respuestaGrillaMantienen) {
        this.respuestaGrillaMantienen = respuestaGrillaMantienen;
    }

    public List<InformeGestionDTO> getRespuestaGrillaVer() {
        return respuestaGrillaVer;
    }

    public void setRespuestaGrillaVer(List<InformeGestionDTO> respuestaGrillaVer) {
        this.respuestaGrillaVer = respuestaGrillaVer;
    }

    public Integer getCodPresentadoPor() {
        return codPresentadoPor;
    }

    public void setCodPresentadoPor(Integer codPresentadoPor) {
        this.codPresentadoPor = codPresentadoPor;
    }

    public PresentadoPor[] getPresentadoPor() {
        return presentadoPor;
    }

    public void setPresentadoPor(PresentadoPor[] presentadoPor) {
        this.presentadoPor = presentadoPor;
    }

    public Integer getCodTipoPeriodo() {
        return codTipoPeriodo;
    }

    public void setCodTipoPeriodo(Integer codTipoPeriodo) {
        this.codTipoPeriodo = codTipoPeriodo;
    }

    public TipoPeriodo[] getTipoPeriodo() {
        return tipoPeriodo;
    }

    public void setTipoPeriodo(TipoPeriodo[] tipoPeriodo) {
        this.tipoPeriodo = tipoPeriodo;
    }

    public List<InformeGestionNuevosDTO> getRespuestaGrillaNuevosDos() {
        return respuestaGrillaNuevosDos;
    }

    public void setRespuestaGrillaNuevosDos(List<InformeGestionNuevosDTO> respuestaGrillaNuevosDos) {
        this.respuestaGrillaNuevosDos = respuestaGrillaNuevosDos;
    }

    public void mapea(List<InformeGestionNuevos> resultados) {

        for (InformeGestionNuevos data : resultados) {
            InformeGestionNuevosDTO informeDTO = new InformeGestionNuevosDTO();
            informeDTO.setRun(data.getRun()!=null ? Utils.formatearRUT(data.getRun()):"");
            informeDTO.setNombre(data.getNombre()!=null ? data.getNombre():"");
            informeDTO.setApellidoPaterno(data.getApellidoPaterno()!=null ? data.getApellidoPaterno():"");
            informeDTO.setApellidoMaterno(data.getApellidoMaterno()!=null ? data.getApellidoMaterno():"");
            informeDTO.setTipoPensionado(data.getTipoPensionado()!=null ? data.getTipoPensionado():"");
            informeDTO.setInstitucion(data.getInstitucion()!=null ? data.getInstitucion():"");
            this.respuestaGrillaNuevos.add(informeDTO);
        }
    }
    public void mapeaNuevos(List<InformeGestionNuevos> resultados) {

        for (InformeGestionNuevos data : resultados) {
            InformeGestionNuevosDTO informeDTO = new InformeGestionNuevosDTO();
            informeDTO.setRun(data.getRun()!=null ? Utils.formatearRUT(data.getRun()):"");
            informeDTO.setNombre(data.getNombre()!=null ? data.getNombre():"");
            informeDTO.setApellidoPaterno(data.getApellidoPaterno()!=null ? data.getApellidoPaterno():"");
            informeDTO.setApellidoMaterno(data.getApellidoMaterno()!=null ? data.getApellidoMaterno():"");
            informeDTO.setTipoPensionado(data.getTipoPensionado()!=null ? data.getTipoPensionado():"");
            informeDTO.setInstitucion(data.getInstitucion()!=null ? data.getInstitucion():"");
            this.respuestaGrillaNuevosDos.add(informeDTO);
        }
    }

    public void mapeaExtintos(List<InformeGestionExtinto> resultados) {

        for (InformeGestionExtinto data : resultados) {
            InformeGestionExtintoDTO informeDTO = new InformeGestionExtintoDTO();
            informeDTO.setRun(data.getRun()!=null ? Utils.formatearRUT(data.getRun()):"");
            informeDTO.setNombre(data.getNombre()!=null ? data.getNombre():"");
            informeDTO.setApellidoPaterno(data.getApellidoPaterno()!=null ? data.getApellidoPaterno():"");
            informeDTO.setApellidoMaterno(data.getApellidoMaterno()!=null ? data.getApellidoMaterno():"");
            informeDTO.setTipoPensionado(data.getTipoPensionado()!=null ? data.getTipoPensionado():"");
            informeDTO.setInstitucion(data.getInstitucion()!=null ? data.getInstitucion():"");
            this.respuestaGrillaExtinciones.add(informeDTO);
        }
    }

    public void mapeaMantienen(List<InformeGestionMantienen> resultados) {

        for (InformeGestionMantienen data : resultados) {
            InformeGestionMantienenDTO informeDTO = new InformeGestionMantienenDTO();
            informeDTO.setRun(data.getRun()!=null ? Utils.formatearRUT(data.getRun()):"");
            informeDTO.setNombre(data.getNombre()!=null ? data.getNombre():"");
            informeDTO.setApellidoPaterno(data.getApellidoPaterno()!=null ? data.getApellidoPaterno():"");
            informeDTO.setApellidoMaterno(data.getApellidoMaterno()!=null ? data.getApellidoMaterno():"");
            informeDTO.setTipoPensionado(data.getTipoPensionado()!=null ? data.getTipoPensionado():"");
            informeDTO.setInstitucion(data.getInstitucion()!=null ? data.getInstitucion():"");
            this.respuestaGrillaMantienen.add(informeDTO);
        }
    }
}