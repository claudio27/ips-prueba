var checker = 0;

function determinarEnlace(){
    var actual = location.href.split("/")[ location.href.split("/").length - 1 ].split("#")[0].split("?")[0],
        aside = $("aside.navbar-nav"),
        enlace = aside.find("a[href*='" + actual + "']"),
        padres = enlace.parentsUntil("aside"),
        niveles = padres.length;

    enlace.addClass("active");

    //6 --> ul li ul li ul li --> 3 niveles
    //4 --> ul li ul li       --> 2 niveles
    //2 --> ul li             --> 1 nivel
    if(niveles == 6){
        setTimeout(function(){
            console.log(1);
            enlace.parent().parent().parent().parent().parent().find("button").first().trigger("click").addClass("active");
        }, 500);
        setTimeout(function(){
            console.log(2);
            enlace.parent().parent().parent().find(" > a").trigger("click")
        }, 1000);
    } else if(niveles == 4){
        setTimeout(function(){
            console.log(2);
            enlace.parent().parent().parent().find(" > button").trigger("click").addClass("active");
        }, 1000);
    }
}

$(function() {
    $('#olvido').popover();
});

$("#boton-menu1").click(function(a) {
    a.preventDefault();
    $("#menu1").toggle(500);
    $("#boton-menu1").toggleClass("active");
});

$("#boton-menu2").click(function(a) {
    a.preventDefault();
    $("#menu2").toggle(500);
    $("#boton-menu2").toggleClass("active");
});

$("#boton-menu3").click(function(a) {
    a.preventDefault();
    $("#menu3").toggle(500);
    $("#boton-menu3").toggleClass("active");
});

$("#boton-menu4").click(function(a) {
    a.preventDefault();
    $("#menu4").toggle(500);
    $("#boton-menu4").toggleClass("active");
});

$(document).ready(function() {
    determinarEnlace()


    $(".dropdown-submenu > a").submenupicker();
    $("#elmenu [role='data-accordion']").accordion();
    $("#multiple [role='data-accordion']").accordion({singleOpen:!1});
    $("#single [role='data-accordion']").accordion({transitionEasing:"cubic-bezier(0.455, 0.030, 0.515, 0.955)", transitionSpeed:200});
    $("#elmenu [role='data-accordion']").accordion();
    $("#multiple [role='data-accordion']").accordion({singleOpen:!1});
    $("#elmenu [role='data-accordion']").accordion();
    $("#multiple [role='data-accordion']").accordion({singleOpen:!1});

    $("#menu1").hide();
    $("#menu2").hide();
    $("#menu3").hide();
    $("#menu4").hide();

    var menu_actual = actual = location.href.split("/")[ location.href.split("/").length - 1 ].split("#")[0].split("?")[1];

    if(menu_actual == "seccion=1"){
        $("#menu1").show();
        $("#boton-menu1").addClass("active");
    }
    if(menu_actual == "seccion=2"){
        $("#menu2").show();
        $("#boton-menu2").addClass("active");
    }
    if(menu_actual == "seccion=3"){
        $("#menu3").show();
        $("#boton-menu3").addClass("active");
    }
    if(menu_actual == "seccion=4"){
        $("#menu4").show();
        $("#boton-menu4").addClass("active");
    }

    //bloquear input específicos según valores
    $(function() {
        $("#modal_agregar_entidad").on('change', function() {
            if ($(this).val() == "mutual" || $(this).val() == "seguro") {
                $('#archiv_nomina_agregar').attr("disabled", true);
            }else {
                $('#archiv_nomina_agregar').attr("disabled", false);
            }
        });
        $("#modal_editar_entidad").on('change', function() {
            if ($(this).val() == "mutual" || $(this).val() == "seguro") {
                $('#archiv_nomina_editar').attr("disabled", true);
            }else {
                $('#archiv_nomina_editar').attr("disabled", false);
            }
        });
    })
    //termina bloqueo input
});
$("#menu-toggle").click(function(a) {
    a.preventDefault();
    $("#faq").toggleClass("toggled");
});
$("#menu-toggle2").click(function(a) {
    a.preventDefault();
    $("#wrapper").toggleClass("toggled");
    $("#elmenu").toggleClass("toggled");
    $("#lefts").toggleClass("toggled");
    $("#cambio").toggleClass("fa-angle-double-right");
});


$("#select1, #select2").on("focusout", function() {
    validarOnFocusOut($(this));
});

$("#anios").on("focusout", function() {
    validarOnFocusOut($(this));
});

$(".ingresarRun").on("focusout", function() {
    validarOnFocusOut($(this));
});
$("#meses, #anios").on("focusout", function() {
    validarOnFocusOut($(this));
});

function CompruebaCheckBox(checkerbox,div){
    if (checkerbox.checked){
        document.getElementById(div).style.display="block"
    } else {
        document.getElementById(div).style.display="none"
    }
}

function scrollToTop(){
    $('html, body').animate({
        scrollTop: $("#page-wrapper").offset().top
    }, 500);
}

function scrollTo(selector){
    $('html, body').animate({
        scrollTop: $(selector).offset().top
    }, 500);
}

$('.ingresarRun').Rut({
    /*on_error: function(){ alert('Rut incorrecto'); },*/
    format_on: 'keyup'
});

/*Menu height 100%*/
$(".navbar-toggle" ).click(function() {
    setTimeout( function(){
        if($('.navbar-collapse').attr('aria-expanded') === "true") {
            $('.navbar-inverse').addClass('menu-expandido');
        }else{
            $('.navbar-inverse').removeClass('menu-expandido')
        };
    }, 10)
});

/**** Validaciones extras de rut
 * caracteres extraños
 * espacios dentro del campo
 * borar ceros
 * *****/
$('.ingresarRun').on('keypress',function() {
    $('.ingresarRun').val($.trim($('.ingresarRun').val()));
});

$(".ingresarRun").on("focusout", function () {
    if( parseInt($(this).val()) <= 0 ){
        $(this).val("");
    }
    $(this).val( $(this).val().replace(/\ /g, "") );
});

$(".ingresarRun").on("keyup", function(event){
    if(
        event.keyCode != 8 &&
        event.keyCode != 9 &&
        event.keyCode != 46 &&
        event.keyCode != 48 &&
        event.keyCode != 49 &&
        event.keyCode != 50 &&
        event.keyCode != 51 &&
        event.keyCode != 52 &&
        event.keyCode != 53 &&
        event.keyCode != 54 &&
        event.keyCode != 55 &&
        event.keyCode != 56 &&
        event.keyCode != 57 &&
        event.keyCode != 75 &&
        event.keyCode != 96 &&
        event.keyCode != 97 &&
        event.keyCode != 98 &&
        event.keyCode != 99 &&
        event.keyCode != 100 &&
        event.keyCode != 101 &&
        event.keyCode != 102 &&
        event.keyCode != 103 &&
        event.keyCode != 104 &&
        event.keyCode != 105
    ){
        $(this).val( $(this).val().substr(0, $(this).val().length - 1) )
    }
});

