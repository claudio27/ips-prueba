/**

 * Funcion que se ejecuta al seleccionar una opcion del primer select

 */

function cargarSelect2(valor)

{

    /**

     * Este array contiene los valores sel segundo select

     * Los valores del mismo son:

     *  - hace referencia al value del primer select. Es para saber que valores

     *  mostrar una vez se haya seleccionado una opcion del primer select

     *  - value que se asignara

     *  - testo que se asignara

     */

    var arrayValores=new Array(

        new Array(1,1,"Pendiente"),

        new Array(1,2,"Cerrado"),

        new Array(2,1,"Fecha 1"),

        new Array(3,1,"Gestionar emisión"),

        new Array(3,2,"Cargar Tablas Paramétricas"),
		
		new Array(3,3,"Pagos diarios")

    );

    if(valor==0)

    {

        // desactivamos el segundo select

        document.getElementById("select2").disabled=true;

    }else{

        // eliminamos todos los posibles valores que contenga el select2

        document.getElementById("select2").options.length=0;

 

        // añadimos los nuevos valores al select2

        document.getElementById("select2").options[0]=new Option("Seleccionar opción", "0");

        for(i=0;i<arrayValores.length;i++)

        {

            // unicamente añadimos las opciones que pertenecen al id seleccionado

            // del primer select

            if(arrayValores[i][0]==valor)

            {

                document.getElementById("select2").options[document.getElementById("select2").options.length]=new Option(arrayValores[i][2], arrayValores[i][1]);

            }

        }

 

        // habilitamos el segundo select

        document.getElementById("select2").disabled=false;

    }

}

 

/**

 * Una vez selecciona una valor del segundo selecte, obtenemos la información

 * de los dos selects y la mostramos

 */

function seleccinado_select2(value)
{

    var v1 = document.getElementById("select1");

    var valor1 = v1.options[v1.selectedIndex].value;

    var text1 = v1.options[v1.selectedIndex].text;

    var v2 = document.getElementById("select2");

    var valor2 = v2.options[v2.selectedIndex].value;

    var text2 = v2.options[v2.selectedIndex].text;

    alert("Se ha seleccionado en ("+text1+") un sub valor ("+text2+") para su busqueda");

}
