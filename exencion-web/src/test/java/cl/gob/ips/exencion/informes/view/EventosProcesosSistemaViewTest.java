package cl.gob.ips.exencion.informes.view;

import cl.alaya.component.utils.DateUtil;
import cl.gob.ips.exencion.BaseTest;
import cl.gob.ips.exencion.dao.BitacoraDao;
import cl.gob.ips.exencion.dto.EventosProcesosSistemaDTO;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.model.Bitacora;
import cl.gob.ips.exencion.service.BitacoraServices;
import cl.gob.ips.exencion.util.MensajesUtil;
import cl.gob.ips.exencion.util.Reportes;
import cl.gob.ips.exencion.view.base.BaseControllerView;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.context.RequestContext;

import javax.ejb.EJBException;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(PowerMockRunner.class)
@PrepareForTest({EventosProcesosSistemaView.class, MensajesUtil.class, Reportes.class})
public class EventosProcesosSistemaViewTest extends BaseTest{

    private static final Logger LOG = Logger.getLogger(EventosProcesosSistemaView.class);

    @Mock
    @Inject
    private BitacoraServices bitacoraMock;

    @Mock
    @Inject
    private BitacoraDao bitacoraDaoMock;

    @Mock
    @Inject
    private BaseControllerView baseControllerViewMock;

    @Inject
    @InjectMocks
    private EventosProcesosSistemaView ev;

    @Mock
    private MensajesUtil me;

    @Mock
    private Reportes reportesMock;

    @Mock
    private File fileMock;

    private Date fechaPeriodo;

    @Before
    public void setUp() throws Exception {PowerMockito.mockStatic(FacesContext.class);
        MockitoAnnotations.initMocks(this);

        //se genera maqueta para los mensajes de la aplicación
        PowerMockito.mockStatic(MensajesUtil.class);
        PowerMockito.mockStatic(Reportes.class);
        PowerMockito.doNothing().when(MensajesUtil.class, "setMensajeErrorContexto", Mockito.any(String.class));

        this.fechaPeriodo = DateUtil.generarFecha("2017-04-01 00:00:00", DateUtil.sdfFechaGuionesHoraSegEstandar);
    }

    @Test
    public void buscarEventos() throws DataAccessException {
        List<Bitacora> bitacoras = null;

        List<EventosProcesosSistemaDTO> respuestaGrilla = new ArrayList<EventosProcesosSistemaDTO>();
        respuestaGrilla.add(new EventosProcesosSistemaDTO());
        respuestaGrilla.add(new EventosProcesosSistemaDTO());
        respuestaGrilla.add(new EventosProcesosSistemaDTO());
        respuestaGrilla.add(new EventosProcesosSistemaDTO());
        //this.bitacoraServices.obtenerBitacoraPorPeriodo(periodoConsulta);
        Mockito.doReturn(respuestaGrilla).when(bitacoraMock).obtenerBitacoraPorPeriodo(Mockito.isA(Integer.class));
        //Mockito.when(bitacoras)
        this.ev.setPeriodo(this.fechaPeriodo);
        this.ev.buscar();
    }

    @Test
    public void buscarNull() throws Exception {
        List<EventosProcesosSistemaDTO> respuestaGrilla = null;

        Mockito.doReturn(null).when(bitacoraMock).obtenerBitacoraPorPeriodo(Mockito.isA(Integer.class));

        EventosProcesosSistemaView even = Mockito.spy(new EventosProcesosSistemaView());

        super.ignoreSuperImplement(EventosProcesosSistemaView.class, "mostrarMensajeError");

        this.ev.setPeriodo(this.fechaPeriodo);
        this.ev.buscar();
    }

    @Test
    public void buscarnpe() throws Exception {
        super.ignoreSuperImplement(EventosProcesosSistemaView.class, "mostrarMensajeError");
        this.ev.buscar();
    }

    @Test
    public void buscarejbe() throws Exception {
        Mockito.when(this.bitacoraMock.obtenerBitacoraPorPeriodo(Mockito.isA(Integer.class))).thenThrow(EJBException.class);
        super.ignoreSuperImplement(EventosProcesosSistemaView.class, "muestraErrorDeAccesoaDatos", Throwable.class);

        this.ev.setPeriodo(this.fechaPeriodo);
        this.ev.buscar();
    }

    @Test
    public void limpiar() {
        this.ev.limpiar();
    }

    @Test
    public void obtenerExcelOK() throws Exception {
        //se genera maqueta del objeto file
        PowerMockito.doReturn(fileMock).when(Reportes.class,
                "obtenerEventosProcesosSistema", Mockito.any(List.class),
                    Mockito.any(String.class));
        super.ignoreSuperImplementProtectedMethod(BaseControllerView.class,
                "generarExcel", File.class);

        this.ev.setPeriodo(this.fechaPeriodo);
        this.ev.obtenerExcel();
    }

    @Ignore
    public void obtenerExcelFileE() throws Exception {
        //Maqueta para obtener FileNotFoundException
        //se genera maqueta del objeto file con valor null

        PowerMockito.doReturn(null).when(Reportes.class, "obtenerEventosProcesosSistema", Mockito.any(List.class));

        this.ev.obtenerExcel();
    }
}