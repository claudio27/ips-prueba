package cl.gob.ips.exencion.util;

import cl.gob.ips.exencion.BaseTest;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;
import java.util.Hashtable;
import java.util.ResourceBundle;

import static org.junit.Assert.assertNull;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LDAPUtil.class, InitialDirContext.class})
public class LDAPUtilTest extends BaseTest {
    private static transient ResourceBundle properties;

    private static final Logger LOG =Logger.getLogger(LDAPUtil.class);

    @Mock
    private InitialDirContext initialDirContextMock;

    @Test
    public void testGetLDAPProperties() throws Exception {
        LDAPUtil.getLDAPProperties();
    }

    @Test
    public void testGetLDAPProperties_NOK() throws Exception {

        ResourceBundle properties = null;

        mockStatic(ResourceBundle.class);
        //ResourceBundle mockService = Mockito.mock(ResourceBundle.class);
        when(ResourceBundle.getBundle(Mockito.isA(String.class))).thenReturn(properties);
    }

    @Test
    public void testInciarSesionLDAP() throws Exception {
        PowerMockito.whenNew(InitialDirContext.class).withArguments(Mockito.isA(Hashtable.class)).thenReturn(this.initialDirContextMock);
        Assert.assertNotNull(LDAPUtil.inciarSesionLDAPAD("a","a"));
    }

    @Test
    public void testIniciarSesionLDAPNOK() throws Exception {
        PowerMockito.whenNew(InitialDirContext.class).withArguments(Mockito.isA(Hashtable.class)).thenReturn(null);
        assertNull(LDAPUtil.inciarSesionLDAPAD("A","B"));
    }

    @Test
    public void testIniciarSessionException() throws Exception {
        PowerMockito.whenNew(InitialDirContext.class).withArguments(Mockito.isA(Hashtable.class)).thenThrow(NamingException.class);
    }
}