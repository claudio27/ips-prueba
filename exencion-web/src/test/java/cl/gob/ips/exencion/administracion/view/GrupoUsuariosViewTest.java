package cl.gob.ips.exencion.administracion.view;

import cl.gob.ips.exencion.BaseTest;
import cl.gob.ips.exencion.administracion.view.GrupoUsuariosView;
import cl.gob.ips.exencion.dao.GrupoUsuariosDao;
import cl.gob.ips.exencion.exceptions.DataAccessException;
import cl.gob.ips.exencion.exceptions.IpsBussinesException;
import cl.gob.ips.exencion.informes.view.EventosProcesosSistemaView;
import cl.gob.ips.exencion.model.Funcionalidades;
import cl.gob.ips.exencion.model.GrupoUsuarios;
import cl.gob.ips.exencion.service.GrupoUsuariosService;
import cl.gob.ips.exencion.util.MensajesUtil;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.inject.Inject;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

@RunWith(PowerMockRunner.class)
@PrepareForTest({GrupoUsuariosView.class, GrupoUsuarios.class, GrupoUsuariosService.class, MensajesUtil.class})
public class GrupoUsuariosViewTest extends BaseTest {

    private static final Logger LOG = Logger.getLogger(GrupoUsuariosViewTest.class);

    @Inject
    @InjectMocks
    private GrupoUsuariosView grupoUsuariosViewMock;

    @Inject
    @Mock
    private GrupoUsuariosService grupoUsuariosService;

    @Mock
    private GrupoUsuarios grupoUsuariosMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(MensajesUtil.class);
        PowerMockito.doNothing().when(MensajesUtil.class, "setMensajeErrorContexto", Mockito.any(String.class));
        super.ignoreSuperImplement(GrupoUsuariosView.class, "mostrarMensajeError");
        //grupoUsuariosDao = PowerMockito.mock(GrupoUsuariosDao.class); //here is the actual mocking call

        //grupoUsuariosService = new GrupoUsuariosService();


    }

    @Spy
    List<GrupoUsuarios> grupoUsuariosList = new ArrayList<GrupoUsuarios>();

    @Test
    public void eliminarGrupoUsuarios_OK() throws DataAccessException {
        //grupoUsuariosViewMock.eliminarGrupoUsuarios(new GrupoUsuarios());
        Mockito.doNothing().when(grupoUsuariosService).eliminarGrupoUsuarios(Mockito.isA(GrupoUsuarios.class));
        this.grupoUsuariosViewMock.eliminarGrupoUsuarios();
    }

    @Test
    public void eliminarGrupoUsuarios_NOK() throws Exception {
        Mockito.doThrow(new DataAccessException()).when(this.grupoUsuariosService).
                eliminarGrupoUsuarios(this.grupoUsuariosMock);

        this.grupoUsuariosViewMock.eliminarGrupoUsuarios();
    }

    @Test
    public void crearGrupoUsuarioGrupoNull() {
        //maqueta para cuando GrupoUsuarios sea null;
        this.grupoUsuariosViewMock.doCrearGrupoUsuarios();
    }

    @Test
    public void crearGrupoUsuarioDescripcionNull() {
        //maqueta para cuendo this.grupoUsuariosMock.getGruUsuDescripcion es null
        this.grupoUsuariosViewMock.setGrupoUsuariosCreate(this.grupoUsuariosMock);
        this.grupoUsuariosViewMock.doCrearGrupoUsuarios();
    }

    @Test
    public void crearGrupoUsuarioRepetido() throws DataAccessException {

        //maqueta para cuando GrupoUsuarios está repetido
        Mockito.doReturn(new String()).when(this.grupoUsuariosMock).getGruUsuDescripcion();
        Mockito.doReturn(Mockito.mock(GrupoUsuarios.class)).when(this.grupoUsuariosService).buscarGrupoUsuario(Mockito.isA(String.class));
        this.grupoUsuariosViewMock.setGrupoUsuariosCreate(this.grupoUsuariosMock);

        this.grupoUsuariosViewMock.doCrearGrupoUsuarios();
    }

    @Test
    public void crearUsuarioNoRepetido() throws DataAccessException {
        //maqueta para cuendo GrupoUsuarios no está repetido, por ende la
        //funcionalidad del metodo this.grupoUsuariosViewMock.doCrearGrupoUsuarios()
        //es válida;

        Mockito.doReturn(new String()).when(this.grupoUsuariosMock).getGruUsuDescripcion();
        Mockito.doReturn(null).when(this.grupoUsuariosService).buscarGrupoUsuario(Mockito.isA(String.class));
        this.grupoUsuariosViewMock.setGrupoUsuariosCreate(this.grupoUsuariosMock);

        this.grupoUsuariosViewMock.doCrearGrupoUsuarios();
    }
}