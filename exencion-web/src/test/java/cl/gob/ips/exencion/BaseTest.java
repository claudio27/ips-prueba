package cl.gob.ips.exencion;

import org.apache.log4j.Logger;
import org.powermock.api.mockito.PowerMockito;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class BaseTest {

    private static final Logger LOG = Logger.getLogger(BaseTest.class);

    /*
    * Este método ayuda a ignorar super implementations,
    * resoecto pruebas hechas en Mockito
    *
    * Asegurese que el metodo que desea ignorar sea public y no contenga parámetros
    * */
    protected void ignoreSuperImplement(Class clazz, String methodName) throws Exception {

        Method method = null;

        try {
            method = clazz.getMethod(methodName);

            PowerMockito.replace(method).with(new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    return null;
                }
            });
        } catch (NoSuchMethodException e) {
            throw new Exception("Método con nombre: " + methodName + " no encontrado, razón: " + e.getMessage());
        }
    }

    /*
    * Este método ayuda a ignorar super implementations,
    * resoecto pruebas hechas en Mockito
    *
    * Asegurese que el metodo que desea ignorar sea public y que contenga al menos un parametro
    * Ejemplo:
    *       ignoreSuperImplement(NombreClase.class, nombreMetodo, String.class)
    * */
    protected void ignoreSuperImplement(Class clazz, String methodName, Class<?>... params) throws Exception {

        Method method = null;

        try {

            method = clazz.getMethod(methodName, params);

            PowerMockito.replace(method).with(new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    return null;
                }
            });
        } catch (NoSuchMethodException e) {
            throw new Exception("Método con nombre: " + methodName + " no encontrado, razón: " + e.getMessage());
        }
    }

    protected void ignoreSuperImplementProtectedMethod(Class clazz, String methodName, Class<?>... params) throws Exception {

        Method method = null;

        try {

            method = clazz.getDeclaredMethod(methodName, params);

            PowerMockito.replace(method).with(new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    return null;
                }
            });
        } catch (NoSuchMethodException e) {
            throw new Exception("Método con nombre: " + methodName + " no encontrado, razón: " + e.getMessage());
        }
    }
}
